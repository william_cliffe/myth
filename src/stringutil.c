/****************************************************************************
 MYTH - String utilities/functions.
 Coded by Saruk (01/25/99)
 ---
 Fun stuff?
 ****************************************************************************/

#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define NO_PROTO_VARARGS
//#include <varargs.h>
#include <stdarg.h>

#include "db.h"
#include "net.h"
#include "externs.h"

char *pstr(char *fmt, ...)
{
	char buf[HUGE_BUFSIZE];
	va_list ap;
	
	va_start(ap, fmt);
	vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);
	
	return(stralloc(buf));
}

char *make_ln(char *instr, int amt)
{
	char buf[MAX_BUFSIZE];
	int count;
	
	strcpy(buf, instr);
	amt--;
	for(count = 0; count < amt; count++)
		strcat(buf, instr);
	
	return stralloc(buf);
}

char *rjust(str, num)
	char *str;
	int num;
{
	static char buf[MAX_BUFSIZE];
	int before, size;
	
	size = strlen(strip_color(str));
	if(size > num) {
		strcpy(buf, format_color(str, num));
		return(buf);
	}
	before = num - size;
	
	sprintf(buf, "%s%s", make_ln(" ", before), str);
	
	return(buf);	
}

char *cjust(str, num)
	char *str;
	int num;
{
	static char buf[MAX_BUFSIZE];
	int before, size;
	
	size = strlen(strip_color(str));
	if(size > num) {
		strcpy(buf, format_color(str, num));
		return(buf);
	} 
	
	before = (num - size) / 2;
	
	sprintf(buf, "%s%s", make_ln(" ", before), str);
	sprintf(buf, "%s%s", buf, make_ln(" ", before));
	sprintf(buf, "%s%s", buf, (strlen(strip_color(buf)) < num) ? " ":"");
	
	return(buf);
}

// cstr - string to be checked
// instr - string cstr is checked against.
int string_prefix(instr, cstr)
	char *instr;
	char *cstr;
{
	if(!*instr || !*cstr) return 0;

	while(*instr && *cstr && tolower(*instr) == tolower(*cstr))
		instr++, cstr++;
	
	return(*instr == '\0');
}

int strcmpm(instr, cstr)
	char *instr;
	char *cstr;
{
	if(!instr || !cstr)
		return 1;
		
	while(*instr && *cstr && tolower(*instr) == tolower(*cstr))
		instr++, cstr++;
	
	return (tolower(*cstr) - tolower(*instr));
}

#define DOWNCASE(x) lower_case(x)

char to_lower(x)
	int x;
{
	if(x < 'A' || x > 'Z') return x;
	
	return 'a' + (x - 'A');			
}

char to_upper(x)
	int x;
{
	if(x < 'a' || x > 'z') return x;
	
	return 'A' + (x - 'a');
}

char *lower_case(instr)
	char *instr;
{
	static char buf[MAX_BUFSIZE];
	char *p = buf, *s;
	
	for(s = instr; *s; s++, p++)
		*p = to_lower(*s);
	
	*p = '\0';
	return(buf);
}

char *fparse(char *instr, int c)
{
	static char buf[MAX_BUFSIZE];
	int i = 0;
	
	strcpy(buf, instr);
	while(*instr && *instr != c)
		instr++, i++;
	
	if(i == 0)
	  return("");
	  
	buf[i]='\0';
	
	return(stralloc(buf));
}

char *rparse(char *instr, int c)
{
	static char buf[MAX_BUFSIZE];
	
	while(*instr && *instr != c)
		instr++;
		
	if(*instr) {
		strcpy(buf, instr + 1);
		if(buf[strlen(buf)-1]=='\n')
			buf[strlen(buf)-1]='\0';
	} else {
		return("");
	}
	
	return(stralloc(buf));
}

// match a key name - first created for exits.
// instr - this is the 'key'.
// matchstr - this is the list of keys that the 'key' is supposed to look at.
int match_key(char *matchstr, char *instr)
{
  char buf[4096];
  char *p = buf, *r;
  
  if(!*matchstr || !*instr)
    return(FALSE);
    
  strcpy(buf, matchstr);
  while( (r = parse_up(&p, ';'))) {
    if(!strcmpm(r, instr))
      return TRUE;
  }

  return(FALSE);
}

// wee!
void make_header(player, headerstr)
	object *player;
	char *headerstr;
{
	notify(player, pstr("%s", make_ln("=", 78)));
	notify(player, pstr("%s", cjust(return_config("myth_name"),78)));
	notify(player, pstr("%s", cjust(headerstr, 78)));
	notify(player, pstr("%s", make_ln("-", 78)));
}

char *parse_it(args, delimiter)
	char **args;
	int delimiter;
{
	char **str = args;
	char *s = *str, *os = *str;
	
	if(!*s)
		return (NULL);
	
	while(*s && (*s!=delimiter))
		s++;
	
	if(*s)
		*s++='\0';
	
	*str = s;
	return (os);	
}


char *flip(s)
	char *s;
{
	char buf[MAX_BUFSIZE];
	char *p=buf;

	p = strlen(s) + buf;
	for(*p--='\0';*s;p--,s++)
		*p=*s;
		
	return stralloc(buf);
}

char *comma(amt)
	unsigned long long amt;
{
	char buf[MED_BUFSIZE];
	char temp[MED_BUFSIZE], temp2[MED_BUFSIZE];
	char *p = buf;
	int ctr = 0, a = 0;
	
	strcpy(temp, pstr("%llu", amt));
	strcpy(temp2, flip(temp));
	
	for(ctr=0,a=0;ctr < strlen(temp2); ctr++, a++,p++) {
		if(a==3) { *p= ','; a=0; p++;}
		*p = temp2[ctr];
		
	}
	
	*p = '\0';
	return flip(buf);	
}

int ISNUM(int x)
{
	if(x < 0)
		return 0;
	else 
		return 1;
}

char *get_inside(str, start, end, separator)
	char *str;
	int start;
	int end;
	int separator;
{
	char buf[MAX_BUFSIZE];
	char *p, *s;
	int depth = 0;

	strcpy(buf, str);
	for(s = buf, p = s; *s; s++)
		if(*s == start) {
			s++;
			p = s;
			depth++;
			break;
		}
		
	for(; *s; s++)
		if(*s == start)
			depth++;
		else if (*s == end) {
			if(*(s+1) == separator) {
				s++;
				break;
			}
			depth--;
		}
		else if (depth == 0)
			break;
		
	s--;
	*s = '\0';		
	return stralloc(p);
}

int isalphanumeric(char c)
{
  if((c > 64 && c < 91) || (c > 47 && c < 58) || (c > 95 && c < 123))
    return TRUE;
  
  return FALSE;
}

int isanumber(char *value)
{
	if(*value == '-') value++;
	while(*value && isdigit(*value))
		value++;
				
	if(*value)
		return FALSE;
	
	return TRUE;
}

char *stringalloc(buffer)
	char *buffer;
{
	char *p;
	
	if(!buffer || !*buffer)
	  return "";
	  
	p = (char *)stack_em(strlen(buffer) + 1, VOLATILE);
	strncpy(p, buffer, strlen(buffer));
	
	return p;
}

char *strset(buffer)
	char *buffer;
{
	char *p;
	
	if(!buffer || !*buffer)
		return(NULL);
		
	p = (char *)stack_em(strlen(buffer) + 1, SOLID);
	strncpy(p, buffer, strlen(buffer));
	
	return(p);
}

/* l(ast)|f(irst) of - gives the last or first of, depending on tag. */
/* 1 = return front, 0 return last */
char *lfof(argument, tag)
	char *argument;
	int tag;
{
	char buf[MAX_BUFSIZE], tmp[MAX_BUFSIZE];
	
	strcpy(buf, "");
	strcpy(tmp, argument);
	while(*restof(tmp)) {
		sprintf(buf, "%s%s%s", buf, (*buf) ? " ":"", front(tmp));
		strcpy(tmp, restof(tmp));
	}
	
	if(tag)
		return stralloc(buf);
	else
		return stralloc(tmp);
}

/* Random salt, values given by Morgoth of TinyINN */
char *mcrypt(string)
	char *string;
{
	char salt[3];

	do { salt[0] = (rand()%  44) + 46; } while (salt[0] > 57 && salt[0] < 65);
	do { salt[1] = (rand()% 44) + 46; } while (salt[1] > 57 && salt[1] < 65);
	salt[2] = 0;
		
	return(crypt(string, salt));
}

int dcrypt(password, string)
	char *password;
	char *string;
{
	char salt[3];
	
	salt[0] = password[0];
	salt[1] = password[1];
	salt[2] = 0;

	return(strcmp(crypt(string, salt), password));	
}

int NULLIFIED(string)
	char *string;
{
	if(!string || !*string)
		return TRUE;
		
	return FALSE;
}

char *parse_up(str, delimit)
	char **str;
	int delimit;
{
	int deep = 0;
	char *s = *str, *os = *str;
	
	if(!*s)
		return(NULL);
	
	while(*s && (*s!=delimit)) {
	  if(*s++ == '{') // open curly bracket
	  {
	    deep = 1;
	    while(deep && *s)
	      switch(*s++) {
		case '{':
		  deep++;
		break;
		case '}':
		  deep--;
		break;
	      }
	  }
	 }
	
	if(*s)
		*s++=0;
	
	*str = s;
	return(os);
}
