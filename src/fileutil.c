/****************************************************************************
 MYTH - File utilities.
 Coded by Saruk (01/21/99)
 ---
 When I get it right....
 ****************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// Get a string of characters from a file.
char *rrr_getstring_noalloc(buffer, size, fp)
	char *buffer;
	int size;
	FILE *fp;
{
	char *p=buffer;
	int num_read=0, ch = '\0';
	
	for(;;) {
		ch =(int)fgetc(fp);
		if(ch == EOF || !ch) 
			break;
		if (ch == '\n') {
			*p++ = ch;
			num_read++;
			break;
		}
		*p++ = ch;
		num_read++;
		if(num_read > size)
			break;
	}
	
	*p = '\0';
	
	return buffer;
}	

char *getstring_noalloc(buffer, size, fp)
	char *buffer;
	int size;
	FILE *fp;
{
	char buf[MAX_BUFSIZE];
	char *p = buffer;
	int result;
 	
        if( fgets(buf, sizeof(buf), fp) != NULL) {
	  buf[strlen(buf)-1] = '\0';
	  p = buf;
	} else {
	  p = '\0';
	}

	return p;
}

// Get a string using getstring_noalloc.
char *getstring(FILE *fp)
{
	static char buf[4096];
		
	strcpy(buf, "");
	//getstring_noalloc(buf, sizeof(buf), fp);
	if(fgets(buf, sizeof(buf), fp) != NULL) {
		if(buf[strlen(buf) - 1]== '\n')
			buf[strlen(buf)-1]='\0';
	} else {
		strcpy(buf, "\0");
	}

	return(buf);
}

// Return an integer from file.
int getint(FILE *x)
{
	char buf[1024];
	return (atoi(fgets(buf,1024,x)));
}

// Return a long from file.
long getlong(FILE *x)
{
	char buf[1024];
	
	return(atol(fgets(buf,1024,x)));
}

long long getllong(FILE *fp)
{
  char buf[256];
	
  return(atoll(fgets(buf, 256, fp)));
}

float getfloat(FILE *fp)
{
  char buf[64];
  
  return(atof(fgets(buf, 64, fp)));
}
