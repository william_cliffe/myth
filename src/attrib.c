/****************************************************************************
 MYTH - Revised Attribute System
 Coded by Saruk (04/05/99)
 Revised (01/08/03)
 Revised (12/21/03)
 Revised (08/05/04)
 ---
 This is a new revised attribute system. Please be advised that 'player' refers
 in many cases to any object.

 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// Append a new attribute to the object's attribute list.
void append_attribute(thing, anew)
	object *thing;
	attr *anew;
{
	if(!thing->alist) {
	  anew->next = NULL;
	  anew->prev = NULL;
	  thing->alist = anew;
	  thing->atail = anew;
	} else {
	  anew->next = NULL;
	  anew->prev = thing->atail;
	  thing->atail->next = anew;
	  thing->atail = anew;
	}
}

ATTR *global_attr_create(a, player)
	attr *a;
	object *player;
{
	ATTR *anew;
	
	GENERATE(anew, ATTR);
	anew->atype = a->atype;
	anew->name = NULL;
	SET(anew->name, a->name);
	anew->value = NULL;
	anew->flags = a->flags;
	anew->powlvl = a->powlvl;
	anew->dbref = player->ref;

	/** With the new code, append attribute to the end **/
	append_attribute(player, anew);
		
	return(anew);
}


ATTR *user_attr_create(player, name, flag)
	object *player;
	char *name;
	int flag;
{
	ATTR *anew;
	
	GENERATE(anew, ATTR);
	anew->atype = -1;
	anew->name = NULL;
	SET(anew->name, name);
	anew->value = NULL;
	anew->flags = flag;
	anew->powlvl = -1;
	anew->dbref = player->ref;
	
	/** With the new code, append attribute to the end **/
	append_attribute(player, anew);
				
	return(anew);
}


ATTR *is_attr(atrname)
	char *atrname;
{
	int ctr; 
	
	for(ctr = 0; myth_defined[ctr].name; ctr++)
		if(!strcmpm(myth_defined[ctr].name, atrname))
			return(&myth_defined[ctr]);

	return(NULL);	
}

ATTR *__is_set_attr(atrtype)
	int atrtype;
{
	int ctr;
	
	for(ctr = 0; myth_defined[ctr].name; ctr++)
	  if(myth_defined[ctr].atype == atrtype)
	    return(&myth_defined[ctr]);
	
	return(NULL);
}


/* Not only does this search to see whether this is an attribute than is hardcoded, it will
   search for user def'd attrs. */
int __all_attribute_search(atrname)
	char *atrname;
{
	int i = 0;
	attr *a;
	
	if((a = is_attr(atrname)))
	  return TRUE;
	
	while(i < dbtop) {
	  if(db[i]) {
 	    for(a = (db[i])->alist; a; a=a->next) {
	      if( (a->atype == -1) && (a->flags & AUSER) ) {
	        if(!strcmpm(a->name, atrname))
	          return TRUE;
	      }
	    }
	  }
	  i++;
	}

	return FALSE;
}

/* Check to see if the attribute is a builtin */
int __is_builtin(atrname)
	char *atrname;
{
	int i = 0;
	char *builtins[]={"description", "name", "powlvl", "password", "exitkeys", "owner", "link", "home", ""};
	
	for(i=0; *builtins[i]; i++) {
		if(string_prefix(atrname, builtins[i]))
			return TRUE;
	}
	
	return(FALSE);
}

/* Deletion should now be a cinch */
void __delete_attribute(thing, atr)
	object *thing;
	attr *atr;
{
	if(atr->prev)
	  atr->prev->next = atr->next;
	
	if(atr->next)
	  atr->next->prev = atr->prev;
	
	if(thing->alist == atr)
	  thing->alist = atr->next;
	
	if(thing->atail == atr)
	  thing->atail = atr->prev;
	
	if(atr->name)
	  block_free(atr->name);
	if(atr->value)
	  block_free(atr->value);
	
	block_free(atr);	
}


void delete_attribute(thing, atr)
	OBJECT *thing;
	ATTR *atr;
{
	ATTR *a, *anext = NULL;

	for(a = thing->alist; a; a=anext) {
	  anext = a->next;
	  if(a == atr)
	    __delete_attribute(thing, atr);
	}
}

/* Check to see if the attribute is the same as any other of the same type */
/* Returns false if it is */
int __check_attribute(player, atr, value)
	object *player;
	attr *atr;
	char *value;
{
	ATTR *a;
	int i = 0;
	
	while(i < dbtop) {
	  if(db[i]) {
	    for(a = (db[i])->alist; a; a=a->next) {
	      if((player->ref != a->dbref) && (a->atype == atr->atype)) {
	        if(!strcmpm(strip_color(a->value), strip_color(value)))
	          return FALSE;
	      }
	    }
	  }
	  i++;
	}
	
	return TRUE;
}

/* Checks to see if alias is someone ELSE. */
int __check_alias(player, value)
	object *player;
	char *value;
{
	object *o;

	if(ISNUM(match_num(value)))
		return FALSE;
	
	if(!strcmpm("me", value))
	  return FALSE;
	
	if(!strcmpm("self", value))
	  return FALSE;
	  
	if(!strcmpm("here", value))
	  return FALSE;  	
	
	if(!strcmpm(player->name, value))
		return FALSE;
		
	if(!(o = match_player(strip_color(value))))
		return TRUE;
	
	if(o == player)
		return TRUE;
	
	return FALSE;
}

int __atr_set(setter, player, a, value)
	object *setter, *player;
	ATTR *a;
	char *value;	
{
	
	if( (a->flags & AWIZARD) && !(setter->flags & WIZARD)) 
		return M_ENOCNTRL;
	
	if( (a->powlvl > setter->powlvl) ) 
		return M_EBADPOW;

	if(!*value) {	
		delete_attribute(player, a);
		return TRUE;
	}
	
	if((a->flags & ACHECK))
	  if(!__check_attribute(player, a, value))
	    return M_ENOCANDO;
	
	if(a->atype == A_ALIAS) {
		if(!__check_alias(player, value)) 
			return M_ENOCANDO;
	}
	
	if(a->value)
	  block_free(a->value);
	  
	SET(a->value, value);
	return TRUE;	
}

void KILL_ATTRS(player)
	object *player;
{
	attr *a, *anext = NULL;
	
	
	for(a = player->alist; a; a=anext) {
	  anext = a->next;
	  delete_attribute(player, a);
	}
}


char *extended_atr_get(caller, player, atrtype)
	object *caller;
	object *player;
	int atrtype;
{
	PARENT *p;
	attr *a;
	char *c;

	for(a = player->alist; a; a=a->next) {
	  if((a->atype == atrtype) && ( (a->flags & AINH) || (a->dbref == caller->ref)) )
	    return(a->value);
    }
    
    for(p = player->parent; p; p=p->next) {
          if((c = extended_atr_get(caller, p->what, atrtype)))
            return(c);
    }

	return("");
}

/** ATR_GET covers global attributes. **/
char *atr_get(player, atrtype)
	object *player;
	int atrtype;
{
	return(extended_atr_get(player, player, atrtype));
}


int DOPARENTATR(player, atrname, atr)
	object *player;
	char *atrname;
	ATTR *atr;
{
	PARENT *p;
	
	for(p = player->parent; p; p=p->next) {
	  if(p->what) { // recurse through parent list
	    if(DOPARENTATR(p->what, atrname, atr))
	      return TRUE;
	  }

	  if(atr->dbref == p->what->ref) {
	    if(!strcmpm(atr->name, atrname))
	      return TRUE;
	  }
	}
	
	return(0);
}


// This is the parser's attribute get since it cannot distinguish between user and global
// attributes.
char *parser_atr_get(player, atrname)
	object *player;
	char *atrname;
{
	char *c;
	ATTR *a;
	PARENT *p;
	
	for(a = player->alist; a; a=a->next) {
	    if(!strcmpm(a->name, atrname)) {
	      if((a->flags & AUSER) && !(a->value))
	        return "";
	      else
	        return a->value;
	    }
	}
	
	for(a = player->alist; a; a=a->next) {
	  if(DOPARENTATR(player, atrname, a)) {
	    if( (a->flags & AUSER) && !(a->value) )
	      return "";
	    else
	      return(a->value);
	  }
	}
	
	if(!strcmpm(atrname, "desc"))
	  return player->desc;
	else if (!strcmpm(atrname, "description"))
	  return player->desc;
	
	return("");
}


void SET_ATR(player, atrtype, value)
	object *player;
	int atrtype;
	char *value;
{
	attr *a = NULL, *aset;

	for(a = player->alist; a; a=a->next)
	  if( (a->atype == atrtype)) {
	    __atr_set(rootobj, player, a, value);
	    return;
	  }

	if(!*value) 
		return;
	
	if(!(aset = __is_set_attr(atrtype)))
		return;
	
	if(!(a = global_attr_create(aset, player)))
		return;

	SET(a->value, value);
}


/** SETTING A GLOBAL ATTRIBUTE 

    This function will set an attributes value, or if no such attribute exists,
    it will create the attribute on the global list and add said value to it.
    
**/
int atr_set(setter, player, atrtype, value)
	object *setter;
	object *player;
	int atrtype;
	char *value;
{
	attr *a = NULL, *aset = NULL;
	
	if(player->flags & LOCKED && setter != rootobj)
	  return M_EISLOCKED;
	
	for(a = player->alist; a; a=a->next)
	  if( (a->atype == atrtype))
	    return(__atr_set(setter, player, a, value));

	if(!*value)
		return TRUE;
	  
	// Okay, the attribute is not on the player.

	// Check to see if the attribute is a server attribute.
	if(!(aset = __is_set_attr(atrtype)))
		return M_EILLATTR;

	// It is, so let's check to see if we can create it.
	if(!(a = global_attr_create(aset, player)))
		return M_EILLATTR;

	// We've created the attribute, temporarily...
	
	// If we can change it, do so and leave. If not, delete the temp
	// and return error.
	if( (a->flags & AWIZARD) && !(setter->flags & WIZARD)) {
	  delete_attribute(player, a);
	  return M_ENOCNTRL;
	}
	
	if( (a->powlvl > setter->powlvl) ) {
	  delete_attribute(player, a);
	  return M_EBADPOW;
	}

	if( (a->flags & ACHECK) ) {
	  if(!__check_attribute(player, a, value)) {
	    delete_attribute(player, a);
	    return M_ENOCANDO;
	  }
	}
	
	if(atrtype == A_ALIAS) {
	  if(!__check_alias(player, value)) {
	    delete_attribute(player, a);
	    return M_ENOCANDO;
	  }
	}
	
	if(a->value)
	  block_free(a->value);
	  
	SET(a->value, value);
	return TRUE;
}

int edit_atr_set(setter, player, name, value)
	object *setter;
	object *player;
	char *name;
	char *value;
{
	ATTR *a = NULL;
	
	if( (a = is_attr(name)))
		return(atr_set(setter, player, a->atype, value));
	
	for(a = player->alist; a; a=a->next) {
	    if(!strcmpm(a->name, name)) 
	      break;
	}
	
	if(!a)
	  return M_EILLATTR;
	
	if(!has_pow(setter, player, POW_MODIFY) && !can_control(setter, player))
	  return M_ENOCNTRL;
	
	if( (a->flags & AWIZARD) && !(setter->flags & WIZARD))
	  return M_ENOCNTRL;
	
	// We don't delete user-defined attributes.
	// We just free them. Free! FREE ATTR!
	if(!*value) {
	  block_free(a->value);
	  a->value = NULL;
	  return TRUE;
	}

	if(a->value)
	  block_free(a->value);
	  
	SET(a->value, value);	  
	return TRUE;
}


/** This is the MYTH-wide attribute examine. **/
void atr_examine(player, victim, atrname)
	object *player;
	object *victim;
	char *atrname;
{
        ATTR *a;
        
	for(a = victim->alist; a; a=a->next)
	  if(!strcmpm(a->name, atrname) )
	    break;

	if(!a) {
	  if(!(a = is_attr(atrname))) {
	  	notify(player, "What attribute?");
	  	return;
	  }

	  if( ((a->flags & AWIZARD) && (a->flags & ASEE)) || ( ((a->flags & AWIZARD) && !(a->flags & ASEE)) 
	      && (player->flags & WIZARD)) || !(a->flags & AWIZARD)) {
	      ncnotify(player, pstr("%s(%s): %s%sEmpty%s", a->name, aflaglist(a->flags), ANSI_HILITE, ANSI_YELLOW, ANSI_NORMAL));
	  } else {
	      notify(player, "You're not able to see that attribute.");
	  }
	  return;
	}
	
	if( ((a->flags & AWIZARD) && (a->flags & ASEE)) || ( ((a->flags & AWIZARD) && !(a->flags & ASEE)) 
	  && (player->flags & WIZARD)) || !(a->flags & AWIZARD))
	  ncnotify(player, pstr("%s(%s): %s", a->name, aflaglist(a->flags),
	    (a->value && *(a->value)) ? a->value : "(NULL)"));
	else
	  notify(player, "You're not able to see that attribute.");

}



int __user_aflags(player, atrflags)
	object *player;
	char *atrflags;
{
	char buf[MAX_BUFSIZE];
	char *p = buf, *k;
	int flags = AUSER;
	
	strcpy(buf, atrflags);
	while( (k = parse_up(&p, ' '))) {
	  if(string_prefix(k, "inherit"))
	    flags |= AINH;
	  else if (string_prefix(k, "wizard")) {
	    if( (player->flags & WIZARD))
	      flags |= AWIZARD;
	  } else if (string_prefix(k, "dark")) 
	    flags |= ADARK;
	  else if (string_prefix(k, "visible") || string_prefix(k, "see"))
	    flags |= ASEE;
	  else if (string_prefix(k, "important")) 
	    flags |= AIMP;
	  else if (string_prefix(k, "program") || string_prefix(k, "softcode"))
	    flags |= APROGRAM;
	  else if (string_prefix(k, "function"))
	    flags |= AFUNC;
	}
	
	return(flags);	
}


void redefine_attrflags(setter, player, a, atrflags)
	object *setter, *player;
	attr *a;
	char *atrflags;
{
	char buf[MAX_BUFSIZE];
	char *p = buf, *k;
	
	a->flags = AUSER;
	strcpy(buf, atrflags);
	while( (k = parse_up(&p, ' '))) {
	  if(string_prefix(k, "inherit"))
	    a->flags |= AINH;
	  else if (string_prefix(k, "wizard")) {
	    if( (setter->flags & WIZARD))
	      a->flags |= AWIZARD;
	  } else if (string_prefix(k, "dark")) 
	    a->flags |= ADARK;
	  else if (string_prefix(k, "visible") || string_prefix(k, "see"))
	    a->flags |= ASEE;
	  else if (string_prefix(k, "important")) 
	    a->flags |= AIMP;
	  else if (string_prefix(k, "program") || string_prefix(k, "softcode"))
	    a->flags |= APROGRAM;
	  else if (string_prefix(k, "function"))
	    a->flags |= AFUNC; 
	}
}


/*** 
  do_defattr()
  Ie: @defattr <obj>/<atr>[=<options>]
 ***/
void do_defattr(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	ATTR *a = NULL;
	object *who;
	char *atrname, *atrflags, *obj;
	int flags = 0;
	
	obj = fparse(arg1, '/');
	atrname = rparse(arg1, '/');
	atrflags = arg2; 
	
	if(!*obj || !*atrname) {
		notify(player, "What are you going to do?");
		return;
	}
	
	if(!(who = remote_match(player, obj))) {
	  notify(player, "Who are you talking about?");
	  return;
	}
	
	
	if(!has_pow(player, who, POW_MODIFY) && !can_control(player, who)) {
		notify(player, "Sorry, you cannot do that.");
		return;
	}
	
	if( (a = is_attr(atrname))) {
	  notify(player, "You cannot shadow a builtin attribute.");
	  return;
	}
	
	for(a = who->alist; a; a=a->next)
	  if(!strcmpm(a->name, atrname)) // okay, a is an attr, we redefine its flags.
	    break;

	if(a && (!(a->flags & AWIZARD) || (player->flags & WIZARD))) {
	  redefine_attrflags(player, who, a, atrflags);
	  notify(player, "Options set.");
	  return;
	}
	
	flags = __user_aflags(who, atrflags);
	a = user_attr_create(who, atrname, flags);

	notify(player, pstr("%s has been defined on %s", 
	  a->name, unparse_object(who)));
}


void do_undefattr(player, arg1)
	object *player;
	char *arg1;
{
	object *who;
	ATTR *a, *anext = NULL;
	char *obj, *attr;
	
	DEBUG(pstr("%s", arg1));
	IS_ARGUMENT(player, (arg1));

	obj = fparse(arg1, '/');
	attr = rparse(arg1, '/');
	
	if(!(who = remote_match(player, obj))) {
	  notify(player, "Who?");
	  return;	
	}
	
	if(!*attr) {
	  notify(player, "Undo what attribute?");
	  return;
	}
	
	if(!can_control(player, who) && !has_pow(player, who, POW_MODIFY)) {
	  notify(player, "You do not have control over that.");
	  return;
	}
	
	for(a = who->alist; a; a=anext) {
	  anext = a->next;
	  if((a->flags & AUSER)) 
	    if(!strcmpm(a->name, attr) && ( !(a->flags & AWIZARD) || (player->flags & WIZARD)) ) {
	      notify(player, pstr("%s deleted.", a->name));
	      __delete_attribute(who, a);
	      return;
	    } else {
	      notify(player, "You may not delete that attribute.");
	      return;
	    }
	}
	
	notify(player, "What attribute?");
}


/****************************************************************************
 MKATTR is the attribute loading procedure.

 All attribute code should go above this. 
 ****************************************************************************/
void mkattr(player, instr)
	object *player;
	char *instr;
{
	ATTR *a = NULL, *attr;
	char buf[MAX_BUFSIZE];
	char inattr[4][MAX_BUFSIZE];
	int flags;


	strcpy(buf, instr);
	strcpy(inattr[0], fparse(buf, ':')); // owner
	strcpy(buf, rparse(buf, ':'));
	strcpy(inattr[1], fparse(buf, ':')); // flags
	flags = atoi(inattr[1]);
	strcpy(buf, rparse(buf, ':'));
	strcpy(inattr[2], fparse(buf, ':')); // name
	strcpy(inattr[3], rparse(buf, ':')); // value
	
	if(!(a = is_attr(inattr[2])))
	  if(!(flags & AUSER))
	    return;

	// We've determined that this IS an attribute to be saved.
	// Save it to the global heap.
	if(a) 
	  attr = global_attr_create(a, player);
	else {
	  attr = user_attr_create(player, inattr[2], flags);
	}
	attr->flags = flags;

	if(*(inattr[3]))
	  SET(attr->value, inattr[3]);
}


/* INSTR is: <attr> <object>=<text> */

void do_easyattr(player, instr)
	object *player;
	char *instr;
{
	ATTR *a;
	object *victim;
	char *attr, *who, *text;
	
	attr = front(instr);
	who = fparse(restof(instr), '=');
	text = rparse(restof(instr), '=');
	
	if(!(victim = remote_match(player, who))) {
	  notify(player, pstr("I don't recognize %s.", who));
	  return;
	}
	
	if(!can_control(player, victim) && !has_pow(player, victim, POW_MODIFY)) {
	  notify(player, "You cannot do that.");
	  return;
	}
	
	if(__is_builtin(attr)) {
	  do_set(player, "attr", who, pstr("%s:%s", attr, text));
	  return;
	}
	
	if(edit_atr_set(player, victim, attr, text) > -1) {
	  notify(player, pstr("%s set.", attr));
	  return;
	}
	
	// There was no such attribute. Define it.
	do_defattr(player, pstr("%s/%s", who, attr), "program");
	
	if(!edit_atr_set(player, victim, attr, text)) {
	  notify(player, "Something is terribly wrong!");
	  return;
	}
	
	notify(player, pstr("%s set.", attr));	
}

