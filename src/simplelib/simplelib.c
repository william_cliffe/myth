/**
 A simple shared object library.
 Does nothing, just gets compiled.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "api.h"

#define FOOBUBBLE

void froth P((object *, char *, char *));
void math_twice2 P((void));

CL simple_cmd_add[]={
  {"froth", froth, 0, 0},
  {"froth2", froth, 0, 0},
  { NULL }
};


CL *additional_command()
{
  AddFuncCallList("math", math_twice2);
  return simple_cmd_add;
}

void __attribute__((constructor)) simple_init(void)
{
}

void froth(object *player, char *arg1, char *arg2)
{
	notify(player, pstr("You've just used a dynamic command that strcats %s and %s to make %s%s",
	  arg1, arg2, arg1, arg2));	
}

void math_twice2()
{
  DEBUG("48");
}