/****************************************************************************
 MYTH - Add Build
 Coded by Saruk (03/25/99)
 ---
 I should do this with a perl script. Perl is probably has more portability 
 for this...
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

char *stralloc(str)
	char *str;
{
	char *p = malloc(strlen(str) + 1);
	strcpy(p, str);
	
	return(p);
}

/** Man this function sucks **/
char *restof(char *str)
{
	char buf[1024];
	
	while(*str && *str != ' ')
		str++;
	
	if(*str) {
		str++;
		strcpy(buf, str);
		if(buf[strlen(buf)-1]=='\n')
			buf[strlen(buf)-1] = '\0';
	} else {
		strcpy(buf, "");
	}
	
	return stralloc(buf);
}

int main(int argc, char *argv[])
{
	FILE *fp;
	char buf[4][1024];
	int major, minor, tiny, build, upgrade_flag = 0;
	
	if(!argv[1]) {
		printf("** ERROR ** Specify a file.\n");
		return 0;
	}
	if(!(fp = fopen(argv[1], "r"))) {
		printf("** ERROR ** This file does not exist.\n");
		return 0;
	}

	fgets(buf[0], sizeof(buf[0]), fp);
		buf[0][strlen(buf[0])-1] = '\0';
	fgets(buf[1], sizeof(buf[1]), fp);
		buf[1][strlen(buf[1])-1] = '\0';
	fgets(buf[2], sizeof(buf[2]), fp);
		buf[2][strlen(buf[2])-1] = '\0';
	fgets(buf[3], sizeof(buf[3]), fp);
		buf[3][strlen(buf[3])-1] = '\0';
	fclose(fp);
	
	printf("* Updating version information...\n");
	major = atoi(restof(restof(buf[0])));
	minor =  atoi(restof(restof(buf[1])));
	tiny =  atoi(restof(restof(buf[2])));
	build =  atoi(restof(restof(buf[3])));
	
	if((build++) >= 100) {
		build = 1;
		upgrade_flag = 1;
		if((tiny++)>=9) {
			tiny = 0;
			upgrade_flag = 2;
			if((minor++)>=9) {
				minor = 0;
				upgrade_flag = 3;
				major++;
			}
		}
	}
		
	fp = fopen(argv[1], "w");
	system("touch help.c");
	fprintf(fp, "#define VER_MAJOR %d\n", major);
	fprintf(fp, "#define VER_MINOR %d\n", minor);
	fprintf(fp, "#define VER_TINY %d\n", tiny);
	fprintf(fp, "#define BUILD_NUM %d\n", build);
	fprintf(fp, "#define LAST_BUILD_DATE %ld\n", (long)time(0));
	fprintf(fp, "#define UPGRADE_FLAG %d\n", upgrade_flag);
	fclose(fp);

	return;
}

/** END OF ADDBUILD.C **/
