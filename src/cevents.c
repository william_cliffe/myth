/** cevents.c $ havencrag (11/22/2004)


  - Cevents cover attribute modifier events from fights, spell events, skill events,
    combat softcode events, etc.
  
**/
#include <stdlib.h> 
#include <string.h> 
#include <stdio.h> 
#include <time.h>

#include "db.h"
#include "net.h"
#include "externs.h"

CEVENT *cevents = NULL;
CEVENT *ceventtail = NULL;

/** EventId():
**/
unsigned int EventId()
{
  CEVENT *e;
  unsigned int ret;
  
  if(!cevents)
    return 0;
  
  for(ret = 0, e = cevents; e; e=e->next)
    if(e->id > ret)
      ret = e->id;
  
  return(ret + 1);
}

CEVENT *NewEvent(int type, time_t length)
{
  CEVENT *newe;
  
  GENERATE(newe, CEVENT);
  newe->id = EventId();
  newe->type = type;
  newe->flags = 0;
  newe->thing = NULL;
  newe->func = NULL;
  newe->softcode = NULL;
  newe->started = time(0);
  newe->item = NULL;
  newe->fight = NULL;
  newe->endtime = time(0) + length;
  
  if(!cevents) {
    newe->prev = newe->next = NULL;
    cevents = ceventtail = newe;
  } else {
    newe->next = NULL;
    newe->prev = ceventtail;
    ceventtail->next = newe;
    ceventtail = newe;
  }
  
  return(newe);
}

void RemoveEvent(CEVENT *event)
{
  ITEMLIST *i;
  
  if(event->next)
    event->next->prev = event->prev;
  
  if(event->prev)
    event->prev->next = event->next;
  
  if(cevents == event)
    cevents = event->next;
  
  if(ceventtail == event)
    ceventtail = event->prev;
  
  if(event->softcode)
    block_free(event->softcode);

  block_free(event);  
}

/** StartEvent(): returns the event.
**/
CEVENT *StartEvent(int type, char *data, time_t length)
{
  CEVENT *event;
  
  event = NewEvent(type, length);

  if(!data)
    return(event);

  // Deal with the type if data is !NULL.  
  switch(type) {
    case SOFTCODE_EVENT:
      SET(event->softcode, data);
      break;
    default:
      break;
  }
  
  return(event);
}

/** 
**/
int StartItemEvent(object *thing, ITEMLIST *item, int type)
{
  CEVENT *event;
  
  for(event = cevents; event; event=event->next) {
    if( (event->flags & EVENT_INFIGHT) && (event->type == DELIMODIFIER_EVENT) && (event->thing == thing))
      return FALSE;
  }
  
  event = StartEvent(type, NULL, -1);   // type is either DELIMODIFIER or ADDIMODIFIER
  event->flags |= EVENT_NEVERENDING|EVENT_INFIGHT;	// It is up to the fight queue to stop this event.
  event->num_fights = 1;
  event->thing = thing;
  event->item = item;
  
  return TRUE;
} 

void DelItemModifierEvent(CEVENT *event)
{
  ITEMATTR *i;
  CSTATS *c;
  
  if(!event->item || !event->thing->combat)
    return;
  
  c = event->thing->combat;

  for(i = event->item->list; i; i=i->next) {
    switch(i->type) {
      case MOD_STRENGTH:
        c->strmod -= i->value.i;
        break;
      case MOD_WISDOM:
        c->wismod -= i->value.i;
        break;
      case MOD_DEXTERITY:
        c->dexmod -= i->value.i;
        break;
      case MOD_STAMINA:
        c->stamod -= i->value.i;
        break;
      case MOD_LUCK:
        c->lckmod -= i->value.i;
        break;
      case MOD_CONSTITUTION:
        c->conmod -= i->value.i;
        break;
      case MOD_CHARISMA:
        c->chamod -= i->value.i;
        break;
      case MOD_INTELLIGENCE:
        c->intmod -= i->value.i;
        break;
      default:
        break;
    } 
  }  
}

void ParseEvent(CEVENT *event)
{
  switch(event->type) {
    case DELIMODIFIER_EVENT:
      DelItemModifierEvent(event);
      break;
    default:
      break;
  }
  
  RemoveEvent(event);
}

/** TraverseEvents():
**/
void TraverseEvents()
{
  CEVENT *c, *cnext = NULL;
  time_t now = time(0);
  
  for(c = cevents; c; c=cnext) {
    cnext = c->next;
    if(c->flags & EVENT_NEVERENDING)
      continue;
    if(c->endtime <= now)
      ParseEvent(c);
  }
}

/** CheckFightEvents():
**/
void CheckFightEvents(object *attacker, object *defender)
{
  CEVENT *c, *cnext = NULL;
  if(!cevents)
    return;
  
  for(c = cevents; c; c=cnext) {
    cnext = c->next;
    if(!(c->flags & EVENT_INFIGHT))
      continue;
    if( (c->thing == attacker) || (c->thing == defender) ) {
      DEBUG(pstr("Found a fight for %s with %d", color_name(c->thing, -1), c->num_fights));
      if(c->num_fights > 1)
        c->num_fights--;
      else
        ParseEvent(c);
    }  
  }
}

void ListEvents(player)
  object *player;
{
  CEVENT *c;
  
  if(!cevents) {
    notify(player, "There are no combat events pending...");
    return;
  }

  for(c = cevents; c; c=c->next)
    notify(player, pstr("Event %d on %s of type %d started %s will end %s.", c->id, color_name(c->thing, -1), c->type, get_date(c->started), get_date(c->endtime)));  
}

/** END OF CEVENTS.C **/

