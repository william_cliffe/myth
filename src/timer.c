/****************************************************************************
 MYTH - Timer/Time routines
 Coded by Saruk (01/27/99)
 ---
 Timer routines and some time stuff.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/* Timer declarations */
int myth_ticks = 0;
int database_saves = 0;
int dump_time = 1800;
int update_time = 0;
time_t last_dump_time = 0;
time_t last_update = 0;
time_t last_update_frames = 0;

// Return the date.
char *get_date(intm)
	time_t intm;
{
	static char buf[512];
	
	strcpy(buf, ctime(&intm));
	
	if(buf[strlen(buf)-1]=='\n')
		buf[strlen(buf)-1]='\0';
		
	return buf;
}

char *get_timestamp(intm)
  time_t intm;
{
  static char buf[512];
  struct tm *delta;

  delta = localtime(&intm);
  
  sprintf(buf, "%d/%d %d:%.2d:%.2d", delta->tm_mon + 1, delta->tm_mday, delta->tm_hour, delta->tm_min, delta->tm_sec);
  
  return buf;
}

// A nice time format return.
char *time_format2(intm)
	time_t intm;
{
	static char buf[1024];
	char times[5][24];
	struct tm *delta;
	int weeks, days;

	for(weeks = 0; weeks < 5; weeks++)
		strcpy(times[weeks], "");
		
	if (intm < 0) intm = 0;	
	delta = gmtime(&intm);
	
	days = delta->tm_yday;
	
	if (days >= 7) {
		weeks = (days / 7);
		if ( (days = days - (weeks * 7)) < 0)
			days = 0;
	} else
		weeks = 0;

	delta->tm_year -= 70;		
	if(delta->tm_year > 0) {
		if (delta->tm_year > 1)
			sprintf(times[0], "%d years, ", delta->tm_year);
		else
			strcpy(times[0], "1 year, ");
	}
	
	if(weeks > 1)
		sprintf(times[0], "%s%d weeks, ", times[0], weeks);
	else if (weeks == 1)
		sprintf(times[0], "%s1 week, ", times[0]);
	
	if(days > 1)
		sprintf(times[1], "%d days, ", days);
	else if (days == 1)
		strcpy(times[1], "1 day, ");
		
	if(delta->tm_hour > 0) {
		if(delta->tm_hour > 1)
			sprintf(times[2], "%d hours, ", delta->tm_hour);
		else
			strcpy(times[2], "1 hour, ");
	}
	
	if (delta->tm_min > 0) {
		if(delta->tm_min > 1)
			sprintf(times[3], "%d minutes, and ", delta->tm_min);
		else
			strcpy(times[3], "1 minute, and ");
	}
	
	if (delta->tm_sec > 0) {
		if (delta->tm_sec > 1)
			sprintf(times[4], "%d seconds.", delta->tm_sec);
		else
			strcpy(times[4], "a second.");
	} else {
		strcpy(times[4], "0 seconds.");
	}	
	sprintf(buf, "%s%s%s%s%s", times[0], times[1], times[2], times[3], times[4]);
	
	return buf;
}

// Another style of returning time.
char *time_format(intm)
	time_t intm;
{
	static char buf[1024];
	struct tm *delta;
	int days;
	if (intm < 0) intm = 0;
	
	strcpy(buf, "");
	delta = gmtime(&intm);
	days = delta->tm_yday;
	
	if(days > 1)
		sprintf(buf, "%dd ",days);
	else if (days == 1)
		sprintf(buf, "1d ");

	sprintf(buf, "%s%02d:%02d",buf, delta->tm_hour, delta->tm_min);
	
	return buf;
}

/** TimeFormat():
**/
char *TimeFormat(time_t intm)
{
	char buf[1024];
	char first[64], second[64];  	
	time_t years, weeks, days, hours, minutes, seconds;
	
	
	years = (intm/31449600) ? intm/31449600 : 0;
	intm -= (years * 31449600);
	weeks = (intm/604800) ? intm/604800 : 0;
	intm -= (weeks * 604800);
	days = (intm/86400) ? intm/86400 : 0;
	intm -= (days  * 86400);
	hours = (intm / 3600) ? intm/3600 : 0;
	intm -= (hours * 3600);
	minutes = (intm / 60) ? intm/60 : 0;
	intm -= (minutes * 60);
	seconds = intm;
	
	*first = *second = *buf = '\0';
	
	if(years)
	  sprintf(first, "%ldy", years);
	
	if(weeks) {
	  if(*first)
	    sprintf(second, "%ldw", weeks);
	  else
	    sprintf(first, "%ldw", weeks);
	}
	
	if(days) {
	  if(*first && *second) {
	    sprintf(buf, "%s %s", first, second);
	    return(stralloc(buf));
	  }
	  if(*first)
	    sprintf(second, "%ldd", days);
	  else
	    sprintf(first, "%ldd", days);
	}
	
	if(hours) {
	  if(*first && *second) {
	    sprintf(buf, "%s %s", first, second);
	    return(stralloc(buf));
	  }
	  if(*first)
	    sprintf(second, "%ldh", hours);
	  else
	    sprintf(first, "%ldh", hours);
	}
	
	if(minutes) {
	  if(*first && *second) {
	    sprintf(buf, "%s %s", first, second);
	    return(stralloc(buf));
	  }
	  if(*first)
	    sprintf(second, "%ldm", minutes);
	  else
	    sprintf(first, "%ldm", minutes);
	}	
	
	if(seconds) {
	  if(*first && *second) {
	    sprintf(buf, "%s %s", first, second);
	    return(stralloc(buf));
	  }
	  if(*first)
	    sprintf(second, "%lds", seconds);
	  else
	    sprintf(first, "%lds", seconds);
	}
	
	if(*first && *second)
	  sprintf(buf, "%s %s", first, second);
	else
	  sprintf(buf, "%s", ((*first)) ? first : "0s");
	
	return(stralloc(buf));
}

// return length of idle by seconds, minutes, days, or weeks.
char *count_idle(intm)
	time_t intm;
{
	static char buf[1024];
	
	if(intm > 60) {
	  if(intm > 3600) {
	    if (intm > 86400) {
	      if (intm > 604800) {
	        strcpy(buf, pstr("%ldw", (intm / 604800)));
	      } else {
	        strcpy(buf, pstr("%ldd", (intm / 86400)));
	      }
	    } else {
	      strcpy(buf, pstr("%ldh", (intm / 3600))); 
	    }
	  } else {
	    strcpy(buf, pstr("%ldm", (intm / 60)));
	  }
	} else {
	  strcpy(buf, pstr("%lds", intm));
	}
	
	return buf;	
}


void call_queue(time_t now)
{
	QUEUE *q, *qnext = NULL;
	int i;
	
	for(q = queue; q; q=q->next) {
	  qnext = q->next;
	  if(q->seconds <= now) {
	    genactor = q->enactor;
	    for(i = 0; i < NUMARGS; i++)
	      SET(gvar[i], q->var[i]);
	    parse_queue(q->enactor, q->action, 0, NULL);
	    //filter_command(q->enactor, q->action, 0); 
	    if((q->enactor->clev-=2)<0)
	      q->enactor->clev = 0;
	    q->enactor->qlev--;
	    delete_from_queue(q);
	  }
	}
}


void do_timer()
{
	time_t now;
	char result[MAX_BUFSIZE], *p, *k;

	time(&now);	
	myth_ticks++;
	
	if((now - last_dump_time) > dump_time) {
		/**sort_bboard();**/
		database_saves++;
		if(database_saves > 15)
			database_saves = 0;
		fork_and_dump();
		time(&last_dump_time);
	}


	DO_QNOW();

	// Is there a queue?
	if(queue) {
          call_queue(now);
        }

        // Is there a fight queue?
        if(fightlist) {
          FightAction();
        }
        
        CombatTimer();
        
	if(resock) 
		do_resock();
}


/** HALT: Right now, we're just going to @halt whatever the player has started in @wait unless
	an 'ALL' is passed, then halt the entire queue.
**/
void do_halt(player, arg1, arg2)
	object *player;
	char *arg1, *arg2;
{
	object *who = NULL;
	QUEUE *q, *qnext = NULL;
	int halt = FALSE;
	
	if(!*arg1) {
	  who = player;
	} else if (strcmpm(arg1, "ALL")) {
	  if(!(who = remote_match(player, arg1))) {
	    notify(player, pstr("I don't see %s.", arg1));
	    return;
	  }
	}
	
	if(who) {
	  if(!can_control(player, who)) {
	    notify(player, "You cannot @halt their events.");
	    return;
	  }
	}
	
	for(q = queue; q; q=qnext)
	{
		qnext = q->next;
		if( (q->enactor == player) || (q->enactor == who) || (!strcmpm(arg1, "ALL") && can_control(player, q->enactor))) {
			halt = TRUE;
			delete_from_queue(q);
		}
	}
	
	if(halt) {
	  notify(player, "Halted.");
	}
			
}

void ShowRegen(object *player)
{
  REGENQUEUE *r;
  
  if(!regenlist) {
    notify(player, "Nothing in the regen queue.");
    return;
  } 
  
  for(r = regenlist; r; r=r->next) {
    notify(player, pstr("%s is regenerating...", color_name(r->player, -1)));
  }
}

void ShowFight(object *player)
{
  FIGHTQUEUE *f;
  
  if(!fightlist) {
    notify(player, "There are no fights.");
    return;
  }
  
  for(f = fightlist; f; f=f->next) {
    notify(player, pstr("%s (%d blocks, %d hits) is fighting %s (%d blocks, %d hits).", color_name(f->attacker, -1), 
    	f->a_blocks, f->a_hits, color_name(f->target, -1), f->t_blocks, f->t_hits));
  }
}


/** PS: Right now we're just going to show everyone everything in the QUEUE
**/
void do_ps(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	QUEUE *q;
	object *who = NULL;
	
	if(!*arg1) 
	  arg1 = "ALL";
	
	if(string_prefix(arg1, "regen")) {
	  ShowRegen(player);
	  return;
	} else if (string_prefix(arg1, "fight")) {
	  ShowFight(player);
	  return;
	} else if (string_prefix(arg1, "cevents")) {
	  ListEvents(player);
	  return;
	}

	if(strcmp(arg1, "ALL")) {
	  if(!(who = remote_match(player, arg1))) {
	    notify(player, "I'm sorry, who?");
	    return;
	  }
	}
	
	if(who) {
	  if(!can_control(player, who)) {
	    notify(player, "You cannot see their processes.");
	    return;
	  }
	}
	
	if(!queue) {
	  notify(player, "There is nothing in the queue.");
	  return;
	}

	notify(player, "QUEUE");
	notify(player, pstr("^ch^cw%-12.12s ^cc| ^cw%-12.12s ^cc| ^cw%-24.24s ^cc|^cw %s",
	  "ENACTOR", "ACTIVATOR", "UNTIL", "ACTION"));
	for(q = queue; q; q=q->next)
	{
	  if( can_control(player, q->enactor) ) {
	    notify(player, pstr("%s | %s | %-24.24s | %s",
	      color_name(q->enactor, 12), color_name(q->activator, 12), get_date(q->seconds), q->action));
	  }
	}
}

void do_wait(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	int value;
	
	if(isanumber(arg1))
		value = atoi(arg1);
	
	if(value < 0) {
		notify(player, "* Sorry, cannot be a negative number.");
		return;
	}
	
	
	IS_ARGUMENT(player, (arg2));
	parse_queue(player, arg2, value, NULL);
}

