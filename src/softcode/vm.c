/** Virtual Machine 
    Rudimentary Draft Version
**/

#include <stdint.h>
#include <stdio.h>
#include <string.h>

typedef struct opstack_t {
	uint8_t	idx;
	char *data;
} OPSTACK;

OPSTACK *opstack = NULL;

void VMmul(char *);
void VMdiv(char *);
void VMsub(char *);
void VMjmp(char *);
void VMprint(char *);
void VMadd(char *);
void VMexit(void);

#include "opcodes.h"

unsigned int __INDEX__ = 0;
OPCODES opcodes[OPCODEMAX];

void OPStackAdd(unsigned int hash, char *data, int index)
{
	OPSTACK *o;
	
	o = (OPSTACK *)realloc(opstack, sizeof(OPSTACK) * (index+1));
	opstack = o;
	
	opstack[index].hash = hash;
	opstack[index].data = NULL;
	if(!data || !*data)
	  return;
	opstack[index].data = (char *)malloc(strlen(data));
	memset(opstack[index].data, 0, strlen(data));
	strcpy(opstack[index].data, data);
	//strcpy(opstack[index].data = (char *)malloc(strlen(data)+1), data);
	//opstack[index+1].hash = 0;
	//opstack[index+1].data = NULL;
}

void OPStackInit(unsigned int hash, char *data)
{
	opstack = (OPSTACK *)malloc(sizeof(OPSTACK));
	opstack[0].hash = hash;
	opstack[0].data = NULL;
	opstack[0].data = (char *)malloc(strlen(data));
	memset(opstack[0].data, 0, strlen(data));
	strcpy(opstack[0].data, data);
	//opstack[1].hash = 0;
	//opstack[1].data = NULL;	
}

void VMexit(void)
{
	exit(1);
}

void VMjmp(char *str)
{
	__INDEX__ = strtol(str, (char **)NULL, 10);
}

char *front(char *str, char delimit)
{
	static char buf[4096];
	char *e = buf;
	
	*e = '\0';
	while(*str && *str != delimit)
	  *e++ = *str++;
	
	*e++ = '\0';
	return(buf);
}

char *rear(char *str, char delimit)
{
	static char buf[4096];
	char *e = buf;
	
	*e = '\0';
	while(*str && *str != delimit) str++;
	str++;
	while(*str)
	  *e++ = *str++;
	*e++ = '\0';
	return(buf);
}

int16_t	lvalue(char *str)
{
	return((int16_t)strtol(front(str, ','), NULL, 10));
}

int16_t rvalue(char *str)
{
	return((int16_t)strtol(rear(str, ','), NULL, 10));
}

void VMsub(char *str)
{
	printf("%d\n", lvalue(str) - rvalue(str));
}

void VMadd(char *str)
{
	printf("%d\n", lvalue(str) + rvalue(str));
}

void VMmul(char *str)
{
	printf("%d\n", lvalue(str) * rvalue(str));
}

void VMdiv(char *str)
{
	int16_t lv, rv;
	
	lv = lvalue(str);
	rv = rvalue(str);
	
	if(rv < 1) {
	  printf("DIVIDE BY ZERO ERROR\n");
	  return; 
	}
	printf("%d\n", lv/rv);
}

void VMprint(char *string)
{
	fprintf(stdout, "%s\n", string);
}

int main(int argc, char **argv)
{
	return 1;
}
