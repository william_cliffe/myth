/* softcode/parser.c $ MYTH 

   Softcode for MYTH will be larger and more versatile than TinyMUSE softcode.
   In fact, we will have a larger, global environment, coupled with smaller
   object environments. 
   
   Each object will have an allowance for a 4K environment.
   
   Some objects will need to allocate more of this, in which case extra 4K 
   blocks will be assigned. Those objects will need to be given the ability to
   allocate more than their share of blocks.
*/

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

#include "db.h"
#include "net.h"
#include "externs.h"
#include "ansi.h"

// STRING FUNCTIONS

// MATH FUNCTIONS

// OBJECT FUNCTIONS

// MISC FUNCTIONS


// ANSI SPECIFICS
char *ansi_chartab[256] = {
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,           0,             ANSI_BBLUE,  ANSI_BCYAN,
    0,           0,             0,           ANSI_BGREEN,
    0,           0,             0,           0,
    0,           ANSI_BMAGENTA, 0,           0,
    0,           0,             ANSI_BRED,   0,
    0,           0,             0,           ANSI_BWHITE,
    ANSI_BBLACK, ANSI_BYELLOW,  0,           0,
    0,           0,             0,           0,
    0,           0,             ANSI_BLUE,   ANSI_CYAN,
    0,           0,             ANSI_BLINK,  ANSI_GREEN,
    ANSI_HILITE, ANSI_INVERSE,  0,           0,
    0,           ANSI_MAGENTA,  ANSI_NORMAL, 0,
    0,           0,             ANSI_RED,    0,
    0,           ANSI_UNDER,    0,           ANSI_WHITE,
    ANSI_BLACK,  ANSI_YELLOW,   0,           0,
    0,           0,             0,           0,                                                                                                                                                                                    	
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
};

void cat_str(str, text)
	char *str;
	char *text;
{
	char *a;

	a = str;
	
	while(*str)
	  str++;
	
	while(*text)
	  *str++ = *text++;
	
	*str++ = 0;	
}

/* EXEC();

  This is going to do everything now, from pronoun substitution, to 
  colour subs, to functions, etc.
  
  We can have special flags like:
  ENOFUNC	- No function evaluation
  ENOPCT	- No percent substitution
  ENOCOLOR	- Do not add ANSI colours, but parse out the %x codes.
  ECOMPSPC	- Compress spaces, normally we do not.
  
*/
void exec(str, rbuf, doer, privs, flags)
	char **str;		// string to parse
	char *rbuf;		// return buffer
	object *doer;		// who's doing this?
	object *privs;		// what privileges do they get?
	long flags;		// special EXEC() flags
{
	char result[8000];
	char *s = **str;
	char *r = result;
	int alldone = FALSE;

	result[0] = '\0';
	while(**str && !alldone) {
	  switch(**str) {
	    case ' ': // to eat || not to eat spaces...
	      //if(!mythconf.eat_space)
	      //  strcat(result, " ");
	      *r++ = ' ';
	      break;
	    case '$': // Ie: say Hi there, this is $MYTH_NAME!
	      switch(**str++) {
	        case '$': // Okay, skip back, move on.
	          *str--;
	          break;
	        case '_': // built-in global variable
	          break;
	        default:  // Normal variable.
	          break;
	      }
	      break;
	    case '%':
	        switch(**str++) {
	          case '0':  // This gets the environment args
	          case '1':  // which are stored 0 .. 9
	          case '2':  
	          case '3':
	          case '4':
	          case '5':
	          case '6':
	          case '7':
	          case '8':
	          case '9':
	            break;
	          default:
	            *str--;
	            break;
	        }
	      break;
	    case '[': // FUNC() open

	      sstr = (*str)++;
	      sbuf = parse_it(str, ']');
	      DEBUG(pstr("%s", str));	      
	      
	      
	      break;
	    case '(': // ARGLIST open
	      break;
	    default:
	      *r++ = **str;
	      break;
	  }
	}	
}

