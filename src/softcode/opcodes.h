/** OpCodes **/
/* Now our opcodes are going to be nifty and plentiful so that the Wraith VM can
   do a lot of nifty niftiness in its nifty entirety. (Maybe I should've called 
   this 'Nifty'). Regardless of monikers, this is the header file that contains
   all the opcodes.

   OUR BYTECODE MAGICNUM--> 0xFADEBACC == 4,208,900,812

   PUSHINT8	- Push an 8-bit signed integer onto the stack
   PUSHINT16	- Push a 16-bit signed integer onto the stack
   PUSHINT32	- Push a 32-bit signed integer onto the stack
   PUSHINT64	- Push a 64-bit signed integer onto the stack
   PUSHINT128	- Push a 128-bit signed integer onto the stack
   SREAD	- Read an 8-bit integer
   IREAD	- Read a 16-bit integer
   LREAD	- Read a 32-bit integer
   LLREAD	- Read a 64-bit integer
   LLLREAD	- Read a 128-bit integer
   
*/

#include "opcodedefs.h"

typedef struct opcodes_t {
	char *name;
	unsigned int opcode;
	void (*func)();
} OPCODES;

OPCODES opcode_list={
	{"NOOP",	VM_NOOP,	VMnoop},
	{"PUSHINT",	VM_PUSHINT,	VMpushint},
	{"PUSHLNG",	VM_PUSHLNG,	VMpushlng},
	{"PUSHSTR",	VM_PUSHSTR,	VMpushstr},
	{"READINT",	VM_READINT,	VMreadint},
	{"READLNG",	VM_READLNG,	VMreadlng},
	{"READSTR",	VM_READSTR,	VMreadstr},
	{"	
	{NULL}};
