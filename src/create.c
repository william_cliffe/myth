/****************************************************************************
 MYTH - Dig, create, exit_create functions.
 Coded by Saruk (02/06/99)
 ---
 These functions 'should' create new objects, rooms, and exits. Exits will
 require a bit more work though.
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// internal create calls
object *create_exit P((object *, char *, char *));

// okay, this has to return just the name of the exit for both front and end exits.
// type 0 - returns first exit name
// type 1 - returns last exit name
// type 2 - returns first exit keys
// type 3 - returns last exit keys
char *get_exitname(str, type)
	char *str;
	int type;
{
	// because we want to save everything, we need to strcpy everything (until someone explains
	// a better way to do it for me.)
	static char junk[512], firstexit[512], lastexit[512], enterkeys[512], exitkeys[512];

	// the exit name is basically whatever is stored after an = and before a ;.
	// first off, we want to get that.
	
	strcpy(junk, rparse(str, '=')); // now we have everything after the equal sign.
	if(!*junk)
		return "";
		
	strcpy(firstexit, fparse(junk, ','));
	strcpy(lastexit, rparse(junk, ','));
		
	// we don't parse the list of keys here. we just make sure there are some. if not, we exit with "".
	strcpy(enterkeys, rparse(firstexit, ';'));
	strcpy(firstexit, fparse(firstexit, ';'));
	strcpy(exitkeys, rparse(lastexit, ';'));
	strcpy(lastexit, fparse(lastexit, ';'));
	
	if(type == 0)
		if(!*firstexit) return "";
		else return firstexit;
	else if(type==1)
		if(!*lastexit) return "";
		else return lastexit;
	else if (type==2)
		if(!*enterkeys) return "";
		else return enterkeys;
	else if (type==3)
		if(!*exitkeys) return "";
		else return exitkeys;
	else 
		return "";	
}

// syntax: dig [Room Name]{=[Entrance Name];<KEYS>,[Exit Name];<KEYS>}
// If there's no equal, assume they're just digging a stand-alone room.
// Do not let them build a room with no exit.
// Do not let them build a room with no entrance or exit keys.
// The problem with writing dig is that we need to code exit at the same time.
// So therefore, we have to figure out exit structures and such.
void do_dig(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	OBJECT *digger, *newroom, *exitto = NULL, *exitfrom = NULL;
	char buf[1024];
	static char roomname[512], exitname[512], entername[512], exitkeys[512], enterkeys[512];
	int quotaamt = 0;
	
	// reconstruct command line.
	if(*arg2)
		sprintf(buf, "%s=%s", arg1, arg2);
	else
		strcpy(buf, arg1);
	
	if(!__allowed_name(strip_color(buf))) {
		error_report(player, M_EILLCHAR);
		return;
	}

	if(*rparse(buf, '=')) {
		if(!(player->location->flags & LINK_OK)) {
			if(!can_control(player, player->location)) {
				error_report(player, M_ENODIG);
				return;
		  	}
		}
	}
	
	/* Let's set the digger... */
	if(!(digger = match_player(atr_get(player, A_AUTOOWN))))
		digger = player;
	
	// grab attribute quota for easier manipulation
	if(!has_pow(digger, digger, POW_FREE)) 
	  quotaamt = atoi(atr_get(digger, A_QUOTA)); // just so it's easier
        else
          quotaamt = 999;
        	
	// okay, now we have to figure out how to parse everything in order to make a room.
	strcpy(roomname, fparse(buf, '='));
	
	// We can't build nothing.
	if(!*roomname) {
		error_report(player, M_EILLSTRING);
		return;
	}
	
		
	// get the exits.
	strcpy(entername, get_exitname(buf, 0));
	strcpy(exitname, get_exitname(buf, 1));
	strcpy(exitkeys, get_exitname(buf, 3));
	strcpy(enterkeys, get_exitname(buf, 2));
	
	// Check for appropriate quota...
	if(!*entername && !*exitkeys && !*enterkeys && !*exitname && (quotaamt < 1) ) {
		error_report(player, M_ENOQUOTA);
		return;
	} else if (*entername && *exitname && *exitkeys && *enterkeys && (quotaamt < 3) ) {
		error_report(player, M_ENOQUOTA);
		return;
	}
	
	if( (!*entername && !*exitkeys && !*enterkeys && !*exitname) ||
		(*entername && *exitname && *exitkeys && *enterkeys)) {
		newroom = create_object(digger->ref, TYPE_ROOM, roomname);
		quotaamt--;
		SET(newroom->desc, "This room is undescribed.");
		notify(player, pstr("%s dug with dbref #%d", newroom->name, newroom->ref));
		newroom->location = newroom;
		newroom->ownit = digger;
		newroom->linkto = newroom;
		SET_ATR(newroom, A_ZONE, atr_get(player->location, A_ZONE));
		newroom->zone = player->location->zone;
		add_content(newroom, newroom);
	} else {
		error_report(player, M_ETWOEXIT);
		return;
	}
		
	if(*exitname && *entername && *exitkeys && *enterkeys) {
		// exitto - the exit located in the new room, leading to the old room.	
		exitto = create_exit(digger, exitname, exitkeys);
		exitto->loc = newroom->loc;
		exitto->link = player->location->ref;
		exitto->linkto =  player->location;
		exitto->location = newroom;
		add_content(newroom, exitto);
		notify(player, pstr("[EXIT]: Exit %s created with dbref %d from %s",
			exitto->name, exitto->ref, exitto->location->name));
	
		// exitfrom - the exit located in the old room, leading to the new room.
		exitfrom = create_exit(digger, entername, enterkeys);
		exitfrom->loc = player->loc;
		exitfrom->location = player->location;
		exitfrom->link = newroom->ref;
		exitfrom->linkto = newroom;
		notify(player, pstr("[EXIT]: Exit %s created with dbref %d to %s",
			exitfrom->name, exitfrom->ref, exitfrom->location->name));
		add_content(player->location, exitfrom);
		quotaamt -= 2;
	} else {
		notify(player, "[DIG]: You created a floating room.");
	}
	
	if(!has_pow(digger, digger, POW_FREE))
	  atr_set(rootobj, digger, A_QUOTA, pstr("%d", quotaamt));
}


// Conforms to the MUSE @open command.
void do_open(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *linkto, *exit;
	
	if(!*arg2)
	  linkto = NULL;
	else if (!(linkto = match_room(arg2))) {
	  notify(player, "Which room?");
	  return;
	}
	
	if(!strchr(arg1, ';')) {
	  notify(player, "Invalid exit format...");
	  return;
	}
	
	// CREDIT DECREMENTING SHOULD GO HERE.
	if(!*rparse(arg1, ';')) {
	  notify(player, "You must choose an exit key.");
	  return;
	}
	
	exit = create_exit(player, fparse(arg1, ';'), rparse(arg1, ';'));
	exit->loc = player->loc;
	exit->location = player->location;
	if(linkto) 
		exit->link = linkto->ref;
	else
		exit->link = exit->ref;
	exit->linkto = linkto;
	add_content(player->location, exit);
	
	if(linkto)
	  notify(player, pstr("Your exit (%s) has been created with a link to %s", unparse_object(exit), unparse_object(linkto)));
	else
	  notify(player, pstr("Your unlinked exit (%s) has been created.", unparse_object(exit))); 
}

// Joining rooms means teleporting to one room and then roomjoining the other room via this
// command. (NOTE TO SELF: Add checks for non_controlled rooms!)
// open exits to and from rooms (in case you want to join some rooms).
// Syntax: roomjoin <room> [Entrance <KEY>;<keylist>],[Exit <KEY>;<keylist>]
// Make a note that only dig currently requires an '=' separator for 'spaced names'.
// <room> must be the room's reference number.
void do_roomjoin(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *digger, *roomlink = NULL, *exitto = NULL, *exitfrom = NULL;
	int quotaamt = 0;
	char buf[1024];
	static char exitname[512], entername[512], exitkeys[512], enterkeys[512];
	
	if(!*arg1 || !*arg2) {
		error_report(player, M_EINVARG);
		return;
	}
	
	if(!(digger = match_player(atr_get(player, A_AUTOOWN))))
		digger = player;
		
	if(!(roomlink=match_room(arg1))) {
		error_report(player, M_ENOROOM);
		return;
	}
	
	sprintf(buf, "%s=%s", roomlink->name, arg2);
	if(!__allowed_name(strip_color(buf))) {
		error_report(player, M_EILLCHAR);
		return;
	}
	
	strcpy(entername, get_exitname(buf, 0));
	strcpy(exitname, get_exitname(buf, 1));
	strcpy(exitkeys, get_exitname(buf, 3));
	strcpy(enterkeys, get_exitname(buf, 2));

	if(!(roomlink->flags & LINK_OK)) {
	  	if(!can_control(player, roomlink)) {
			error_report(player, M_ENOCNTRL);
			return;
	  	}
  	}
	
	if(!has_pow(digger, digger, POW_FREE)) {	
  	  if( (atoi(atr_get(digger, A_QUOTA))) < 2) {
		error_report(player, M_ENOQUOTA);
		return;
	  }
	}
	
	if(!has_pow(digger, digger, POW_FREE)) {
	  quotaamt = atoi(atr_get(digger, A_QUOTA));
	}
	
	
	
	if(!*entername || !*exitname || !*exitkeys || !*enterkeys) {
		error_report(player, M_ETWOEXIT);
		return;
	}
	
	// we get this far, now we want to spawn exits from one location (where we are)
	// to the new location...

	// exit located in the player room, pointing to the roomlink.	
	exitto = create_exit(digger, entername, enterkeys);
	exitto->loc = player->loc;
	exitto->location = player->location;
	exitto->link = roomlink->ref;
	exitto->linkto = roomlink;
	add_content(player->location, exitto);
	notify(player, pstr("[ROOMJOIN]: Entrance to %s created with object ref %d",
		color_name(roomlink, -1), exitto->ref));
	
	exitfrom = create_exit(digger, exitname, exitkeys);	
	exitfrom->loc = roomlink->loc;
	exitfrom->location = roomlink;
	exitfrom->link = player->location->ref;
	exitfrom->linkto = player->location;
	add_content(roomlink, exitfrom);
	notify(player, pstr("[ROOMJOIN]: Exit from %s created with object ref %d",
		color_name(roomlink, -1), exitfrom->ref));
	
	if(!has_pow(digger, digger, POW_FREE)) 
	  atr_set(rootobj, digger, A_QUOTA, pstr("%d", (quotaamt -= 2)));
}

// create an exit, returning the new exit and using object->pass to store exit keys.
object *create_exit(player, exitname, exitkeys)
	object *player;
	char *exitname;
	char *exitkeys;
{
	object *newexit;
	
	newexit = create_object(player->ref, TYPE_EXIT, exitname);
	newexit->ownit = player;
	SET(newexit->desc, "You see an exit.");
	SET(newexit->pass, exitkeys);
	
	return newexit;
}

// player creates thing (arg1)
void do_create(player, cmdswitch, arg1)
	object *player;
	char *cmdswitch;
	char *arg1;
{
	object *thing, *creator;
	
	printf("%s\n", arg1);

	if(*cmdswitch) {
	  if(string_prefix(cmdswitch, "item")) // create an item template
	    ItemCreate(player, arg1);
	  else if (string_prefix(cmdswitch, "creature")) 
	    CreateCreatureTemplate(player, arg1);
	  //else if(string_prefix(cmdswitch, "group"))
	  //else if(string_prefix(cmdswitch, "access"))
	  
	  // If we have a swtich, return from do_create.
	  return;
	}
	if(!__allowed_name(strip_color(arg1))) {
		error_report(player, M_EILLCHAR);
		return;
	}
	
	if(!(creator = match_player(atr_get(player, A_AUTOOWN))))
		creator = player;

	if( (thing = match_player(arg1)) ) {
		error_report(player, M_ESHDNAME);
		return;
	}
	
	if(!has_pow(creator, creator, POW_FREE)) {
	  if(atoi(atr_get(creator, A_QUOTA)) < 1) {
		error_report(player, M_ENOQUOTA);
		return;
	  }
	}
	
	thing = create_object(creator->ref, TYPE_THING, arg1);
	SET(thing->desc, "You see an undescribed thing.");
	thing->ownit = creator;
	thing->link = player->ref;
	thing->linkto = player;
	thing->location = player;
	if(player->flags & INHERIT) thing->flags |= INHERIT;
	add_content(player, thing);
	if(!has_pow(creator, creator, POW_FREE)) {
	  atr_set(rootobj, creator, A_QUOTA, pstr("%d", atoi(atr_get(creator, A_QUOTA)) -1 ));
	}
	notify(player, pstr("[CREATE]: %s (%d) created.", 
		color_name(thing, -1), thing->ref));
}
