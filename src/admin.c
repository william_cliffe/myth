/****************************************************************************
 MYTH v1.01 - Administrative functions
 Coded by Saruk (1/09/00)
 ---
 forcing, nuking, etc. will go in here
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "db.h"
#include "net.h"
#include "externs.h"

void do_as(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	/* This function will do a function _as_ arg1 */
	object *victim = NULL, *old = NULL;
	int store = -1;
	DESCRIPTOR *d;
	
	if(!(victim=match_object(arg1))) {
		error_report(player, M_EILLNAME);
		return;
	}
	
	if(!can_control(player, victim)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	old = player;
	for(d=descriptor_list;d;d=d->next) 
		if(d->player == player) {
			d->player = victim;
			store = d->descriptor;
			break;
		}
	
	if(store == -1) {
	  log_channel("log_imp", pstr("Invalid @as attempted by %s", unparse_object(player)));
	  return;
	}
	
	player = victim;
	//do_command(d, arg2);
	filter_command(victim, arg2, 0);
	player = old;
	for(d = descriptor_list; d; d=d->next)
		if(d->descriptor == store)
			d->player = player;
			
	
}

void do_admin(player)
	object *player;
{
	object *o;
	
	
	// NAME         -  ALIAS - POSITION                 - ON/OFFLINE
	
	notify(player, pstr(" --< Administration >%s", make_ln("-", 56)));
	notify(player, pstr("| %-12.12s | %-5.5s | %-36.36s | %-12.12s |",
				"NAME", "ALIAS", "POSITION", "ON/OFFLINE"));
	notify(player, pstr("|%s|", make_ln("-", 76)));
	for(o = player_list; o; o=o->next)
		if( (o != rootobj) && (o->flags & WIZARD) )
			notify(player, pstr("| %s | %s | %-36.36s | %s |",
				color_name(o, 12), format_color(atr_get(o, A_ALIAS), 5), atr_get(o, A_POSITION), 
					(o->flags & CONNECT) ? (!(o->flags & INVISIBLE)||can_control(player, o)) ? format_color("ONLINE", 12) : format_color("OFFLINE", 12) : format_color("OFFLINE", 12)));
	notify(player, pstr("%s", make_ln("-", 78)));
}

void do_alist_list(player)
	object *player;
{
	ATTR *a;
	int i = 0, ctr = 0;

	while(i < dbtop) {
	  if(db[i]) {
	    for(a = (db[i])->alist; a; a=a->next)
	      ctr++;
	  }
	  i++;
	}	
	
	notify(player, pstr("There are %d attributes listed.", ctr));
}

void do_su(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	DESCRIPTOR *d, *d2;
	object *who;
	
	if(!(who=match_player(arg1))) {
		error_report(player, M_EILLNAME);
		return;
	}
	
	for(d = descriptor_list; d; d=d->next)
		if(d->player == player)
			break;
	
	if(!d) {
		notify(player, "uh? wrong!");
		return;
	}
	
	if(can_control(player, who)) {
	        d->player->flags &= ~CONNECT;
	        for(d2 = descriptor_list; d2; d2=d2->next) {
	          if(d2->player == d->player && (d2->descriptor != d->descriptor))
	            d2->player->flags |= CONNECT;
	        }
		d->player = who;
		d->player->flags |= CONNECT;
		notify(who, pstr("You have become %s", color_name(who, -1)));
		log_channel("log_sens", pstr("SU: %s becomes %s", 
			color_name(player, -1), color_name(who, -1)));
		return;
	}
	
	/* it's obvious that the player does not have control over 'who'. 
	   now we'll determine if 'player' gets the password right! */
	
	if(!*arg2) // okay, they're dumb...
	{
		log_channel("log_sens",pstr("SU: %s attempted to become %s",
			color_name(player, -1), color_name(who, -1)));
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	if(!dcrypt(who->pass, arg2)) { // they are successful
	  d->player->flags &= ~CONNECT;
	  for(d2 = descriptor_list; d2; d2=d2->next) {
	    if(d2->player == d->player && (d2->descriptor != d->descriptor))
	      d2->player->flags |= CONNECT;
          }
	  d->player = who;
	  d->player->flags |= CONNECT;
	  
	  notify(who, pstr("You have become %s", color_name(who, -1)));
	  log_channel("log_sens",pstr("SU: %s becomes %s", 
	                color_name(player, -1), color_name(who, -1)));
	  return;
	}
	
	log_channel("log_sens",pstr("SU: %s attempted to become %s",
			color_name(player, -1), color_name(who, -1)));
	error_report(player, M_ENOCNTRL);
}

ptype name_to_pow(nam)
	char *nam;
{
	int k=0;
	
	for(k = 0; k < NUM_POWS; k++)
		if(!strcmpm(powers[k].name,nam))
			return powers[k].pnum;
	
	return 0;
}

char *pow_to_name(pow)
	ptype pow;
{
	int k;
	
	for(k = 0; k<NUM_POWS; k++)
		if(powers[k].pnum == pow)
			return powers[k].name;
	
	return "<unknown power>";	
}

void do_empower(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who;
	ptype pow, powval;
	int k;
	char *i;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	if(!(who = remote_match(player, arg1))) {
	  notify(player, "Who?");
	  return;
	}

	if(!(who->flags & TYPE_PLAYER)) {
	  notify(player, "You can only give powers to players.");
	  return;
	}
	
	if(!who->pows)
		return;

	i = strchr(arg2, ':');
	if(!i) {
		notify(player, "Badly formed power specification. You must use <PowerType>:<Power Value>");
		return;
	}
	*(i++) = '\0';
	
	if(!strcmpm(i, "yes"))
		powval = PW_YES;
	else if (!strcmpm(i, "yeseq"))
		powval = PW_YESEQ;
	else if (!strcmpm(i, "yeslt"))
		powval = PW_YESLT;
	else if (!strcmpm(i, "no"))
		powval = PW_NO;
	else
	{
		notify(player, "The power value must be one of YES, YESEQ, YESLT, or NO");
		return;
	}
	
	if(!(pow = name_to_pow(arg2))) 
	{
		notify(player, pstr("Unknown power: %s", arg2));
		return;
	}
	
	if(!has_pow(player, who, POW_SETPOW)) {
		log_channel("log_important", pstr("%s failed to: @empower %s=%s:%s",
			color_name(player, -1), color_name(who, -1), arg2, i));
		notify(player, "Sorry. Bad user!");
		return;
	}
	
	if(get_pow(player, pow) < powval && player!=rootobj) {
		notify(player, pstr("You can't @empower %s with that power and level!", color_name(who, -1)));
		return;
	}
	
	for(k = 0; k < NUM_POWS; k++)
		if(powers[k].pnum == pow) 
		{
			if(powers[k].max_pow[class_to_list_pos(*who->ownit->pows)]>=powval) {
				set_pow(who, pow, powval);
				log_channel("log_imp", pstr("%s executed: @empower %s=%s", 
					color_name(player, -1), color_name(who, -1), pow_to_name(pow)));
				if(powval!=PW_NO) {
					notify(who, pstr("You have been given the power of %s.", pow_to_name(pow)));
					notify(player, pstr("%s has been given the power of %s.", color_name(who, -1), pow_to_name(pow)));
				}
				switch(powval)
				{
					case PW_YES:
						notify(who, "You can use it on anyone.");
						break;
					case PW_YESEQ:
						notify(who, "You can use it on people your class and under.");
						break;
					case PW_YESLT:
						notify(who, "You can use it on people under your class.");
						break;
					case PW_NO:
						notify(who, pstr("Your power of %s has been removed.", pow_to_name(pow)));
						notify(player, pstr("%s's power of %s has been removed.", color_name(who, -1), pow_to_name(pow)));
						break;
				}
				return;
			} else {
				notify(player, "Sorry, that is beyond the maximum power level for that player.");
				return;
			}
		}

	notify(player, "wtf?");	
}

void do_powers(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who;
	int k=0, h=0;

	if(!*arg1)
		who = player;
	else {
		if(!(who = match_player(arg1))) {
			error_report(player, M_EILLNAME);
			return;
		}
	}
	
	if(!can_control(player, who)) {
		notify(player, "You can't see their powers.");
		return;
	}
	
	if(!who->pows) {
		notify(player, pstr("%s has no powers.", color_name(who, -1)));
		return;
	}
	
	
	notify(player, pstr("%s's powers:", color_name(who, -1)));
	for(k=0;k < NUM_POWS; k++)
	{
		ptype m;
		char *l;
		
		m = get_pow(who, powers[k].pnum);
		l = "";
		switch(m) 
		{
			case PW_YES:
				l = "YES", h++;
				break;
			case PW_YESLT:
				l = "YESLT", h++;
				break;
			case PW_YESEQ:
				l = "YESEQ", h++;
				break;
			case PW_NO:
				continue;
		}
		if(l) 
			notify(player, pstr("[%-16.16s:%-5.5s] - %s",
				powers[k].name, l, powers[k].desc)); 
	}	
	
	notify(player, pstr("%s has %d of %d possible powers.", color_name(who, -1), h, NUM_POWS));
	notify(player, "---- End of List ----");
	
}

void do_class(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who;
	int i, k;

	if(!*arg1) 
	{
		if(!player->pows)
			return;
		notify(player, pstr("You are a %s", class_to_name(*player->pows)));
		return;
	}
	
	if(!strcmpm(arg1, "list")) { // list the @classes
	        notify(player, pstr("%s Classes:", return_config("myth_name")));
		for(i = 0; i < NUM_LIST_CLASSES; i++)
		  notify(player, pstr("\t%s",class_to_name(i+1)));
		return;
	}
	
	if(!(who = remote_match(player, arg1))) {
		error_report(player, M_EILLNAME);
		return;
	}
	
	if(!(who->flags & TYPE_PLAYER)) {
	  notify(player, "They must be a player!");
	  return;
	}
	
	if(!*arg2) { // we just want to know what class they are.
		if(!who->pows)
		  return;
		notify(player, pstr("%s is of class: %s",
		  color_name(who, -1), class_to_name(*who->pows)));
		return;	
	}
	
	i = name_to_class(arg2);
	
	if(!i)
	{
		notify(player, "What class are you talking about?");
		return;
	}
	
	
	if(!has_pow(player, who, POW_CLASS) || 
	      who == rootobj) { // || ( (i >= player->pows[0]) && player != rootobj)) {
	  notify(player, "You can't do that.");
	  return;
	}

	
	if(!who->pows)
	{
		who->pows = malloc(sizeof(ptype)*2);
		who->pows[1] = 0;
	}
	
	who->pows[0] = i;
	for(k = 0; k<NUM_POWS; k++)
	{
		set_pow(who, powers[k].pnum, powers[k].init_pow[class_to_list_pos(i)]);
	}
	notify(player, pstr("%s is now a %s", color_name(who, -1), class_to_name(*who->pows)));
	notify(who, pstr("%s just classed you as: %s", color_name(player, -1), class_to_name(*who->pows)));
	log_channel("log_imp", pstr("* %s just classed %s to %s", color_name(player, -1), color_name(who, -1), class_to_name(*who->pows)));
}

void do_force(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *thing;
	char buf[MAX_BUFSIZE];
	char *k, *bf = buf;
	object *saveenactor;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	
	// let's try something....
	pronoun_substitute(buf, player, arg1, player);
	bf = buf + strlen(player->name) + 1;
	
	
	if(!(thing = remote_match(player, bf))) {
		error_report(player, M_EOBJID);
		return;
	}
	
	if(!can_control(player, thing)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	notify(player, pstr("%s forced", color_name(thing, -1)));
	
	/** We don't need to parse an @force into the queue right now. 
	**/
	saveenactor = genactor;
	genactor = thing;
	while((k = (char *)parse_up(&arg2, ';'))) 
		parse_queue(thing, k, 0, NULL);
	genactor = saveenactor;
}


void do_swap(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *thing, *thing2;
	object *tmp;
	MAIL *m;
	int ref, ref2;
	ATTR *a;
	OBJECT *o;
	PARENT *p;
	
	notify(player, "This command is unavailable.");
	return;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	if(!(thing = remote_match(player, arg1))) {
	  notify(player, "What do you want to swap?");
	  return;
	}

	if(!has_pow(player, thing, POW_MODIFY) && !can_control(player, thing)) {
	  notify(player, "You cannot do that.");
	  return;
	}
	
	if(!(thing2 = remote_match(player, arg2))) {
	  notify(player, "What do you want to swap?");
	  return;
	}

	if( (thing == thing2) || (thing->ref == 0) || (thing2->ref == 0)) {
	  notify(player, "Uh, no.");
	  return;
	}

	if(!has_pow(player, thing2, POW_MODIFY) && !can_control(player, thing2)) {
	  notify(player, "You cannot do that.");
	  return;
	}
	
	/** In order to swap, due to the nature of TinyMYTH, you have to go through
	    and swap all the attribute dbrefs, reset the zones and inherency,
	    and you'd have to scour the database for any mention of one dbref and replace
	    it with the new dbref.
	**/
	
	
		
	ref = thing2->ref;
	ref2 = thing->ref;

	for(p = thing->parent; p; p=p->next)
	  update_parent_attribute(p->what, pstr("#%d", thing->ref));
	
	for(p = thing->children; p; p=p->next)
	  update_child_attribute(p->what, pstr("#%d", thing->ref));

	for(p = thing2->parent; p; p=p->next)
	  update_parent_attribute(p->what, pstr("#%d", thing2->ref));
	
	for(p = thing2->children; p; p=p->next)
	  update_child_attribute(p->what, pstr("#%d", thing2->ref));
	  

	thing->ref = ref;
	tmp = thing;
	thing2->ref = ref2;
	db[ref2] = thing2;
	db[ref] = tmp;	
	
	


	// ATTRs
/*	for(a = alist; a; a=a->next)
	  if(a->dbref == ref)
	    a->dbref = ref2;
	  else if (a->dbref == ref2)
	    a->dbref = ref;
*/	
	//MAIL
	for(o = player_list; o; o=o->next)
	  for(m = o->mail; m; m=m->next)
	    if(m->fromref == ref)
	      m->fromref = ref2;
	    else if(m->fromref == ref2)
	      m->fromref = ref;

	set_zones();
	
	/** In order to fix inherency...
	
	    Our best bet is to repace oldref with newref in the A_PARENT & A_CHILDREN
	    attributes, then push a set_inherency() call through, thus resetting
	    the inherency on each object.

		
	 **/

	notify(player, "Sorry, @swap is not ready yet.");
}


void do_del(player, cmdswitch, arg1, arg2)
  object *player;
  char *cmdswitch, *arg1, *arg2;
{
  if(string_prefix(cmdswitch, "shop")) {
    if(!has_pow(player, player, POW_COMBAT)) {
      notify(player, "You cannot manipulate combat properties.");
      return;
    }
    DelFromShop(player, arg1, arg2);
  }
}

void do_add(player, cmdswitch, arg1, arg2)
  object *player;
  char *cmdswitch, *arg1, *arg2;
{
  if(string_prefix(cmdswitch, "shop")) {
    if(!has_pow(player, player, POW_COMBAT)) {
      notify(player, "You cannot manipulate combat properties.");
      return;
    }
    AddToShop(player, arg1, arg2);
  }
}


void do_dpage(object *player, char *cswitch, char *arg1, char *arg2)
{
  DESCRIPTOR *d = NULL;
  int des = atoi(arg1);
  
  if(!has_pow(player, player, POW_SECURITY)) {
    notify(player, "You may not use the @dpage command.");
    return;
  }

  if(!des) {
    notify(player, "Incorrect descriptor identifier.");
    return;
  }  
  
  for(d = descriptor_list; d; d=d->next)
    if(d->descriptor == des) break;

  if(d) {
    if(string_prefix(cswitch, "invisible"))
      dwrite(d, tprintf("%s\n", arg2));
    else
      dwrite(d, tprintf("%s pages: %s\n", player->name, arg2));
    notify(player, tprintf("You paged descriptor %d with: %s", d->descriptor, arg2));
  } else {
    notify(player, "No such descriptor available.");
  }
  
}

//log_channel("log_err", "* Error loading item from database.");
void log_channel(char *channel, char *mesg)
{
  char buf[1024];
  FILE *fp;
  
  sprintf(buf, "logs/%s", channel);
  
  if(!(fp = fopen(buf, "a"))) {
    printf("error saving channel.");
    return;
  }
  
  fprintf(fp, "(%s) %s\n", get_date(time(0)), mesg);
  LogChannel(channel, mesg);
 
 fclose(fp);
} 

void AddFuncCallList(char *name, void *(call))
{
  FLIST *f;
  
  GENERATE(f, FLIST);
  
  f->name = NULL;
  SET(f->name, name);
  f->func = call;

  if(!flist) {
    f->next = f->prev = NULL;
    flist = ftail = f;
  } else {
    ftail->next = f;
    f->prev = ftail;
    f->next = NULL;
    ftail = f;
  }  
}

FLIST *CallFuncCall(char *name)
{
  FLIST *f;
  
  for(f = flist; f; f=f->next)
  {
    printf("%s\n", f->name);
    if(!strcmpm(f->name, name))
      return f;
  }
  
  return(NULL);
}

/** END OF ADMIN.C **/
