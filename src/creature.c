/** creature.c $ havencrag (11/28/2004)

  - If creatures are done as a list in a ROOM, then fight.c has to be rewritten to accomodate.
  - Otherwise, if creatures remain an object of TYPE_THING, the creature system will have to
    make sure it recognizes the differences.
    
**/

#include <stdlib.h> 
#include <string.h> 
#include <stdio.h> 
#include <time.h>

#include "db.h"
#include "net.h"
#include "externs.h"

unsigned int creaturetop = 0;

CRTEMPLATE *crtemplates = NULL;
CRTEMPLATE *crtemplatestail = NULL;
CREATURE *creatures = NULL, *creaturestail = NULL, **creaturedb = NULL;

/** UniqueCreatureId():
**/
unsigned int UniqueCreatureId()
{
  CREATURE *c;
  int ref = 0;

  if(!creatures)
    return 1;
  
  for(c = creatures; c; c=c->next)
    if(ref < c->id)
      ref = c->id + 1;
  
  return(ref);
}

/** UniqueTemplateId():
**/
unsigned int UniqueTemplateId()
{
  CRTEMPLATE *c;
  int ref = 0;
  
  if(!crtemplates) return 1;
  
  for(c = crtemplates; c; c=c->next)
  {
    if(ref < c->id) ref = c->id + 1;
  }
  
  return ref;
}

/** IsACreatureTemplate():
**/
CRTEMPLATE *IsACreatureTemplate(char *str)
{
  CRTEMPLATE *c;
  
  for(c = crtemplates; c; c=c->next)
    if(!strcmpm(c->name, str))
      return(c);
  
  return(NULL);
}

/** __CreateCreatureTemplate():
**/
CRTEMPLATE *__CreateCreatureTemplate(char *name)
{
  CRTEMPLATE *c;
  CSTATS *stats;

  if(!name || !*name)
    return(NULL);
  
  GENERATE(c, CRTEMPLATE);
  c->id = UniqueTemplateId();
  c->name = NULL;
  SET(c->name, name);
  c->flags = 0;
  c->iq = c->climate = c->area = 0;
  c->gender = c->preference = 0;
  c->gestation = time(0) + MAX_CREATURE_GESTATION;
  GENERATE(stats, CSTATS);  
  
  /** GENERATE STATS **/
  /* Zero out all modifiers */
  stats->strmod = stats->intmod = stats->dexmod = stats->wismod = stats->dexmod = 0;
  stats->chamod = stats->lckmod = stats->conmod = 0;
  stats->level = 0;
  stats->exp = 0;
  stats->str = stats->intel = stats->dex = stats->wis = 0;
  stats->con = stats->cha = stats->lck = stats->sta = 0;
  stats->mp = stats->maxmp = 0;
  stats->hp = stats->maxhp = 0;
  stats->weight = stats->height = stats->carry = 0.00;
  
  /** Copy stats to Creature **/
  c->stats = stats;

  if(!crtemplates) {
    c->next = c->prev = NULL;
    crtemplates = crtemplatestail = c;
  } else {
    c->next = NULL;
    c->prev = crtemplatestail;
    crtemplatestail->next = c;
    crtemplatestail = c;
  }
  
  return(c);
}

/** CreateCreatureTemplate():
**/
void CreateCreatureTemplate(object *player, char *arg1)
{
  CRTEMPLATE *creature;
  
  if(!*arg1) {
    notify(player, "What kind of creature are you attempting to create?");
    return;
  }
  
  if( (creature = IsACreatureTemplate(arg1))) {
    notify(player, "That creature already exists.");
    return;
  }
  
  creature = __CreateCreatureTemplate(arg1);
  
  notify(player, "Your creature has been created.");
}

/** ModifyCreatureTemplate():
**/
void ModifyCreatureTemplate(object *player, CRTEMPLATE *creature, int mod, char *data)
{
  char buf[1024];
  int modified = TRUE;
  
  if(!*data) {
    notify(player, "Not enough arguments for modification.");
    return;
  }
  
  if(!isanumber(data)) {
    notify(player, "That is not a valid number.");
    return;
  }

  switch(mod) {
    case CMOD_MAXHP:
      creature->stats->hp = creature->stats->maxhp = atoi(data);
      break;
    case CMOD_MAXMP:
      creature->stats->mp = creature->stats->maxmp = atoi(data);
      break;
    case CMOD_STR:
      creature->stats->str = atoi(data);
      break;
    case CMOD_INT:
      creature->stats->intel = atoi(data);
      break; 
    case CMOD_WIS:
      creature->stats->wis = atoi(data);
      break;
    case CMOD_STA:
      creature->stats->sta = atoi(data);
      break;
    case CMOD_CON:
      creature->stats->con = atoi(data);
      break;
    case CMOD_DEX:
      creature->stats->dex = atoi(data);
      break;
    case CMOD_LCK:
      creature->stats->lck = atoi(data);
      break;
    case CMOD_CHA:
      creature->stats->cha = atoi(data);
      break;
    case CMOD_HEIGHT:
      creature->stats->height = atof(data);
      break;
    case CMOD_WEIGHT:
      creature->stats->weight = atof(data);
      break;
    case CMOD_IQ:
      creature->iq = atoi(data);
      break;
    case CMOD_GENDER: // -1 Male, 0 - Neutral, 1 - Female
      creature->gender = atoi(data);
      break;
    case CMOD_PREFERENCE:
      creature->preference = atoi(data);
      break;
    case CMOD_GESTATION:
      creature->gestation = atol(data);
      break;
    case CMOD_AREA:
      creature->area = atoi(data);
      break;
    case CMOD_CLIMATE:
      creature->climate = atoi(data);
      break;
    case CMOD_ALIGNMENT:
      creature->stats->alignment = atoi(data);
      break;
    case CMOD_LEVEL:
      creature->stats->level = atoi(data);
      break;
    default:
      modified = FALSE;
      *buf = '0';
      break;
  }
  
  if(!modified) {
    notify(player, "I'm not sure what modifications you wish to perform.");
    return;
  }
  
  notify(player, pstr("%s modified with value: %s", creature->name, data));
}

/** SetCreatureFlag(): 
**/
int SetCreatureFlag(creature, flag)
  CRTEMPLATE *creature;
  char *flag;
{
  int truth, i;
  
  if(!(truth = (*flag == '!') ? 0 : 1)) flag++;
  
  for(i = 0; creatureflag_list[i].name; i++)
  {
    if(string_prefix(creatureflag_list[i].name, flag)) break;
  }
  
  if(!creatureflag_list[i].name) return FALSE;
  
  if(truth) { creature->flags &= ~creatureflag_list[i].flag; }
  else      { creature->flags |= creatureflag_list[i].flag; }
  
  return TRUE;
}


/** CreatureSetTemplate(): @set/creature Wasp=maxhp:5
**/
void CreatureSetTemplate(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  CRTEMPLATE *creature;
  CSTATS *stats;
  char *arg, *data;
  
  if(!(creature = IsACreatureTemplate(arg1))) {
    notify(player, "What creature are you referring to?");
    return;
  }
  
  if(!*(arg = fparse(arg2, ':'))) {
    notify(player, "I do not understand that argument.");
    return;
  }
  
  if(!*(data = rparse(arg2, ':'))) {
    notify(player, "Not enough arguments.");
    return;
  }
  
  if(!(stats = creature->stats)) {
    notify(player, "Error in template. Talk to an administrator!");
    return;  
  }
  
  if (string_prefix(arg, "name")) {
    if(creature->name) { block_free(creature->name); }
    SET(creature->name, data);
  } else if(string_prefix(arg, "maxhp")) {
    ModifyCreatureTemplate(player, creature, CMOD_MAXHP, data);
  } else if(string_prefix(arg, "maxmp")) {
    ModifyCreatureTemplate(player, creature, CMOD_MAXMP, data);
  } else if(string_prefix(arg, "strength")) {
    ModifyCreatureTemplate(player, creature, CMOD_STR, data);
  } else if(string_prefix(arg, "intelligence")) {
    ModifyCreatureTemplate(player, creature, CMOD_INT, data);
  } else if(string_prefix(arg, "dexterity")) {
    ModifyCreatureTemplate(player, creature, CMOD_DEX, data);
  } else if(string_prefix(arg, "luck")) {
    ModifyCreatureTemplate(player, creature, CMOD_LCK, data);
  } else if(string_prefix(arg, "wisdom")) {
    ModifyCreatureTemplate(player, creature, CMOD_WIS, data);
  } else if(string_prefix(arg, "charisma")) {
    ModifyCreatureTemplate(player, creature, CMOD_CHA, data);
  } else if(string_prefix(arg, "stamina")) {
    ModifyCreatureTemplate(player, creature, CMOD_STA, data);
  } else if(string_prefix(arg, "constitution")) {
    ModifyCreatureTemplate(player, creature, CMOD_CON, data);
  } else if(string_prefix(arg, "weight")) {
    ModifyCreatureTemplate(player, creature, CMOD_WEIGHT, data);
  } else if(string_prefix(arg, "height")) {
    ModifyCreatureTemplate(player, creature, CMOD_HEIGHT, data);
  } else if(string_prefix(arg, "flags")) {
    if(!SetCreatureFlag(creature, data)) {
      notify(player, "Invalid flag!");
      return;
    }
    notify(player, pstr("%s's flag set.", creature->name));
  } else if(string_prefix(arg, "iq")) {
    ModifyCreatureTemplate(player, creature, CMOD_IQ, data);
  } else if(string_prefix(arg, "gestation")) {
    ModifyCreatureTemplate(player, creature, CMOD_GESTATION, data);
  } else if(string_prefix(arg, "climate")) {
    ModifyCreatureTemplate(player, creature, CMOD_CLIMATE, data);
  } else if(string_prefix(arg, "area")) {
    ModifyCreatureTemplate(player, creature, CMOD_AREA, data);
  } else if(string_prefix(arg, "gender")) {
    ModifyCreatureTemplate(player, creature, CMOD_GENDER, data);
  } else if(string_prefix(arg, "alignment")) {
    ModifyCreatureTemplate(player, creature, CMOD_ALIGNMENT, data);
  } else if(string_prefix(arg, "level")) {
    ModifyCreatureTemplate(player, creature, CMOD_LEVEL, data);
  } else if(string_prefix(arg, "preference")) {
    ModifyCreatureTemplate(player, creature, CMOD_PREFERENCE, data);
  }
  
}

void CreatureList(object *player, char *arg1)
{
  CRTEMPLATE *c;

  if(!crtemplates) {
    notify(player, "No creatures have been created.");
    return;
  } 
  
  if(!*arg1) {
    notify(player, "List of Creatures:\r\n---");
    for(c = crtemplates; c; c=c->next)
    {
      notify(player, pstr("Creature: %s (%d)", c->name, c->id));
    }
    notify(player, "---");
  } else {
    for(c = crtemplates; c; c=c->next)
    {
      if(string_prefix(arg1, c->name)) break;
    }
    
    if(!c) { notify(player, "No such creature appears to exist."); return; }
    
    notify(player, pstr("Creature %s (%d)", c->name, c->id));
    notify(player, pstr("Max Hitpoints: %d\nMax Magic Points: %d", c->stats->maxhp, c->stats->maxmp));
  }
  
  
}

    
/** END OF CREATURE.C **/

