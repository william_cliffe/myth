/*** trust.c $ MYTH ***/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

#include "trust.h"

PERMISSION permissions[]={
	{PERM_SHUTDOWN, "Shutdown", "Permission to shutdown the MU*"},
	{PERM_REBOOT, "Reboot", "Permission to reboot the MU*"},
	{PERM_PCREATE, "Pcreate", "Permission to create players."},
	{PERM_NUKE, "Nuke", "Permission to destroy players."},
	{PERM_BOOT, "Boot", "Permission to disconnect players from the game."},
	{PERM_REMOTE, "Remote", "Permission to address remote players."},
	{PERM_JOIN, "Join", "Permission to join another player."},
	{PERM_SUMMON, "Summon", "Permission to summon another player."},
	{PERM_MODIFY, "Modify", "Permission to modify another's objects/attributes."},
	{PERM_EXAMINE, "Examine", "Permission to examine another's objects/attributes."},
	{PERM_ATTRSET, "Attrset", "Permission to set 'Wizard' attributes."},
};

// Default root permission grants
struct permission_grant default_root_perms[]={
	{PERM_SHUTDOWN, PERM_ALL},
	{PERM_REBOOT, PERM_ALL},
	{PERM_PCREATE, PERM_ALL},
	{PERM_NUKE, PERM_ALL},
	{PERM_BOOT, PERM_ALL},
	{PERM_REMOTE, PERM_ALL},
	{PERM_JOIN, PERM_ALL},
	{PERM_SUMMON, PERM_ALL},
	{PERM_MODIFY, PERM_ALL},
	{PERM_EXAMINE, PERM_ALL},
	{PERM_ATTRSET, PERM_ALL},
};

ACCESSLIST default_root_list = {0, "Root List", default_root_perms};
