/****************************************************************************
 MYTH - Object handling.
 Coded by Saruk (02/08/99)
 ---
 Handle objects routines such as content adding, manipulation of locations, 
 etc.
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// Add contents of an object.
void add_content(player, what)
	object *player;
	object *what;
{
	CONTENTS *c;
	
	GENERATE(c, CONTENTS);
	c->content = what;
	c->next = player->contents;
	player->contents = c;
}

// Delete contents of an object.
void del_content(player, what)
	object *player;
	object *what;
{
	CONTENTS *c, *cprev = NULL;
	
	for(c = player->contents; c; c=c->next) {
		if(c->content == what) 
			break;
		cprev = c;
	}
	
	if(!c)
		return;
	
	if(!cprev)
		player->contents = c->next;
	else
		cprev->next = c->next;
	
	block_free(c);
}			

// This will use the move_to procedure and move an object to an object.
void do_teleport(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim, *where = NULL;
	
	IS_ARGUMENT(player, (arg1));
	
	if(!(victim = match_object(arg1))) {
		error_report(player, M_EILLNAME);
		return;
	}
	
	if(*arg2) {
	  if(!strcmpm(arg2, "home"))
	    where = victim->linkto;
	  else {
		if(!(where = match_object(arg2))) {
			error_report(player, M_EILLNAME);
			return;
		}
	  }
	}
	
	// Let's rewrite this as CAN_CONTROL checks for WIZARD and INHERENCY...
		
	if(!where) {
		if(player->location == victim) {
			notify(player, "* You're already at this location!");
			return;
		}
		if( (victim->flags & CAN_TEL) || can_control(player, victim) || player->linkto == victim) {
			//notify(player, pstr("[TELEPORT]: Teleporting you to %s",
			//	color_name(victim, -1)));
			move_to(player, victim);
		} else 
			error_report(player, M_ENOCNTRL);
	} else {
		//if( !(player->flags & WIZARD) && (victim->flags & WIZARD)) {
		//	error_report(player, M_ENOCNTRL);
		//	return;
		//}
		if ( (can_control(player, victim) || (victim->location == player->location && 
			player->location->ownit == player)) && (can_control(player, where) || (where->flags & CAN_TEL) ||
				(where == victim->linkto))) {
				if(victim->location == where) {
					notify(player, pstr("* %s is already at that location!", color_name(victim, -1)));
					return;
				}
			//notify(player, pstr("[TELEPORT]: Teleporting %s to %s",
			//	color_name(victim, -1), color_name(where, -1)));
			move_to(victim, where);
			//notify(victim->ownit, pstr("[TELEPORT]: %s moved %s to %s",
			//	(victim->ownit == player) ? "You":color_name(player, -1), 
			//		(victim->ownit == victim) ? "you": color_name(victim, -1),
			//			color_name(where, -1)));
		} else {
			error_report(player, M_ENOCNTRL);
		}
	}
}

// Return a string of flags. Like "PWc" (Player Wizard connect)
char *flaglist(player)
	object *player;
{
	char buf[MAX_BUFSIZE];
	char *p = buf;
	int ctr;
	
	for(ctr = 0; flag_table[ctr].name; ctr++) 
		if( (player->flags & flag_table[ctr].flag) && (player->flags & flag_table[ctr].type))
			if(!(flag_table[ctr].flag & SUSPECT))
				*p++ = flag_table[ctr].parsed;	
	
	*p++ = '\0';
	return stralloc(buf);
}

char *aflaglist(aflags)
	int aflags;
{
	char buf[MAX_BUFSIZE];
	char *p = buf;
	int ctr;
	
	for(ctr = 0; aflag_table[ctr].name; ctr++)
		if( (aflags & aflag_table[ctr].flag) )
			*p++ = aflag_table[ctr].parsed;
	
	*p++ = '\0';
	return stralloc(buf);
}

void do_take(player, str)
	object *player;
	char *str;
{
	object *thing;
	
	IS_ARGUMENT(player, (str));
	
	if(!(thing = match_moveable(str))) 
		if(!(thing = match_partial(player->location, str))) {
			error_report(player, M_EILLNAME);
			return;
		}
	
	if(thing->location != player->location) {
		error_report(player, M_EREMOTE);
		return;
	}
	
	take_trigger(player, thing);
}

void do_drop(player, str)
	object *player;
	char *str;
{
	object *thing;

	IS_ARGUMENT(player, (str));
	
	if(!(thing = match_moveable(str))) 	
		if(!(thing = match_partial(player, str))) {
			error_report(player, M_EILLNAME);
			return;
		}
	
	if(thing->location != player) {
		error_report(player, M_ENOTHELD);
		return;
	}
	
	move_to(thing, player->location);
	notify(player, pstr("[DROP]: You dropped %s...",
		color_name(thing, -1)));
}

char *unparse_object(victim)
	object *victim;
{
	char buf[MAX_BUFSIZE];
	
	sprintf(buf, "%s (#%d%s)", color_name(victim, -1), victim->ref, flaglist(victim));
	
	return stralloc(buf);
}

int change_ownership(player, victim, newvalue)
	object *player;
	object *victim;
	char *newvalue;
{
	object *newowner;
	char buf[MAX_BUFSIZE];
	
	if(!(newowner = match_player(newvalue)))
		return M_ENOPLAY;

	if(victim->flags & TYPE_PLAYER)
		return M_ENOCANDO;
	
	if(!can_control(player, newowner)) 
		return M_ENOCNTRL;
			
	victim->owner = newowner->ref;
	victim->ownit = newowner;
	strcpy(buf, color_name(newowner, -1));
	log_channel("log_sens", pstr("* Owner of %s changed to: %s",
		unparse_object(victim), color_name(newowner, -1)));
	notify(player, pstr("[BUILTIN]: Owner of %s changed to %s...",
		unparse_object(victim), buf));
		
	return 1;
}


int change_home(player, victim, newvalue)
	object *player;
	object *victim;
	char *newvalue;
{
	object *newlink;
	char buf[MAX_BUFSIZE];
	
	if(!(newlink = match_object(newvalue)))
		if(!(newlink = match_special(player, newvalue)))
			return M_ENOPLAY;
	
	if(newlink->flags & TYPE_EXIT)
		return M_EBADLNK;
		
	if(!can_control(player, newlink))
		return M_ENOCNTRL;
	
	victim->link = newlink->ref;
	victim->linkto = newlink;
	strcpy(buf, color_name(newlink, -1));
	notify(player, pstr("[BUILTIN]: %s of %s changed to %s...",
		(victim->flags & TYPE_PLAYER) ? "Home":"Link", unparse_object(victim), buf));

	return 1;	
}

void do_chownall(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim, *who, *o;
	
	if(!(victim = remote_match(player, arg1))) {
	  notify(player, "Who?");
	  return;
	}
	
	if(!(who = remote_match(player, arg2))) {
	  notify(player, "Who?");
	  return;
	}
	
	if(!(player->flags & TYPE_PLAYER)) {
	  log_channel("log_imp", pstr("%s tried to @chownall %s", 
	  	color_name(player, -1), color_name(victim, -1)));
	  return;
	}
	
	if(!can_control(player, who)) {
	  notify(player, "You can't force ownership upon them.");
	  return;
	}
	
	if(!can_control(player, victim)) {
	  notify(player, "You can't take things from them.");
	  return;
	}
	
	for(o = room_list; o; o=o->next)
	  if(o->ownit == victim) {
	    o->owner = who->ref;
	    o->ownit = who;
	  }
	
	for(o = thing_list; o; o=o->next)
	  if(o->ownit == victim) {
	    o->owner = who->ref;
	    o->ownit = who;
	  }	  
	
	for(o = exit_list; o; o=o->next)
	  if(o->ownit == victim) {
	    o->owner = who->ref;
	    o->ownit = who;
	  }
	  
	notify(player, pstr("All of %s's objects have been chowned to %s",
	  color_name(victim, -1), color_name(who, -1)));
}

