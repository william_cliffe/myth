/*****************************************************************************
 A simple play with encryption.
 Coded by sarukie (08/09/04)
 ------------------------------
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/*
	
*/
void vigenere_print(object *player, char *string, char *key)
{
	char encrypt[1024], keystring[1024];
	char buf[2048];
	char *e = encrypt, *p = keystring;
	char *f, *c, *r, v;

	
	for(*p = '\0', c = string, r = key; *c; c++, r++) {
	    *c = toupper(*c);
	    if(!*r)
	      r=key;
	    if(isupper(*c))
	      *p++ = toupper(*r);
	    else
	      r--;
	}
	*p++ = '\0';

	for(*e = '\0', c = string, f = keystring; *c; c++,f++) {
	  if(!isupper(*c)) {
	    f--;
	    *e++ = *c;
	    continue;
	  }
   	  v = *c + (*f - 'A');
	  if(v > 'Z')
	    *e++ = v - 26;
     	   else
	    *e++ = v;
	}
	*e++ = '\0';
	
	notify(player, pstr("Your string has been encrypted to: %s", encrypt)); 
}


// I know the key and I know the encrypted text, let's return the plain text.

void vigenere_decrypt(object *player, char *encrypted, char *key)
{
	char decrypt[1024], keystring[1024];
	char *e = decrypt, *k = keystring;
	char *r, *x, v;

	for(*k = '\0', r = encrypted, x = key; *r; r++, x++) {
	  if(!*x)
	    x = key;
	  if(isupper(*r))
	    *k++ = toupper(*x);
	  else
	    x--;
	}
	*k++ = '\0';
	
	for(*e = '\0', r = encrypted, x = keystring; *r; r++, x++) {
	  if(!isupper(*r)) {
	    x--;
	    *e++ = *r;
	    continue;
	  }
	  v = *r - (*x - 'A');
	  if(v < 'A')
	    *e++ = v + 26;
	  else
	    *e++ = v;
	}
	*e++ = '\0';

	notify(player, pstr("Your string has been decrypted to: %s", decrypt));
}
