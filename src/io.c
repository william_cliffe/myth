/****************************************************************************
 MYTH - (Input/Output, Basic engine)
 Coded by Saruk (01/10/99)
 ---
 This is the main game engine for MYTH. It was initially started January 10th, 
 1999 - though there were attempts made as far back as Late Spring/Early Summer
 of 1994.
 ****************************************************************************/
/** General Includes **/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>
#include <errno.h>
#include <dlfcn.h>

/** MYTH Includes **/
#include "db.h"
#include "net.h"
#include "externs.h"

// Local Definitions
#define DEF_MODE	0644
#define MAX_PID		65535
#define SEND		1
#define IS		0
#define MAX_COMMAND_LEN	4096

// Global variables
int myth_reboots	= 0;
int shutdown_flag	= 0;
int reboot_style	= 0;
int wizlock		= 0;
int sock		= 0;
int resock		= 0;
int maxd		= 0;
int reboot_flag		= 0;
int ndescriptors	= 0;
int num_mallocs		= 0;
int num_frees		= 0;
int MYTH_connected	= 0;
int ConnectRecord	= 0;

long total_input	= 0;
long total_output	= 0;
long myth_uptime	= 0;

time_t myth_reboot	= 0;
time_t now		= 0;

size_t tot_mem_act	= 0;

DESCRIPTOR *descriptor_list = 0;
DESCRIPTOR *descriptor_tail = 0;

#include "sigtable.h"
#include "ioheader.h"


void get_connects()
{
	FILE *fp;
	
	
	if(!(fp = fopen("msgs/record", "r"))) {
		ConnectRecord = MYTH_connected = 0;
		return;
	}
	
	ConnectRecord = getint(fp);
	fclose(fp);
}

void record_connects(state)
	int state;
{
	FILE *fp;
	
	if(state == FALSE) {
		MYTH_connected--;
		return;
	}
	
	MYTH_connected++;
	
	if(MYTH_connected < ConnectRecord)
		return;
	
	ConnectRecord = MYTH_connected;
		
	if(!(fp = fopen("msgs/record", "w"))) {
	  log_channel("log_imp", "Error saving to record file!");
	  return;
	}
	
	fprintf(fp, "%d\n", ConnectRecord);
	fclose(fp);
}

char *upgrade_flag[5]={
	0,     "TINY",      "MINOR",
	"MAJOR",    0
};

int main(argc, argv)
	int argc;
	char *argv[];
{
	FLIST *f;
	
	if(!load_config()) {
		printf("** ERROR ** Please create myth.cnf!\n");
		exit(1);
	}
	
	MYTH_connected = 0;
	get_connects();
	redirect_io();
	set_signals();
	
	printf("=========== log.it ============\n");
	printf("Start Time: %s\n", get_date(time(0)));
	
	myth_uptime = time(0);
	myth_reboot = time(0);
	myth_reboots = 0;

	setup_racetable();
	setup_classtable();
	build_cmdlist();
	open_sockets();
	load_database();
	ComDBLoad(return_config("comdb"));
	  
	attach_players();
	load_mail();
	//load_channel_database();
	set_args();
	//initialize_connection();
	
	report_online();
	init_startups(); // QUEUE all the A_STARTUP code.
	// set the global args.



	if(UPGRADE_FLAG > 0) 
	  log_channel("public", pstr("* %s version upgrade to %s v%d.%d.%d (Build: #%03d)",
	    upgrade_flag[UPGRADE_FLAG], SERVER_CODE, VER_MAJOR, VER_MINOR, VER_TINY, BUILD_NUM));

	//ComDBLoad(return_config("comdb"));
	LoadTemplates(return_config("item_template"));
	LoadItems(return_config("item_db"));
	
	/** Specialty Stuff **/
	
	do_game_state();

        UnloadPlugins();
	CleanUpCombat();
	close_sockets();
	if(reboot_flag != 5) {
	  save_database(return_config("db_name"));
	  save_mail(return_config("maildb"));
	  //save_channel_database(return_config("comdb"));
	  ComDBSave(return_config("comdb"));
	  SaveTemplates(return_config("item_template"));
	  SaveUserItems(return_config("item_db"));
	}
	
	fflush(stdout);	
	if (reboot_flag) {
		fcntl(sock, F_SETFD, 1);
	} else {
		close(sock);
	}

	if (reboot_flag) {
		execv(argv[0], argv);
		unlink("socket_table");
	}

#ifdef CATCHSEG
	unlink("panic_list");	
#endif /*CATCHSEG*/
	return 1; // we're outta here.
}

char *strip_ansi(s)
  const char *s;
{
  static char buf[MAX_BUFSIZE];
  char *p = buf;
  
  if(*s) {
    while(*s == ESC_CHAR) {
      skip_esccode(s);
    }
    
    while(*s) {
      *p++ = *s++;
      while(*s == ESC_CHAR) {
        skip_esccode(s);
      }
    }
  }
  
  *p = '\0';
  *p++ = 0;
  return(buf);
}

int process_output(d)
	DESCRIPTOR *d;
{
	TQUEUE *t;
	char buf[MAX_BUFSIZE];
	int colour_tag = 1, cnt = 0;
	int result;
	
    if(d->state == PASTE_MODE) {
	  d->output_flag = 0;
	  return 1;
	}
	
	if(d->player)
	  if(!(d->player->flags & ANSI)) 
	    colour_tag = 0;

	for(t = d->output_queue; t; t=t->next) {
	  strcpy(buf, (colour_tag) ? t->buffer : strip_ansi(t->buffer));
	  cnt = write(d->descriptor, buf, strlen(buf)); 
	  if(cnt < 0) {
	    if(errno == EWOULDBLOCK)
	      return 1;
	    return 0;
	  }
	}

	if( (d->output_lines >= MAX_OUTPUT) && d->player) {
	  strcpy(buf, "** OUTPUT FLUSHED **\r\n");
	  result = write(d->descriptor, buf, strlen(buf));
	}	
			
	d->output_queue = TQUEUE_FREE(d->output_queue);
	d->output_lines = 0;
	d->output_flag = 0;	
	
	return 1;
}

/** CheckDescriptors():
**/
void CheckDescriptors(void)
{
  DESCRIPTOR *list, *lnext = NULL;
  struct stat sbuf;
  
  for(list =descriptor_list; list; list=lnext) {
    lnext = list->next;
    if(!fstat(list->descriptor, &sbuf)) {
      DeleteDescriptor(list);
    }
  }
}


void do_game_state(void)
{
	int errval, ready;
	int available_descriptors;
	fd_set input_set, output_set;
	struct timeval timeout;
	struct descriptor_data *d, *newd, *dnext = NULL;

	sock = make_socket(atoi(return_config("port")));
	if(maxd <= sock)
		maxd = sock + 1;
	
	available_descriptors = getdtablesize() - 5;

	while(!shutdown_flag) {
	  process_commands();
	  clear_stack();
	  if(shutdown_flag)
	    break;
		
	  do_timer();
	  func_zerolev();
			
	  for(d=descriptor_list; d; d=d->next) 
	    if(d->output_flag)
	      process_output(d); 
		
	  FD_ZERO(&input_set);
	  FD_ZERO(&output_set);
		
	  if(sock > -1)
	    FD_SET(sock, &input_set);

          /**
          if(hub_desc != -1) {
            if(!FD_ISSET(hub_desc, &input_set))
              FD_SET(hub_desc, &input_set);	
	    else
	      FD_SET(hub_desc, &output_set);
	  }
	  **/
	  
	  for(d=descriptor_list; d; d=d->next)
	    if(d->output_flag)
	      FD_SET(d->descriptor, &output_set);
	    else if (!d->input_flag)
	      FD_SET(d->descriptor, &input_set);
		
		
	  for(d=descriptor_list; d; d=d->next)
	    if(FD_ISSET(d->descriptor, &output_set))
	      if(d->output_flag)
	        process_output(d); 
	  
	  plugin_readset(&input_set, &maxd);
	  plugin_writeset(&output_set, &maxd);
	
	  if(queue) {
	    timeout.tv_sec = 0;
	    timeout.tv_usec = 1;
	  } else {
	    timeout.tv_sec = 1;
	    timeout.tv_usec = 1;
	  }
	  
	  ready = select(maxd, &input_set, &output_set, NULL, &timeout);

	  if( ready <= 0 ) {
	    if(ready < 0) {
              if(errno == EBADF) {
                log_channel("log_error", pstr("We're assuming a bad file descriptor..."));
                // We have a nasty bad file descriptor here. Let's fuck whatever descriptor caused it right off our list.
                CheckDescriptors();
              }
            }
	    continue;
	  } else {
	    if(FD_ISSET(sock, &input_set)) {
	      if(!(newd = new_connection(sock))) {
	        printf("<<ERROR>> new_connection();\n");
	      } else {
	        if(newd->descriptor >= maxd)
	          maxd = newd->descriptor + 1;
	      }
	    }
	  }

	  for(d=descriptor_list; d; d=dnext) {
	    dnext = d->next;
	    if(FD_ISSET(d->descriptor, &input_set)) {
	      if (!d->input_flag) {
	        if(!process_input(d)) {
	          DeleteDescriptor(d);
		  //dfree(d->descriptor);
		}
	      }
	    }
	  }
	  
	  for(d=descriptor_list; d; d=dnext) {
	    dnext = d->next;
	    if(FD_ISSET(d->descriptor, &output_set)) {
	      if(d->output_flag) {
	        if(!process_output(d)) 
	          DeleteDescriptor(d);
	          //dfree(d->descriptor);
	      }
	    }
	  }
	 
	  /**
	  if((hub_desc > -1) && FD_ISSET(hub_desc, &output_set))
	    hub_read();
	    
	  if((hub_desc > -1) && FD_ISSET(hub_desc, &input_set))
	    hub_read();
	  **/
          
	  plugin_selectchk(input_set, output_set);
		
	}
}

static int make_socket(port)
	int port;
{
	int s, opt;
	int errval;
	struct sockaddr_in6 server;
	int serversize = sizeof(server);
	
	s = socket(AF_INET6, SOCK_STREAM, 0);
	if (s < 0) {
		printf("error creating socket!\n");
		shutdown_flag = 1;
		return -1;
	}
	
	opt = 1;
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0) {
		printf("error in setsockopt!\n");
		shutdown_flag = 1;
		close(s);
		return -1;
	}
	
	server.sin6_family = AF_INET6;
	server.sin6_addr = in6addr_any;
	server.sin6_port = htons(port);
	
	if ( (errval = bind(s, (struct sockaddr *)&server, sizeof(server))) !=0) {
		printf("bind(): %s\n", strerror(errval));
		resock = 1;
		close(s);
		return -1;
	}
	
	resock = 0;
	listen(s, 5);
	return s;
}


void close_sockets(void)
{
	DESCRIPTOR *d;
	FILE *f;
	char buf[MAX_BUFSIZE];
	int result;

	if(!reboot_flag || sock < 0)
		return;

#ifdef CATCHSEG
	/* Obviously rebooting properly, kill the panic_list. */
	unlink("panic_list");
#endif /*CATCHSEG*/
	
	unlink("socket_table");
	if (!(f = fopen("socket_table", "w")))
		return;
	
	fprintf(f, "%010d %010d %010ld\n", reboot_flag, myth_reboots, myth_uptime);
	fprintf(f, "%d\n", sock);
	fcntl(sock, F_SETFD, 0);
	for(d = descriptor_list; d; d=d->next) {
	   process_output(d);
	   if((d->state == CONNECTED||d->state == PASTE_MODE) && d->player && !d->internal) {
	     if(reboot_flag < 2) {
	       strcpy(buf, "Server Rebooting, please hold...\n");
	       result = write(d->descriptor, buf, strlen(buf));
	     } 
	     d->player->flags &= ~CONNECT;
	     fprintf(f, "%010d %010d %010ld %010ld %010d %010d\n", d->descriptor, d->player->ref,
	                  d->connected_at, d->last_time, d->term_width, d->term_height);
	     fcntl(d->descriptor, F_SETFD, 0);
	   } else {
	     if(reboot_flag < 2) {
	       strcpy(buf, "Server Rebooting, please try again...\n");
	       result = write(d->descriptor, buf, strlen(buf));
	     }
	     close(d->descriptor);
	   }
	}
	
	fclose(f);
}

void set_sockets(int amt)
{
	int s;
	
	for(s = 0; s < amt; s++)
		fcntl(s, F_SETFD, 1);
}

void resort_desclist()
{
	DESCRIPTOR *d, *dtmp, *dnext;
	
	dtmp = descriptor_list;
	descriptor_list = NULL;
	
	for(d = dtmp; d; d=dnext) {
		dnext = d->next;
		d->next = descriptor_list;
		descriptor_list = d;
	}
}
		
void open_sockets()
{
	DESCRIPTOR *d;
	FILE *f;
	char buf[MAX_BUFSIZE];

#ifdef CATCHSEG	
	if( (f = fopen("panic_list", "r")) ) {
		unlink("panic_list");	// we must have segfaulted, restore the descriptor_list.
	} else {
#endif /*CATCHSEG*/
		if(!(f = fopen("socket_table", "r")))
			return;	// Clean boot up; refresh everything.
		
		unlink("socket_table");	// Proper reboot, load us up.
#ifdef CATCHSEG
	}
#endif /*CATCHSEG*/

	/* Grab the first bunch of data. */
	if(fgets(buf, sizeof(buf), f)==NULL) {
		return;
	}
	
	reboot_style = atoi(buf);	// get the first integer.
	myth_reboots = atoi(buf + 11) + 1;	// get number of reboots.
	myth_uptime = atoi(buf + 22);	// get the myth uptime.	
	myth_reboot = time(0);		// reset the myth reboot time.
	
	/* Grab the second bunch of data - namely the socket number */
	if(fgets(buf, sizeof(buf), f)==NULL) { return; }
	
	sock = atoi(buf);		// Get this socket number.
	fcntl(sock, F_SETFD, 1);	// reset the socket flag.
	close(sock);			// close it up.
	
	set_sockets(1000);		// loop and set (X) number of sockets.

	/* Get the remaining data in its chunks and parse them. */
	while( fgets(buf, sizeof(buf), f) ) {
		int desc = atoi(buf);
		struct sockaddr_in6 in;
		int namelen = sizeof(in);
		
		fcntl(desc, F_SETFD, 1);
		getpeername(desc, (struct sockaddr *)&in, &namelen);
		
		{
			char buf[INET6_ADDRSTRLEN];
			
			inet_ntop(AF_INET6, &in.sin6_addr, buf, sizeof(buf));
			d = initializesock(desc, &in, buf, RELOADCONNECT);
		}
		
		// get the player reference.
		d->ref = atoi(buf+11);
		
		// get the connected at and last command time.
		d->connected_at = atoi(buf+22);
		d->last_time = atoi(buf+33);
		d->term_width = atoi(buf+44);
		d->term_height = atoi(buf+55);
		if(reboot_style == 4)
			wizlock = 1;
		else
			wizlock = 0;
		record_connects(TRUE);
		
		AppendDescriptor(d);
	}				
	
	fclose(f);
	
#ifdef CATCHSEG
	panic_list();
#endif /*CATCHSEG*/
}

struct descriptor_data *new_connection	(sock)
	int sock;
{
	DESCRIPTOR *d;
	int newsock, sdconn = -1;
	struct sockaddr_in6 addr;
	int addr_len = sizeof(addr);
	char buff[INET6_ADDRSTRLEN];

	newsock = accept(sock, (struct sockaddr *)&addr, &addr_len);
	if (newsock < 0) {
	  printf("ERROR: %s\n", strerror(errno));
	  return NULL;
	} else {
        getpeername(newsock, (struct sockaddr *)&addr, &addr_len);
        inet_ntop(AF_INET6, &addr.sin6_addr, buff, sizeof(buff));
        log_channel("log_io", pstr("Connected: %s", buff));
	    //strcpy(buff, inet_ntoa(addr.sin6_addr));
	}
	
	d = initializesock(newsock, &addr, buff, WAITCONNECT);
	AppendDescriptor(d);
	
	return d;
}


void dwrite(d, buf) 
	DESCRIPTOR *d;
	char *buf;
{
	if(!buf || !*buf)
	  return;

	total_output += strlen(buf);
	if(!d) {
		printf("PANIC ERROR: Trying to write to NULL descriptor!\n");
		return;
	}
	if (d->output_lines >= MAX_OUTPUT)
		return;

        d->output_queue = add_to_tqueue(d->output_queue, buf);
	
	d->output_lines++;
	d->output_flag = 1;
}

#ifdef CATCHSEG
/* Dump the panic list on connection */
void panic_list()
{
	DESCRIPTOR *d;
	FILE *f;
	
	f = fopen("panic_list", "w");
	
	reboot_flag = 3;
	fprintf(f, "%010d %010d %010ld\n", reboot_flag, myth_reboots, myth_uptime);
	reboot_flag = 0;
        fprintf(f, "%d\n", sock);
	for(d = descriptor_list; d; d=d->next)
		if(d->state == CONNECTED && d->player)
			fprintf(f, "%010d %010d %010ld %010ld\n", d->descriptor, d->player->ref, d->connected_at, d->last_time);

	fclose(f);
}
#endif /*CATCHSEG*/


/** AppendDescriptor():
**/
void AppendDescriptor(DESCRIPTOR *d)
{
  DESCRIPTOR *list = NULL;
  
  if(!descriptor_list) {
    d->prev = d->next = NULL;
    descriptor_list = descriptor_tail = d;
  } else {
    for(list = descriptor_list; list; list=list->next)
      if(list->connected_at < d->connected_at)
        break;

    if(list) {
      if(list->prev)
        list->prev->next = d;
      
      d->prev = list->prev;
      d->next = list;
      list->prev = d;
      if(descriptor_list == list)
        descriptor_list = d;  
    } else {
      d->next = NULL;
      d->prev = descriptor_tail;
      descriptor_tail->next = d;
      descriptor_tail = d;
    }
  }
}

struct descriptor_data *initializesock(s, a, addr, state)
	int s;
	struct sockaddr_in6 *a;
	char *addr;
	int state;
{
	struct descriptor_data *d;
	char buf[MAX_BUFSIZE];
	char query_ttype[]={ IAC, DO, TELOPT_TTYPE, '\0'};
	
	GENERATE(d, DESCRIPTOR);
    log_channel("log_debug", pstr("Adding descriptor...%s", addr));
	d->descriptor = s;
	//make_nonblocking(s);

	/** Set flags **/
	d->active = 0;
	d->ref = 0;
	d->state = state;
	d->internal = SET_DONE;

	d->input_flag = 0;
	d->paste_flag = 0;
	d->output_flag = 0;
	d->output_lines = 0;
	d->paste_lines = 0;
	d->term_width = 0;
	d->term_height = 0;
	
	// text queues.
	d->input_queue = NULL;
	d->paste_queue = NULL;
	d->output_queue = NULL; 
	d->raw_input = 0;
	d->raw_input_at = 0;
	
	d->paste_cmd = NULL;

	// time values
	d->connected_at = time(&now);
	d->last_time = time(&now);
	
	// address stuff.
	if(a)
	  d->address = *a;
	if(addr)
	  strncpy(d->addr, addr, INET6_ADDRSTRLEN);
	
	fcntl(s, F_SETFL, O_NONBLOCK);
	// IAC DO TELOPT_TTYPE?
	//dwrite(d, query_ttype);
	
	// figure out output on state
	switch(d->state) {
	  case RELOADCONNECT:
	    d->state = CONNECTED;
	    break;		
	  case WAITCONNECT:
	    log_channel("log_io", pstr("* (ESTABLISHED LINK) [%d] from %s at %s",
	      d->descriptor, d->addr, get_date(d->connected_at)));
	    sprintf(buf, "[%s] user@%s (Build: %d)\r\n",
	      return_config("myth_name"), addr, BUILD_NUM);
	    dwrite(d, buf);
	    show_welcome(d);
	    break;
	  default:
	    break;
	}	
	
	// 0 out the player.
	d->player = NULL;
	
	// add to descriptor records stuffies
	ndescriptors++;
	if(d->descriptor >= maxd)
	  maxd = d->descriptor+1;
	
	return d;
}

void make_nonblocking(s)
	int s;
{
	if(fcntl(s, F_SETFL, FNDELAY) == -1) {
  	  printf("error in nonblocking!\n");
	}
}

TQUEUE *add_to_tqueue(tqueue, buffer) 
	TQUEUE *tqueue;
	char *buffer;
{
	TQUEUE *t, *tnew, *tprev = NULL;
	
	if(!*buffer)
	  buffer = "\r";
	
	if(tqueue) {
		for(t = tqueue; t; t=t->next)
			tprev = t;
	}
		
	GENERATE(tnew, TQUEUE);
	tnew->buffer = NULL;

	SET(tnew->buffer, buffer);
	tnew->next = NULL;
		
	if(!tprev) 
		tqueue = tnew;
	else 
		tprev->next = tnew;
	
	return tqueue;
}

// Parse and Interpret IAC strings
void do_iac_parse(DESCRIPTOR *d, unsigned char *str)
{
  // IAC has already been parsed out.
  switch(*str) {
    case WILL:
      str++;
      switch(*str) {
        case TELOPT_TTYPE:
          // they'll return terminal type, let's request it.
          dwrite(d, pstr("%c%c%c%c%c%c", IAC, SB, TELOPT_TTYPE, SEND, IAC, SE));
          break;
        case TELOPT_LINEMODE:
          DEBUG("Client will line mode");
          break;
        default:
          break;
      }
      break;
    case WONT:
      str++;
      switch(*str) {
        case TELOPT_TTYPE:
          break;
        case TELOPT_NAWS:
          DEBUG("Client won't NAWS!");
          break;
        default:
          break;
      }
      break;
    case SB:
      str++;
      switch(*str) {
        case TELOPT_TTYPE:
          str++;
          switch(*str) {
            case IS:
              str++;
              DEBUG(pstr("Terminal Type: %d", (unsigned int)*str));
              break;
            default:
              break;
          }
          break;
        case TELOPT_NAWS: // Now we're dealing with width[1] width[0] height[1] height[0]
          str++; // save this data in d->term_width, d->term_height
          d->term_width = (str[0]*256) + str[1];
          d->term_height = (str[2]*256) + str[3];
          //DEBUG(pstr("Setting terminal width + height tp %dX%d", d->term_width, d->term_height));
          break;
        default:
          break;
      }
    defaualt:
      break;
  }
}


/**
 * Partly ripped from PennMUSH bsd.c to help our input not have to restrict buffers
 * so much. I liked the way they handled inputs better.
 */
static void process_input_helper(DESCRIPTOR *d, char *tbuf1, int got)
{
  unsigned char *p, *pend, *q, *qend;
 
  if(!d->raw_input) {
    d->raw_input = stack_em(sizeof(char) * MAX_COMMAND_LEN + 1, SOLID);
    if(!d->raw_input)
      printf("* ERROR * Out of memory!\n");
    d->raw_input_at = d->raw_input;
  }
  p = d->raw_input_at;
  d->input_chars += got;
  pend = d->raw_input + MAX_COMMAND_LEN - 1;
  for(q = (unsigned char *) tbuf1, qend = (unsigned char *)tbuf1 + got; 
      q < qend; q++)
  {
    if (*q == '\r') {
      /* A broken client (read: WinXP telnet) might send only CR, and not CRLF
       * so it's nice of us to try to handle this.
       */
      *p = '\0';
      if (p > d->raw_input)
        d->input_queue = add_to_tqueue(d->input_queue, d->raw_input);
      p = d->raw_input;
      if (((q + 1) < qend) && (*(q + 1) == '\n'))
        q++;                    /* For clients that work */
    } else if (*q == '\n') {
      *p = '\0';
      if (p > d->raw_input)
        d->input_queue = add_to_tqueue(d->input_queue, d->raw_input);
      p = d->raw_input;
    } else if (*q == '\b') {
      if (p > d->raw_input)
        p--;
    } else if ((unsigned char) *q == IAC) {     // Telnet option foo
      do_iac_parse(d, q+1);
      break;
      /*if (q >= qend)
        break;
      q++;
      if (!TELNET_ABLE(d) || handle_telnet(d, &q, qend) == 0) {
        if (p < pend && isprint(*q))
          *p++ = *q;
      }
      */
    } else if (p < pend && ( (isprint(*q) && isascii(*q))||(*q>32)) ) {
      *p++ = *q;
    }
  }
  if(p > d->raw_input) {
    d->raw_input_at = p;
  } else {
    block_free(d->raw_input);
    d->raw_input = 0;
    d->raw_input_at = 0;
  }

}

int process_input(d)
  DESCRIPTOR *d;
{
  char buf[MAX_COMMAND_LEN * 2];
  int got = 0; 
  
  errno = 0;
  
  got = recv(d->descriptor, buf, sizeof buf, 0);

  if( got <= 0) {
#ifdef EAGAIN
    if ((errno == EWOULDBLOCK) || (errno == EAGAIN) || (errno == EINTR))
#else
    if ((errno == EWOULDBLOCK) || (errno == EINTR))
#endif
      return 1;
    else
     return 0;
  }
  
  process_input_helper(d, buf, got);
  d->input_flag = 1;
  
  return 1;
}

int old_process_input(d)
	DESCRIPTOR *d;
{
	unsigned char buf[8192], buf2[8192];
	unsigned char *p, *i;
	int got = 0, get = MAX_COMMAND_LEN;
	int finished, inp_size;
	
	// check for RAW input waiting.
	if(d->raw_input) {
	  get -= d->raw_input_len;
	  if(get <= 0) {
	    sprintf(buf, "%s\n", d->raw_input);
	    block_free(d->raw_input);
	    d->raw_input = NULL;
	    d->raw_input_len = 0;
	  }
	}
	
	if(get > 0) {
	  //if( (got = read(d->descriptor, buf, get)) < 0)
	  if ( (got = recv(d->descriptor, buf, sizeof buf, 0)) < 0)
	    return( ( (errno == EAGAIN)||(errno=EINTR) ) ? 1:0);
	  
	  if(got == 0)
	    return 0;
	  
	  total_input += got;
	  
	  if(d->raw_input) {
	    memcpy(buf2, buf, got);
	    memcpy(buf, d->raw_input, d->raw_input_len);
            memcpy(buf + d->raw_input_len, buf2, got);
	    got += d->raw_input_len;
	    block_free(d->raw_input);
	    d->raw_input = NULL;
            d->raw_input_len = 0; 
	  }
	
	  buf[got] = '\0';
	}


	finished = 1;
	inp_size = 0;
	
	for(p = buf, i = buf2; p-buf < got;) {
	  if(*p == '\r') {
	    if(finished || *(p+1) == '\n') {
	      p++;
	      continue;
	    }
	   if(*p == '\n' && *(p+1) == '\n') {
	     p++;
	     continue;
	   }
	    //*p = '\n';
	  }
	  
	  finished = 0;	  
	
	  if(isprint(*p)) {
	    *i++ = *p++;
	    inp_size++;
	    continue;
	  }
	  
	  // This is not as sophisticated as we could get.
	  // Perhaps later.
	  if(*p == IAC) {
	    do_iac_parse(d, p+1);
	    return 1;
	  }
	  
	  if(*p != '\n') {
	    p++;
	    continue;
	  }
	  
	  *i = '\0';
	  
	  finished = 1;
	  d->input_queue = add_to_tqueue(d->input_queue, buf2);
	  
	  i = buf2;
	  inp_size = 0;
	  p++;
	}

	if(!finished) {
	  *i = '\0';
	  d->raw_input = stack_em(inp_size + 1, SOLID);
	  memcpy(d->raw_input, buf2, inp_size);
	  d->raw_input[inp_size] = '\0';
	  d->raw_input_len = inp_size;
	}


	
	d->input_flag = 1;		

	return 1;
}


	
void connect_player(d)
	DESCRIPTOR *d;
{
	record_connects(TRUE);
	
	d->state = CONNECTED;
	// IAC DO TELOPT_NAWS?
	dwrite(d, pstr("%c%c%c%c", IAC, DO, TELOPT_NAWS, '\0'));
	d->player->flags |= CONNECT;
#ifdef CATCHSEG
	panic_list();
#endif /*CATCHSEG*/
	log_channel("connect", pstr("* (CONNECT) %s",
	  color_name(d->player, -1)));
	log_channel("log_io", pstr("* (CONNECT) [%d] %s from %s on %s",
	  d->descriptor, color_name(d->player, -1), d->addr, get_date(d->connected_at)));
	__announce_mail(d->player);


	// Show them the new commands.
	dwrite(d, "--- COMMANDS TO REMEMBER ---\n");
	dwrite(d, "+ansi (Toggles your ANSI colour on and off)\n");
	dwrite(d, "+mutype (Toggles whether you use MUD or MUSH say shortcut)\n");
	dwrite(d, "----------------------------\n");
	
	filter_command(d->player, "look", 1);
	filter_command(d->player, "+ch/join public", 1);
	filter_command(d->player, "+ch/default public", 1);
	/* Show whatever file you want, probably MOTD */
	display_file(d, "msgs/motd");

	notify(d->player, pstr("\r\nYou have connected from...: %s",d->addr));
	notify_in(d->player->location, NULL, pstr("%s has connected!", color_name(d->player, -1)));
	SET_ATR(d->player, A_LASTON, get_date(time(0)));
}

void parse_password(d, buffer)
	DESCRIPTOR *d;
	char *buffer;
{
	
	if(!dcrypt(d->player->pass, buffer)) {
		echo_on(d, "");
			connect_player(d);
	} else {
		echo_on(d, "");
		dwrite(d, "Sorry, that password is invalid.\r\n");
		d->state = WAITCONNECT;
		d->player = NULL;
	}
}


// Simple display user list...
void display_users(who)
	DESCRIPTOR *who;
{
	DESCRIPTOR *d;


	dwrite(who, pstr("%-16.16s %-10.10s %-10.10s %-8.8s %-28.28s\r\n",
		"NAME", "ALIAS", "ONFOR", "IDLE", "DOING"));	
	for(d = descriptor_list; d; d=d->next)
	  if(d->state == CONNECTED || d->state == PASTE_MODE)
	    if( !(d->player->flags & INVISIBLE) )
	      dwrite(who, pstr("%s%-16.16s %-10.10s %-10.10s %-8.8s %-28.28s\r\n", ((d->state == PASTE_MODE)) ? "*":"",
	        d->player->name, strip_color(atr_get(d->player, A_ALIAS)), 
		  time_format(time(0) - d->connected_at), count_idle(time(0) - d->last_time),
		    strip_color(atr_get(d->player, A_DOING))));
	dwrite(who, "---\r\n");
}

void process_commands(void)
{
	DESCRIPTOR *d, *dnext = NULL;
	TQUEUE *t;
	int flagit = 0;
	int result;
	
	for(d=descriptor_list; d; d=dnext) {
	  dnext = d->next;
	  if(d->state == BOOTITOFF) 
	    DeleteDescriptor(d);
	    //dfree(d->descriptor);
	  else if(d->input_flag) {
	    for(t = d->input_queue; t; t=t->next) {
	      if(!strcmpm(t->buffer, "QUIT"))
	        flagit = 1;
	      else if(d->state == CONNECTED) {
	        d->active = 1;
		do_command(d, t->buffer);
		d->active = 0;
	      } else if (d->state == PASTE_MODE) {
	        do_paste(d, t->buffer);
	      } else if (d->state == WAITCONNECT)
		parse_connect(d, t->buffer);
	      else if (d->state == WAITPASS)
	 	parse_password(d, t->buffer);
	    }
			
	    d->input_queue = TQUEUE_FREE(d->input_queue);
	    d->input_flag = 0;	
	    if(flagit) {
	      result = write(d->descriptor, "Come back soon!\n", 16);
	      DeleteDescriptor(d);
	      //dfree(d->descriptor);
	      flagit = 0;
	    }
	  }
	}
}

int create_guest(d)
	DESCRIPTOR *d;
{
	object *o = NULL;
	char guest[MAX_BUFSIZE];
	int ctr;
	
	if(MAX_GUESTS <= 0) 
		return 0;
		
	for(ctr = 0; ctr < MAX_GUESTS; ctr++) {
		sprintf(guest, "%s%d", GUEST_PREFIX, ctr);
		if(!(o = match_player(guest)))
			break;
	}
	
	if(o)
		return 0;
	
	o = create_player(rootobj, guest, guest);
	o->powlvl = 25;
	o->flags |= GUEST;
	d->player = o;
	d->state = CONNECTED;	
	filter_command(o, "+ch/join guest", 1);
	filter_command(o, "+ch/default guest", 1);
	do_class(rootobj, pstr("#%d", o->ref), class_to_name(CLASS_GUEST));
	o->powlvl = 10;
	sprintf(guest, "%c%d", *GUEST_PREFIX, ctr);
	SET_ATR(o, A_ALIAS, guest);
	connect_player(d);
	return 1;
}

// Connect a player!
void parse_connect(d, instr)
	DESCRIPTOR *d;
	char *instr;
{
	OBJECT *o;
	char *cmd, *name, *password;

	if(!d || !*instr)
		return;

	cmd = fparse(instr, ' '); // first argument
	name = rparse(instr, ' '); // restof arguments
	if(*(rparse(name, ' ')))
	  password = flip(fparse(flip(name), ' '));
	else
	  password = "";
	  
	if(*password)
	  name[strlen(name) - strlen(password) - 1] = '\0';
	
	if(string_prefix(cmd, "connect")) {
		if(!strcmpm(name, "guest")) {
		  if(!wizlock) {
			if(!create_guest(d)) {
				dwrite(d, "Sorry, all guest IDs are busy...\r\n");
				DeleteDescriptor(d);
				//dfree(d->descriptor);
				return;
			} 
			d->state = CONNECTED;
			return;
		  } else {
		  	dwrite(d, "This MYTH is currently wizlocked. Come back later!\r\n");
		  	//dfree(d->descriptor);
		  	DeleteDescriptor(d);
		  	return;
		  }
		} 
		
		if(!(o = match_player(name))) {
			dwrite(d, "Sorry, no such player exists.\r\n");
			d->state = WAITCONNECT;
			d->player = 0;
			return;
		}
		
		d->player = o;

		if(!*password) {
			echo_off(d, "Password: ");
			d->state = WAITPASS;
			return;
		}
		
		parse_password(d, password);
		return;
	} else if (string_prefix(cmd, "create")) {
		if(!ENABLE_NEW) {
			dwrite(d, "Character creation has been disabled. Please connect as a guest.\r\n");
			return;
		}
		if(!*name) {
			dwrite(d, "You want to create what?\r\n");
			return;
		}
		if(!*password || (!strcmpm(password, " "))) {
			dwrite(d, "Please try giving it a password.\r\n");
			return;
		}
		if(!(o = create_player(rootobj, name, password))) {
		  dwrite(d, "I am sorry, you cannot create that player.\r\n");
		  return;
		}
		dwrite(d, pstr("Your character %s has been created with password '%s'.\r\n",
			name, password));
		do_class(rootobj, pstr("#%d", o->ref), class_to_name(CLASS_CITIZEN));
		filter_command(o, "+ch/join public", 0);
		d->state = CONNECTED;
		d->player = o;
		connect_player(d);
		return;
	} else if (!strcmp(cmd, "WHO")) {
		display_users(d);
		return;
	}
	
	dwrite(d, "I'm sorry, I don't understand that command.\r\n");
}

void do_command(d, instr)
	DESCRIPTOR *d;
	char *instr;
{
	MAIL *m;
	
	while(*instr && isspace(*instr))
	  instr++;
	  
	if(!instr || !*instr)
	  return;

	if(!strcmp(instr, "WHO")) {
	  display_users(d);
	  return;
	}
	
	if( (d->player) && ((time(0) - d->last_time) >= NOTIFY_IDLE)  ) {
	  if(!(d->player->flags & INVISIBLE) )
	    log_channel("log_io", pstr("* %s just unidled after %s",
	      color_name(d->player, -1), time_format2(time(0)-d->last_time))); 
	   for(m = d->player->mail; m; m=m->next)
	     if(m->flags & MAIL_NEW)
	       notify(d->player, "* You have unread +mail!");
	}

	d->last_time = time(0);
	filter_command(d->player, instr, 1);
}

// delete an entire queue.
// we want to go through and clear every thing from the queue.
TQUEUE *TQUEUE_FREE(tqueue)
	TQUEUE *tqueue;
{
	TQUEUE *t, *tnext = NULL;
	
	for(t = tqueue; t; t=tnext) {	
		tnext = t->next;
		if(!t->buffer || !*t->buffer) continue;
		
		// clear the buffer
		if(t->buffer)
			block_free(t->buffer);
		if(t)
			block_free(t);
	}
	
	return (NULL);
}

/** DeleteDescriptor():
**/
void DeleteDescriptor(DESCRIPTOR *d)
{
  DESCRIPTOR *list;

  // Get rid of all previous output
  process_output(d);
  
  // Drop their connection.
  close(d->descriptor); 
  
  if(d->prev)
    d->prev->next = d->next;
  
  if(d->next)
    d->next->prev = d->prev;
    
  if( descriptor_tail == d )
    descriptor_tail = d->prev;
  
  if( descriptor_list == d)
    descriptor_list = d->next;
  
  // We've removed the descriptor from its list. Now we can do what's needed.
  log_channel("log_io", pstr("* (Descriptor: %d) %s has disconnected on %s", 
    d->descriptor, (d->player) ? color_name(d->player, -1) : "UNKNOWN", get_date(time(0))));
  
  if(d->player)
    d->player->flags &= ~CONNECT;
    
  // Peruse the list to make sure there's no duplicate player.
  // If there is, reset the CONNECT flag.
  for(list = descriptor_list; list; list=list->next)
    if( (list->state == CONNECTED || list->state == PASTE_MODE) ) {
      if(list->player && (list->player == d->player))
        d->player->flags |= CONNECT;  
    }
  
  if(d->player) {
    SET_ATR(d->player, A_LASTOFF, get_date(time(0)));
    log_channel("connect", pstr("* %s has disconnected.", color_name(d->player, -1)));
    notify_in(d->player->location, NULL, pstr("%s has disconnected!", color_name(d->player, -1)));
    record_connects(FALSE);
    
    if((d->player->flags & GUEST))
      guest_precycle(rootobj, d->player);
  }
 
  block_free(d->paste_cmd);
  
  if(d->paste_queue)
    d->paste_queue = TQUEUE_FREE(d->paste_queue);
  
  block_free(d);
  
#ifdef CATCHSEG
  panic_list();
#endif  
}

// delete a descriptor.
void dfree(ddel)
	int ddel;
{
	DESCRIPTOR *d, *dprev=NULL;	
	int catchref=-1, prevstate = -1;
	
	for(d=descriptor_list; d; d=d->next) {
		if(d->descriptor == ddel)
			break;
		dprev = d;
	}
	
	if(!d)
		return;
	
	process_output(d);
	close(d->descriptor);
	
	if(!dprev)
		descriptor_list = d->next;
	else
		dprev->next = d->next;
	
	prevstate = d->state;
	// check to see if player is connected on another descriptor.
	if(d->player) {
		catchref = d->player->ref;
		d->player->flags &= ~CONNECT;
		
	}
	
	log_channel("log_io", pstr("* (DISCONNECT) [%d] %s disconnected on %s",
		d->descriptor, (d->player) ? color_name(d->player, -1) : "UNKNOWN", get_date(time(0))));
	
	if(d->player) {
		SET_ATR(d->player, A_LASTOFF, get_date(time(0)));
		log_channel("connect", pstr("* %s has disconnected.", color_name(d->player, -1)));
		notify_in(d->player->location, NULL, pstr("%s has disconnected!", color_name(d->player, -1)));
		record_connects(FALSE);		
	}
	
	// if this player is flagged GUEST, get rid of it.
	if(d->player) {
		if(d->player->flags & GUEST) {
			guest_precycle(rootobj, d->player);
			d->player = 0;
		}
	}
	
	if(d->paste_cmd)
		block_free(d->paste_cmd);
	
	if(d->paste_queue)
	  d->paste_queue = TQUEUE_FREE(d->paste_queue);
			
	block_free(d);
	
	d = NULL;
		
	// check to see if the player is connected elsewhere.
	for(d=descriptor_list; d; d=d->next) {
		if(d->state == CONNECTED||d->state == PASTE_MODE) 
			if(catchref == d->player->ref)
				d->player->flags |= CONNECT;
	}
	
#ifdef CATCHSEG
	panic_list();
#endif /*CATCHSEG*/
}

static void sig_report(int t_sig)
{
	int i;
	
	for(i = 0; sig_table[i].retstr; i++)
		if(sig_table[i].t_sig == t_sig)
			printf("Exit due to signal: %s\n", sig_table[i].retstr);

	printf("Exiting due to signal: %d\n", t_sig);
	exit(t_sig);			
}

#ifdef CATCHSEG
void panic_boot()
{
	FILE *f;
	int scramble = 0;
	char buf[MAX_BUFSIZE];
	char *argv[] = {"myth", NULL};
	
	printf("** PANIC **\n");
	printf("We have crashed! Forking a new process (25 second wait)\n");
	/* TODO:
		Write some code or a watchdog that will not have to use potentially
		fubared code to reboot.
		
		As for now, protect against this by not saving the descriptor_list.
		My idea would be to do an actual descriptor_list dump on every log
		in; and if that foobared it would just reboot and use that descriptor
		list. But if there was no problem, then on a real reboot it would
		use a 'socket_table' dump and delete the other descriptor list dump
		
		** And if it crashed dumping that descriptor_list, at least it would
		still reboot the server.
	*/
	
	/* at least try and keep the socket open */
	fcntl(sock, F_SETFD, 1);
	if( (f = fopen("panic_list", "r")) ) {
		scramble= getint(f);
		scramble = getint(f);
		while(fgets(buf, sizeof(buf), f))
			fcntl(atoi(buf), F_SETFD, 0);
	}
	fclose(f);

	sleep(10);
		
	if(fork()==0) {
		signal(SIGSEGV, SIG_DFL);
		kill(getpid(), 11);
		exit(0);
	}
	
	generate_stack();	
	execv("myth", argv);
	unlink("socket_table");	
}
#endif /*CATCHSEG*/

static void set_signals(void)
{
	signal(SIGPIPE, SIG_IGN);
	signal(SIGCHLD, SIG_IGN);
	signal(SIGQUIT, sig_report);
	signal(SIGHUP, SIG_IGN);
	signal(SIGILL, sig_report);
#ifdef CATCHSEG
	signal(SIGSEGV, panic_boot);
	signal(SIGFPE, panic_boot);
#endif
	signal(SIGKILL, sig_report);
	signal(SIGSTOP, sig_report);
	signal(SIGTSTP, sig_report);
}

void redirect_io(void)
{
	int fp;
	
	close(fileno(stdin));
	
	fp = open("log.it", O_WRONLY | O_CREAT | O_APPEND, DEF_MODE);
	close(fileno(stdout));
	
	dup2(fp, fileno(stdout));
	setvbuf(stdout, NULL, _IOLBF, 0);
	
	close(fp);
}

void echo_off(DESCRIPTOR *d, char *string)
{
	char buf[MAX_BUFSIZE];
	char echo_off_str[]={IAC,WILL,TELOPT_ECHO,'\0'};
	
	dwrite(d, echo_off_str);	
	sprintf(buf, "%s", string);
	dwrite(d, buf);
}

void echo_on(DESCRIPTOR *d, char *string)
{
	char buf[MAX_BUFSIZE];
	char echo_on_str[]={IAC,WONT,TELOPT_ECHO,'\0'};
	
	dwrite(d, echo_on_str);

	sprintf(buf, "%s", string);
	dwrite(d, buf);
}

void shutdownsock(player, arg1)
	dbref player;
	char *arg1;
{
	int descriptor;
	
	if ( (descriptor = atoi(arg1)) < 1)
		return;

		
}

void boot_off(player)
	dbref player;
{
	DESCRIPTOR *d, *dnext = NULL;
	
	for(d=descriptor_list; d; d=dnext) {	
		dnext = d->next;
		if(d->state == CONNECTED || d->state == PASTE_MODE || d->state == WAITPASS ) {
			if(d->player->ref == player) {
				//d->state = BOOTITOFF;
				DeleteDescriptor(d);
				//dfree(d->descriptor);
			}
		}
	}
}

	
// re-attach objects to decriptor_list[x]s.
void attach_players()
{
	DESCRIPTOR *d;
	object *o;

	if(!reboot_style)
		return;
			
	for(o=player_list; o; o=o->next)
		for(d = descriptor_list; d; d=d->next)
			if(d->ref == o->ref) {
				d->player = o;
				d->player->flags |= CONNECT;
#ifdef CATCHSEG
				panic_list();
#endif /*CATCHSEG*/
			}
}

void do_resock()
{
	sock = make_socket(atoi(return_config("port")));
}

void report_online()
{
	if(reboot_style == 0)
		return;
		
	if(reboot_style == 3) {
		reboot_style = 0;
		world_dwrite("[PANIC ERROR] * Illegal memory reference - server just rebooted.\r\n");
	} else if (reboot_style > 0) {
		world_dwrite("Server restart completed.\r\n");
	}	
	
	reboot_style = 0;
	log_channel("log_io", pstr("* %s resumes operation...",
		return_config("myth_name")));
}


/** END OF IO.C **/
