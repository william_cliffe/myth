/****************************************************************************
 Dynamic Library Loader
 Coded by sarukie (08/06/04)
 ---------------------------
 There are still a lot of functions to add, as well as some error checking and
 niceties.
 ****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <sys/stat.h>

#include "db.h"
#include "net.h"
#include "externs.h"

PHANDLE *phandle = NULL;
PHANDLE *phandletail = NULL;

void append_handle(char *cmd, char *name, void *handle)
{
	PHANDLE *pnew;
	
	GENERATE(pnew, PHANDLE);
	pnew->name = NULL;
	SET(pnew->name, name);
	pnew->cmd = NULL;
	SET(pnew->cmd, cmd);
	pnew->handle = handle;
	pnew->timestamp = time(0);
	pnew->freadset = dlsym(handle, "do_plugin_readset");
	pnew->fwriteset = dlsym(handle, "do_plugin_writeset");
	pnew->fselectchk = dlsym(handle, "do_plugin_selectchk");
	
	if(!phandle) {
	  pnew->next = NULL;
	  pnew->prev = NULL;
	  phandle = pnew;
	  phandletail = pnew;
	} else {
	  pnew->next = NULL;
	  pnew->prev = phandletail;
	  phandletail->next = pnew;
	  phandletail = pnew;
	}
}

void delete_handle(PHANDLE *p)
{
	if(p->prev)
	  p->prev->next = p->next;
	
	if(p->next)
	  p->next->prev = p->prev;
	
	if(phandle == p && p->next)
	  phandle = p->next;
	else if (phandle == p && !p->next)
	  phandle = NULL;
	
	if(phandletail == p && p->prev)
	  phandletail = p->prev;
	else if (phandletail == p && !p->prev)
	  phandletail = NULL;
	
	if(p->name)
	  block_free(p->name);
	dlclose(p->handle);
	if(p)
	  block_free(p);
}

int initiate_plugin_cmds(PHANDLE *handle)
{
  CL *c, *(*func)();
  int i;
  
  if( !(func = dlsym(handle, "additional_command")) ) {
    log_channel("log_err", "Error loading plugin.");
    return FALSE;
  }
	
  if(!dlerror()) {
    if( (c = func()) == NULL) {
      return TRUE;
    }

    for(i = 0; c[i].command; i++)
      build_hashcmd(&c[i], handle);
  } else
    return FALSE;

  return TRUE;
}

// Eventually we have to build a plugin holder so we can dlclose via handles.
void load_plugin(object *player, char *text)
{
        PHANDLE *p;
	char directory[1024];
	void *handle;
        
        if(!has_pow(player, player, POW_SECURITY)) {
          DEBUG(pstr("%s: %d", color_name(player, -1), has_pow(player, player, POW_SECURITY)))
          notify(player, "You are not able to do such a thing.");
          return;
        }
	
	// eventually change plugins to return_config("plugin_directory")
	for(p = phandle; p; p=p->next)
	{
	  if(!strcmpm(p->name, text)) {
	    notify(player, "That plugin is already loaded.");
	    return;
	  }
        }
	
	sprintf(directory, "plugins/lib%s.so", text);
    DEBUG(pstr("directory: %s", directory));
	handle = dlopen(directory, RTLD_NOW);
	if(!handle) {
	  notify(player, pstr("Plugin '%s' could not be loaded.", text));
      notify(player, pstr("Last error: %s", dlerror()));
	  return;
	}
    DEBUG("just letting you know I'm here.");
	
	// Load the standard command list.
	if(!initiate_plugin_cmds(handle)) {
	  notify(player, tprintf("There was an error attempting to load plugin commands (%s)!", dlerror()));
	  dlclose(handle);
	  return;
	}
	
	append_handle(NULL, text, handle);
	notify(player, "Plugin loaded.");
	log_channel("log_imp", tprintf("\033[1m\033[33m*\033[0m %s loaded the %s plugin.", color_name(player, -1), text));
}

void unload_plugin(player, name)
	object *player;
	char *name;
{
	PHANDLE *p, *pnext;
	
	if(!has_pow(player, player, POW_SECURITY)) {
          notify(player, "You are not able to do such a thing.");
          return;
        }
	
	for(p = phandle; p; p=pnext) {
	  pnext = p->next;
	  if(!strcmpm(p->name, name)) {
	    log_channel("log_imp", tprintf("\033[1m\033[33m*\033[0m %s unloaded the %s plugin.", color_name(player, -1), name));
	    purge_commands(p);
	    delete_handle(p);
	    notify(player, pstr("Plugin %s unloaded...", name));
	    return;
	  }
	}
	
	notify(player, "What plugin?");
}

void list_plugins(object *player)
{
	PHANDLE *p;
	
	if(!phandle) {
	  notify(player, "There are no plugins loaded.");
	  return;
	}
	
	notify(player, "Plugins:");
	for(p = phandle; p; p=p->next)
	  notify(player, pstr("%s is loaded.", p->name));
	notify(player, "---");
}

// Credits to itsme @ TinyMAZE for these functions to hook into
// do_game_state().
void plugin_selectchk(fd_set rset, fd_set wset)
{
  PHANDLE *p;
  
  for(p = phandle; p; p=p->next)
    if(p->fselectchk)
      p->fselectchk(rset, wset);
}

void plugin_readset(fd_set *set, int *top)
{
  int *newset, *i;
  PHANDLE *p;

  for(p = phandle;p;p = p->next)
  {
    if(!p->freadset)
      continue;

    if(!(newset = p->freadset()))
      continue;

    for(i = newset;*i != -1;i++)
    {
      FD_SET(*i, set);
      if(*i >= *top)
        *top = (*i)+1;
    }
  }
}

void plugin_writeset(fd_set *set, int *top)
{
  int *newset, *i;
  PHANDLE *p;

  for(p = phandle;p;p = p->next)
  {
    if(!p->fwriteset)
      continue;
      
    if(!(newset = p->fwriteset()))
      continue;

    for(i = newset;*i != -1;i++)
    {
      FD_SET(*i, set);
      if(*i >= *top)
        *top = (*i)+1;
    }
  }
}

                                                                                                                            
void UnloadPlugins()
{
  PHANDLE *p, *pnext;
  
  for(p = phandle; p; p=pnext)
  {
    pnext = p->next;
    purge_commands(p);
    delete_handle(p);
  }
}


/** PluginCommand():
**/
void PluginCommand(object *player, char *cswitch, char *arg1)
{
  if(!*cswitch && !*arg1)
    cswitch = "list";

  if(!strcmpm(cswitch, "load")) {
    load_plugin(player, arg1);
    return;
  } else if (!strcmpm(cswitch, "unload")) {
    unload_plugin(player, arg1);
    return;
  } else if (!strcmpm(cswitch, "list")) {
    list_plugins(player);
    return;
  } else if (!strcmpm(cswitch, "reload")) {
    if(!has_pow(player, player, POW_SECURITY)) {
      notify(player, "You cannot do that.");
      return;
    }
    unload_plugin(player, arg1);
    load_plugin(player, arg1);
    return;
  }
    
  notify(player, "I'm not sure what you're trying to do.");
}

/** END OF PLUGIN.C **/
