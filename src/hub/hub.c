/****************************************************************************
 MYTH - HUB
 Coded by sarukie (07/20/04)
 ---
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ctype.h>
#include <stdarg.h>
#include <arpa/inet.h>

#include "db.h"
#include "net.h"
#include "externs.h"

#define	HUB_HOST	"tinymaze.com"
#define HUB_PORT	4225

// HUB RESPONSE CODES
#define HUB_UNUSED	0x0	// Uh...
#define HUB_ERROR	0x1	// Error
#define HUB_PING	0x2	// Ping Request
#define HUB_RPING	0x3	// Ping Reply
#define HUB_LOGIN	0x4	// Login request
#define HUB_RLOGIN	0x5	// Login Reply
#define HUB_PAGE	0x6	// Page request
#define HUB_RPAGE	0x7	// Page reply
#define HUB_WLIST	0x8	// Worldlist request
#define HUB_RWLIST	0x9	// Worldlist reply
#define HUB_WHO		0xA	// WHO request
#define HUB_RWHO	0xB	// WHO reply

#define ERROR_BAD_DEST    0x1 /* Malformed destination              */
#define ERROR_NO_WORLD    0x2 /* Destination is not connected       */
#define ERROR_BAD_CMD     0x3 /* The command is unknown             */
#define ERROR_BAD_LOGIN   0x4 /* Login to the hub has been denied   */
#define ERROR_NOT_READY   0x5 /* Data submitted without logging in  */
#define ERROR_BAD_SIZE    0x6 /* Embedded len doesn't match strlen  */
#define ERROR_BAD_MSG     0x7 /* Msg didn't contain what's expected */

typedef struct rwho_doer_t {
	unsigned char sid;
	int enactor;
	struct timeval tv;
} rwho_doer;

rwho_doer rwho_doer_list[100];

int hub_desc = -1;
static unsigned char r_id = 1;
time_t sent = 0;

int hub_connect()
{
	struct sockaddr_in addr;
	struct hostent *hp;
	int fd;
	
	
	addr.sin_family = AF_INET;
	addr.sin_port = htons(HUB_PORT);
	
	if( (hp = gethostbyname(HUB_HOST)) == 0) 
	  return -1;
	
	bcopy(hp->h_addr, (char *)&addr.sin_addr, hp->h_length);
	addr.sin_family = hp->h_addrtype;
	if(inet_aton(HUB_HOST, &addr.sin_addr)) {
	  log_channel("log_io", "* Connection to hub failed.");
	  return -1;
	}
	
	if(( fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
	  log_channel("log_io", "* error creating hub connection.");
	  return -1;
	}	
	
	//fcntl(fd, F_SETFL, O_NONBLOCK);
	connect(fd, (struct sockaddr *)&addr, sizeof(addr));
	
	return fd;
}

void rwho_doers_zero()
{
	int i;
	
	for(i = 0; i < 100; i++)
	  rwho_doer_list[i].sid = (unsigned char)0xFF;
}

void initialize_connection()
{
	char msg[1024];

	// set all the rwho_doers to 0
	rwho_doers_zero();
	
	// Let's connect. 
	if( (hub_desc = hub_connect()) != -1)
	  log_channel("log_io", pstr("* Successful connection to hub established on socket %d!", hub_desc));
	else {
	  log_channel("log_io", "* Error connection to hub.");
	  return;
	}

 	sprintf(msg, "%c%c%c%c%s", (unsigned char)r_id++, 0, 0, (HUB_LOGIN << 1)|1, return_config("myth_name"));
 	*(unsigned short *)(msg + 1) = htons(strlen(msg+3));
	write(hub_desc, msg, strlen(msg+3)+3);
}


void parse_hubwho(whostr)
	char *whostr;
{
	int i, c;
	object *who;
	char buf[2048], buf2[2048];
	char *d = buf2, *p;
	
	for(i = 0; i < 100; i++) {
	  if(rwho_doer_list[i].sid == (unsigned char)*whostr) {
	    break;
	  }
	}
	if(i>99)
	  return;

	who = db[rwho_doer_list[i].enactor];
	rwho_doer_list[i].sid = 0xFF;
	whostr += 4;
	c = (int)*whostr;
	whostr += c+1;
	c = (int)*whostr;	// COLUMN #s...
	p = whostr;
	for(i = 0; i < (c+1); i++) p++;
	whostr = p;
	strcpy(buf2, "");
	for(p = whostr, i = 0; *p; p++)
	  if(*p == '\n') {
	    i++;
	    if(i >= c) {
	      *d++ = '\n';
	      i = 0;
	    } else {
	      *d++ = '\t';
	    }
	  } else {
	    *d++ = *p;
	  }
	*d++ = '\0';

	notify(who, strip_ansi(buf2));
	
}


char *retworld(instr, len)
	char *instr;
	unsigned char len;
{
	char buf[1024];
	char *c = buf, *p, *pend;
	
	strcpy(buf, "");
	pend = instr + len;
	for(p = instr; p < pend; p++)
		*c++ = *p;

	*c++ = '\0';
	return(stralloc(buf));
}

/** HUB: FROM\nTO\nMESSAGE**/
void parse_hubpage(pagestr)
	char *pagestr;
{
	object *who;
	unsigned char sid = *pagestr;
	char buf[1024], *world;
	char *c;
	unsigned char len;
	
	pagestr += 4;
	len = *pagestr;
	pagestr++;
	
	c = pagestr;
	pagestr += len;

	world = retworld(c, len);
	strcpy(buf, strip_ansi(pagestr));
	if(!(who = match_player(fparse(rparse(buf, '\n'), '\n')))) {
	  //DEBUG("Someone wants to page a non-existant player.");
	  sprintf(buf, "%c%c%c%c%c%s%c%c", sid, 0, 0, (HUB_RPAGE << 1)|0, (unsigned char)strlen(world), world,
	    0x1, 0x1);
	  *(unsigned short *)(buf + 1) = htons(strlen(buf+3));
	  write(hub_desc, buf, strlen(buf+3)+3);
	  return;
	}	
	
	notify(who, pstr("%s@%s pages you with: %s", fparse(buf, '\n'), world,  rparse(rparse(buf,'\n'),'\n')));
	sprintf(buf, "%c%c%c%c%c%s%c%s", sid, 0, 0, (HUB_RPAGE << 1)|0, (unsigned char)strlen(world), world,
	  0x2, who->name);
	*(unsigned short *)(buf + 1) = htons(strlen(buf+3));
	write(hub_desc, buf, strlen(buf+3)+3);
}

void send_who(sid, size, msg)
	unsigned char sid;
	unsigned char size;
	char *msg;
{
	DESCRIPTOR *d;
	char buf[2048];
	char wholist[1024];

	strcpy(wholist, "");
	for(d = descriptor_list; d; d=d->next)
	  if(d->player)
	    sprintf(wholist + strlen(wholist), "\n%s\n%s\n%s\n%ld\n%ld", d->player->name, atr_get(d->player, A_ALIAS),
	      "N/A", time(0) - d->connected_at, time(0) - d->last_time);
	
	sprintf(buf, "%c%c%c%c%c%s%c%c%c%c%c%cName\nAlias\nFlags\nOnfor\nIdle%s", sid, 0, 0, (HUB_RWHO << 1) | 0, 
	   size, msg, 5, 1, 2, 3, 4, 5, wholist);
	
	*(unsigned short *)(buf + 1) = htons(strlen(buf+3));
	write(hub_desc, buf, strlen(buf + 3) + 3);		
}

void hub_ping(msg)
	char *msg;
{
	char buf[1024];
	
	sprintf(buf, "%c%c%c%c%c%s", (unsigned char)*msg, 0, 0, (HUB_RPING << 1) | 0, (unsigned char)*(msg+4), msg+5);
	*(unsigned short *)(buf + 1) = htons(strlen(buf+3));
	write(hub_desc, buf, strlen(buf +3)+3);
}

void hub_pingrcv(msg)
	char *msg;
{
	object *who;
	unsigned char sid =(unsigned char) *msg;
	int i;
	struct timeval tv;
	struct timeval tv2;
	
	for(i = 0; i < 100; i++)
	  if(rwho_doer_list[i].sid == sid)
	    break;
	
	
	if(i > 99) {
	  DEBUG("Invalid ping response SID\n");
	  return;
	}
	
	rwho_doer_list[i].sid = 0xFF;
	if(!(who = db[rwho_doer_list[i].enactor])) {
	  DEBUG("Invalid user attempt.");
	  return;
	}
	
	gettimeofday(&tv, NULL);
	tv2 = rwho_doer_list[i].tv;
	
	tv.tv_usec -= tv2.tv_usec;
	tv.tv_sec -= tv2.tv_sec + (tv.tv_usec < 0);
	tv.tv_usec += (tv.tv_usec < 0) ? 1000000 : 0;

	if(*(msg+4)>0)
	  notify(who, pstr("Ping to %s received in %01ld.%06ld seconds.", msg+5, tv.tv_sec, tv.tv_usec));
	else
	  notify(who, pstr("Ping to HUB received in %01ld.%06ld seconds.", tv.tv_sec, tv.tv_usec));
}

/** HANDLER FOR PAGE RECEIVE **/
void hub_pagercv(msg)
	char *msg;
{
	char buf[1024];
	unsigned char sid = (unsigned char)*msg;
	int id, c;
	object *who;
	
	for(id = 0; id < 100; id++) {
	  DEBUG(pstr("SID: %02X -vs- %02X", sid, rwho_doer_list[id].sid));
	  if(rwho_doer_list[id].sid == sid)
	    break;
	}
	
	if(id > 99) {
	  DEBUG("Error accessing remote ID list...");
	  return;
	}
	
	rwho_doer_list[id].sid = 0xFF;
	
	if(!(who = db[rwho_doer_list[id].enactor])) {
	  DEBUG("Error finding user...");
	  return;
	}
	
	c = *(msg+4);
	msg += (5 + c);
	DEBUG(pstr("SID is %02X and PLAYER is %s", sid, who->name));
	if(*msg == 02) { // NO ERROR.
	  notify(who, "Page sent successfully.");
	} else if (*msg == 01) {
	  if(*(msg+1) == 01) {
	    notify(who, "Invalid user!");
	  } else if (*(msg + 1) == 2) {
	    notify(who, "Recipient is either hidden or disconnected.");
	  }
	}
}

/** HANDLE WORLD LIST **/
void hub_worldrcv(msg)
	char *msg;
{
	object *who;
	char buf[1024];
	unsigned char sid = *msg;
	char *p = buf;
	int cnt = 0;
	int id;
	
	for(id = 0; id < 100; id++)
	  if(rwho_doer_list[id].sid == sid)
	    break;
	
	if(id > 99) {
	  DEBUG("Invalid remote SID...");
	  return;
	}
	
	if(!(who = db[rwho_doer_list[id].enactor])) {
	  DEBUG("Invalid user!");
	  return;
	}
	
	strcpy(buf, msg+4);
	for(;*p;p++)
	  if(*p == '\n') {
	    if(cnt++ < 3) 
	      *p = '\t';
	    else
	      cnt = 0;
          }
          
	*p++ = '\0';
	
	notify(who, "World List:");
	notify(who, buf);
}

void hub_read()
{
	int len;
	unsigned char msg[4096];
	unsigned char cmd;
	
	*msg = '\0';
	if(hub_desc < 0)
	  return;
	  
	if( (len = read(hub_desc, msg, sizeof(msg)-1)) < 0)
	  return;
	  
	if(!len) {
	  DEBUG("We've disconnected from the hub...");
	  close(hub_desc);
	  hub_desc = -1;
	  return;
	}

	msg[len] = 0;
	cmd = msg[3] >> 1;
	DEBUG(pstr("SID: %02X", (unsigned char)msg[0]));
	
	switch(cmd) {
	  case HUB_ERROR:
	    switch(msg[4]) {
	      case ERROR_BAD_DEST:
	        DEBUG("Bad destination.");
	        break;
	      case ERROR_NO_WORLD:
	        DEBUG("Ain't no world as such, beey0tch!");
	        break;
	      case ERROR_BAD_CMD:
	        DEBUG("You couldn't COMMAND a shoelace!");
	        break;
	      case ERROR_BAD_LOGIN:
	        DEBUG("Bad login, not bisquit!");
	        break;
	      case ERROR_NOT_READY:
	      	DEBUG("Have you ever heard of privacy!? OMIGOD!");
	      	break;
	      case ERROR_BAD_SIZE:
	        DEBUG("How many inches are you? Huh!?");
	        break;
	      case ERROR_BAD_MSG:
	        DEBUG("Tell someone who cares!");
	        break;
	      default:
	        DEBUG(pstr("Unknown error %02X", msg[4]));
	        break;
	    }
	    break;
	  case HUB_PING:
	    hub_ping(msg);
	    break;
	  case HUB_RPING:
	    DEBUG("We have a ping reply.");
	    hub_pingrcv(msg);
	    break;
	  case HUB_RLOGIN:
	    DEBUG("We've logged into the host.");
	    break;
	  case HUB_RWLIST:
	    DEBUG("We've received a world list.");
	    hub_worldrcv(msg);
	    break;
	  case HUB_PAGE:
	    DEBUG("We've received a page...");
	    parse_hubpage(msg);
	    break;
	  case HUB_RPAGE:
	    hub_pagercv(msg);
	    break;
	  case HUB_WHO:
	    send_who(*msg, *(msg + 4), msg+5);
	    break;
	  case HUB_RWHO:
	    parse_hubwho(msg);
	    break;
	  default:
	    DEBUG("Unused COMMAND value.");
	    break;
	}
}

unsigned char get_next_free_remote()
{
	int i;
	
	for(i = 0; i < 100; i++) {
	  if(rwho_doer_list[i].sid == (unsigned char)0xFF) 
	    return i;
	}
	
	return 0xFF;
}

void hub_rwho(player, arg1)
	object *player;
	char *arg1;
{
	unsigned char sid = r_id++;
	unsigned char i = get_next_free_remote();
	char buf[1024];

	if(i == 0xFF) {
	  DEBUG(pstr("HOLY CRAP!!! ALL THE IDS HAVE BEEN USED BROTHAS!"));
	  return;
	}
		
	rwho_doer_list[i].sid = sid;
	rwho_doer_list[i].enactor = player->ref;
	
	sprintf(buf, "%c%c%c%c%c%s", sid, 0, 0, (HUB_WHO << 1)|0, (unsigned char)strlen(arg1), arg1);
	*(unsigned short *)(buf + 1) = htons(strlen(buf + 3));
	write(hub_desc, buf, strlen(buf+3)+3);	
}

void hub_rpage(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	char buf[1024];
	char *to;
	char *world;
	unsigned char sid = r_id++;
	int id = get_next_free_remote();
	
	to = fparse(arg1, '@');
	world = rparse(arg1, '@');
	
	DEBUG(pstr("RPAGE() SID: %02X", sid));
	rwho_doer_list[id].sid = sid;
	rwho_doer_list[id].enactor = player->ref;
	DEBUG(pstr("remote player: %d", rwho_doer_list[id].enactor));
	 
	if(!*to || !*world || !*arg2) {
	  notify(player, "Pardon?");
	  return;
	}
	
	//DEBUG(pstr("Sending: %02X", sid));
	sprintf(buf, "%c%c%c%c%c%s%s\n%s\n%s", sid, 0, 0, (HUB_PAGE << 1)|0,
	  (unsigned char)strlen(world), world, player->name, to, arg2);
	*(unsigned short *)(buf + 1) = htons(strlen(buf+3));
	write(hub_desc, buf, strlen(buf+3)+3);
}

void hub_rping(player, arg1)
	object *player;
	char *arg1;
{
	char buf[1024];
	unsigned char sid = r_id++;
	int id = get_next_free_remote();
	struct timeval tv;
	
	rwho_doer_list[id].sid = sid;
	rwho_doer_list[id].enactor = player->ref;
	gettimeofday(&tv, NULL);
	rwho_doer_list[id].tv = tv;
	
	if(!*arg1) { // SEND THE HUB A PING
	  sprintf(buf, "%c%c%c%c", sid, 0, 0, (HUB_PING << 1)|1);
	  *(unsigned short *)(buf + 1)=htons(strlen(buf+3));
	  write(hub_desc, buf, strlen(buf+3)+3);
	  notify(player, "Pinging the hub...");
	  return;
	}
		
	sprintf(buf, "%c%c%c%c%c%s", sid, 0, 0, (HUB_PING << 1)|0,
	  (unsigned char)strlen(arg1), arg1);
	*(unsigned short *)(buf + 1) = htons(strlen(buf+3));
	write(hub_desc, buf, strlen(buf + 3)+3);
	notify(player, pstr("Sending a ping to %s", arg1));
}

void hub_rworld(player)
	object *player;
{
	char buf[1024];
	unsigned char sid = r_id++;
	int id = get_next_free_remote();
	
	rwho_doer_list[id].sid = sid;
	rwho_doer_list[id].enactor = player->ref;

	sprintf(buf, "%c%c%c%c", sid, 0, 0, (HUB_WLIST << 1)|1);
	*(unsigned short *)(buf + 1) = htons(strlen(buf + 3));
	write(hub_desc, buf, strlen(buf+3)+3);
}


