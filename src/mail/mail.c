/**
 A simple shared object library.
 Does nothing, just gets compiled.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "api.h"

void SendMail P((object *, char *, char *));

CL simple_cmd_add[]={
  {"@mail", SendMail, 0, 0},
  { NULL }
};


CL *additional_command()
{
	return simple_cmd_add;
}

void __attribute__((constructor)) simple_init(void)
{
}

void SendMail(object *player, char *arg1, char *arg2)
{
  object *whom;
  
  if(!(whom=remote_match(player, arg1))) {
    notify(player, "I'm sorry, I'm not sure of whom you were referring.");
    return;
  }
  
  notify(whom, "You have new mail!");
  notify(player, "New @mail sent.");
}

