/*****************************************************************************
 MYTH - Matching Arguments 
 Coded by Saruk (04/25/99)
 ---
 Match everything
 *****************************************************************************/
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/* this matches to the db list */
object *match_dblist(int num)
{
	if(num < 0 || num > dbtop)
		return(NULL);
	if(!db[num])
		return(NULL);
		
	return(db[num]);
}

/* this matches a partial player WITHIN a location. */
object *match_partial_type(location, matchname, flag)
	object *location;
	char *matchname;
	int flag;
{
	CONTENTS *c;
	int ref;
	char buf[MAX_BUFSIZE];
	
	if(!matchname || !*matchname)
		return (NULL);
	
	ref = match_objnum(matchname);
	
				
	for(c = location->contents; c; c=c->next) {
		strcpy(buf, c->content->name);
		while(*buf) {
			if( (!strcmpm(strip_color(atr_get(c->content, A_ALIAS)), matchname) || 
				string_prefix(matchname, buf) || ref == c->content->ref) && 
					(c->content->flags & flag))
				return c->content;
			strcpy(buf, restof(buf));
		}
	}
	
	return (NULL);		
}

int match_objnum(char *str) 
{
	static char buf[MAX_BUFSIZE];
	char *p = buf;
	
	if(*str == '#')
	  str++;
	else
	  return -1;

	while(*str && isdigit(*str))
		*p++ = *str, str++;
		
	if(*str)
		return -1;
	
	*p++ = '\0';
	return(atoi(buf));
}
	
long match_num(char *str)
{
	static char buf[MAX_BUFSIZE];
	char *p = buf;
	
	if(*str == '-') 
		*p++ = *str, str++;
		
	while(*str && isdigit(*str))
		*p++ = *str, str++;
	
	if(*str)
		return -1;
	
	*p++ = '\0';
	return atol(buf);
}

// Match a player's name.
object *match_player(matchname)
	char *matchname;
{
	object *o;
	int ref;	// if char is a number value, we want to return this as well.
	char *alias;
	
	if(!*matchname)
		return (NULL);

	// let's check to see if it really a 0 or not.
	if((ref = match_objnum(matchname)) > -1)
		return(match_dblist(ref));
	
	for(o=player_list; o; o=o->next) {
	  alias = strip_color(atr_get(o, A_ALIAS));
	  if(!strcmpm(o->name, matchname))
	    return o;
	  if(!alias || !*alias)
	    continue;
	  else if(!strcmpm(alias, matchname))
	    return o;
	}
	
	return (NULL);
}

// Match a room's name.
object *match_room(matchname)
	char *matchname;
{
	object *o;
	int ref;
	
	if(!*matchname)
		return (NULL);
		
	if((ref = match_objnum(matchname))>-1) 
		return(match_dblist(ref));
	for(o = room_list; o; o=o->next) 
		if(!strcmpm(o->name, matchname) || ref == o->ref)
			return o;
	
	return (NULL);
}

// Match a thing.
object *match_thing(matchname)
	char *matchname;
{
	object *o;
	int ref;	// if char is a number value, we want to return this as well.
	
	if(!*matchname)
		return (NULL);
	
	if((ref = match_objnum(matchname)) > -1)
		return(match_dblist(ref));
	for(o=thing_list; o; o=o->next)
		if(!strcmpm(o->name, matchname) || ref == o->ref)
			return o;
	
	return (NULL);
}

int match_type(victim, type)
	object *victim;
	char *type;
{
	int ctr = 0;
	
	for(ctr = 0; flag_table[ctr].name; ctr++)
		if((string_prefix(type, flag_table[ctr].name) || *type == flag_table[ctr].parsed) &&
			(victim->flags & flag_table[ctr].flag))
			return 1;
	
	return 0;
}

object *match_special(player, matchname)
	object *player;
	char *matchname;
{
	if(!*matchname)
		return (NULL);

	if(!strcmpm(matchname, "me") || !strcmpm(matchname, "self"))
		return player;
	else if (!strcmpm(matchname, "here"))
		return player->location;
	
	return (NULL);
}

int match_exitkey(exit, matchname)
	object *exit;
	char *matchname;
{
	char *p, *k;
	
	if(!*exit->pass)
	  return 0;

	k = exit->pass;
	while( (p = parse_up(&k, ';')) )
	  if(!strcmpm(p, matchname))
	    return 1;
	
	return 0;    	  
}

object *match_partial(location, matchname)
	object *location;
	char *matchname;
{
	char buf[MAX_BUFSIZE];
	CONTENTS *c;
	
	if(!matchname || !*matchname)
		return (NULL);
	
	
	for( c = location->contents; c; c=c->next) {
	  strcpy(buf, c->content->name);
	  while(*buf) {
	    if(string_prefix(matchname, buf)  || match_exitkey(c->content, matchname))
	      return c->content;
	    strcpy(buf, restof(buf));
	  }
	}
		
	/*for(cont = location->contents; cont; cont=cont->next) {
		strcpy(buf, cont->content->name);
		while(*buf) {
			if(string_prefix(matchname, buf) || match_exitname(c->content)
				return cont->content;
			strcpy(buf, restof(buf));
		}
	}*/
	
	return (NULL);
}

object *match_moveable(matchname)
	char *matchname;
{
	object *o;
	
	if(!*matchname)
		return (NULL);
	
	if(!(o = match_player(matchname)))
		if(!(o = match_thing(matchname)))
			return (NULL);
	
	return o;
}



object *match_exit(matchname)
	char *matchname;
{
	object *o;
	int ref;
	
	if(!*matchname)
		return (NULL);
	
	if((ref = match_objnum(matchname)) > -1)
		return(match_dblist(ref));
	
	for(o=exit_list; o; o=o->next)
	  if(!strcmpm(o->name, matchname) || ref == o->ref) 
	    return o;
	
	return (NULL);
}

// Match with all objects.
object *match_object(matchname)
	char *matchname;
{
	object *o;
	
	if(!*matchname)
		return (NULL);
		
	if ( (o=match_player(matchname)) )
		return o;
	
	if( (o=match_room(matchname)) )
		return o;
				
	if( (o=match_thing(matchname)) )
		return o;

	if( (o = match_exit(matchname)) )
		return o;		
	
			
	return (NULL);	
}


object *match_pseudoexit(thing, key)
	object *thing;
	char *key;
{
	ATTR *a;
	object *what;
	
	for(a = thing->location->alist; a; a=a->next) {
	 if((a->flags & AEXIT) && string_prefix(key, a->name)) {
	   if((what = match_room(a->value))) {
	     notify(thing, pstr("Through the exit marked %s you see...", a->name));
	     return(what);
	   }
	 }
	}
	
	return(NULL);
}

object *remote_match(player, arg1)
	object *player;
	char *arg1;
{
	object *who;

	if(*arg1 == '*')  {
	  if(!(who = match_player(arg1+1)))
	    return(NULL);
	 
	  if(!has_pow(player, who, POW_REMOTE) && !is_grouped(player, who) && (who->ownit != player))
	    return(NULL);
	  
	  return(who);
	} else if (*arg1 == '#') {
	  if(!isdigit(*(arg1 + 1)))
	    return(NULL);
	    
	  if(!(who = match_dblist(atoi(arg1+1))))
	    return(NULL);
	
	  if((player != who) && !has_pow(player, who, POW_REMOTE) && !is_grouped(player, who) && (who->ownit != player))
	    return(NULL);
	
	  return(who);
	} else {
	  if((who = match_player(arg1))) {
	    if(!can_control(player, who)) {
	      who = NULL;
	    } else {
	      return(who);
	    }
	  }
	      
	  if(!(who = match_special(player, arg1)))
	    if(!(who = match_partial(player->location, arg1)))
	      return(NULL);
	
	  if(who->location == player || who->location == player->location)
	    return(who);
		
	  return(NULL);
	}
	
	return(NULL); // i don't know how we'd get here.
}

