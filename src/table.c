/****************************************************************************
 MYTH - Tables
 Coded by Saruk (02/12/99)
 ---
 This is where all external tables are defined.
 ****************************************************************************/

#include "db.h"
#include "net.h"
#include "externs.h"

#include "tables.h"
