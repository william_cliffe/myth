/****************************************************************************
 MYTH - Miscellaneous I/O Routines 
 Coded by Saruk (02/04/99)
 ---
 These are here so as not to clutter io.c
 ****************************************************************************/

#include <stdio.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int show_welcome(d)
	DESCRIPTOR *d;
{
	FILE *fp;
	char buf[MAX_BUFSIZE];
	
	if ( (fp = fopen(return_config("welcome_file"), "r")) == NULL) {
		printf("[ERROR]: Cannot open welcome file.\n");
		return 0;
	}
	
	while(fgets(buf, sizeof(buf), fp)) {	
	  if(buf[strlen(buf)-1] == '\n')
	    buf[strlen(buf)-1] == '\0';
		dwrite(d, buf);
		dwrite(d, "\r");
	}
	
	fclose(fp);
	
	// success!
	return 1;
}

int display_file(d, filename)
	DESCRIPTOR *d;
	char *filename;
{
	FILE *fp;
	char buf[MAX_BUFSIZE], str[MAX_BUFSIZE];
	
	
	if( (fp=fopen(filename, "r")) == NULL) {
		printf("[ERROR]: Cannot open %s\n", filename);
		return 0;
	}
	
	while(fgets(buf, sizeof(buf), fp)) {
	  if(buf[strlen(buf)-1] == '\n')
	    buf[strlen(buf)-1] == '\0';
		dwrite(d, buf);
		dwrite(d, "\r");
	}
	
	fclose(fp);
	
	// success!
	return 1;
}

void world_dwrite(str)
	char *str;
{
	DESCRIPTOR *d;
	
	for(d = descriptor_list; d; d=d->next)
		if(d->state == CONNECTED)
			if(reboot_style < 2)
				dwrite(d, str);
}

void world_dwrite_q(str)
	char *str;
{
	DESCRIPTOR *d;
	
	for(d=descriptor_list; d; d=d->next)
		if(d->state == CONNECTED && d->player)
			if( (!(d->player->flags & QUIET)) && reboot_style < 2)
				dwrite(d, str);
}

void do_showterm(object *player)
{
	DESCRIPTOR *d;
	
	for(d = descriptor_list; d; d=d->next)
	  if(d->player == player && d->active)
	    notify(player, pstr("Terminal Width: %d \t Terminal Height: %d", d->term_width, d->term_height));
}
