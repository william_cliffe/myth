/****************************************************************************
 MYTH - Mail System
 Coded by Saruk (03/26/99)
 ---
 This is a new, rewritten mail system for MYTH. The old one was clunky and I 
 didn't feel safe with it.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int __count_mail(mail)
	MAIL *mail;
{
	MAIL *m;
	int amt = 0;

	for(m = mail; m; m=m->next)
		amt++;
	
	return amt;	
}

void __delete_mail(player, mail)
	object *player;
	MAIL *mail;
{
	MAIL *m, *mprev = NULL;
	
	if(!mail)
		return;
		
	for(m = player->mail; m; m=m->next) {
		if(m == mail)
			break;
		mprev = m;
	}
	
	if(!m)
		return;
	
	if(!mprev)
		player->mail = m->next;
	else
		mprev->next = m->next;
	
	if(m->message)
		block_free(m->message);
	
	block_free(m);
}

void free_mail(thing)
	object *thing;
{
	MAIL *m, *mnext = NULL;
	object *o;
	
	for(m = thing->mail; m; m=mnext) {
		mnext = m->next;
		__delete_mail(thing, m);
	}
	
	for(o = player_list; o; o=o->next)
		for(m = o->mail; m; m=mnext) {
			mnext=m->next;
			if(m->from == thing)
				__delete_mail(o, m);
		}
}

MAIL *__allocate_mail(player, message)
	object *player;
	char *message;
{
	MAIL *m=NULL, *mprev = NULL;
	
	for(m = player->mail; m; m=m->next)
		mprev = m;
				
	GENERATE(m, MAIL);
	m->age = time(0);
	m->message = NULL;
	SET(m->message, message);
	m->from = 0;
	m->fromref = 0;
	m->flags = 0;
	m->next = NULL;
	
	if(!mprev)
		player->mail = m;
	else
		mprev->next = m;
	
	return(m);
}

void __internal_mail_check()
{
	OBJECT *o;
	MAIL *m;
	
	
	for(o = player_list; o; o=o->next)
		for(m = o->mail; m; m=m->next)
			if( ((time(0) - m->age) > MAIL_TOO_OLD) && (m->flags & MAIL_READ) 
				&& (!(m->flags & MAIL_PROTECTED)) )
				__delete_mail(o, m);
}

void __announce_mail(player)
	object *player;
{
	MAIL *m;
	char buf[MAX_BUFSIZE];
	int ctr = 0, new = 0;
	
	if(!player->mail) {
		notify(player, "* You have no messages.");
		return;
	}
	
	for(m = player->mail; m; m=m->next) {
		if(m->flags & MAIL_NEW)
			new++;
		ctr++;
	}
	
	sprintf(buf, "* You have %d message%s", ctr, (ctr == 1) ? "":"s");
	if(new > 0)
		sprintf(buf, "%s - %d of them are new.", buf, new);
	
	notify(player, buf);
}

void __list_mail(player, victim)
	object *player;
	object *victim;
{
	MAIL *m;
	int ctr = 1;
	
	notify(player, pstr("/%s\\", make_ln("-", 76)));
	    
	if(!victim->mail) {
		notify(player, "[MAIL]: Mailbox is empty.");
		notify(player, pstr("\\%s/", make_ln("-", 76)));
		return;
	}	
	
	for(m = victim->mail; m; m=m->next) {
		notify(player, pstr("[%d]%cFrom: %s (%s)", ctr, 
		        (m->flags & MAIL_NEW) ? '*':' ', color_name(m->from, -1),
		        get_date(m->age)));
		ctr++;
	}
	
	notify(player, pstr("\\%s/", make_ln("-", 76)));
}

MAIL *__find_mail(player, msg)
	object *player;
	int msg;
{
	MAIL *m;
	int ctr = 1;
	
	for(m = player->mail; m; m=m->next) {
		if(ctr == msg)
			return m;
		ctr++;
	}
	
	return (NULL);
}

char *__mail_flags(mail)
	MAIL *mail;
{
	char buf[MAX_BUFSIZE];
	
	strcpy(buf, "");
	if(mail->flags & MAIL_READ)
		strcat(buf, " Read");
	if(mail->flags & MAIL_PROTECTED)
		strcat(buf, " Protected");
	if(mail->flags & MAIL_URGENT)
		strcat(buf, " Urgent");
	if(mail->flags & MAIL_NEW)
		strcat(buf, " New");
	if(mail->flags & MAIL_FORWARD)
		strcat(buf, " Forwarded");
		
	return stralloc(buf);	
}

void __display_mail(player, victim, msgno)
	object *player;
	object *victim;
	int msgno;
{
	MAIL *m;
	
	if(!(m = __find_mail(victim, msgno))) {
		error_report(player, M_ENOMSG);
		return;
	}
	

	
	notify(player, pstr("/%s\\", make_ln("-", 76)));
	notify(player, pstr("Message was sent by %s on %s", color_name(m->from, -1), get_date(m->age)));
	notify(player, pstr("Message flags: %s", __mail_flags(m)));
	notify(player, pstr("Message: %s",
		substitute(m->message)));
	notify(player, pstr("\\%s/", make_ln("-", 76)));
	
	if(player == victim) {
		m->flags &= ~MAIL_NEW;
		m->flags |= MAIL_READ;
	}
}

int delete_mail(player, msgno)
	object *player;
	int msgno;
{
	MAIL *m;
	
	if(!(m = __find_mail(player, msgno))) {
		error_report(player, M_ENOMSG);
		return 0;
	}
	
	if(m->flags & MAIL_PROTECTED) {
		error_report(player, M_EPROTECTED);
		return 0;
	}
		
	__delete_mail(player, m);
	return 1;
}

void delete_list_mail(player, msgs)
	object *player;
	char *msgs;
{
	int ctr, msgend, msgstart, nummsgs, numdel;
		
	nummsgs = __count_mail(player->mail);
	
	if( ISNUM(msgstart = match_num(fparse(msgs, '-'))) && ISNUM(msgend = match_num(rparse(msgs, '-'))) ) {
		if(msgstart > nummsgs) {
			error_report(player, M_ENOMSG);
			return;
		}
		
		if(msgend > nummsgs)
			msgend = nummsgs;
		
		numdel = msgend - msgstart;
		for(ctr = 0; ctr < (numdel + 1); ctr++)
			delete_mail(player, msgstart);
					
		notify(player, pstr("[MAIL]: %d messages deleted...", numdel + 1));
	} else {
		error_report(player, M_ENOMSG);
		return;
	}
}

void send_mail(player, victim, msg, extraflag)
	object *player;
	object *victim;
	char *msg;
	int extraflag;
{
	MAIL *m;
	
	m = __allocate_mail(victim, msg);
	m->fromref = player->ref;
	m->from = player;
	m->flags |= MAIL_NEW;
	m->flags |= extraflag;
	notify(player, pstr("[MAIL]: You sent %s new mail...",
		color_name(victim, -1)));
	notify(victim, pstr("[MAIL] * You have received new mail from %s!",
		color_name(player, -1)));
}

void list_mail(player, victim, msgnum)
	object *player;
	object *victim;
	char *msgnum;
{
	MAIL *m;
	int ctr = 1, flagged = 0;
	
	if(!*msgnum && !can_control(player, victim)) {
		notify(player, pstr("/%s\\", make_ln("-", 76)));
		for(m = victim->mail; m; m=m->next) {
			if(m->fromref == player->ref) {
				notify(player, pstr("[%d] Message from you.", ctr));
				flagged = 1;
			}
			ctr++;
		}
		if(!flagged)
			notify(player, "* They have no mail from you.");
		notify(player, pstr("\\%s/", make_ln("-", 76)));
	} else if (!*msgnum && can_control(player, victim)) {
		__list_mail(player, victim);
	} else if ( ISNUM(flagged = match_num(msgnum)) ) {
		if( (m = __find_mail(victim, flagged)) ) {
			if(!can_control(player, victim) && m->from == player)
				__display_mail(player, victim, flagged);
			else if (can_control(player, victim))
				__display_mail(player, victim, flagged);
			else
				error_report(player, M_ENOCNTRL);
		} else {
			error_report(player, M_ENOMSG);
			return;
		}
	}				
	
}

int __protect_mail(player, msgno)
	object *player;
	int msgno;
{
	MAIL *m;
	
	if(!(m = __find_mail(player, msgno)))
		return 0;
	
	m->flags |= MAIL_PROTECTED;
	return 1;
}

int __unprotect_mail(player, msgno)
	object *player;
	int msgno;
{
	MAIL *m;
	
	if(!(m = __find_mail(player, msgno)))
		return FALSE;
	
	m->flags &= ~MAIL_PROTECTED;
	
	return TRUE;
}

// arg1 - player to send t to; arg2 - mail to send.
void forward_mail(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who;
	MAIL *m;
	char buf[MAX_BUFSIZE];
	int msg;
	
	if(!(who = match_player(arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	if(!parse_lock(player, atr_get(who, A_PLAYERLOCK)) || !parse_lock(who, atr_get(player, A_PLAYERLOCK))) {
		error_report(player, M_EPAGELOCK);
		return;
	}

	if (!ISNUM(msg=match_num(arg2))) {
		error_report(player, M_ENOMSG);
		return;
	}
	
	if(!(m = __find_mail(player, msg))) {
		error_report(player, M_ENOMSG);
		return;
	}

	sprintf(buf,"Original message by: %s%%r%s",color_name(m->from, -1), m->message);
	send_mail(player, who, buf, MAIL_FORWARD);
	
}

void do_mail(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim;
	int mail_num = 0;
	
	if(!*arg1 && !*arg2) { // No possibilities aside from listing self mail.
		__list_mail(player, player);
		return;
	}
	
	if( ISNUM(mail_num = match_num(arg1)) && !*arg2) { // List 
		__display_mail(player, player, mail_num);
		return;
	}
	
	/* Search for options first */
	if(!strcmp(arg1, "delete")) {
		if ( ISNUM(mail_num = match_num(arg2)) ) {
			if(delete_mail(player, mail_num)) 
				notify(player, pstr("[MAIL]: Message %d deleted...",
					mail_num));
			return;
		} else if (strchr(arg2, '-')) {
			delete_list_mail(player, arg2);
			return;
		} else {
			error_report(player, M_ENOMSG);
			return;
		}
	} else if (!strcmp(arg1, "list")) {
		if(!(victim = match_player(front(arg2)))) {
			error_report(player, M_ENOPLAY);
			return;
		}
		list_mail(player, victim, restof(arg2));
		return;
	} else if (!strcmp(arg1, "protect")) {
		if ( ISNUM(mail_num = match_num(arg2)) ) {
			if(__protect_mail(player, mail_num))
				notify(player, pstr("[MAIL]: Message %d protected...",
					mail_num));
				return;
		} else {
			error_report(player, M_ENOMSG);
			return;
		}
	} else if (!strcmp(arg1, "unprotect")) {
		if( ISNUM(mail_num = match_num(arg2)) ) {
			if(__unprotect_mail(player, mail_num))
				notify(player, pstr("[MAIL]: Message %d unprotected...",
					mail_num));
				return;
		} else {
			error_report(player, M_ENOMSG);
			return;
		}
	} else if (!strcmpm(arg1, "forward")) {
		forward_mail(player, front(arg2), restof(arg2));
		return;
	}
	
	if(!*arg2) {
		notify(player, "What do you want to tell them?");
		return;
	}
	
	if(!(victim = match_player(arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	if(!parse_lock(player, atr_get(victim, A_PLAYERLOCK)) || !parse_lock(victim, atr_get(player, A_PLAYERLOCK))) {
		error_report(player, M_EPAGELOCK);
		return;
	}	
	send_mail(player, victim, arg2, 0);	
}

void __dump_mail(fp, player, mail)
	FILE *fp;
	object *player;
	MAIL *mail;
{
	fprintf(fp, "%d\n", player->ref);
	fprintf(fp, "%s\n", mail->message);
	fprintf(fp, "%ld\n", mail->age);
	fprintf(fp, "%d\n", mail->fromref);
	fprintf(fp, "%d\n", mail->flags);
}

void save_mail(filename)
	char *filename;
{
	OBJECT *o;
	MAIL *m;
	FILE *fp;
	int ctr = 0;
	
	for(o = player_list; o; o=o->next)
		for(m = o->mail; m; m=m->next)
			ctr++;

	fp = fopen(filename, "w");
	fprintf(fp, "%d\n", ctr);
	for(o=player_list; o; o=o->next)
		for(m = o->mail; m; m=m->next)
			__dump_mail(fp, o, m);
	
	fclose(fp);	
}

int reference_from(mail)
	MAIL *mail;
{
	OBJECT *o;
	
	if(!(o = match_player(pstr("#%d", mail->fromref))))
		return 0;
	mail->from = o;
	return 1;
}

int __get_mail(fp)
	FILE *fp;
{
	OBJECT *o;
	MAIL *m;
	
	if(!(o = match_player(pstr("#%d", getint(fp)))))
		return 0;
	
	m = __allocate_mail(o, getstring(fp));
	m->age = getlong(fp);
	m->fromref = getint(fp);
	m->flags = getint(fp);
	if(!reference_from(m))
		return 0;
	return 1;
}
	
void load_mail()
{
	FILE *fp;
	int ctr = 0, amt = 0;
	
	if(!(fp = fopen(return_config("maildb"), "r"))) {
		printf("** ERROR ** Cannot load mail database...\n");
		return;
	}
	
	amt = getint(fp);
	world_dwrite(pstr("* Loading %d message%s...\n", amt, (amt > 1) ? "s":""));
	
	for(ctr = 0; ctr < amt; ctr++) {
		if(!__get_mail(fp)) {
			printf("** ERROR ** Mail database corrupted, exiting.\n");
			return;
		}
	}
		
	
	fclose(fp);	
}

void dump_mail(filename)
	char *filename;
{
	save_mail(filename);
}

