/****************************************************************************
 MYTH - Channel Database
 Coded by Saruk (04/10/99)
 ---
 Save all the channel objects and player-channel objects.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int calculate_onchannel(channel)
	CHANNEL *channel;
{
	OBJECT *o;
	CHANNEL *c;
	int ctr = 0;
	
	for(o = player_list; o; o=o->next)
		for(c = o->channels; c; c=c->next)
			if(c->chobj == channel)
				ctr++;

	return(ctr);
}

int calculate_channels()
{
	CHANNEL *c;
	int ctr = 0;
	
	for(ctr = 0, c = channel_list; c; c=c->next)
		ctr++;
	
	return (ctr);
}

void CHDUMP_PLAYER(fp, o, channel)
	FILE *fp;
	OBJECT *o;
	CHANNEL *channel;
{
	fprintf(fp, "%d\n", o->ref);
	fprintf(fp, "%s\n", (channel->locks) ? channel->locks : "NULL");
	fprintf(fp, "%s\n", (channel->title) ? channel->title : "NULL");
	fprintf(fp, "%s\n", channel->alias);
	fprintf(fp, "%s\n", channel->colour);
	fprintf(fp, "%d\n", channel->flags);
	fprintf(fp, "%d\n", channel->powlvl);
	fprintf(fp, "%d\n", channel->status);
	fprintf(fp, "%d\n", channel->chdef);
}

void DROP_CHANNEL_PLAYERS(fp, channel)
	FILE *fp;
	CHANNEL *channel;
{
	OBJECT *o;
	CHANNEL *clist;
	
	for(o = player_list; o; o=o->next)
		for(clist = o->channels; clist; clist=clist->next)
			if(clist->chobj == channel)
				CHDUMP_PLAYER(fp, o, clist);
}

void OUT_CHANNEL(fp, channel)
	FILE *fp;
	CHANNEL *channel;
{
	fprintf(fp, "%s\n", channel->name);
	fprintf(fp, "%s\n", channel->desc);
	fprintf(fp, "%d\n", channel->ref);
	fprintf(fp, "%d\n", channel->oref);
	fprintf(fp, "%s\n", (channel->title) ? channel->title : "NULL");
	fprintf(fp, "%s\n", (channel->locks) ? channel->locks : "NULL");
	fprintf(fp, "%s\n", (channel->alias) ? channel->alias : "NULL");
	fprintf(fp, "%s\n", channel->colour);
	fprintf(fp, "%d\n", channel->flags);
	fprintf(fp, "%d\n", channel->powlvl);
	fprintf(fp, "%d\n", channel->status);
	fprintf(fp, "%d\n", channel->chdef);
	fprintf(fp, "%d\n", calculate_onchannel(channel));
	DROP_CHANNEL_PLAYERS(fp, channel);
	fprintf(fp, "\\\n");	
}

void save_channel_database(filename)
	char *filename;
{
	FILE *fp;
	CHANNEL *clist;
	
	if (!(fp = fopen(filename, "w"))) {
		printf("** Could not save channel db.\n");
		return;
	}
	
	fprintf(fp, "%d\n", calculate_channels());
	for(clist = channel_list; clist; clist = clist->next)
		OUT_CHANNEL(fp, clist);
	
	fclose(fp);
	
}

int REFERENCE_CHANNEL_PLAYER(pchannel)
	CHANNEL *pchannel;
{
	if(!strcmpm(pchannel->locks, "NULL"))
		pchannel->locks = NULL;
	if(!strcmpm(pchannel->title, "NULL"))
		pchannel->title = NULL;

	return TRUE;
}

int LOAD_PLAYER(fp, channel)
	FILE *fp;
	CHANNEL *channel;
{
	OBJECT *o;
	CHANNEL *newc;
	
	if(!(o = match_player(pstr("#%d", getint(fp)))))
		return 0;
	
	newc = __add_channel(o, channel);	
	SET(newc->locks, getstring(fp));
	SET(newc->title, getstring(fp));
	SET(newc->alias, getstring(fp));
	SET(newc->colour, getstring(fp));
	newc->flags = getint(fp);
	newc->powlvl = getint(fp);
	newc->status = getint(fp);
	newc->chdef = getint(fp);
	if(!REFERENCE_CHANNEL_PLAYER(newc))
		return 0;
	
	return TRUE;
}

int LOAD_CHANNEL_PLAYERS(fp, channel, amt)
	FILE *fp;
	CHANNEL *channel;
	int amt;
{
	int ctr;
	
	for(ctr = 0; ctr < amt; ctr++)
		if(!LOAD_PLAYER(fp, channel))
			return 0;
	
	return TRUE;
}

int REFERENCE_CHANNEL(channel)
	CHANNEL *channel;
{
	OBJECT *o;
	char searchname[MIN_BUFSIZE];
	
	sprintf(searchname, "#%d", channel->oref);
	if(!(o = match_player(searchname)))
		return 0;
	
	channel->owner = o;	
	if(!strcmpm(channel->title, "NULL"))
		channel->title = NULL;
	if(!strcmpm(channel->locks, "NULL"))
		channel->locks = NULL;
	if(!strcmpm(channel->alias, "NULL"))
		channel->alias = NULL;
	if(!strcmpm(channel->colour, "NULL"))
		channel->colour = NULL;
	channel->chobj = channel;

	return TRUE;
}

int LOAD_CHANNEL(fp)
	FILE *fp;
{
	CHANNEL *channel;
	
	channel = __create_channel(getstring(fp), NULL);
	SET(channel->desc, getstring(fp));
	channel->ref = getint(fp);
	channel->oref = getint(fp);
	SET(channel->title, getstring(fp));
	SET(channel->locks, getstring(fp));
	SET(channel->alias, getstring(fp));
	SET(channel->colour, getstring(fp));
	channel->flags = getint(fp);
	channel->powlvl = getint(fp);
	channel->status = getint(fp);
	channel->chdef = getint(fp);
	if(!REFERENCE_CHANNEL(channel))
		return 0;
	if(!LOAD_CHANNEL_PLAYERS(fp, channel, getint(fp)))
		return 0;
	if(!*getstring(fp) == '\\')
		return 0;
		
	return TRUE;
}

void load_channel_database()
{
	FILE *fp;
	int amt, ctr;
	
	if(!(fp = fopen(return_config("comdb"), "r"))) {
		printf("** Could not load channel db.\n");
		return;
	}
	
	amt = getint(fp);
	world_dwrite(pstr("* Loading %d channels...\n", amt));
	for(ctr = 0; ctr < amt; ctr++)
		if(!LOAD_CHANNEL(fp)) {
			printf("error loading channel database\n");
			return;
		}
		
	fclose(fp);
}

