/****************************************************************************
 MYTH - error handling.
 Coded by Saruk (02/06/99)
 ---
 This should handle errors via their specific M_E(RROR NUMBER)...
 ****************************************************************************/

#include <stdio.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int call_error_report(player, errornum, line, file)
	object *player;
	int errornum;
	int line;
	char *file;
{
	int index;
	
	if(errornum > 0)
		return errornum;
	
	for(index = 0; error_table[index].errmsg; index++)
		if(errornum == error_table[index].errornum)
			notify(player, pstr("[ERROR (%d:%s)]: %s", 
			line, file, error_table[index].errmsg));
	
	return 0;
}

