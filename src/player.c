/****************************************************************************
 MYTH - Player routines
 Coded by Saruk (01/23/99)
 ---
 Takes care of player creation and other player-specific functions.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// for crypt();
#include <unistd.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// Create a player.
object *create_player(player, name, pass)
	object *player;
	char *name;
	char *pass;
{
	OBJECT *o;
	char password[1024];
	
	if(!*name || !*pass) {
		perm_denied(player, "You left out an argument.");
		return(NULL);
	}
	
	// copy the encrypted password.
	strcpy(password, mcrypt(pass));
	
	
	// check for duplicity in names.
	if(__find_player(name) || !__allowed_name(name)) {
		error_report(player, M_EILLNAME);
		return(NULL);	
	}
	
	o = create_object(player->ref, TYPE_PLAYER, name);
	SET(o->pass, password);
	o->location = loc(o);
	o->link = o->location->ref;
	o->ownit = o;
	o->linkto = o->location;
	add_content(o->location, o);
	
	// set Quota.
	SET_ATR(o, A_QUOTA, pstr("%d", INIT_QUOTA));
	SET_ATR(o, A_CHANNELQUOTA, pstr("%d", INIT_CHQUOTA));
	
	// set their default WHO_FLAGS
	SET_ATR(o, A_WHOFLAGS, "noicD");
	
	notify(player, pstr("[CREATE]: %s has been created.", o->name)); //
	return(o);
}

void do_pcreate(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *o;
	
	o = create_player(player, arg1, arg2);
	
	if(!o)
		return;
	
	log_channel("log_imp", pstr("%s @pcreated %s",
		color_name(player, -1), unparse_object(o)));
	do_class(rootobj, pstr("#%d", o->ref), class_to_name(CLASS_CITIZEN));				
	// add player to public channel.
	filter_command(o, "+ch/join public", 0);
	
}
	
// Find out if a player is connected. (MOVE THIS!)
int connected_player(player)
	OBJECT *player;
{
	DESCRIPTOR *d;
	
	for(d=descriptor_list; d; d=d->next)
		if(d->state == CONNECTED)
			if(d->player == player)
				return 1;
		
	return 0;
}

// Find out if player has control over victim.
int can_control(player, victim)
	object *player;
	object *victim;
{
	if (victim->ownit == rootobj && player != rootobj)
	  return 0;
				
	if(player == rootobj || player == victim->ownit || player == victim)
	  return 1;
	
	if(!(player->flags & TYPE_PLAYER) && (player->flags & INHERIT)) 
	  if(can_control(player->ownit, victim))
	    return 1;
		
	if ( (player->flags & WIZARD) && (player->powlvl >= victim->ownit->powlvl) )
	  return 1;
	
	if(is_grouped(player, victim))
	  return 1;
	return 0;
}

// this could get complex later on.
// Destroy all linked exits from or to a room.
void relink_and_destroy(victim)
	object *victim;
{
	object *o, *onext = NULL;
	int quota = 0;
	
	quota = atoi(atr_get(victim->ownit, A_QUOTA));
	for(o = exit_list; o; o=onext) {
		onext = o->next;
		if(o->linkto == victim || o->location == victim) {
			del_content(o->location, o);
			delete_object(&exit_list, &exitlist_tail, o);
			quota++;
		}
	}
	
	SET_ATR(victim->ownit, A_QUOTA, pstr("%d", quota));
	
	// first we check the link, if it's to the victim, we set it to 0
	// next we check the loc, if it's to the victim, we set it to the link.
	// then we move them.
	
	for(o=player_list; o; o=o->next) {
		if(o->linkto == victim) {
			o->link = 0;
			o->linkto = linkit(o);
		}
		if(o->location == victim) {
			move_to(o, o->linkto);
			o->loc = o->link;
			o->location = o->linkto;
		}
	}
	
	for(o=thing_list; o; o=o->next) {
		if(o->linkto == victim) {
			o->link = o->owner;
			o->linkto = o->ownit;
		}
		if(o->location == victim) {
			move_to(o, o->linkto);
			o->loc = o->link;
			o->location = o->linkto;
		}
	}
}

// this is the overall recycle function.
void do_recycle(player, cmdswitch, arg1, arg2)
	object *player;
	char *cmdswitch;
	char *arg1;
	char *arg2;
{
	object *victim;
	int quota = 0;
	
	if(*cmdswitch) {
	  if(string_prefix(cmdswitch, "item")) {
	    if(!has_pow(player, player, POW_COMBAT)) {
	      notify(player, "You do not have the ability to modify combat properties.");
	      return;
	    }
	    RemoveTemplate(player, arg1);
	    return;
	  }
	}
	
	// can't destroy nothing.
	if(!(victim = match_object(arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}

	// now this would totally screw us over - don't think so bub.
	if(victim == rootobj || victim->ref == 0) {	
		error_report(player, M_EWAYBAD);
		return;
	}
	
	// make sure they can do it.
	// can't do it if they don't own it, or are the victim.
	// also can't do it if the victim isn't owned by them and they aren't a wizard)
	if(!can_control(player, victim)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	if(victim->flags & LOCKED) {
		error_report(player, M_EISLOCKED);
		return;
	}
	
	quota = atoi(atr_get(victim->ownit, A_QUOTA));	
	// figure out what type of object they are and delete them.
	if(victim->flags & TYPE_PLAYER) {
		// tell them to use the player_nuke procedure, we don't want people accidentally
		// nuking someone. let's make it precycle. Yah, that's good.
		perm_denied(player, "Use the precycle function, instead.");
	} else if (victim->flags & TYPE_ROOM) {
		// There should be code in here to do the following:
		//	1. delete all exits linked to and from the victim.
		//	2. set all links to this room to owners (for things) and back to 0 (for players)
		//	3. notify_in(victim, exception, "destroyed this room"); (exception == NULL)
		//	4. Maybe later on do smart exit/enter checking and if the room is located between
		//	   two others, link the other two back together.
		
		// this will relink everything associated with victim and destroy all linked
		// exits.
		notify(player, pstr("[RECYCLE]: Expunging %s from database.",
			color_name(victim, -1)));
		SET_ATR(victim->ownit, A_QUOTA, pstr("%d", ++quota));
		relink_and_destroy(victim);
		delete_object(&room_list, &roomlist_tail, victim);
	} else if (victim->flags & TYPE_EXIT) {
		// this cannot be allowed. this will screw up the exit/entrance ratio. not gonna happen.
		// not gonna eat that broccoli mama. wouldn't be prudent. not at this juncture.
		// notify player that they aren't allowed to delete this victim.
		error_report(player, M_EDELEXIT);
		return;
	} else if (victim->flags & TYPE_THING) {
		// make sure we check that everyone/thing is moved back to where they're supposed to
		// be. 
		notify(player, pstr("[RECYCLE]: Expunging %s (%d) from database.",
			color_name(victim, -1), victim->ref));
		del_content(victim->location, victim);
		SET_ATR(victim->ownit, A_QUOTA, pstr("%d", ++quota));
		relink_and_destroy(victim);
		delete_object(&thing_list, &thinglist_tail, victim);
	}		
		
}

                                        // this searches an entire player's onwership and delete it.
                                        
// this searches an entire player's onwership and delete it.
void find_and_destroy(victim)
	object *victim;
{
	object *o, *onext = NULL;
	
	// let's go through the database and change the owner of any players to themselves.
	for(o=player_list; o; o=o->next)
		if(o->ownit == victim && o->ownit != victim) {
			o->owner = o->ref;
			o->ownit = o;
		}
	
	for(o=room_list; o; o=onext) {
		onext = o->next;
		if(o->ownit == victim) {
			relink_and_destroy(o);
			delete_object(&room_list, &roomlist_tail, o);
		}
	}
	
	onext = NULL;
	for(o=thing_list; o; o=onext) {
		onext = o->next;
		if(o->ownit == victim) {
			del_content(o->location, o);
			delete_object(&thing_list, &thinglist_tail, o);
		}
	}
	
}


// let's just hope the player know's what they're doing.
// because this goes through everything the player owns and wipes it all out.
// this requires a player's password.

void do_precycle(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim;
	
	if(!(victim=match_player(arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	if(!(victim->flags & TYPE_PLAYER)) {
		error_report(player, M_EILLNAME);
		return;
	}
	
	if(!can_control(player, victim)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	if(victim == rootobj) {
		error_report(player, M_EWAYBAD);
		return;
	}
	
	if (player->ref == victim->ref || 
		( (victim->owner != player->ref) && !(player->flags & WIZARD)) ) {
		error_report(player, M_ENOCANDO);
		return;
	}	
	
	if(dcrypt(player->pass, arg2)) {
		error_report(player, M_EBADPASS);
		return;
	}
	
	notify(victim, "[NUKED]: Expunging you from the database.");
	
	boot_off(victim->ref);
	
	if(!victim)
		return;
	find_and_destroy(victim);
	log_channel("log_imp", pstr("%s @nuked %s",
		color_name(player, -1), unparse_object(victim)));
	notify(player, pstr("[RECYCLE]: Expunging %s from the database.",
		color_name(victim, -1)));
	printf("[URGENT!]: %s was player-recycled by %s!", victim->name, player->name);
	del_content(victim->location, victim);
	delete_object(&player_list, &playerlist_tail, victim);
}

void guest_precycle(player, victim)
	object *player;
	object *victim;
{
	// unless this is the root object, no one can call this.
	// also, if the victim is not a guest, not going to recycle it.
	if ( (player != rootobj) || (!(victim->flags & GUEST)) )
		return;
	
	find_and_destroy(victim);
	del_content(victim->location, victim);
	delete_object(&player_list, &playerlist_tail, victim);
}

// One must be SURE which exit he is deleting, so object ids will be FORCED.
void do_remove_exit(player, arg1)
	object *player;
	char *arg1;
{
	object *check;
	
	if(*arg1 == '#') {
	  if (!ISNUM(match_num(arg1+1))) {
            error_report(player, M_EOBJID);
	    return;
	  }
	} else {
	  notify(player, "You must use an exit's dbref.");
	  return;
	}
	
	if(!(check = match_exit(arg1))) {
		error_report(player, M_EBADEXIT);
		return;
	}
	
	if(!can_control(player, check)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	notify(player, pstr("[EXIT]: %s expunged from database.",
		color_name(check, -1)));
	SET_ATR(check->ownit, A_QUOTA, pstr("%d", atoi(atr_get(check->ownit, A_QUOTA)) + 1));
	del_content(check->location, check);
	delete_object(&exit_list, &exitlist_tail, check);
}

// ANYTHING BEYOND THIS SHOULD BE MOVED TO: db/objects.c
// move player to newloc.
int move_to(player, newloc)
	object *player;
	object *newloc;
{
	CONTENTS *exception = NULL;
	
	// check newloc and return if it's an exit.
	if( (newloc->flags & TYPE_EXIT) || (player->flags & TYPE_ROOM) || (player->flags & TYPE_EXIT)) {
		error_report(player, M_EBADLNK);
		return 0;
	}
	
	if(player->combat) {
	  if( (player->combat->cflags & FIGHT) && !(player->combat->cflags & FLEE) ) {
	    notify(player, "You can't move while fighting!");
	    return 0;
	  }
	}
	
	add_exception(&exception, player);
	if(!(player->flags & INVISIBLE))
		notify_in(player->location, exception, pstr("%s has left.", color_name(player, -1)));
	
	del_content(player->location, player);
	player->loc = newloc->ref;
	player->location = newloc;
	add_content(player->location, player);
	if(!(player->flags & INVISIBLE))
		notify_in(player->location, exception, pstr("%s has arrived.", color_name(player, -1)));
	
	do_look(player, "");
	clear_exception(exception);
	
	return 1;
}

// join matched arg1.
void do_join(player, arg1)
	object *player;
	char *arg1;
{
	object *victim;
	
	if(!(victim = match_object(arg1))) {
		error_report(player, M_EILLNAME);
		return;
	}
	
	if(!has_pow(player, victim, POW_JOIN) && !can_control(player, victim)) {
  	  error_report(player, M_ENOCNTRL);
	  return;
	}
	
	move_to(player, victim->location);
}

// summon matched arg1
void do_summon(player, arg1)
	object *player;
	char *arg1;
{
	object *victim;
	
	if(!(victim = match_object(arg1))) {
		error_report(player, M_EILLNAME);
		return;
	}
	
	if(!can_control(player, victim)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	move_to(victim, player->location);
}

void do_enter(player, arg1)
	object *player;
	char *arg1;
{
	object *thing;
	
	if(!(thing = match_partial(player->location, arg1))) {
	  notify(player, "Enter what?");
	  return;
	}
	//if(!(thing = match_object(arg1))) {
	//	error_report(player, M_EILLNAME);
	//	return;
	//}
	
	if(!(thing->flags & ENTER_OK)) {
		if(!can_control(player, thing)) {
			error_report(player, M_ENOENTER);
			return;
		}
	}
	
	move_to(player, thing);
	
}

void do_leave(player)
	object *player;
{
	if(player->location == player) {
	  notify(player, "You're stuck inside yourself; try 'home'.");
	  return;
	}
	
	if((player->location->flags & TYPE_ROOM)) {
		notify(player, "Leave what?");
		return;
	}
	
	move_to(player, player->location->location);
}

void do_home(player)
	object *player;
{
	move_to(player, player->linkto);
}

void do_boot(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim;
	
	if(!*arg1 || !*arg2) {
		error_report(player, M_EINVARG);
		return;
	}
	
	if(!(victim = match_player(arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	if(!can_control(player, victim)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	printf("Player %s booted by %s for..: %s\n", victim->name, player->name, arg2);
	log_channel("log_imp", pstr("* Player %s booted by %s for..: %s",
		color_name(victim, -1), color_name(player, -1), arg2));
	notify(victim, pstr("* You have been booted for...: %s", arg2));
	notify(player, pstr("[BOOT]: You booted %s...",
		color_name(victim, -1)));
	boot_off(victim->ref);
	
}

void do_cboot(player, arg1)
	object *player;
	char *arg1;
{
	DESCRIPTOR *d;
	int des;
	
	IS_ARGUMENT(player, (arg1));
	
	if(!isanumber(arg1)) {
		error_report(player, M_ENOTANUM);
		return;
	}
	
	des = match_num(arg1);
	for(d = descriptor_list; d; d=d->next)
		if(des == d->descriptor)
			d->state = BOOTITOFF;
	log_channel("log_imp", pstr("* %s cbooted descriptor: %d",
		color_name(player, -1), des));
	return;
}


void do_give(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	CONTENTS *c = NULL;
	object *who, *what = NULL;
	long long gold, whogold, playergold;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	if(!(who = remote_match(player, arg1))) {
	  notify(player, "What player?");
	  return;
	}
	
	if(!isanumber(arg2)) {
	  if(!(what = remote_match(player, arg2))) {
	    notify(player, "What are you trying to give?");
	    return;
	  }
	} else {
	  if(!(player->flags & WIZARD) && !has_pow(player, who, POW_FREE) && player == who) {
	    notify(player, "That's kind of silly.");
	    return;
	  }
	  gold = atoll(arg2);
	  playergold = atoll(atr_get(player, A_GOLD));
	  if(gold < 0) { // they want to take gold.
	    gold *= -1;
	    if(!can_control(player, who) && !has_pow(player, who, POW_MODIFY)) {
	      notify(player, "You can't do that.");
	      return;
	    }
	    whogold = atoll(atr_get(who, A_GOLD));
	    if(whogold < gold) {
	      notify(player, "They don't have that much!");
	      return;
	    }
	    SET_ATR(who, A_GOLD, pstr("%lld", whogold - gold));
	    SET_ATR(player, A_GOLD, pstr("%lld", playergold + gold));
	    notify(player, pstr("You took %s %s from %s.", 
	      comma(gold), return_config("myth_currency"), color_name(who, -1)));
	    notify(who, pstr("%s took %s %s from you.", 
	      color_name(player, -1), comma(gold), return_config("myth_currency")));
	    return;
	  } else if (gold == 0) {
	    notify(player, "Why?");
	    return;
	  } else {
	    whogold = atoll(atr_get(who, A_GOLD));
	    if(!(player->flags & WIZARD) && !has_pow(player, who, POW_FREE)) {
	      if(playergold < gold) {
	        notify(player, "You don't have enough.");
	        return;
	      }
	    }
	    SET_ATR(who, A_GOLD, pstr("%lld", whogold + gold));
	    if(!(player->flags & WIZARD) && !has_pow(player, who, POW_FREE))
	      SET_ATR(player, A_GOLD, pstr("%lld", playergold - gold));
	    notify(player, pstr("You gave %s %s %s.", 
	      color_name(who, -1), comma(gold), return_config("myth_currency")));
	    notify(who, pstr("%s just gave you %s %s.",
	      color_name(player, -1), comma(gold), return_config("myth_currency")));
	    return;
	  }
	}
	
	// Giving does not take ownership.
	if(what == who) {
	  notify(player, "No.");
	  return;
	}
	
	for(c = player->contents; c; c=c->next)
	  if(c->content == what)
	    break;
	
	if(!c) {
	  notify(player, "You don't have that to give.");
	  return;
	}
	
	// move_to bypasses any locks.
	move_to(what, who);
	notify(player, pstr("You just gave %s to %s.",
	  color_name(what, -1), color_name(who, -1)));
	notify(who, pstr("%s just gave you %s.",
	  color_name(player, -1), color_name(what, -1)));
}


void do_gold(player, arg1)
	object *player;
	char *arg1;
{
	object *target;
	
	if(!*arg1)
		target = player;
	else {
		if(!(target=match_object(arg1))) {
			error_report(player, M_EILLNAME);
			return;
		}
	}
	
	if(target == player) 
	  notify(player, pstr("You have %s %s.", comma(atoll(atr_get(target, A_GOLD))),
	    return_config("myth_currency")));
	else
	  notify(player, pstr("%s has %s %s.", color_name(target, -1), 
	    comma(atoll(atr_get(target, A_GOLD))), return_config("myth_currency")));
}

void do_foreach(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	char *ptrsrv[NUMARGS];
	char buf[MAX_BUFSIZE], buff[MAX_BUFSIZE], *bf = buff;
	char *k, *x;
	int c;
	
	if((player->flags & HAVEN))
	  return;
	
	for(c = 0; c < NUMARGS; c++)
		ptrsrv[c] = gvar[c];
	
	for(c = 0; c < NUMARGS; c++)
		gvar[c] = "";
	

	pronoun_substitute(buff, player, arg1, player);
	bf = buff + strlen(player->name) + 1;
	strcpy(buf, bf);
	x = buf; 
	c = 0;
	while((k = parse_up(&x, ' '))) {
		gvar[0] = k;
		//filter_command(player, arg2, 0);
		parse_queue(player, arg2, 0, NULL);
		if(player->flags & HAVEN)
		  return;
		c++;
	}
	
	//DEBUG(pstr("Counting %d, letting go of %d, player->clev: %d - %d = %d", c, c, player->clev, c, player->clev - c));
	player->clev -= c;	
	for(c = 0; c < NUMARGS; c++)
		gvar[c] = ptrsrv[c];
	
}

void do_trigger(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who;
	char *ptrsrv;
	char *what, *atr;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	if(!strchr(arg1, '/')) { // we don't have anything to do if not triggering an attribute.
		return;
	}
	
	what = fparse(arg1, '/');
	atr = rparse(arg1, '/');
	
	if(!(who = remote_match(player, what))) {
		notify(player, "Who/what do you want?");
		return;
	}	
	
	
	ptrsrv = gvar[0];
	gvar[0] = arg2;	
	parse_queue(who, parser_atr_get(who, atr), 1, NULL);
	gvar[0] = ptrsrv;
}

void do_switch(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	char *argv[10], buf[MAX_BUFSIZE];
	char *ptrsv[10];
	char *k = buf, buf2[MAX_BUFSIZE];
	int a, c, matched = FALSE;

	for(a = 0; a < 10; a++)
	  ptrsv[a] = gvar[a];
	
	strcpy(buf, arg2);
	for(a = 0; a < 10; a++)
	  argv[a] = parse_up(&arg2, ',');
	
	for(a = 0; a < 10; a++) {
	  if(argv[a]) {
	    exec(&argv[a], buf2, player, cmd_player, 0);
	    argv[a] = stringalloc(buf2);
	  //  //strcpy(argv[a] = (char *) malloc(strlen(buf2)+1), buf2);
	  //  //argv[a] = buf2;
	  } else {
	    argv[a] = NULL;
	  }
	}

	
	pronoun_substitute(buf2, player, arg1, player);

	for(a = 0; (a < 10) && argv[a] && argv[a+1]; a+=2) {
	  if(switch_match_wild(argv[a], buf2 + strlen(player->name) + 1)) {
	      matched = TRUE;
	      for(c = 0; c < 10; c++)
	        gvar[c] = ptrsv[c];
	      parse_queue(player, argv[a+1], 0, NULL);
	  }
	}
        
        
            	 
	if(!matched && argv[a]) 
	  parse_queue(player, argv[a], 0, NULL);

	//for(a = 0; argv[a]; a++)
        //  if(argv[a])
        //    free(argv[a]);
}

void do_talk(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	// there is no remote matching in talk
	// arg2 is not used yet. Maybe later we can add things like:
	// talk <object>=<text>
	object *who;
	
	if(!(who = match_partial_type(player->location, arg1, TYPE_THING))) {
	  notify(player, "Talk to whom?");
	  return;
	}
	
	if(!*(atr_get(who, A_ATALK))) {
	  notify(player, pstr("%s has nothing to say.", color_name(who, -1)));
	  return;
	} else {
	  filter_command(who, atr_get(who, A_ATALK), 0);
	}
}

void do_mutype_toggle(player)
	object *player;
{
	if(atoi(atr_get(player, A_MUTYPE)) == 1) {
	  SET_ATR(player, A_MUTYPE, "0");
	} else {
	  SET_ATR(player, A_MUTYPE, "1");
	}

	notify(player, pstr("Your MU* Token type has been changed to '%s'.", 
	  (atoi(atr_get(player, A_MUTYPE))) ? "MUD" : "MUSH"));
}

void do_inventory(player, arg1)
	object *player;
	char *arg1;
{
	ITEMLIST *i;
	CONTENTS *c;
	object *who;
	
	if(*arg1) {
	  if(!(who = remote_match(player, arg1))) {
	    notify(player, "About whom are you referring?");
	    return;
	  }
	} else {
	  who = player;
	}
	
	if(!can_control(player, who)) {
	  notify(player, "Sorry, you cannot view their inventory.");
	  return;
	}
	
	if(!who->contents && !who->items) {
	  notify(player, pstr("%s are carrying nothing.", (who == player) ? "You" : "They"));
	  return;
	}
	
	notify(player, pstr("%s is carrying:", color_name(who, -1)));
	for(c = who->contents; c; c=c->next) {
	  notify(player, unparse_object(c->content));
	}
	for(i = who->items; i; i=i->next)
	  notify(player, pstr("%d %s", i->onhand, i->name));
}


/** END OF PLAYER.C **/
