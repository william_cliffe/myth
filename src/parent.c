/****************************************************************************
 MYTH - Parent/Children
 Coded by sarukie (12/19/03)
 ---
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int check_parent(who, what)
	object *who;
	object *what;
{
	PARENT *p;
	
	for(p = what->parent; p; p=p->next)
		if(p->what == what)
			return 1;
	
	return 0;
}


PARENT *__add_to_parent(parent, o)
	PARENT *parent;
	object *o;
{
	PARENT *p, *pnew;
	
	for(p = parent; p; p=p->next) {
		if(p->what == o)
			return(parent);
	}
	
	GENERATE(pnew, PARENT);
	pnew->what = o;
	pnew->next = parent;
	parent = pnew;
	
	return(parent);
}

PARENT *__delete_parent(parent, o)
	PARENT *parent;
	object *o;
{
	PARENT *p, *pprev = NULL;
	
	for(p = parent; p; p = p->next) {
		if(p->what == o) 
			break;
		pprev = p;
	}
	
	
	if(!p)
	  return(parent);
	
	if(!pprev)
		parent = p->next;
	else
		pprev->next = p->next;
	
	block_free(p);
	
	return(parent);	
}

void update_parent_attribute(o, what)
	object *o;
	char *what;
{
	char buf[MAX_BUFSIZE], keep[MAX_BUFSIZE];
	char *p = buf, *k, *f = keep;

	strcpy(buf, atr_get(o, A_PARENT));
	strcpy(keep, "");
	while( (k = parse_up(&p, ' ')))
	  if(strcmpm(k, what)) {
	    if(!*f)
	      strcpy(keep, k);
	    else
	      sprintf(keep, "%s %s", keep, k);
	  }
		
	SET_ATR(o, A_PARENT, keep);	
}

void update_child_attribute(o, what)
	object *o;
	char *what;
{
	char buf[MAX_BUFSIZE], keep[MAX_BUFSIZE];
	char *p = buf, *k, *f = keep;

	strcpy(buf, atr_get(o, A_CHILDREN));
	strcpy(keep, "");
	while( (k = parse_up(&p, ' ')))
	  if(strcmpm(k, what)) {
	    if(!*f)
	      strcpy(keep, k);
	    else
	      sprintf(keep, "%s %s", keep, k);
	  }
		
	SET_ATR(o, A_CHILDREN, keep);	
}

void clear_children(what)
	object *what;
{
	PARENT *p, *pnext = NULL;
	
	for(p = what->children; p; p=pnext) {
	  pnext = p->next;
	  update_parent_attribute(p->what, pstr("#%d", what->ref));
	  p->what->parent = __delete_parent(p->what->parent, what);
	  block_free(p);
	}
}

void clear_parent(what)
	object *what;
{
	PARENT *p, *pnext = NULL;
	
	for(p = what->parent; p; p=pnext) {
		pnext = p->next;
		update_child_attribute(p->what, pstr("#%d", what->ref));
		p->what->children = __delete_parent(p->what->children, what);
		block_free(p);
	}
}

void add_parent(what, str, type)
	object *what;
	char *str;
	int type;
{
	object *o;
	
	if(!str || !*str)
		return;

	if(!(o = match_object(str)))
		return;
	
	if(type == 1)
		what->parent = __add_to_parent(what->parent, o);
	else if (type == 2)
		what->children = __add_to_parent(what->children, o);
	
}

/* is what a descendent of who?
*/

int is_descendent(who, what)
	object *who;
	object *what;
{
	PARENT *p;
	
	for(p = who->children; p; p=p->next) {
	  if(p->what == what)
	    return(TRUE);
	  if(is_descendent(p->what, what))
	    return(TRUE);
	}

	return(FALSE);
}

void do_addparent(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who, *what;
	char buf[MAX_BUFSIZE];
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	if(!(who = remote_match(player, arg1))) {
		notify(player, "I'm not sure who you're speaking of.");
		return;
	}
	
	if(!(what = remote_match(player, arg2))) {
		notify(player, "What are you trying to parent?");
		return;
	}
	
	if(!has_pow(player, who, POW_MODIFY) && !can_control(player, who)) {
		notify(player, "Sorry, you can't do that.");
		return;
	}
	
	if( (what->flags & TYPE_PLAYER) || (what->flags & TYPE_EXIT) || (what->flags & TYPE_ROOM)) {
		notify(player, "That cannot be a parent.");
		return;
	}
	
	if( !(what->flags & INHERIT)) {
		notify(player, "Try setting this object INHERIT before continuing.");
		return;
	}

	if( check_parent(who, what) ) { // if what is already a parent, well, you know.
		notify(player, pstr("%s is already a parent of %s.",
			color_name(what, -1), color_name(who, -1)));
		return;
	}

	if(who == what) {
		notify(player, "That's already a parent of itself.");
		return;
	}
	
	if(is_descendent(who, what)) {
	  notify(player, pstr("Sorry, %s is a descendent of %s.",
	    unparse_object(what), unparse_object(who)));
	  return;
	}
	
	if(!*(atr_get(who, A_PARENT)))
		sprintf(buf, "#%d", what->ref);
	else 
		sprintf(buf, "%s #%d", atr_get(who, A_PARENT), what->ref);
	SET_ATR(who, A_PARENT, buf);
	who->parent = __add_to_parent(who->parent, what);
	
	strcpy(buf, "");
	if(!*(atr_get(what, A_CHILDREN)))
		sprintf(buf, "#%d", who->ref);
	else
		sprintf(buf, "%s #%d", atr_get(what, A_CHILDREN), who->ref);
	SET_ATR(what, A_CHILDREN, buf);
	what->children = __add_to_parent(what->children, who);
		
	notify(player, pstr("%s added as a parent to %s.", color_name(what, -1), color_name(who, -1)));
}


void do_delparent(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who, *parent;
	PARENT *p, *pnext = NULL;
	
	if (!(who = remote_match(player, arg1))) {
	  notify(player, "Who?");
	  return;
	}
	
	if(!can_control(player, who) && !has_pow(player, who, POW_MODIFY)) {
	  notify(player, "You cannot do that.");
	  return;
	}
	
	if(!(parent = remote_match(player, arg2))) {
	  notify(player, "What parent?");
	  return;
	}

	if(!who->parent || !parent->children) {
	  notify(player, "No such parent.");
	  return;
	}
	
	for(p = who->parent; p; p=pnext) {
	  pnext = p->next;
	  if(p->what == parent)
	    break;
	}
	
	if(p) {
	    update_parent_attribute(who, pstr("#%d", parent->ref));
	    who->parent = __delete_parent(who->parent, parent);
	} else {
	  notify(player, "That is not a parent.");
	  return;
	}
	
	for(p = parent->children; p; p=pnext) {
	  pnext = p->next;
	  if(p->what == who) {
	    update_child_attribute(parent, pstr("#%d", who->ref));
	    parent->children = __delete_parent(parent->children, who);
	  }
	}
	
	notify(player, pstr("%s has been deleted from %s.", unparse_object(parent), unparse_object(who)));	
}

void do_listparent(player, arg1, arg2)
	object *player;
	char *arg1, *arg2;
{
	PARENT *p;
	object *who;
	
	if(!(who = remote_match(player, arg1))) {
		notify(player, "Who are you talking about?");
		return;
	}
	
	if( (player != who) && !has_pow(player, who, POW_SEEATR)) {
	  notify(player, "You have no power over that.");
	  return;
	}
	
	if(!who->parent && !who->children) {
		notify(player, "They have no children or parents.");
		return;
	}
	
	for(p = who->parent; p; p=p->next)
		notify(player, pstr("%s is a parent of %s", 
			color_name(p->what, -1), color_name(who, -1)));	

	for(p = who->children; p; p=p->next)
	  notify(player, pstr("%s is a child of %s",
	    color_name(p->what, -1), color_name(who, -1)));
	    
}

int DOPARENT(enactor, what, str, triggered)
	object *enactor;	// the object enactor.
	object *what;
	char *str;
	int triggered;
{
	PARENT *p;
	attr *a, *b;

	for(p = what->parent; p; p=p->next) {
	  if(p->what)
	    if(DOPARENT(enactor, p->what, str, triggered)) {
	      return(TRUE);
	    }
	  for(a = p->what->alist; a; a=a->next)
	    if( (a->flags & APROGRAM)) {
	      if(!a->value)
	        continue;
	      if(*a->value == '$') {//genactor
	 	if(listen_match(p->what, a->value + 1, str, 0)) {
	 	  DEBUG(pstr("%s matched for %s!", enactor->name, genactor->name));
	          return(TRUE);
	        }
	      } 
	    }
	}
	return(triggered);
}
