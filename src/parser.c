/****************************************************************************
 MYTH - Parser
 parser.c
 ---
 this is just hacked for now. this is not the eventual REAL softcode system
 i wanted implemented here.
 
 i want to add all the delimiters such as bitwise operators and expressions.


 ****************************************************************************/
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/** #define NUM_FUN_ARGS	NUMARGS **/
#define MAX_LEVEL	1000

FUN *pfunlist = 0;
int lev = 0;
int depth = 0;
//void do_fun P((object *, char **, char *));
ATTR *parent_func P((object *, char *)); // for zone_func

static char *TOUPPER(str)
	char *str;
{
	char *p;
	
	for(p = str; p && *p; p++)
		*p = to_upper(*p);
	
	return(str);
}


static void fun_fdiv(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	double a, b;
	
	a = atof(args[0]);
	b = atof(args[1]);
	
	if(b == 0) {
		strcpy(buf, "Divide by 0 error.");
		return;
	}
	
	sprintf(buf, "%f", a/b);
	
}

static void fun_fabs(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	sprintf(buf, "%f", fabsf(atof(args[0])));
}
        
static void fun_pow(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  sprintf(buf, "%lld", (long long)pow(atoll(args[0]), atoll(args[1])));
}
        
static void fun_fpow(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;  
{
  sprintf(buf, "%f", (float)pow((float)atoll(args[0]), (float)atoll(args[1])));
}

static void fun_fmul(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	sprintf(buf, "%f", atof(args[0]) * atof(args[1]));
}

static void fun_fadd(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	sprintf(buf, "%f", atof(args[0]) + atof(args[1]));
}

static void fun_fsub(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	sprintf(buf, "%f", atof(args[0]) - atof(args[1]));
}

static void fun_add(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	sprintf(buf, "%d", atoi(args[0]) + atoi(args[1]));
}

static void fun_sub(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	sprintf(buf, "%d", atoi(args[0]) - atoi(args[1]));
}

static void fun_mul(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	sprintf(buf, "%d", atoi(args[0]) * atoi(args[1]));
}

static void fun_wmatch(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	char *string = args[0], *word = args[1], *s, *t;
	int c = 0, d = 0;
	
	for(s = string; *s && !d; s++) {
	  c++;
	  while(isspace(*s) && *s) s++;
	  t = s;
	  while(!isspace(*t) && *t) t++;
	  d = (!*t) ? 1 : 0;
	  *t = '\0';
	  if(!strcmpm(s, word)) {
	    sprintf(buf, "%d", c);
	    return;
	  }
	  s = t;
	}
	
	sprintf(buf, "0");
}

static void fun_gt(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int a, b;
	
	a = atoi(args[0]);
	b = atoi(args[1]);
	
	strcpy(buf, (a>b) ? "1" : "0");
}

static void fun_spc(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int num = atoi(args[0]);
	
	if(num <= 0 || num > 950) {
		strcpy(buf, "#-1 Number out of range.");
		return;
	}      

	strcpy(buf, "");
	for(;num > 0; num--)
	  strcat(buf, " ");	
}

static void fun_strlen(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	char *p;
	int len;
	
	for(len = 0, p = args[0]; *p; p++) {
	  len++;
	}

	sprintf(buf, "%d", len);
}

static void fun_ljust(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int num = atoi(args[1]);
	char *text, buff[MED_BUFSIZE], tbuf1[MED_BUFSIZE];
	int leader, i;
	
	if(num < 0 || num > 950) {
	  strcpy(buf, "#-1 Number out of range.");
	  return;
	}
	
	text = args[0];
	leader = num - strlen(strip_color(args[0]));
	
	if(leader <= 0) {
	  strcpy(buf, format_color(text, num));
	  return;
	}
	
	if(leader > 950)
	  leader = 950;
	
	for(i = 0; i < leader; i++)
	  tbuf1[i] = ' ';
	
	tbuf1[i] = '\0';
	sprintf(buff, "%s%s", text, tbuf1);
	strcpy(buf, buff);
}

static void fun_mid(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int l=atoi(args[1]), len = atoi(args[2]);
	
	if((l <0) || (len < 0) || ((len + l) > 1000)) {
	  strcpy(buf, "#-1 Number out of range.");
	  return;
	}
	
	if(l < strlen(args[0]))
	  strcpy(buf, args[0] + l);
	else
	  *buf = 0;
	
	buf[len] = 0;
}

static void fun_time(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	strcpy(buf, get_date(time(0)));
}

static void fun_stime(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	if(!*args[0]) {
	  strcpy(buf, "");
	  return;
	}
	
	if(atoi(args[1]) == 1)
	  sprintf(buf, "%s", time_format(atol(args[0])));
	else
	  sprintf(buf, "%s", count_idle(atol(args[0])));
	
}

static void fun_getdate(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	strcpy(buf, get_date(atol(args[0])));
}

static void fun_mtime(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	struct timeval tv1;

	gettimeofday(&tv1, NULL);
	sprintf(buf, "%ld.%06ld", tv1.tv_sec, tv1.tv_usec); 
}

static void fun_div(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	int x = atoi(args[1]);
	
	if(x == 0)
		x = 1;
	sprintf(buf, "%d", atoi(args[0]) / x);
}


static void fun_first(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	char *bf;
	
	if(!args[0]) {
		strcpy(buf, "");
		return;
	}
	
	bf = fparse(args[0], ' ');
	strcpy(buf, bf);
}

static void fun_remove(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	char *s = buf, *t = args[0];
	int w = atoi(args[1]), num = atoi(args[2]), i;
	
	if(w < 1) {
		strcpy(buf, "OUT OF RANGE");
		return;
	}
	
	for(i = 1; i < w && *t; i++) {
		while(*t && *t != ' ') *s++ = *t++;
		while(*t && *t == ' ') *s++ = *t++;
	}
	
	for(i = 0; i < num && *t; i++) {
		while(*t && *t != ' ') t++;
		while(*t && *t == ' ') t++;
	}
	
	if(!*t) {
		if( s != buf) s--;
		*s = '\0';
		return;
	}
	
	while((*s++ = *t++));
	return;
}

static void fun_lnum(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int num = atoi(args[0]);
	int i;
	
	if(num < 1 || num > 256) {
		strcpy(buf, "#-1 Number out of range.");
		return;
	}
	
	strcpy(buf, "0");	
	for(i = 1; i < num; i++)
		sprintf(buf, "%s %d", buf, i);
}

static void fun_strcat(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	sprintf(buf, "%s%s", args[0], args[1]);
}

static void fun_cname(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	object *who;
	
	if(!(who = remote_match(doer, args[0])))
		strcpy(buf, "#-1");
	else 
		sprintf(buf, "%s", color_name(who, -1));
}

static void fun_name(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	object *who;
	
	if(!(who = remote_match(doer, args[0])))
	  strcpy(buf, "#-1");
	else 
	  sprintf(buf, "%s", who->name);
}

static void fun_num(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	object *who;
	
	if(!(who = remote_match(player, args[0]))) {
	  strcpy(buf, "#-1");
	  return;
	}
	
	sprintf(buf, "#%d", who->ref);
}

static void fun_stripcolor(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	strcpy(buf, strip_color(args[0]));
}


static void fun_eval(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	char *bf;
	
	pronoun_substitute(buf, player, args[0], player);
	bf = buf + strlen(player->name) + 1;
	strcpy(buf, bf);
}

static void fun_get(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	object *who;
	
	if(!(who = match_special(doer, args[0])))
		if(!(who = remote_match	(doer, args[0]))) 
		{
			strcpy(buf, "");
			return;
		}

	sprintf(buf, "%s", parser_atr_get(who, args[1]));
}

static void fun_lwho(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	DESCRIPTOR *d;
	char buff[MAX_BUFSIZE];
	
	*buff = '\0';
	
	for(d = descriptor_list; d; d=d->next) {
	  if(!d) continue;
	  if(d->state == CONNECTED && d->player) {
	    if( !(d->player->flags & INVISIBLE) || can_control(player, d->player)) {
	      if(*buff)
		strcpy(buff, pstr("%s #%d", buff, d->player->ref));
	      else
	        sprintf(buff, "#%d", d->player->ref);
	    }
	  }
	}
	
	strcpy(buf, buff);
	
}

static void fun_ldesc(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  DESCRIPTOR *d = NULL;
  char buff[MAX_BUFSIZE];

  *buff = '\0';
  for(d = descriptor_list; d; d=d->next) {
    if(!d) continue;
    if(d->state == CONNECTED && d->player) {
      sprintf(buff + strlen(buff), "%d ", d->descriptor); 
    }
  }
  
  buff[strlen(buff)-1] = '\0';
  strcpy(buf, buff);
}

static void fun_idesc(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  DESCRIPTOR *d = NULL;
  int desc = atoi(args[0]);
  
  if(!has_pow(player, player, POW_SECURITY)) { return; }
  
  if(desc <= 0) { return; }
  
  for(d = descriptor_list; d; d=d->next)
    if(d->descriptor == desc) break;
   
  if(!d) { return; }
  
  if(string_prefix(args[1], "onfor"))
    sprintf(buf, "%ld", time(0) - d->connected_at);
  else if (string_prefix(args[1], "idle"))
    sprintf(buf, "%ld", time(0) - d->last_time);
  else if (string_prefix(args[1], "name"))
    sprintf(buf, "%s", d->player->name);
  else if (string_prefix(args[1], "cname"))
    sprintf(buf, "%s", atr_get(d->player, A_RAINBOW));
  else if (string_prefix(args[1], "dbref"))
    sprintf(buf, "#%d", d->player->ref);
  else
    sprintf(buf, "Illegal flag.");
}

// istrue
static int istrue(str)
	char *str;
{
	return( ( (strcmp(str, "#-1")==0) || (strcmp(str, "")==0) ||
		(strcmp(str, "#-2")==0) ||
		((atoi(str) == 0) && isdigit(str[0]))) ? 0 : 1);	
}

static void fun_if(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	if(istrue(args[0]))
		sprintf(buf, "%s", args[1]);
	else
		*buf='\0';
	
	return;
}

static void fun_ifelse(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	if(istrue(args[0]))
		sprintf(buf, "%s", args[1]);
	else
		sprintf(buf, "%s", args[2]);
	
	return;
}


static void fun_match(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	int a;
	int wcount = 1;
	char *s = args[0];
	char *ptrsrv[NUM_FUN_ARGS];
	
	for(a = 0; a < NUM_FUN_ARGS; a++)
		ptrsrv[a] = gvar[a];
	
	do {
		char *r;
		
		while(*s && (*s == ' '))
			s++;
		r = s;
		
		while(*s && (*s != ' '))
			s++;
		if(*s)
			*s++ = 0;
		
		// convert both cases.
		TOUPPER(args[1]);
		TOUPPER(r);	
		if(match_wild(args[1], r)) {
			sprintf(buf, "%d", wcount);
			for(a = 0; a < NUM_FUN_ARGS; a++)
				gvar[a] = ptrsrv[a];
			return;
		}
		wcount++;
	}
	while(*s);

	strcpy(buf, "0");
	for(a = 0; a < NUM_FUN_ARGS; a++)
		gvar[a] = ptrsrv[a];
}


static void fun_tolower(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];	
	object *player;
	object *doer;
	int nargs;
{
	strcpy(buf, lower_case(args[0]));
}

static void fun_foreach(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	char *k;
	char buff[MAX_BUFSIZE];
	char *ptrsrv;
	int i = 0, j = 0;
	
	ptrsrv = gvar[0];
	
	if(!args[0] || !strcmpm(args[0], "")) {
		buf[0] = '\0';
		return;
	}
	
        while((k = parse_up(&args[0], ' ')) && i < 1000)
	{
	  gvar[0] = k;
	  pronoun_substitute(buff, doer, args[1], player);
	  for(j = strlen(doer->name)+1; buff[j] && i < 1000; i++, j++)
	    buf[i] = buff[j];
	  buf[i] = '\0';
	} 
	
	if(i > 0) buf[i] = '\0';
	gvar[0] = ptrsrv;
}


static void fun_v(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	char c, *p;

	if(args[0][0] && args[0][1]) {
	  if( (p = parser_atr_get(player, args[0])) ) {
	    strcpy(buf, p);
	    return;
	  }
	}
	
	c = *args[0];
	switch(*args[0]) 
	{
		case '#':
			sprintf(buf, "#%d", genactor->ref);
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			if(!gvar[c-'0']) {
				strcpy(buf, "");
				break;
			}
			sprintf(buf, "%s", gvar[c-'0']);
			break;
		case 'v':
		case 'V': {
			int a;
			
			if(!args[0][1])
				break;
			a = to_upper(args[0][1]);
			if((a < 'A') || (a > 'Z')) {
				*buf = '\0';
				return;
			}
			strcpy(buf, atr_get(player, A_VA + (a-65))); //1215+a));
			break;
		}
		case 'p':
		case 'P':
			if(string_prefix(atr_get(player, A_SEX), "male"))
				strcpy(buf, "his");
			else if (string_prefix(atr_get(player, A_SEX), "female"))
				strcpy(buf, "her");
			else
				strcpy(buf, "eir");
			break;
		case 's':
		case 'S':
			if(string_prefix(atr_get(player, A_SEX), "male"))
				strcpy(buf, "he");
			else if (string_prefix(atr_get(player, A_SEX), "female"))
				strcpy(buf, "she");
			else
				strcpy(buf, "it");
			break;
		case 'o':
		case 'O':
			if(string_prefix(atr_get(player, A_SEX), "male"))
				strcpy(buf, "him");
			else if (string_prefix(atr_get(player, A_SEX), "female"))
				strcpy(buf, "her");
			else
				strcpy(buf, "it");
			break;		
		case 'n':
		case 'N':
			strcpy(buf, strip_color(genactor->name));
			break;
		default:
			strcpy(buf, "");
			break;
	}
		
}


static void fun_idle(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	object *who;
	DESCRIPTOR *d;
	
	if(!args[0]) {
		strcpy(buf, "");
		return;
	}
	
	if(!(who = remote_match(doer, args[0]))) {
		strcpy(buf, "");
		return;
	}
	
	for(d = descriptor_list; d; d=d->next)
	  if(d->player && d->state == CONNECTED)
	    if(d->player == who) {
	      sprintf(buf, "%ld", time(0) - d->last_time);
	      return;
	    }

	strcpy(buf, "-1");
}

static void fun_onfor(buf, args, player, doer, nargs)
	char *buf;
	char *args[NUM_FUN_ARGS];
	object *player;
	object *doer;
	int nargs;
{
	object *who;
	DESCRIPTOR *d;
	
	if(!args[0]) {
		strcpy(buf, "");
		return;
	}
	
	if(!(who = remote_match(doer, args[0]))) {
		strcpy(buf, "");
		return;
	} 
	
	for(d = descriptor_list; d; d=d->next)
	  if(d->state == CONNECTED && d->player) {
	    if(d->player == who) 
	      sprintf(buf, "%ld", time(0) - d->connected_at);
	  }
}


static void fun_parents(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	object *who;
	
	if(!(who = remote_match(player, args[0])))  {
		strcpy(buf, "#-1");
		return;
	}
	
	strcpy(buf, atr_get(who, A_PARENT));
}

static void fun_children(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	object *who;
	
	if(!(who = remote_match(player, args[0])))  {
		strcpy(buf, "#-1");
		return;
	}
	
	strcpy(buf, atr_get(who, A_CHILDREN));
}

static void fun_zone(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	object *who;
	
	if(!(who = remote_match(player, args[0]))) {
	  strcpy(buf, "#-1");
	  return;
	}
	
	strcpy(buf, atr_get(who, A_ZONE));
}

static void fun_inzone(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	object *who, *o;
	
	if(!(who = remote_match(player, args[0]))) {
	  strcpy(buf, "#-1");
	  return;
	}
	
	*buf = '\0';
	for(o = room_list; o; o=o->next)
	  if(o->zone == who) {
	    if(!strlen(buf)) {
	      strcpy(buf, pstr("#%d", o->ref));
	    } else if (strlen(buf) > 950) {
	      buf[strlen(buf)-1] = '\0';
	      return;
	      strcat(buf, " #-1");
	      return;
	    } else {
	      strcat(buf, pstr(" #%d", o->ref));
	    }
	  }
}

static void fun_zwho(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	CONTENTS *c;
	object *who, *o;
	
	if(!(who = remote_match(player, args[0]))) {
	  strcpy(buf, "#-1");
	  return;
	}
	
	if(!(who->flags & TYPE_THING) || !(who->flags & ZONE)) {
	  strcpy(buf, "#-1 Not a zone.");
	  return;
	}
	
	strcpy(buf, "");
	for(o = room_list; o; o=o->next)
	  if(o->zone == who) {
	    for(c = o->contents; c; c=c->next) {
	      if(c->content->flags & TYPE_PLAYER) {
	        if(!strlen(buf)) {
	          strcpy(buf, pstr("#%d", c->content->ref));
	        } else if (strlen(buf) > 950) {
	          strcat(buf, " #-1");
	          return;
	        } else {
	          strcat(buf, pstr(" #%d", c->content->ref));
	        }
	      }
	    }
	  }
}

static void fun_loc(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	object *loc;
	
	if(!(loc = remote_match(player, args[0]))) {
		strcpy(buf, "#-1");
		return;
	}
	
	sprintf(buf, "#%d", loc->location->ref);
}

static void fun_string(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int idx;
	int lmt = atoi(args[1]);
	
	if(lmt < 0 || (lmt * strlen(args[0])) > 900) {
	  strcpy(buf, "#-1 String too long.");
	  return;
	}
	
	strcpy(buf, "");
	for(idx = 0; idx < lmt; idx++)
		strcat(buf, args[0]);	
}

static void fun_controls(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	object *who, *what;
	
	if(!(who = remote_match(player, args[0]))) {
	  strcpy(buf, "#-1");
	  return;
	}
	
	if(!(what = remote_match(player, args[1]))) {
	  strcpy(buf, "#-1");
	  return;
	}
	
	if(!can_control(who, what)) 
	  strcpy(buf, "0");
	else
	  strcpy(buf, "1");
}

static void fun_extract(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int start = atoi(args[1]);
	int len = atoi(args[2]);
	char *s = args[0], *r;
	
	if( (start < 1) || (len < 1)) {
		*buf = 0;
		return;
	}
	
	start--;
	while(start && *s) {
	  while(*s && (*s == ' '))
	    s++;
	  while(*s && (*s != ' '))
	    s++;
	  start--;
	}
	while(*s && (*s == ' '))
	  s++;
	
	r = s;
	
	while(len && *s) {
	  while(*s && (*s == ' '))
	    s++;
	  while(*s && (*s != ' '))
	    s++;
	  len--;
	}
	*s = 0;
	
	strcpy(buf, r);
}

static void fun_random(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int rnum;
	
	if( (rnum = atoi(args[0])) <= 0)
	  strcpy(buf, "0");
	else
	  sprintf(buf, "%ld", random()%atoi(args[0]));	
}

static void fun_wcount(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	int wcount = 0;
	char *s;
	
	if(!*args[0]) {
	  sprintf(buf, "0");
	  return;
	}
	  
	for(wcount = 1, s = args[0]; *s; s++) {
	  if(*s == ' ') {
	    if(*(s-1) == ' ') {
	      continue;
	    } else {
	      wcount++;
	    }
	  }
	}
	
	sprintf(buf, "%d", wcount);
}

static void fun_lcon(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  object *what;
  CONTENTS *list;
  
  if(!(what = remote_match(doer, args[0]))) {
    strcpy(buf, "#-1 - Invalid object");
    return;
  }

  *buf = '\0';
  
  if(!what->contents)
    return;
  
  for(list = what->contents; list; list=list->next)
    if(list->content->ref != what->ref)
      sprintf(buf + strlen(buf), "#%d ", list->content->ref);
  
  if(*buf)
    buf[strlen(buf) - 1] = '\0';
}

static void fun_lobj(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  object *what;
  int i;
  
  if(!(what = remote_match(doer, args[0]))) {
    strcpy(buf, "#-1 - Invalid object");
    return;
  }
  
  *buf = '\0';
  
  for(i = 0; i < dbtop; i++) {
    if(!(db[i]))
      continue;
    if( (db[i])->owner == what->ref)
      sprintf(buf + strlen(buf), "#%d ", (db[i])->ref);
  }
  
  if(*buf)
    buf[strlen(buf) - 1] = '\0';
}

static void fun_chr(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  unsigned char chr = atoi(args[0]);

  sprintf(buf, "%c", chr);
}          

static void fun_lattr(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  ATTR *a;
  object *what;
  
  if(!(what = remote_match(doer, args[0])))
    return;
  
  *buf = '\0';
  
  for(a = what->alist; a; a=a->next)
    sprintf(buf + strlen(buf), "%s ", a->name);
  
  if(*buf) 
    buf[strlen(buf)-1] = '\0';
}
      

static void fun_lflags(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  object *what;
  
  if(!(what = remote_match(doer, args[0])))
    return;
    
  strcpy(buf, flaglist(what));
}      

static void fun_lexits(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  object *loc;
  CONTENTS *l;
  
  if(!(loc = remote_match(doer, args[0])))
    return;

  *buf = '\0';  
  
  for(l = loc->contents; l; l=l->next)
  {
    if((l->content->flags & TYPE_EXIT)) { sprintf(buf + strlen(buf), "#%d ", l->content->ref); }
  }
  
  if(*buf) { buf[strlen(buf) - 1] = '\0'; }
   
}

static void fun_hasflag(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  object *what;
  char *flist;
  
  if(!*args[0] || !*args[1])
    return;
  
  if(!(what = remote_match(doer, args[0])))
    return;
    
  if(!(flist = flaglist(what)))
    return;

  if(!strchr(flist, (char)*args[1]))
    strcpy(buf, "0");
  else
    strcpy(buf, "1");
}

static void fun_noop(buf, args, player, doer, nargs)
	char *buf, *args[NUM_FUN_ARGS];
	object *player, *doer;
	int nargs;
{
	strcpy(buf, "");
}

static void fun_wordwrap(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  unsigned int word_at = atoi(args[1]), idx = 0;
  char *c = args[0];
  
  while(*c) {
    if(idx++ > word_at) {
      while(!isspace(*c) && *c) c--;
      *c = '\n';
      idx = 0;
    }
    c++;
  }
    
  strcpy(buf, args[0]);
}

// FUNLIST

FUN funlist[]=
	{
	{"add", fun_add, 2},
	{"sub", fun_sub, 2},
	{"mul", fun_mul, 2},
	{"div", fun_div, 2},
	{"fadd", fun_fadd, 2},
	{"fsub", fun_fsub, 2},
	{"pow", fun_pow, 2},
	{"fpow", fun_fpow, 2},
	{"fmul", fun_fmul, 2},
	{"fdiv", fun_fdiv, 2},
	{"fabs", fun_fabs, 1},
	{"strcat", fun_strcat, 2},
	{"name", fun_name, 1},
	{"cname", fun_cname, 1},
	{"get", fun_get, 2},
	{"if", fun_if, 2},
	{"ifelse", fun_ifelse, 3},
	{"match", fun_match, 2},
	{"lwho", fun_lwho, 0},
	{"ldesc", fun_ldesc, 0},
	{"idesc", fun_idesc, 2},
	{"tolower", fun_tolower, 1},
	{"foreach", fun_foreach, 2},
	{"v", fun_v, 1},
	{"stripcolor", fun_stripcolor, 1},
	{"idle", fun_idle, 1},
	{"onfor", fun_onfor, 1},
	{"remove", fun_remove, 3},
	{"first", fun_first, 1},
	{"lnum", fun_lnum, 1},
	{"time", fun_time, 0},
	{"getdate", fun_getdate, 1},
	{"mtime", fun_mtime, 0},
	{"parents", fun_parents, 1},
	{"children", fun_children, 1},
	{"gt", fun_gt, 2},
	{"ljust", fun_ljust, 2},
	{"strlen", fun_strlen, 1},
	{"mid", fun_mid, 3},
	{"spc", fun_spc, 1},
	{"inzone", fun_inzone, 1},
	{"zwho", fun_zwho, 1},
	{"string", fun_string, 2},
	{"loc", fun_loc, 1},
	{"num", fun_num, 1},
	{"wmatch", fun_wmatch, 2},
	{"controls", fun_controls, 2},
	{"extract", fun_extract, 3},
	{"zone", fun_zone, 1},
	{"eval", fun_eval, 1},
	{"rand", fun_random, 1},
	{"wcount", fun_wcount, 1},
	{"stime", fun_stime, 2},
	{"lcon", fun_lcon, 1},
	{"lobj", fun_lobj, 1},
	{"hasflag", fun_hasflag, 2},
	{"lflags", fun_lflags, 1},
	{"lattr", fun_lattr, 1},
	{"chr", fun_chr, 1},
	{"noop", fun_noop, 1},
	{"lexits", fun_lexits, 1},
	{"wordwrap", fun_wordwrap, 2},
	{NULL},
	};



FUN fun_hash_table[5001];

unsigned int hash_buckets[256];

static void fill_buckets()
{
	int i, j;
	int s, k = 7;

	for(i = 0; i < 256; i++)
	  hash_buckets[i] = 0;
	
	for(i = 0; i < 255; i++)
	  hash_buckets[i] = i;

	for(j =0; j < 4; j++) {
	  for(i = 0; i < 255; i++) {
	    s = hash_buckets[i];
	    k = (k + s) % 256;
	    hash_buckets[i] = hash_buckets[k];
	    hash_buckets[k] = s;
	  }
	}
}

int hash(key)
	char *key;
{
	int i;
	unsigned int hash = 0;
	char *s;
	
	for(i = 0, s = key; *s; s++,i++) {
	  hash = hash_buckets[hash^key[i]];
	  //hash = (hash ^ key[i]) + ((hash << 26) + (hash >> 6));
	}
	
	return hash;
}

void insert_func_hash(void)
{
  FILE *fp;
  int i;
  unsigned int h;
  char *s;
  char buf[1024];
  unsigned int prime = 4999;
  
  for(i = 0; i < 5001; i++)
    fun_hash_table[i].name = NULL;
  
  if(!(fp = fopen("wordlist.txt", "r"))) {
    DEBUG("error opening file.");
    return;
  }
  
  while(!feof(fp)) {
    strcpy(buf, getstring(fp));
    for(s = buf; *s; s++)
      h += *s;
    h = h % prime;
    if(fun_hash_table[h].name && *fun_hash_table[h].name) {
      DEBUG(pstr("%s collides with %s!", fun_hash_table[i].name, buf));
    } else {
      fun_hash_table[h].name = stralloc(buf);
    }
  }
  
  fclose(fp);
  
    
  
}

void Xinsert_func_hash(void)
{
	int i, h;
	int col = 0;
	
	for(i = 0; i < 256; i++)
	  fun_hash_table[i].name = NULL;
	//for(i = 0; i < 256; i++)
	//  hash_buckets[i] = 0;
	  
	fill_buckets();
		
	for(i = 0; funlist[i].name; i++) {
	  //fill_buckets();
	  h = hash(funlist[i].name);
	    
	  if(fun_hash_table[h].name) {
	    DEBUG(pstr("collision of %s against %s with hash %d",
	      fun_hash_table[h].name, funlist[i].name, h));
	    col++;
	  }
	  fun_hash_table[h] = funlist[i];
	  //DEBUG(pstr("FUNC %s (%d)", fun_hash_table[h].name, h));
	  
	  //DEBUG(pstr("HASH returns %d", h));
	}
	
	DEBUG(pstr("There were %d functions and %d collisions.", i, col));
}

FUN *addFunc(FUN *f)
{
  FUN *fnew;
 
  GENERATE(fnew, FUN);

  SET(fnew->name, f->name);
  fnew->func = f->func;
  fnew->nargs = f->nargs;  

  fnew->next = pfunlist;
  pfunlist = fnew;
}

void removeFunc(FUN *frem)
{
  FUN *f, *fprev = NULL;
  
  for(f = pfunlist; f; f=f->next)
  {
    if(f == frem) break;
    fprev = f;
  }
  
  if(!f)
    return;
    
  if(!fprev)
    pfunlist = f->next;
  else
    fprev->next = f->next;
  
  if(f->name) block_free(f->name);
  block_free(f);
}

void delFunc(char *func)
{
  FUN *f, *fnext;
  
  for(f = pfunlist; f; f=fnext)
  {
    fnext = f->next;
    if(!strcmpm(f->name, func)) {
      removeFunc(f);
    }
  }
}

// *** !!!!!!! FUNCTIONS WILL NOT WORK WITHOUT +FUNHASH RUN FIRST! !!!!!!! *** //
FUN *lookup_funcs(buf)
	char *buf;
{
	FUN *f;
	int i; //=hash(buf);
	
	for(i = 0; funlist[i].name; i++)
	  if(!strcmpm(funlist[i].name, buf))
	    return &funlist[i];

        // Added to support multi function calling from various plugins
        for(f = pfunlist; f; f=f->next)
        {
	  if(!strcmp(f->name, buf)) { return f; }         
        }
	
	//if(!fun_hash_table[i].name)
	//   return(NULL);
	  
	//if(!strcmpm(fun_hash_table[i].name, buf))
	//  return &fun_hash_table[i];	
	
	return(NULL);
}


ATTR *zone_func(privs, atrname)
	object *privs;
	char *atrname;
{
	if(!privs->location->zone)
	  return FALSE;
	
	return(parent_func(privs->location->zone, atrname));
}

ATTR *parent_func(privs, atrname)
	object *privs;
	char *atrname;
{
	PARENT *p;
	ATTR *a, *aret = NULL;
	
	for(p = privs->parent; p; p=p->next) {
	  if(p->what)
	    if((aret = parent_func(p->what, atrname)))
	      return(aret);
	  for(a = p->what->alist; a; a=a->next)
	    if( (a->flags & AFUNC) )
	      if(!strcmpm(a->name, atrname))
	        return(a);
	}
	
	return(NULL);
}

int udef_fun(str, buff, privs, doer)
	char **str;
	char *buff;
	object *privs;
	object *doer;
{
	char obuff[MED_BUFSIZE], result[MED_BUFSIZE];
	char *args[NUM_FUN_ARGS], *saveptr[NUM_FUN_ARGS];
	int c;
	ATTR *a = NULL;
	PARENT *p;

        for(c = 0; c < NUM_FUN_ARGS; c++) args[c] = NULL;

	for(a = privs->alist; a; a=a->next)
	  if(a->flags & AFUNC)
	    if(!strcmpm(a->name, buff))
	      break; 
		
	if(!a)
	  if(!(a = parent_func(privs, buff)))
	    if(!(a = zone_func(privs, buff)))
	      return FALSE;
	
	// starts with: [flop(me)] 
	// **str == me)]
	// a = flop
	// me)], [get(v(0),Gold)]
	if(!a->value)
	  return FALSE;
	
	strcpy(obuff, a->value);
	
	for(c = 0; (c < NUM_FUN_ARGS) && **str && (**str != ')'); c++) {
	  if(**str == ',')
	   (*str)++;
	 
	  exec(str, obuff, privs, doer, 1);
	  strcpy(args[c] = (char *)stack_em(strlen(obuff)+1, VOLATILE), obuff);
	}
	
	for(c = 0; c < NUM_FUN_ARGS; c++) {
	  saveptr[c] = gvar[c];
	  gvar[c] = args[c];
	}
	
	if(**str)
	  (*str)++;
	  
	pronoun_substitute(result, doer, a->value, privs);
	for(c = 0; c < NUM_FUN_ARGS; c++) 
	  gvar[c] = saveptr[c];

	strcpy(buff, result + strlen(doer->name) + 1);
	 
	return TRUE;	
}

// what do we do in here?
/* do_fun();

	Let's make this procedure run through the 
*/
void do_fun(str, buff, privs, doer)
	char **str;
	char *buff;
	object *privs;
	object *doer;
{
	FUN *fp;
	char *args[NUM_FUN_ARGS];
	char obuff[MED_BUFSIZE], buf[MED_BUFSIZE];
	int a;
	
	for(a = 0; a < NUM_FUN_ARGS; a++) args[a] = NULL;
	
	strcpy(obuff, buff);

	if(!(fp = lookup_funcs(obuff))) {
		if(udef_fun(str, buff, privs, doer)) {
		  return;
		} else {
		int deep = 2;
		char *s = buff + strlen(obuff);
		strcpy(buff, obuff);
		*s++ = '(';
		while(**str && deep)
			switch(*s++=*(*str)++) 
			{
				case '(':
					deep++;
					break;
				case ')':
					deep--;
					break;
			}
		if(**str)
		{
			(*str)--;
			s--;
		}
		*s = 0;
		return;
		}
	}
	
	for(a = 0; (a < NUM_FUN_ARGS) && **str && (**str != ')'); a++)
	{
		if(**str == ',')
			(*str)++;
		
		exec(str, obuff, privs, doer, 1);
		strcpy(args[a] = (char *)stack_em(strlen(obuff)+1, VOLATILE), obuff);
	}
	
	if(**str)
		(*str)++;
	
	if( (fp->nargs != -1) && (fp->nargs != a) )
		sprintf(buff, "#-1 FUNCTION ONLY ACCEPTS %d ARGS", fp->nargs);
	else
		fp->func(buff, args, privs, doer, a);
}



// our current hacked in softcode system.
void exec(str, buff, privs, doer, comma)
	char **str;
	char *buff;
	object *privs;
	object *doer;
	int comma;
{
	char *s, *e = buff;
	int x = 0;
	
	lev+=2;
	if(lev > 900) {
	  strcpy(buff, "Too many levels of recursion.");
	  return;
	}
	*buff = 0;
	for(s = *str; *s && isspace(*s); s++);
	
	for(;*s; s++) {
		switch(*s)
		{
			case ',':
			case ')':
				if(!comma)
					goto cont;
			case ']': // end
				while((--e>=buff) && isspace(*e));
				e[1] = 0;
				*str = s;
				lev--; return;
			case '(':
				while((--e>=buff) && isspace(*e));
				e[1] = 0;
				*str = s + 1;
				if(*buff) 
					do_fun(str, buff, privs, doer);
				lev--; return;
			case '{':
				if(e==buff) {
					int deep = 1;
					e = buff;
					s++;
					while(deep && *s)
						switch(*e++ = *s++) {
							case '{':
								deep++;
								break;
							case '}':
								deep--;
								break;
						}
					if((e>buff) && (e[-1]=='}'))
						e--;
					while((--e>=buff) && isspace(*e));
					e[1]=0;
					*str = s;
					lev--; return;
				} else {
					int deep = 1;
					*e++ = *s++;
					while(deep & *s)
						switch(*e++ = *s++) {
							case '{':
								deep++;
								break;
							case '}':
								deep--;
								break;
						}
					s--;
				}
				break;
			default:
			cont:
				*e++ = *s;
				break;
		}
	}
	
	while((--e>=buff)&&isspace(*e));
	e[1]=0;
	*str = s;
	lev--; return;
}
	
/*
	MYTH expression parser. This will parse the expression, run it through do_fun, and etc.
	
	( - Opening function bracket
	) - Closing function bracket
	[ - Opening expression bracket
	] - Closing expression bracket
	, - Argument separator.
	
	// possible operators.
	| - OR bitwise operator.
	& - AND bitwise operator.
	|| - OR expression.
	&& - AND expression.
*/ 

void __exp_parser(player, buf, code)
	object *player;
	char *buf;
	char **code;
{
	//char buf[8000];
	char *r=buf, *s;
	char *foo;
	
	*buf = 0;
	while(**code) {
	  switch(**code) {
	    case '%':
	      (*code)++;
	      break;
	    case '$':
	      (*code)++;
	      break;
	    case '[':
	      (*code)++;
	      foo = parse_it(code, ']');
	      s = foo;
	      DEBUG(pstr("BEFORE NEXT EXP"));
	      __exp_parser(player, buf, &s);
	      r = buf + strlen(buf);
	      DEBUG("AFTER EXP");
	      break;
	    case '(':
	      (*code)++;
	      break;
	    case '{':
	      (*code)++;
	      break;
	    default:
	      *r++ = *(*code)++;
	      break;
	  }
	  //(*code)++;
	}	
	
	//*r++ = 0;
	//DEBUG(pstr("%s", buf));
}

void exp_parser(player, code)
	object *player;
	char *code;
{
	char buf[8000];
	strcpy(buf, "");
	
	__exp_parser(player, buf, &code);
	DEBUG(pstr("%s", buf));
}
void func_zerolev()
{
	lev = 0;
}

void do_funlist(player)
	object *player;
{
	int i;

	for(i = 0; funlist[i].name; i++);
	
	notify(player, pstr("There are %d functions. To list them, type 'help functions'.", i));
}

void do_atat()
{
	return;
}

void init_startups()
{
	int i;
	
	for(i = 0; i < dbtop; i++)
	  if((db[i])) {
	    if(*atr_get(db[i], A_STARTUP)) {
	      parse_queue(db[i], atr_get(db[i], A_STARTUP), 0, NULL);
	    }
	  }
}
