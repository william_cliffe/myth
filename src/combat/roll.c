#ifdef COMBAT
/****************************************************************************
 MYTH - Rolling System
 Coded by Saruk (03/22/99)
 ---
 This is the die-rolling system. It will need constant revision, because combat
 is not yet finished.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// We specify a type because different combat styles rely on diffrent attributes.
int die_roll(x)
	int x;
{
	return(3 + (random() % ( (!x) ? 1 : x)));
}

/* All we're trying to do here is to see if we can cast a spell on someone else. */
int can_cast(enactor, victim)
	object *enactor;
	object *victim;
{
	STATS *p, *v;
	int die[2];
	
	p = enactor->combat->stats;
	v = victim->combat->stats;
	die[0] = die_roll(v->level);
	die[1] = die_roll(p->level);
	
	if(die[0] > die[1]) 
		return 0;
	else if(die[0] == die[1]) { // we have a 50/50 chance of castability (heh!).
		if(die_roll(random()%15) > 6)
			return 1;
		else
			return 0;
	}
	
	return 1;
}

int can_attack(attacker, victim)
	object *attacker;
	object *victim;
{
	int a, b;
	
	a = die_roll(18);
	b = die_roll(18);
	
	if ( (die_roll(attacker->combat->stats->level + b)) > 
		(die_roll(victim->combat->stats->level + a)) )
			return TRUE;
	
	return FALSE;
}

void exp_calculate(winner, loser)
	object *winner;
	object *loser;
{
	STATS *w, *l;
	int gain;
	
	w = winner->combat->stats;
	l = loser->combat->stats;
	
	if( (gain = die_roll((w->level * (l->exp / l->level)))) <= 0)
		gain = MIN_EXP;
		
	w->exp += gain;
	notify(winner, pstr("|Y+|* |C|You receive |C+|%d |C|experience.",
		gain));
}


void calculate_fight(f)
	FIGHT *f;
{
	if(f->flags & FIGHTDONE)
		return;
		
	if(f->attacker->combat->stats->hp <= 0) {
		notify(f->attacker, pstr("|R+|* |C|You have been killed by |X|%s|C|!",
			color_name(f->victim, -1)));
		notify(f->victim, pstr("|R+|* |C|You have killed |X|%s|C|!",
			color_name(f->attacker, -1)));
		/* calculate experience */
		exp_calculate(f->victim, f->attacker);
		f->attacker->combat->dead = time(0);
		f->flags |= FIGHTDONE;
	} 
	
	if (f->victim->combat->stats->hp <= 0) {
		notify(f->victim, pstr("|R+|* |C|You have been killed by |X|%s|C|!", 
			color_name(f->attacker, -1)));
                notify(f->attacker, pstr("|R+|* |C|You have killed |X|%s|C|!",
                	color_name(f->victim, -1)));
                /* calculate experience*/
                f->victim->combat->dead = time(0);
                exp_calculate(f->attacker, f->victim);
                f->flags |= FIGHTDONE;
	}

	f->offense = f->defense = 0;	
}


/* calculate a rolled fight queue */
void roll_calculate(f)
	FIGHT *f;
{
	if(f->flags & VICTIM_D) {
		if(f->offense > f->defense) {
			f->offense = f->offense / 6;
			notify(f->victim, pstr("|X|%s |C|hits you for |C+|%d |C|damage!",
				color_name(f->attacker, -1), f->offense));
			notify(f->attacker, pstr("|C|You hit |X|%s |C|for |C+|%d |C|damage!",
				color_name(f->victim, -1), f->offense));
			f->victim->combat->stats->hp -= f->offense;
		} else {
			notify(f->victim, pstr("|C|You block |X|%s|C|'s attack!",
				color_name(f->attacker, -1)));
			notify(f->attacker, pstr("|C|Your attack is blocked by |X|%s|C|!",
				color_name(f->victim, -1)));
		}
	} else if (f->flags & ATTACKER_D) {
		if(f->offense > f->defense) {
			f->offense = f->offense / 6;
			notify(f->attacker, pstr("|X|%s |C|hits you for |C+|%d |C|damage!",
				color_name(f->victim, -1), f->offense));
			notify(f->victim, pstr("|C|You hit |X|%s |C|for |C+|%d |C|damage!",
				color_name(f->attacker, -1), f->offense));
			f->attacker->combat->stats->hp -= f->offense;
		} else {
			notify(f->attacker, pstr("|C|You block |X|%s|C|'s attack!",
				color_name(f->victim, -1)));
			notify(f->victim, pstr("|C|Your attack is blocked by |X|%s|C|!",
				color_name(f->attacker, -1)));
		}	
	}

	calculate_fight(f);
	
	f->offense = f->defense = 0;
}

/* We have to get this right. f->turn doesn't necessarily mean defense or
   offense, it just means whose turn it is to roll */
void roll_ai(f, roller)
	FIGHT *f;
	object *roller;
{
	int roll = 0;	// what we're going to roll for defense or offense
	
	/* Randomize It All */
	roll = die_roll( (die_roll(roller->combat->stats->str) + 
		   	   die_roll(roller->combat->stats->dex) +
	   	    	    die_roll(roller->combat->stats->lck)) - 
	   	     	     die_roll(roller->combat->stats->sta/3) );
		
	if((f->flags & ATTACKER_A) || (f->flags & VICTIM_A)) {
		f->offense = roll;
		notify_fight(f, roller, stralloc(pstr("|C|rolled an offense of |C+|%d|C|!",
			f->offense)));
	} else {
		f->defense = roll;
		notify_fight(f, roller, stralloc(pstr("|C|rolled a defense of |C+|%d|C|!",
			f->defense)));
		roll_calculate(f);
	}
	
	fight_rollover(f);
}

int rolled_item(f, player, arg1)
	FIGHT *f;
	object *player;
	char *arg1;
{
	ITEMS *i;
	
	if(!arg1 || !*arg1)
		return 0;
	
	for(i = player->combat->items; i; i=i->next)
		if(!strcmpm(i->name, arg1) && (i->flags & EQUIPPED) ) {
			if( (i->flags & WEAPON) && ((f->flags & ATTACKER_A) ||
				(f->flags & VICTIM_A))) {
			// recode this when items are recoded.
				notify(player, pstr("|C|Rolling |W+|%s|C| for offense!",
					i->name));
				return (i->add - i->sub);
			} else if ( (i->flags & ARMOR) && ( (f->flags & ATTACKER_D) ||
				(f->flags & VICTIM_D))) {
				notify(player, pstr("|C|Rolling |W+|%s|C| for defense!",
					i->name));
				return(i->add - i->sub);
			}
			if((i->flags & WEAPON)&&!(i->flags & ARMOR))
				notify(player, "|R+|* |C|This item is a weapon only.");
			else if ((i->flags & ARMOR) &&!(i->flags & WEAPON))
				notify(player, "|R+|* |C|This item is armor only.");
			
			return 0;
		}
	
	notify(player, "|R+|* |C|No such item!");
	return 0;	
}


void do_roll(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	FIGHT *f;
	object *target;
	int roll;
	
	
	if(!(target=match_combatable(player, arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	f = get_fight(player, target);
	if(!f) {
		error_report(player, MC_ENOTURN);
		return;
	}

	roll = die_roll( (die_roll(player->combat->stats->str) + 
		   	   die_roll(player->combat->stats->dex) +
	   	    	    die_roll(player->combat->stats->lck)) - 
	   	     	     die_roll(player->combat->stats->sta/3) );
	   	  
	roll += rolled_item(f, player, arg2);
	
	if((f->flags & ATTACKER_A) || (f->flags & VICTIM_A)) {
		f->offense = roll;
		notify_fight(f, player, stralloc(pstr("|C|rolled an offense of |C+|%d|C|!",
			f->offense)));
	} else {
		f->defense = roll;
		notify_fight(f, player, stralloc(pstr("|C|rolled a defense of |C+|%d|C|!",
			f->defense)));
		roll_calculate(f);
	}
	
	fight_rollover(f);
}

int offense_equipped(thing)
	object *thing;
{
	ITEMS *i;
	int offense = 0;
	
	for(i = thing->combat->items; i; i=i->next)
		if( (i->flags & EQUIPPED) && (i->flags & WEAPON) )
			offense += i->add;
	
	return offense;
}

int defense_equipped(thing)
	object *thing;
{
	ITEMS *i;
	int defense = 0;
	
	for(i = thing->combat->items; i; i=i->next)
		if( (i->flags & EQUIPPED) && (i->flags & ARMOR) ) 
			defense += i->add;
	
	return defense;
}
#endif /*COMBAT*/