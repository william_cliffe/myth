/****************************************************************************
 MYTH - Combat Fight Queue
 Coded by Saruk (04/18/99)
 ---
 The fight queue for the combat system.
 ****************************************************************************/

#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int queued_fights = 0;
time_t last_combat_update = 0;
FIGHT *fight_queue = NULL;
SPQUEUE *spell_queue = NULL;

void __delete_fight(fight)
	FIGHT *fight;
{
	FIGHT *f, *fprev = NULL;
	
	for(f = fight_queue; f; f=f->next) {
		if(f == fight)
			break;
		fprev = f;
	}
	
	if(!f)
		return;
	
	if(!fprev)
		fight_queue = f->next;
	else
		fprev->next = f->next;
	
	block_free(f);
}

void __free_spell(spell)
	SPQUEUE *spell;
{
	SPQUEUE *s, *sprev = NULL;
	
	for(s = spell_queue; s; s=s->next) {
		if(s == spell)
			break;
		sprev = s;
	}
	
	if(!s)
		return;
	
	if(!sprev)
		spell_queue = s->next;
	else
		sprev->next = s->next;
	
	block_free(s);
}

void queue_fight(fight)
        FIGHT *fight;
{
        fight->next = fight_queue;
        fight_queue = fight;
        queued_fights++;
}

int get_fight_id() 
{
	FIGHT *f;
	int ctr;
	
	for(ctr = 1, f = fight_queue; f; f=f->next) ctr++;
	
	return(ctr);
}

FIGHT *__start_fight(attacker, victim)
        object *attacker;
        object *victim;
{
        FIGHT *fight; 
 
        GENERATE(fight, FIGHT);
        fight->id = get_fight_id();
        fight->attacker = attacker;
        fight->victim = victim;
        fight->start_time = time(0);
        fight->turn = attacker;
        fight->flags = ATTACKER_A;
	fight->last_time = time(0);
	fight->offense = fight->defense = 0;
	fight->attacker_success = fight->victim_success = fight->num_turns = 0;
        return(fight);
}

SPQUEUE *__queue_spell(enactor, victim)
	object *enactor;
	object *victim;
{
	SPQUEUE *s;
	
	GENERATE(s, SPQUEUE);
	s->enactor = enactor;
	s->victim = victim;
	s->spell = NULL;
	s->can_do = 0;
	s->next = spell_queue;
	spell_queue = s;
	
	return(s);
}



int in_fight(player)
	object *player;
{
	FIGHT *f;
	
	for(f = fight_queue; f; f=f->next)
		if(f->attacker == player || f->victim == player)
			return TRUE;
	
	return FALSE;
}

void do_fight(attacker, victim)
	object *attacker;
	object *victim;
{
	FIGHT *f;
	
	f = __start_fight(attacker, victim);
	queue_fight(f);
		
}

void fight_rollover(fight)
	FIGHT *fight;
{
	if(!fight)
		return;
		
	if( (fight->flags & ATTACKER_D) ) {
		fight->flags &= ~ATTACKER_D;
		fight->flags |= ATTACKER_A;
		fight->turn = fight->attacker;
	} else if ( (fight->flags & ATTACKER_A) ) {
		fight->flags &= ~ATTACKER_A;
		fight->flags |= VICTIM_D;
		fight->turn = fight->victim;
	} else if( (fight->flags & VICTIM_D) ) {
		fight->flags &= ~VICTIM_D;
		fight->flags |= VICTIM_A;
		fight->turn = fight->victim;
	} else if ( (fight->flags & VICTIM_A) ) {
		fight->flags &= ~VICTIM_A;
		fight->flags |= ATTACKER_D;
		fight->turn = fight->attacker;
	}
	
	time(&fight->last_time);
	notify(fight->turn, "|Y+|* |C|It is your turn.");
}

void fight_queue_second()
{
	OBJECT *o, *onext;
	FIGHT *f, *fnext;
	SPQUEUE *s, *snext;
	time_t now;
	
	time(&now);
	for(f = fight_queue; f; f=fnext) {
		fnext = f->next;
		if(f->flags & FIGHTDONE) {
			__delete_fight(f);
			continue;
		}
		/** check if this is an NPC/MONSTER **/    
		if( (f->turn->combat->flags & MONSTER) || (f->turn->combat->flags & NPC) )
			roll_ai(f, f->turn);
		if( (now - f->last_time) >= MAX_IDLE_FIGHT) {
			notify(f->turn, "|Y+|* |C|Turn forfeited.");
			f->last_time = now;
			fight_rollover(f);
			notify(f->turn, "|Y+|* |C|It is your turn.");
		}
	}
	
	/** Add spell queueing here **/
	for(s = spell_queue; s; s=snext) {
		snext = s->next;
		if(now >= s->can_do) {
			if(s->victim && s->enactor)
				s->spell->spfunc(s->enactor, s->victim);
			__free_spell(s);
		}
	}

	/** Let's go through the fight queue one more time to make sure no one has been
	    killed via spells **/
	 for(f=fight_queue; f; f=f->next)
	 	calculate_fight(f);
	
	/** Do all the combat figures here **/
	for(o = player_list; o; o=o->next) {
		if(!o->combat)
			continue;
		if( (o->combat->dead > 0) && 
			( (now - o->combat->dead) >= DEATH_TIME) )
			o->combat->dead = 0;
	}

	/** If the monster is dead, get rid of it so we don't make it wander **/	
	for(o = thing_list; o; o=onext) {
		if(!o->combat)
			continue;
		onext = o->next;
		if( (o->combat->dead > 0) && ( (o->combat->flags & MONSTER) ||
			(o->combat->flags & NPC) ) )
				recycle_monster(o);
	}
	
	for(o = thing_list; o; o=o->next) {
		if(!o->combat)
			continue;
		if( (now - o->combat->wander) >= WANDER_TIME &&
			(o->combat->flags & WANDER) && !in_fight(o) )
				wander_ai(o);
		if( ((now - o->combat->breed_time) >= 0) && ((o->combat->flags & MONSTER) ||
			(o->combat->flags & NPC)) ) 
				breed_ai(o);
	}

	/* Let's up everyone/thing's HP/MP if it's time */
	if( (time(0) - last_combat_update) >= 0) {	
		last_combat_update = time(0) + COMBAT_UPDATE + 1;
		for(o = player_list; o; o=o->next) {
			if(o->flags & GUEST || !o->combat)
				continue;
			if( o->combat->stats->hp < o->combat->stats->maxhp )
				o->combat->stats->hp += (1 + die_roll((o->combat->stats->maxhp - o->combat->stats->hp)));
			if( o->combat->stats->mp < o->combat->stats->maxmp )
				o->combat->stats->mp += (1 + die_roll((o->combat->stats->maxmp - o->combat->stats->mp)));
			if( o->combat->stats->hp > o->combat->stats->maxhp )
				o->combat->stats->hp = o->combat->stats->maxhp;
			if( o->combat->stats->mp > o->combat->stats->maxmp )
				o->combat->stats->mp = o->combat->stats->maxmp;
				
		}
		for(o = thing_list; o; o=o->next) {
			if(!o->combat)
				continue;
			if(!o->combat->stats) 
				continue;
			if( o->combat->stats->hp < o->combat->stats->maxhp )
				o->combat->stats->hp += (1 + die_roll((o->combat->stats->maxhp - o->combat->stats->hp)));
			if( o->combat->stats->mp < o->combat->stats->maxmp )
				o->combat->stats->mp += (1 + die_roll((o->combat->stats->maxmp - o->combat->stats->mp)));
			if( o->combat->stats->hp > o->combat->stats->maxhp )
				o->combat->stats->hp = o->combat->stats->maxhp;
			if( o->combat->stats->mp > o->combat->stats->maxmp )
				o->combat->stats->mp = o->combat->stats->maxmp;
		}
	}	
	
 }