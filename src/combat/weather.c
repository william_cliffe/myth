#ifdef COMBAT
/****************************************************************************
 MYTH Combat - Weather System
 Coded by Saruk (02/05/99)
 ---
 This weather system will calculate and display information as specific weather
 patterns permit. As of now, no complex calculations are to be perfromed - instead
 we will provide a basic structure for weather.
 ****************************************************************************/
#define WEATHER_TIME	7

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// weather control is updated every MYTH hour.
void update_weather()
{
	object *o;	// search through the room list to designate appropriate weather conditions.
	int ctr;
	int climate = 0, selection = 0;
	
	if(myth_time.weather_counter < WEATHER_TIME)
		return;
	
	myth_time.weather_counter = 0;
	
	/* We want to cycle through the weather and figure out which is going to start for
	   whichever region */
	for(o=room_list; o; o=o->next) {
		climate = atoi(atr_get(o, "Climate"));
		selection = random() % 4;
		for(ctr = 0; weather_system[ctr].message; ctr++)
			if( (climate == weather_system[ctr].climate) &&
				(selection == weather_system[ctr].type)) 
				notify_in(o, NULL, weather_system[ctr].message);
	}
}
#endif /*COMBAT*/