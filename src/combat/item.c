#ifdef COMBAT
/****************************************************************************
 MYTH - Item Handling
 Coded by Saruk (04/18/99)
 ---
 Item handling for combat oriented items.
 ****************************************************************************/
 
#include <string.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

ITEMS *add_item(player, item)
	object *player;
	ITEMS *item;
{
	ITEMS *newitem;
		
	GENERATE(newitem, ITEMS);	
	newitem->name = NULL;
	newitem->desc = NULL;
	SET(newitem->name, item->name);
	SET(newitem->desc, item->desc);
	newitem->id = item->id;
	newitem->flags = item->flags;
	newitem->location = item->location;
	newitem->weight = item->weight;
	newitem->add = item->add;
	newitem->sub = item->sub;
	newitem->loc = 0;
	newitem->next = player->combat->items;
	player->combat->items = newitem;
	
	return(newitem);
}

void do_inventory(player, arg1)
	object *player;
	char *arg1;
{
	ITEMS *i;
	object *target;
	
	if(!*arg1)
		target = player;
	else {
		if(!(target=match_object(arg1))) {
			error_report(player, M_EILLNAME);
			return;
		}
	}
	
	notify(player, pstr("|W+|--- |X|%s|C|'s Itemlist |W+|---", color_name(target, -1)));
	for(i = target->combat->items; i; i=i->next)
		notify(player, pstr("%s", i->name));
	
}
#endif /*COMBAT*/