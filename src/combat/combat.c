#ifdef COMBAT
/****************************************************************************
 MYTH - Combat System
 Coded by Saruk (03/04/99)
 ---
 Our combat system (first edition).. a lot of ideas in this.
 ****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

float combat_version = 0.00;

void combat_system(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
}

/* these point to all the 'set_*' procedures. */
void set_race P((DESCRIPTOR *, char *));
void set_class P((DESCRIPTOR *, char *));
void set_roll P((DESCRIPTOR *, char *));
void roll_player P((object *));
void show_roll P((object *));

/* The only way you'll get pointed here is if your character has been created and not set up */
void handle_extra(d, instr)
	DESCRIPTOR *d;
	char *instr;
{
	// depending on each state, send them to the appropriate procedure.
	
	if(d->internal == TWEAKMODE)
		filter_tweak(d, instr);
	else if(d->internal == SET_RACE)
		set_race(d, instr);
	else if(d->internal == SET_CLASS)
		set_class(d, instr);
	else if(d->internal == SET_ROLL)
		set_roll(d, instr);
	else if(d->internal == SET_DONE)
		d->state = CONNECTED;
		
				
}

void set_race(d, choice)
	DESCRIPTOR *d;
	char *choice;
{
	int ctr;

	for(ctr = 0; race_table[ctr].name; ctr++)
		if(!strcmpm(race_table[ctr].key, choice)) {
			SET_ATR(d->player, "Race", choice);
			d->internal = SET_CLASS;
			do_classlist(d->player);
			dwrite(d, "|C|Selection|W+|: ");
		}
	
	if(d->internal != SET_CLASS) {
		dwrite(d, "|Y+|Invalid |R+|Selection|C|!\n");
		do_racelist(d->player);
	        dwrite(d, "|C|Selection|W+|: ");
	}
}

void set_class(d, choice)
	DESCRIPTOR *d;
	char *choice;
{
	int ctr;
	
	for(ctr = 0; class_table[ctr].name; ctr++)
		if(!strcmpm(class_table[ctr].key, choice)) {
			SET_ATR(d->player, "Class", choice);
			roll_player(d->player);
			d->internal = SET_ROLL;
			show_roll(d->player);
			dwrite(d, "|C|Keep Stats? |C+|[|Y+|Y|W+|/|G+|n|C+|]|W+|: ");			
		}
	
	if(d->internal != SET_ROLL) {
		dwrite(d, "|Y+|Invalid |R+|selection|C|!\n");
		do_classlist(d->player);
		dwrite(d, "|C|Select Key|W+|: ");
	}
}

/* This rolls a player, then checks the Race and Class of a player to determine the right
   adjustment stats. */
void roll_player(player)
	object *player;
{
	STATS *combat;
	CTABLE *r = NULL, *c = NULL;
	int ctr;

	for(ctr = 0; race_table[ctr].name; ctr++)
		if(!strcmpm(race_table[ctr].key, atr_get(player, "Race")))
			r = &race_table[ctr];
	
	for(ctr = 0; class_table[ctr].name; ctr++)
		if(!strcmpm(class_table[ctr].key, atr_get(player, "Class")))
			c = &class_table[ctr];
	
	if(!r || !c) { // if one or the other is not set, default it for now.
		return;
	}		
	
	GENERATE(combat, STATS);
	
	combat->hp = (10 + (int)(random() % 24)) + r->hp + c->hp;
	if(combat->hp < 10)	
		combat->hp = 10;
		
	combat->mp = (4 + (int)(random() % 24)) + r->mp + c->mp;
		
	combat->lck = (4 + (int)(random() % 24)) + r->lck + c->lck;
	if(combat->lck < 4)
		combat->lck = 4;
	
	combat->intel = (4 + (int)(random() % 24)) + r->intel + c->intel;
	if(combat->intel < 4)
		combat->intel = 4;
		
	combat->dex = (4 + (int)(random() % 24)) + r->dex + c->dex;
	if(combat->dex < 4)
		combat->dex = 4;
		
	combat->sta = (4 + (int)(random() % 24)) + r->sta + c->sta;
	if(combat->sta < 4)
		combat->sta = 4;
		
	combat->str = (4 + (int)(random() % 24)) + r->str + c->str;
	if(combat->str < 4)
		combat->str = 4;
		
	combat->wis = (4 + (int)(random() % 24)) + r->wis + c->wis;
	if(combat->wis < 4)
		combat->wis = 4;
		
	combat->vir = (4 + (int)(random() % 24)) + r->vir + c->vir;
	combat->maxhp = combat->hp;
	combat->maxmp = combat->mp;
	combat->level = 1;
	combat->exp = 0;
	combat->weight = (float)(r->weight + (die_roll(25) - die_roll(12)));
	combat->height = (float)(r->height + die_roll(6));
	combat->carry = (float) (((combat->weight + combat->height) / 1.79) + (combat->str * 1.2));
		
	player->combat->stats = combat;	
}

void show_roll(player)
	object *player;
{
	STATS *c;
	
	if(!player->combat)
		return;
		
	c = player->combat->stats;	
	make_header(player, "|W+|--- |C|Your Rolled Stats |W+|---");
	notify(player, pstr("Int: %d\tStr: %d\tDex: %d\tSta: %d",
		c->intel, c->str, c->dex, c->sta));
	notify(player, pstr("Wis: %d\t Mp: %d\t Hp: %d\tLck: %d",
		c->wis, c->mp, c->hp, c->lck));
	notify(player, pstr("Vir: %d", c->vir));
}

void set_roll(d, choice)
	DESCRIPTOR *d;
	char *choice;
{
	if(*choice == ' ')
		choice = "yes";
	if(string_prefix(choice, "yes")) {
		d->internal = SET_DONE;
		handle_extra(d, "");
		connect_player(d);
	} else if (string_prefix(choice, "no")) {
		roll_player(d->player);
		show_roll(d->player);
		dwrite(d, "|C|Keep Stats? |C+|[|Y+|Y|W+|/|G+|n|C+|]|W+|: ");
	}
}

int calculate_items(player)
	object *player;
{
	int ctr = 0;
	ITEM *i;
	
	for(i = player->combat->items; i; i=i->next) ctr++;
	
	return(ctr);
}

int calculate_spells(player)
	object *player;
{
	int ctr = 0;
	SKILLS *s;

	for(s = player->combat->skills; s; s=s->next) ctr++;
	
	return(ctr);	
}

/* Combat object save */
void OUT_COBJ(fp, player)
	FILE *fp;
	object *player;
{
	STATS *stat;
	ITEM *i;
	SKILLS *s;
	
	if(!player->combat)
		initialize_combat(player);
				
	if(!player->combat->stats)
		clear_combat_atrs(player);
		
	stat = player->combat->stats;
		
	fprintf(fp, "#%d\n", player->ref);
	fprintf(fp, "%ld\n", player->combat->flags);
	fprintf(fp, "%ld\n", player->combat->dead);
	fprintf(fp, "%ld\n", player->combat->time_to_breed);
	fprintf(fp, "%ld\n", player->combat->breed_time);
	fprintf(fp, "%010d %010d %010d %010d %010d %010d %010d %010d %010d %010d %010d %010d %010d %010f %010f %010f\n",
		stat->hp, stat->mp, stat->str, stat->dex, stat->intel, stat->wis, stat->vir, stat->sta,
		 stat->lck, stat->exp, stat->level, stat->maxhp, stat->maxmp, (float)stat->carry,
		  (float)stat->weight, (float)stat->height);
	fprintf(fp, "%d\n", calculate_items(player));
	for(i = player->combat->items; i; i=i->next) {
		fprintf(fp, "%d\n", i->id);
		fprintf(fp, "%d\n", i->flags);
		fprintf(fp, "%d\n", i->loc);
	}
	
	fprintf(fp, "%d\n", calculate_spells(player));
	for(s = player->combat->skills; s; s=s->next) {
		fprintf(fp, "%s\n", s->name);
		fprintf(fp, "%d\n", s->cost);
		fprintf(fp, "%d\n", s->level);
		fprintf(fp, "%d\n", s->success);
		fprintf(fp, "%d\n", s->uses);
	}
	
}

/* This is going to dump the combat stats into a separate database for each object. */
/* There's no combat stats on ROOMs or EXITs for now, so just save thing_list and player_list */
void combat_dump_database(filename)
	char *filename;
{
	FILE *fp;
	object *o;
	
	fp = fopen(filename, "w");	
	fprintf(fp, "#%f\n", COMBAT_DB);
	fprintf(fp, "%d\n", calculate_dbsize());
	for(o = player_list; o; o=o->next)
		OUT_COBJ(fp, o);
	for(o = thing_list; o; o=o->next)
		OUT_COBJ(fp, o);
	for(o = room_list; o; o=o->next)
		OUT_COBJ(fp, o);
	for(o = exit_list; o; o=o->next)
		OUT_COBJ(fp, o);
		
	fclose(fp);
}

int ADD_ITEM(player, fp)
	object *player;
	FILE *fp;
{
	ITEM *i, *item;
	
	if(!(item = find_item(getint(fp))))
		return FALSE;
	
	i = add_item(player, item);
	i->flags = getint(fp);
	i->loc = getint(fp);
	return TRUE;
}

int ADD_SPELL(player, fp)
	object *player;
	FILE *fp;
{
	SKILLS *s, *skill;
	int ctr;
	char buf[MIN_BUFSIZE];
	
	strcpy(buf, getstring(fp));	
	for(ctr=0;spell_table[ctr].name; ctr++)
		if(!strcmpm(spell_table[ctr].name, buf)) {
			skill = &spell_table[ctr];
			break;
		}
	
	s = add_spell(player, skill);
	s->cost = getint(fp);
	s->level = getint(fp);
	s->success = getint(fp);
	s->uses = getint(fp);
	
	return TRUE;
}

int IN_COBJ(fp)
	FILE *fp;
{
	object *o;
	int ctr, amt;
	char buf[MAX_BUFSIZE];

	strcpy(buf, getstring(fp));
	if(!(o = match_object(buf)))
		return FALSE;
	
	if(!o->combat)
		initialize_combat(o);
		
	o->combat->flags = getlong(fp);
	o->combat->dead = getlong(fp);
	o->combat->time_to_breed = getlong(fp);
	o->combat->breed_time = getlong(fp);
	mkcombat(o, getstring(fp));	

	amt = getint(fp);
	for(ctr = 0; ctr < amt; ctr++)
		ADD_ITEM(o, fp);
	
	if(combat_version <= (float)1)
		return TRUE;
	
	amt = getint(fp);
	for(ctr = 0; ctr < amt; ctr++)
		ADD_SPELL(o, fp);
	return TRUE;
}

void load_combat_database()
{
	FILE *fp;
	char buf[MIN_BUFSIZE];
	int amt = 0, ctr = 0;
	
	if(!(fp = fopen(return_config("combatdb"), "r"))) {
		printf("error loading combat database\n");
		return;
	}
	
	strcpy(buf, getstring(fp));
	
	if(buf[0] == '#') {
		combat_version = atof(buf + 1);
		amt = getint(fp);
	} else
		amt = atoi(buf);
		
	for(ctr = 0; ctr < amt; ctr++)
		if(!IN_COBJ(fp)) {
			printf("error loading combat database\n");
			return;
		}
	fclose(fp);
}

void mkcombat(player, instr)
	object *player;
	char *instr;
{
	STATS *c;
	char buf[1024];

	strcpy(buf, instr);
	GENERATE(c, STATS);	
	c->hp = atoi(buf);
	c->mp = atoi(buf + 11);
	c->str = atoi(buf + 22);
	c->dex = atoi(buf + 33);
	c->intel = atoi(buf + 44);
	c->wis = atoi(buf + 55);
	c->vir = atoi(buf + 66);
	c->sta = atoi(buf + 77);
	c->lck = atoi(buf + 88);
	c->exp = atoi(buf + 99);
	c->level = atoi(buf + 110);
	c->maxhp = atoi(buf + 121);
	c->maxmp = atoi(buf + 132);
	c->carry = atof(buf + 143);
	c->weight = atof(buf + 154);
	c->height = atof(buf + 165);

	player->combat->stats = c;
}

void clear_combat_atrs(player)
	object *player;
{
	STATS *c;
	
	initialize_combat(player);
	
	GENERATE(c, STATS);
	c->hp = 0;
	c->mp = 0;
	c->str = 0;
	c->dex = 0;
	c->intel = 0;
	c->wis = 0;
	c->vir = 0;
	c->sta = 0;
	c->lck = 0;
	c->exp = 0;
	c->level = 0;
	c->maxhp = 0;
	c->maxmp = 0;
	c->carry = 0.00;
	c->weight = 0.00;
	c->height = 0.00;
	
	player->combat->stats = c;
	if(player->flags & TYPE_PLAYER) {
		SET_ATR(player, "Race", "NULL");
		SET_ATR(player, "Class", "NULL");
	}
}

void initialize_combat(player)
	object *player;
{
	MCOMBAT *combat_startup;
	
	GENERATE(combat_startup, MCOMBAT);
	
	player->combat = combat_startup;
	player->combat->flags = 0;
	player->combat->dead = 0;
	player->combat->wander = 0;
	player->combat->breed_time = 0;
	player->combat->time_to_breed = 0;
	player->combat->stats = NULL;
	player->combat->skills = NULL;
	player->combat->items = NULL;
}

int check_combat(player)
	object *player;
{

	if(!player->combat)
		initialize_combat(player);
	
	if( (player->combat->stats) )
		if(player->combat->stats->level <= 0)
			return 0;
		
	if( (!player->combat->stats) || !*(atr_get(player, "Race")) || !*(atr_get(player, "Class")) )
		return 0;
	
	return 1;		
}
#endif /* COMBAT */