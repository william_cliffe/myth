#ifdef COMBAT
/****************************************************************************
 MYTH - Combat Tweak System
 Coded by Saruk (04/18/99)
 ---
 Tweak certain variables for the combat system.
 ****************************************************************************/

#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

#define SHOW		0
#define SETT		1

#define DBG_STR		0
#define DBG_DEX		1
#define DBG_INTEL	2
#define DBG_WIS		3
#define DBG_VIR		4
#define DBG_STA		5
#define DBG_HP		6
#define DBG_MAXHP	7
#define DBG_MP		8
#define DBG_MAXMP	9
#define DBG_LEVEL	10
#define DBG_CARRY	11
#define DBG_EXP		12

struct list_types_t {
	char *name;
	int (*retfunc)();
	int tag;
};

int return_attr P((object *, int, int, int));
struct list_types_t listtypes[]= {
	{"str", return_attr, DBG_STR},	
	{"dex", return_attr, DBG_DEX},
	{"intel", return_attr, DBG_INTEL},
	{"wis", return_attr, DBG_WIS},
	{"vir", return_attr, DBG_VIR},
	{"sta", return_attr, DBG_STA},
	{"hp", return_attr, DBG_HP},
	{"maxhp", return_attr, DBG_MAXHP},
	{"mp", return_attr, DBG_MP},
	{"maxmp", return_attr, DBG_MAXMP}, 
	{"level", return_attr, DBG_LEVEL},
	{"carry", return_attr, DBG_CARRY},
	{"exp", return_attr, DBG_EXP},
	{NULL}};

int return_attr(thing, flag, tag, newvalue)
	object *thing;
	int flag;
	int tag;
	int newvalue;
{
	switch(flag) {
		case DBG_STR:
			if(tag == SHOW)
				return thing->combat->stats->str;
			else
				return (thing->combat->stats->str = newvalue);
			
		case DBG_DEX:
			if(tag == SHOW)
				return thing->combat->stats->dex;
			else
				return (thing->combat->stats->dex = newvalue);
			 
		case DBG_INTEL:
			if(tag == SHOW)
				return thing->combat->stats->intel;
			else
				return (thing->combat->stats->intel = newvalue);
			 
		case DBG_WIS:
			if(tag == SHOW)
				return thing->combat->stats->wis;
			else
				return(thing->combat->stats->wis = newvalue);
			 
		case DBG_VIR:
			if(tag == SHOW)
				return thing->combat->stats->vir;
			else
				return(thing->combat->stats->vir = newvalue);
			 
		case DBG_STA:
			if(tag == SHOW)
				return thing->combat->stats->sta;
			else
				return(thing->combat->stats->vir = newvalue);
			 
		case DBG_HP:
			if(tag == SHOW)
				return thing->combat->stats->hp;
			else
				return(thing->combat->stats->hp = newvalue);
			 
		case DBG_MAXHP:
			if(tag == SHOW)
				return thing->combat->stats->maxhp;
			else
				return(thing->combat->stats->maxhp = newvalue);
			 
		case DBG_MP:
			if(tag == SHOW)
				return thing->combat->stats->mp;
			else
				return(thing->combat->stats->mp = newvalue);
		case DBG_MAXMP:
			if(tag == SHOW)
				return thing->combat->stats->maxmp;
			else
				return(thing->combat->stats->maxmp = newvalue);
		case DBG_LEVEL:
			if(tag == SHOW)
				return thing->combat->stats->level;
			else
				return (thing->combat->stats->level = newvalue);
		case DBG_CARRY:
			if(tag == SHOW)
				return thing->combat->stats->carry;
			else 
				return (thing->combat->stats->carry = newvalue);
		case DBG_EXP:
			if(tag == SHOW)
				return thing->combat->stats->exp;
			else
				return(thing->combat->stats->carry = newvalue);
			
		default:
			return 0;
			
	}
}

void tweak_parse_print(player, string)
	object *player;
	char *string;
{
	object *who;
	struct list_types_t *lt;
	int ctr;
	char buf[MAX_BUFSIZE];
	
	strcpy(buf, string);
	IS_ARGUMENT(player, (front(buf)));
	IS_ARGUMENT(player, (restof(buf)));
	
	if(!(who = match_object(front(buf)))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	for(ctr = 0; listtypes[ctr].name; ctr++) {
		lt = &listtypes[ctr];
		if(!strcmpm(listtypes[ctr].name, restof(buf)))
			notify(player, pstr("|X|%s|C|->combat->stats->%s = %d",
				color_name(who, -1), restof(buf), lt->retfunc(who, lt->tag, SHOW, 0)));
	}
}

void tweak_parse_input(player, string)
	object *player;
	char *string;
{
	char buf[MAX_BUFSIZE];
	struct list_types_t *lt;
	object *who;
	int ctr;
	
	strcpy(buf, string);
	IS_ARGUMENT(player, (front(buf)));
	IS_ARGUMENT(player, (restof(buf)));
	
	if(!(who = match_object(front(buf)))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	strcpy(buf, restof(buf));
	for(ctr = 0; listtypes[ctr].name; ctr++) {
		lt = &listtypes[ctr];
		if(!strcmpm(listtypes[ctr].name, front(buf)))
			notify(player, pstr("|X|%s|C|->combat->stats->%s = %d",
				color_name(who, -1), front(buf), lt->retfunc(who, lt->tag, SETT, match_num(restof(buf)))));
	}	
}
	
void do_tweak(player)
	object *player;
{
	DESCRIPTOR *d;
	
	for(d=descriptor_list; d; d=d->next)
		if(d->state == CONNECTED) 
			if(d->player == player) {
				d->internal = TWEAKMODE;
				break;
			}
	
	if(!d) 
		return;
				
	notify(player, pstr("|C+|%s |C|Combat Tweak System |W+|(|Y+|ALPHA|W+|)",
		return_config("myth_name")));
	__notify(player, "> ", 0);
}

void filter_tweak(d, buffer)
	DESCRIPTOR *d;
	char *buffer;
{
	if(!strcmpm(buffer, "quit")) {
		d->internal = SET_DONE;
		handle_extra(d, "");
		dwrite(d, "|R+|* |C|Tweaker Exited\n");
		return;
	} else if(!strcmpm(front(buffer), "print")) {
		tweak_parse_print(d->player, restof(buffer));
	} else if (!strcmpm(front(buffer), "set")) {
		tweak_parse_input(d->player, restof(buffer));
	}

	dwrite(d, "> ");	
}
#endif /*COMBAT*/