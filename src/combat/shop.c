#ifdef COMBAT
/****************************************************************************
 MYTH - Combat Shop System
 Coded by Saruk (04/06/99)
 ---
 This is the system to display items within a shop. Not sure how I'm going to 
 do this yet, but I need to get it started.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

ITEMS *find_item(id)
	int id;
{
	int ctr;	
	
	for(ctr = 0; item_table[ctr].name; ctr++)
		if(id == item_table[ctr].id)
			return(&item_table[ctr]);

	return(NULL);
}

int has_item(victim, id)
	object *victim;
	int id;
{
	char buf[MAX_BUFSIZE];
	
	strcpy(buf, atr_get(victim, "ShopItems"));
	while(*(front(buf))) {
		if((id == match_num(front(buf))) && find_item(id))
			return TRUE;
		strcpy(buf, restof(buf));
	}
	
	return FALSE;
}

void do_listshop(player)
	object *player;
{
	ITEMS *item;
	char buf[MAX_BUFSIZE];

	IS_SHOP(player);
	HAS_ITEMS(player);
	
	strcpy(buf, atr_get(player->location, "ShopItems"));
	while(*(front(buf))) {
		if( (item = find_item(match_num(front(buf)))))
			notify(player, pstr("|C+|[|Y+|%-4d|C+|] |R+|%-16.16s |C|%s",
				item->id, item->name, item->desc));
		strcpy(buf, restof(buf));
	}
}

void do_buy(player, arg1)
	object *player;
	char *arg1;
{
	int id;
	ITEMS *i, *item;
	
	IS_SHOP(player);
	HAS_ITEMS(player);	
	IS_ARGUMENT(player, (arg1));
	IS_ANUMBER(player, (arg1));
	
	id = match_num(arg1);

	if(!has_item(player->location, id)) {
		notify(player, "|R+|* |C|That item is not for sale here.");
		return;
	}
	
	item = find_item(id);
	CAN_BUY(player, (item->cost));
	if(!ISWIZARD(player))
		SET_ATR(player, "Cost", pstr("%d", match_num(atr_get(player, "Gold")) - item->cost));
	
	i = add_item(player, item);
	notify(player, pstr("|Y+|* |C|You have received the item |C+|%s|C|.", item->name));
}

void do_sell(player, arg1)
	object *player;
	char *arg1;
{
	IS_SHOP(player);
	
	notify(player, "|Y+|* |R+|This command is still in development.");
}

#endif /*COMBAT*/