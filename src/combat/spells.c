#ifdef COMBAT
/****************************************************************************
 MYTH - Spells 
 Coded by Saruk (03/22/99)
 ---
 There's like spells and stuff here.
 ****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

#include "sp_funcs.c"

// Adding a spell to an object.
SKILLS *add_spell(player, skill)
	object *player;
	SKILLS *skill;
{
	SKILL *snew;
	
	GENERATE(snew, SKILLS);
	SET(snew->name, skill->name);
	SET(snew->desc, skill->desc);
	snew->spfunc = skill->spfunc;
	snew->spflags = skill->spflags;
	snew->base = skill->base;
	snew->type = skill->type;
	snew->flags = skill->flags;
	snew->cost = skill->base;
	snew->level = 1;
	snew->success = 0;
	snew->uses = 0;
	snew->next = player->combat->skills;
	player->combat->skills = snew;
	
	return(snew);
}

int __has_skill(player, skill)
	object *player;
	SKILLS *skill;
{
	SKILLS *s;

	for(s=player->combat->skills; s; s=s->next) {
		if(!strcmpm(s->name, skill->name))
			return TRUE;
	}
			
	return FALSE;
}

/* Figure the time before spell executes */
time_t spell_time(spell, enactor)
	SPELL *spell;
	object *enactor;
{
	time_t len_time;
	
	len_time = (time_t)5;
	
	return(len_time);
}

void spell_cast(enactor, victim, spell)
	object *enactor;
	object *victim;
	SPELL *spell;
{
	SPQUEUE *s;
	
	s = __queue_spell(enactor, victim);
	s->spell = spell;
	s->can_do = (time_t)(time(0) + spell_time(enactor));
}

void castability(spell, enactor, victim)
	SPELL *spell;
	object *enactor;
	object *victim;	
{
	CONTENTS *exception = NULL;
	
	add_exception(&exception, enactor);
	add_exception(&exception, victim);
	
	spell->uses++;
	if(can_cast(enactor, victim)) {
		spell_cast(enactor, victim, spell);
		notify(enactor, pstr("|Y+|* |C|You cast |C+|%s |C|on |X|%s",
			spell->name, color_name(victim, -1)));
		notify(victim, pstr("|Y+|* |X|%s |C|casts |C+|%s |C|on you.",
			color_name(enactor, -1), spell->name));
		notify_in(enactor->location, exception, pstr("|Y+|* |X|%s |C|casts |C+|%s |C|on |X|%s|C|!",
			color_name(enactor, -1), spell->name, color_name(victim, -1)));
		spell->success++;
		enactor->combat->stats->mp -= spell->cost;
	} else {
		notify_in(enactor->location, exception, pstr("|Y+|* |X|%s |C|fails to cast a |C+|%s |C|spell on |X|%s|C|...",
			color_name(enactor, -1), spell->name, color_name(victim, -1)));
		notify(enactor, pstr("|Y+|* |C|You fail to cast a spell on |X|%s",
			color_name(victim, -1)));
		notify(victim, pstr("|Y+|* |X|%s |C|fails to cast a spell on you...",
			color_name(enactor, -1)));
	}
	
	clear_exception(exception);
}

/* This is kind of backwards, but we're only going to use the LAST argument for name. */
// So: cast fire of death Marlock (this will take Marlock as our name)
void do_cast(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	FIGHT *f;
	SKILLS *s;
	object *victim;
	char buf[MAX_BUFSIZE];
	int ctr;

	if(!arg2 || !*arg2) {
		strcpy(buf, arg1);
		victim = player;
	} else {
		sprintf(buf, "%s %s", arg1, arg2);
		if(!(victim = match_combatable(player, lfof(arg2, 0)))) {
			victim = player;
			strcpy(buf, buf);
		} else {
			strcpy(buf, lfof(buf, 1));
		}
	}
	
	if(player->location != victim->location) {
		error_report(player, M_EREMOTE);
		return;
	}
	
	f = get_fight(player, victim);
		
	for(s=player->combat->skills; s; s=s->next) 
		if(!strcmpm(s->name, buf)) 
			break;
	
	if(!s) {
		error_report(player, MC_ENOSPELL);
		return;
	}
	
	if(s->cost > player->combat->stats->mp) {
		notify(player, "|R+|* |C|You do not have enough magic points.");
		return;
	}
	
	if(!f && (s->spflags & AGGRESSIVE)) {
		do_fight(player, victim);
		notify(player, pstr("|Y+|* |C|You |R+|attack |X|%s|C|!", color_name(victim, -1)));
		notify(player, pstr("|Y+|* |C|You are being |R+|attacked |C|by |X|%s|C|!", color_name(player, -1)));
		f = get_fight(player, victim);
	} else if (f) {
		if(f->turn != player) {
			error_report(player, MC_ENOTURN);
			return;
		}
	}

	castability(s, player, victim);	
	fight_rollover(f);
}

void do_listspells(player, arg1)
	object *player;
	char *arg1;
{
	SKILLS *s;
	object *who;
	
	if(!*arg1)
		who = player;
	else if(!(who = match_object(arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	if(!can_control(player, who)) {
		error_report(player, M_ENOCNTRL);
		return;
	}


	if(!player->combat->skills) {
		notify(player, "|R+|* |Y+|No spells");
		return;
	}
	
	notify(player, pstr("|C+|--- |W|%s |R+|Spell |B+|List|C+| ---", color_name(who, -1)));
	for(s = who->combat->skills; s; s=s->next) 
		notify(player, pstr("%s", s->name));
	
}

void do_spelllist(player)
	object *player;
{
	int ctr;
	
	make_header(player, "|W+|--- |C|Spell List |W+|---");
	notify(player, pstr("|W+|%-12.12s DESCRIPTION", "NAME"));
	for(ctr = 0; spell_table[ctr].name; ctr++)
		notify(player, pstr("|C+|%-12.12s |R+|%s", spell_table[ctr].name, spell_table[ctr].desc));
	notify(player, pstr("|B+|%s",make_ln("-",78)));
}

/** I don't know where else to put this yet. **/
char *equipped(player, flag)
	object *player;
	int flag;
{
	char buf[MAX_BUFSIZE];
	ITEMS *i;
	
	for(i = player->combat->items; i; i=i->next)
		if( (i->flags & EQUIPPED) && (i->loc & flag))
			break;
	
	if(!i)
		return " ";
	
	sprintf(buf, "%s |W+|(+|Y+|%d|W+|)", i->name, i->add);
	
	return stralloc(buf);
}

void do_equip_list(player, who)
	object *player;
	object *who;
{
	notify(player, pstr("|C+|Equipment |M+|List |C|for |X|%s |W+|:",
		color_name(who, -1)));
	notify(player, pstr("|B+|Left Hand|W+|: %s |B+|Right Hand|W+|: %s",
		format_color(equipped(who, LEFTHAND),24), format_color(equipped(who, RIGHTHAND),24)));
	notify(player, pstr("|B+|Left Ring|W+|: %s |B+|Right Ring|W+|: %s",
		format_color(equipped(who, LEFTRING),24), format_color(equipped(who, RIGHTRING),24)));
	notify(player, pstr("|B+|Left Arm |W+|: %s |B+|Right Arm |W+|: %s",
		format_color(equipped(who, LEFTARM),24), format_color(equipped(who, RIGHTARM),24)));
	notify(player, pstr("|B+|Feet     |W+|: %s |B+|Legs      |W+|: %s",
		format_color(equipped(who, FEET),24), format_color(equipped(who, LEGS),24)));
	notify(player, pstr("|B+|Head     |W+|: %s |B+|Torso     |W+|: %s",
		format_color(equipped(who, HEAD),24), format_color(equipped(who, TORSO),24)));
	notify(player, pstr("|B+|%s", make_ln("-", 78)));
	notify(player, pstr("|X|%s|C|%s total equipped |R+|Attack |C|advantage is|W+|: |Y+|%d",
		(player == who) ? "|C|Your": color_name(who, -1), (player == who) ?
			"":"'s", offense_equipped(who)));				
	notify(player, pstr("|X|%s|C|%s total equipped |C+|Defense |C|advantage is|W+|: |Y+|%d",
		(player == who) ? "|C|Your": color_name(who, -1), (player == who) ?
			"":"'s", defense_equipped(who)));
}


// Grant a player a spell.
// Eventually add ability to add NPCs spells.
// target == arg1, spell == arg2
void do_grant(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *target;
	SKILLS *s = NULL;
	int ctr = 0;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	for(ctr = 0; spell_table[ctr].name; ctr++) 
		if(!strcmpm(spell_table[ctr].name, arg2)) 
			s = &spell_table[ctr];
	
	if(!s) {
		error_report(player, MC_ENOSPELL);
		return;
	}
	
	if(!(target=match_combatable(player, arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	// Alright, so the spell exists and the target exists. Check for ability to grant.
	if(!(player->flags & WIZARD)) {
		error_report(player, M_ENOTWIZ);
		return;
	}
			
	// Let's just double check to make sure the player does not already have the spell.
	if(__has_skill(target, s)) {
		notify(player, pstr("|R+|* |W|%s |C|has that spell already.", color_name(target, -1)));
		return;
	}
	
	// Does the player have the correct class?
	if(!(s->flags & get_class(target))) {
		notify(player, pstr("|R+|* |C|You cannot grant |W|%s |C+|%s|C|.", color_name(target, -1), s->name));
		return;
	}
	
	target->combat->skills = add_spell(target, s);
	
}

#endif /*COMBAT*/
