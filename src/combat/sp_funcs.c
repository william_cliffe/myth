/****************************************************************************
 MYTH - Spell Functions
 Coded by Saruk (03/22/99)
 ---
 Script system idea scrapped for now - going with hardcoded spell system.
 ****************************************************************************/

void sp_heal(enactor, victim)
	object *enactor;
	object *victim;
{
	int max_heal, healed, depth=0;
	
	if(victim->combat->stats->hp > victim->combat->stats->maxhp) {
		return;
	}
	

	max_heal = die_roll(victim->combat->stats->maxhp - victim->combat->stats->hp);

	while(!((healed = die_roll(victim->combat->stats->intel + victim->combat->stats->wis)) < max_heal)) 
		if((depth++)>20) {
			healed=1;
			break;
		}
		
	
	victim->combat->stats->hp += healed;
	
	notify(victim, pstr("|Y+|* |C|You gain |C+|%d |C|hitpoints!",
		healed));
	notify(enactor, pstr("|Y+|* |X|%s |C|gains |C+|%d |C|hitpoints!",
		color_name(victim, -1), healed));
	
}


/** 
      If your wisdom and intelligence are enough, your strength will combine with them to create a fireball of 
    massive proportions! Only a person of stamina and wisdom may counter this spell successfully.
**/

void sp_fireball(enactor, victim)
	object *enactor;
	object *victim;
{
	int max_damage = 0;
	
	if(victim->combat->stats->hp <= 0) {
		return;
	}
	
	if( (enactor->combat->stats->intel + enactor->combat->stats->wis) > (victim->combat->stats->sta + victim->combat->stats->wis) ) {
		max_damage = die_roll(victim->combat->stats->str);
		victim->combat->stats->hp -= max_damage;
		notify_in(enactor->location, NULL, pstr("%s |R+|hits |W|%s |C|for |Y+|%d |R|damage.", color_name(enactor, -1), color_name(victim, -1), max_damage));
	} else {
		notify_in(enactor->location, NULL, pstr("%s |R+|blocked |W|%s|C|'s |R+|fireball|C|!",
			color_name(victim, -1), color_name(enactor, -1)));
	}
	

}