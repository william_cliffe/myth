#ifdef COMBAT
/****************************************************************************
 MYTH - Miscellaneous Combat Utils
 Coded by Saruk (03/21/99)
 ---
 This does miscellaneous combat calls and whatnot.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// This will return the full race name from the alias.
char *full_race(key)
	char *key;
{
	int ctr;
	
	for(ctr = 0; race_table[ctr].name; ctr++)
		if(!strcmpm(race_table[ctr].key, key)) 
			return race_table[ctr].name;

	return "";
}

char *full_class(key)
	char *key;
{
	int ctr;
	
	for(ctr = 0; class_table[ctr].name; ctr++)
		if(!strcmpm(class_table[ctr].key, key))
			return class_table[ctr].name;
	
	return "";
}

int get_class(player)
	object *player;
{
	char class[MIN_BUFSIZE];
	
	strcpy(class, full_class(atr_get(player, "Class")));
	
	if(!strcmpm(class, "mage"))
		return MAGE;
	else if (!strcmpm(class, "cleric"))
		return CLERIC;
	else if (!strcmpm(class, "warlock"))
		return WARLOCK;
	else if (!strcmpm(class, "necromancer"))
		return NECROMANCER;
	else if (!strcmpm(class, "psionicist"))
		return PSIONICIST;
	else if (!strcmpm(class, "barbarian"))
		return BARBARIAN;
	else if (!strcmpm(class, "knight"))
		return KNIGHT;
	else if (!strcmpm(class, "ninja"))
		return NINJA;
	else if (!strcmpm(class, "mercenary"))
		return MERCENARY;
	else if (!strcmpm(class, "fighter"))
		return FIGHTER;
	else if (!strcmpm(class, "historian"))
		return HISTORIAN;
	else if (!strcmpm(class, "merchant"))
		return MERCHANT;
	else if (!strcmpm(class, "thief"))
		return THIEF;
	else if (!strcmpm(class, "politician"))
		return POLITICIAN;
	else if (!strcmpm(class, "swashbuckler"))
		return SWASHBUCKLER;
	else
		return 0x0;
}

void do_give(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim;
	long gold = 0;
	
	if(!(victim=match_player(arg2))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	if(!isanumber(arg1)) {
		error_report(player, M_ENOTANUM);
		return;
	}
	
	gold = match_num(arg1);
	
	if ( (gold >= atol(atr_get(player, "Gold"))) && !(player->flags & WIZARD)) {
		error_report(player, M_EINSUFF);
		return;
	}
	
	if( !(player->flags & WIZARD) && player->location != victim->location) {
		error_report(player, M_EREMOTE);
		return;
	}
	
	SET_ATR(victim, "Gold", pstr("%ld", atol(atr_get(victim, "Gold")) + gold));
	if(!(player->flags & WIZARD)) 
		SET_ATR(player, "Gold", pstr("%ld", atol(atr_get(victim, "Gold")) - gold));
	notify(player, pstr("|R+|* |C|You just gave |C+|%ld|C| gold to |X|%s",
		gold, color_name(victim, -1)));
	notify(victim, pstr("|Y+|* |X|%s |C|gave you |C+|%ld |C|gold.", color_name(player, -1), gold));
}

FIGHT *get_fight(player, victim)
	object *player;
	object *victim;
{
	FIGHT *f;
	
	for(f=fight_queue; f; f=f->next)
		if( (f->victim == victim && f->attacker == player) || (f->victim == player && f->attacker == victim) )
			return (f);

	return(NULL);
}


int already_equipped(player, item)
	object *player;
	ITEMS *item;
{
	ITEMS *i;
	
	for(i = player->combat->items; i; i = i->next)	
		if( (i->flags & EQUIPPED) && (i->loc & item->location) && i != item) {
			notify(player, pstr("|W+|%s |C|is already equipped, use unequip.",
				i->name));
			return TRUE;
		}
	
	return FALSE;
}

int can_equip(item)
	ITEM *item;
{
	if(!(item->flags & WEAPON) && !(item->flags & ARMOR))
		return FALSE;
	
	return TRUE;
}

void do_equip(player, arg1)
	object *player;
	char *arg1;
{
	ITEMS *i;
	object *who;
	
	if(!arg1 || !*arg1) {
		do_equip_list(player, player);
		return;
	}

	if((who = match_moveable(arg1))) {
		do_equip_list(player, who);
		return;
	}
	
	for(i = player->combat->items; i; i=i->next)
		if(!strcmpm(i->name, arg1)) {
			if( (i->flags & EQUIPPED) ) {
				notify(player, pstr("|W+|%s |C|is already equipped.",
					i->name));
				return;
			} else if (already_equipped(player, i)) {
				return;
			} else if (!can_equip(i)) {
				notify(player, pstr("|W+|%s |C|cannot be equipped.",
					i->name));
				return;
			} else {
				i->flags |= EQUIPPED;
				i->loc |= i->location;
				notify(player, pstr("|W+|%s |C|has been equipped.",
					i->name));
			}
		}
}

void do_unequip(player, arg1)
	object *player;
	char *arg1;
{
	ITEMS *i;
	
	IS_ARGUMENT(player, (arg1));

	for(i = player->combat->items; i; i=i->next)
		if(!strcmpm(i->name, arg1)) {
			if( !(i->flags & EQUIPPED)) {
				notify(player, pstr("|W+|%s |C|is not equipped!",
					i->name));
				return;
			} else {
				i->flags &= ~EQUIPPED;
				i->loc &= ~i->location;
				notify(player, pstr("|W+|%s |C|has been unequipped.",
					i->name));
			}
		}
}
#endif /*COMBAT*/