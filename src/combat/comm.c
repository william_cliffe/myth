#ifdef COMBAT
/****************************************************************************
 MYTH - Combat communication calls.
 Coded by Saruk (03/05/99)
 ---
 These are the internal combat communication calls, to announce daylight,
 night, fighting, weather info, etc.
 ****************************************************************************/
#include <stdio.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

void internal_announce()
{
	char buf[2048];
	DESCRIPTOR *d;
	int ctr;
	
	strcpy(buf, "");	
	if( (myth_time.timeofday++) > 24)
		myth_time.timeofday = 1;
	myth_time.age_world++;
	
	/* REWRITE THE WEATHER SYSTEM!
	myth_time.weather_counter++;
	update_weather();
	*/
	
}

// display MYTH time.
void time_display(player)
	object *player;
{
	long world_age;
		
	world_age = (myth_time.age_world) * 3600;
	world_age += ((time(0) - last_update) * 30);
	
	
	make_header(player, "|W+|--- |C|MYTH World Time |W+|---");
	notify(player, pstr("\t|B+|The age of the world is.|W+|: |Y+|%s",time_format2(world_age)));
	notify(player, pstr("\t|B+|Current MYTH Time is....|W+|: |Y+|%d",(int)myth_time.timeofday));
	notify(player, pstr("\t|B+|Weather counter at......|W+|: |Y+|%d", myth_time.weather_counter));
}

/* How much have we got baby!? */
void do_score(player, arg1)
	object *player;
	char *arg1;
{
	object *who;
	char race[MAX_BUFSIZE], class[MAX_BUFSIZE];
	char exp[MAX_BUFSIZE], level[MAX_BUFSIZE];
	char temp[5][MAX_BUFSIZE];

	if(!*arg1) {
		who = player;
	} else {
		if(!(who=match_player(arg1))) 
			if(!(who = match_thing(arg1))) {
				error_report(player, M_ENOPLAY);
				return;
			}
	}	

	if((who->flags & TYPE_THING) && !( (who->combat->flags & MONSTER) || (who->combat->flags & NPC) ) ) {
		error_report(player, MC_ENOSTAT);
		return;
	}
	
	if(!can_control(player, who)) {
		error_report(player, M_ENOCNTRL);
		return;
	}

		
	/*This is to make it a little less painful...*/
	if( (who->flags & TYPE_PLAYER) || ( (who->flags & TYPE_THING) && (who->combat->flags & NPC)) ) {
		strcpy(race, full_race(atr_get(who, "Race")));
		strcpy(class, full_class(atr_get(who, "Class")));
	} else {
		strcpy(race, atr_get(who, "Race"));
		strcpy(class, atr_get(who, "Class"));
	}
	printf("we get passed here\n");
	strcpy(exp, rjust(comma(who->combat->stats->exp), 31));
	printf("here\n");
	strcpy(level, format_color(comma(who->combat->stats->level),11));
	sprintf(temp[0], "|C+|Magicpoints|W+|: |Y+|%5d|W+|/|Y+|%5d", who->combat->stats->mp, who->combat->stats->maxmp);
	sprintf(temp[1], "|C+|Intelligence|W+|: |Y+|%-5d |C+|Wisdom|W+|: |Y+|%-5d |C+|Luck|W+|: |Y+|%-5d |C+|Virtue|W+|: |Y+|%-5d",
		who->combat->stats->intel, who->combat->stats->wis, who->combat->stats->lck, who->combat->stats->vir);
	sprintf(temp[2], "|C+|Strength|W+|: |Y+|%-5d |C+|Dexterity|W+|: |Y+|%-5d |C+|Stamina|W+|: |Y+|%-5d",
		who->combat->stats->str, who->combat->stats->dex, who->combat->stats->sta);
	sprintf(temp[3], "|C+|Height|W+|: |Y+|%.2f|C+|ft. |C+|Weight|W+|: |Y+|%.2f|C+|lbs",
		(float)(who->combat->stats->height/12), (float)(who->combat->stats->weight));
	sprintf(temp[4], "|C|You can carry |Y+|%.2f|C| pounds.", (float)who->combat->stats->carry);
	notify(player, pstr("|B+|/%s\\", make_ln("-",76)));
	notify(player, pstr("|B+|||X|%s|B+||", cjust(atr_get(who, "Rainbow"),76)));
	notify(player, pstr("|B+|||X|%s|B+||", cjust("|W+|--- |C|Score Sheet |W+|---", 76)));
	notify(player, pstr(" |B|%s ", make_ln("-", 76)));
	notify(player, pstr("|B+|| |C+|Race|W+|: |Y+|%-12.12s |C+|Class|W+|: |Y+|%-12.12s |R+|%s |C|EXP |B+||", 
		race, class, exp));
	notify(player, pstr("|B+|| |C+|Level|W+|: |Y+|%s |C+|Hitpoints|W+|: |Y+|%5d/%5d %s |B+||", 
		level, who->combat->stats->hp, who->combat->stats->maxhp, rjust(temp[0], 32)));
	notify(player, pstr(" |B|%s ", make_ln("-", 76)));
	notify(player, pstr("|B+||%s|B+||", cjust(temp[1], 76)));
	notify(player, pstr("|B+||%s|B+||", cjust(temp[2], 76)));
	notify(player, pstr("|B+||%s|B+||", cjust(temp[3], 76)));
	notify(player, pstr("|B+||%s|B+||", cjust(temp[4], 76)));
	notify(player, pstr("|B+|\\%s/", make_ln("-", 76)));
}

void do_racelist(player)
	object *player;
{
	int ctr;
	CTABLE *r;
	
	make_header(player, "|W+|--- |C|List Of Races |W+|---");
	notify(player, pstr("|W+|%-12.12s %-3.3s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s",
		"RACE", "KEY", "STR", "DEX", "INT", "WIS", "VIR", "STA", "HP", "MP", "LCK"));
	for(ctr = 0; race_table[ctr].name; ctr++) {
		r = &race_table[ctr];
		notify(player, pstr("|C+|%-12.12s |Y+|%-3.3s |C|%5d %5d %5d %5d %5d %5d %5d %5d %5d",
			r->name, r->key, r->str, r->dex, r->intel, r->wis, r->vir, r->sta, r->hp, r->mp, r->lck));
	}
}

void do_classlist(player)
	object *player;
{
	int ctr;
	CTABLE *r;
	
	make_header(player, "|W+|--- |C|List of Classes|W+| ---");
	notify(player, pstr("|W+|%-12.12s %-3.3s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s %5.5s",
		"RACE", "KEY", "STR", "DEX", "INT", "WIS", "VIR", "STA", "HP", "MP", "LCK"));
	for(ctr = 0; class_table[ctr].name; ctr++) {
		r = &class_table[ctr];
		notify(player, pstr("|C+|%-12.12s |Y+|%-3.3s |C|%5d %5d %5d %5d %5d %5d %5d %5d %5d",
			r->name, r->key, r->str, r->dex, r->intel, r->wis, r->vir, r->sta, r->hp, r->mp, r->lck));
	}
}

char *itemflags(flag)
	int flag;
{
	char buf[MAX_BUFSIZE];
	char *p = buf;
	int ctr;
	
	for(ctr = 0; item_flags[ctr].name; ctr++)
		if(flag & item_flags[ctr].flag)
			*p++ = item_flags[ctr].parsed;
	
	*p++='\0';
	return stralloc(buf);
}

void do_itemlist(player)
	object *player;
{
	int ctr;
	
	make_header(player, "|W+|--- |C|List of Items|W+| ---");
	notify(player, pstr("|W+|%-5.5s %-15.15s %-32.32s %-5.5s %-5.5s",
		"ID", "NAME", "DESC", "COST", "TYPE"));
	for(ctr = 0; item_table[ctr].name; ctr++)
		notify(player, pstr("|C+|[|Y+|%3d|C+|] |Y|%-15.15s |C|%-32.32s |G+|%5d |M+|%-5.5s",
			item_table[ctr].id, item_table[ctr].name, item_table[ctr].desc, item_table[ctr].cost,
				itemflags(item_table[ctr].flags)));
}

void do_attack(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim;
	FIGHT *f;
	
	IS_ARGUMENT(player, (arg1));
	IS_DEAD((player), (player));
	/* Most of this should be in one TRUE/FALSE routine */
	
	if(!(victim = match_combatable(player, arg1))) {
		error_report(player, MC_ECANTFIGHT);
		return;
	}
	
	if(!(victim->location->combat->flags & CAN_FIGHT)) {
		error_report(player, MC_ENOFIGHT);
		return;
	}
	
	if(!check_combat(player)||!check_combat(victim)) {
		error_report(player, MC_ENOCOMBAT);
		return;
	}
			
	IS_DEAD((player), (victim));
	
	/** Check if attacker and victim are already fighting **/
	for(f = fight_queue; f; f=f->next)
		if( ((f->attacker == player || f->victim == player)) &&
			((f->attacker == victim) || (f->victim == victim)) ) {
				error_report(player, MC_ENOFIGHT);
				return;
		}
		
	/** Worry about arg2 (called shots) later **/
	do_fight(player, victim);
	notify(player, pstr("|Y+|* |C|You |R+|attack |X|%s|C|!", 
		color_name(victim, -1)));
	notify(victim, pstr("|Y+|* |C|You are |R+|attacked |C|by |X|%s|C|!", 
		color_name(player, -1)));
	notify(player, "|Y+|* |C|It is your turn.");
	/** Now we let the fight code take over **/
}

void notify_fight(fight, activator, string)
	FIGHT *fight;
	OBJECT *activator;
	char *string;
{
	CONTENTS *exception = NULL;
	
	add_exception(&exception, fight->attacker);
	add_exception(&exception, fight->victim);
	
	notify(fight->attacker, pstr("|X|%s |X|%s",
		(fight->attacker == activator) ? "|C|You" : color_name(activator, -1),
			string));
	
	notify(fight->victim, pstr("|X|%s |X|%s",
		(fight->victim == activator) ? "|C|You" : color_name(activator, -1),
			string));
	
	notify_in(fight->attacker, exception, pstr("|X|%s |X|%s", 
		color_name(activator, -1), string));
	
	clear_exception(exception);
}
#endif /* COMBAT */