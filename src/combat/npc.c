#ifdef COMBAT
/****************************************************************************
 MYTH - Non-Player Character Handler
 Coded by Saruk (03/21/99)
 ---
 Used to manipulate/create and generally handle NPC/Monsters...
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/* If the template does not exist, the monsters will not be found. So make sure the user
   knows the name of the monster he wishes to download. */
int create_monster(player, arg1)
	object *player;
	char *arg1;
{
	STATS *c;
	object *monster; 
	char buf[1024];
	FILE *f;
	
	IS_ARGUMENT(player, (arg1));
		
	if(!__allowed_name(strip_color(arg1))) 
		return M_EILLCHAR;

	sprintf(buf, "templates/monsters/%s", LC(arg1));
	
	if(!(f=fopen(buf, "r"))) 
		return M_ENOTEMPL;
	
	monster = create_object(player->ref, TYPE_THING, getstring(f));
	SET_ATR(monster, "Race", getstring(f));
	SET_ATR(monster, "Class", getstring(f));
	SET(monster->desc, getstring(f));
	SET_ATR(monster, "Caption", getstring(f));
	
	GENERATE(c, STATS);
	c->level = getint(f);
	c->exp = getint(f);
	c->str = getint(f);
	c->dex = getint(f);
	c->intel = getint(f);
	c->wis = getint(f);
	c->sta = getint(f);
	c->vir = getint(f);
	c->lck = getint(f);
	c->hp = getint(f);
	c->maxhp = c->hp;
	c->mp = getint(f);
	c->maxmp = c->mp;
	c->height = (float)atof(getstring(f));
	c->weight = (float)atof(getstring(f));
	c->carry = (float) (((c->weight + c->height) / 1.79) + (c->str * 1.2));
	monster->combat->time_to_breed = getlong(f);
	log_channel("log_misc", pstr("time_to_breed is: %ld", monster->combat->time_to_breed));	
	monster->combat->breed_time = time(0) + (monster->combat->time_to_breed/30);
	fclose(f);

	monster->combat->stats = c;
	monster->combat->flags = MONSTER|WANDER;
	monster->ownit = player;
        monster->link = player->ref;
        monster->linkto = player;
        monster->location = player;           
        add_content(player, monster);
        
        return monster->ref;
}

void do_mcreate(player, arg1)
	object *player;
	char *arg1;
{
	int ref;
	object *monster;
	
	if(!(ref = error_report(player, create_monster(player, arg1))))
		return;
	
	if(!(monster = match_thing(pstr("%d", ref)))) {
		error_report(player, M_ENOTEMPL);
		return;
	}
	
	notify(player, pstr("|C+|[|R+|MONSTER|C+|]|C| You created a |R+|%s |W+|(|Y+|%d|W+|)|C|...",
		monster->name, monster->ref));
}

char *cflaglist(player)
	object *player;
{
	char buf[MAX_BUFSIZE];
	flags *c;
	int ctr;
	
	strcpy(buf, "");
	for(ctr = 0; cflag_table[ctr].name; ctr++) {
		c = &cflag_table[ctr];
		if(player->combat->flags & c->flag)
			sprintf(buf, "%s%c", buf, c->parsed);
	}
	
	return stralloc(buf);
}

void wander_ai(ai)
	object *ai;
{
	char *exits[] = {"w", "n", "s", "e", "nw", "ne", "sw", "se", "u", "d"};
	int rand_exit = 0;
	
	rand_exit = random() % 9;
	time(&ai->combat->wander);
	if(match_pseudo_exit(ai, exits[rand_exit])) 
		return;
	if(match_real_exit(ai, exits[rand_exit]))
		return;
}

void breed_ai(ai)
	object *ai;
{
	object *o;
	int ctr = 0;
	
	ai->combat->breed_time = time(0) + (ai->combat->time_to_breed /30);

	for(o = thing_list; o; o=o->next)
		if(!strcmpm(atr_get(o, "Race"), atr_get(ai, "Race")) &&
			(o->combat->flags & MONSTER))
			ctr++;
	
	if(ctr >= MAX_MONSTERS) {
		log_channel("log_misc", pstr("Max monsters reached for %s",
			color_name(ai, -1)));
		return;
	}
	
	/* create the monster */
	if((ctr = create_monster(rootobj, atr_get(ai, "Race"))) < 1)
		return;
	
	if(!(o = match_thing(pstr("%d", ctr))))
		return;
	
	o->ownit = ai->ownit;
}
#endif /*COMBAT*/