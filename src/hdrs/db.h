/****************************************************************************
 MYTH - Database Headers.
 Coded by Saruk (01/12/99)
 ---
 General includes, such as the database and configuration definitions.
 ****************************************************************************/
// GNU C Includes
#include <stdio.h>
//#include <sys/time.h>
#include <time.h>

#define _XOPEN_SOURCE
#include <unistd.h>

typedef struct object_t object;
typedef struct object_t OBJECT;
typedef struct combat_event_queue_t CEVENT;

// General MYTH configuration definitions.
#include "memory.h"
#include "config.h"
#include "powers.h"
#include "plugin.h"
#include "command.h"
#include "ansi.h"

// Flags, errors, mail definitions, attribute table definitions.
// NOTE: Some includes are appended to this file. Some use object_t.
#include "locks.h"
#include "flags.h"
#include "error.h"
#include "func.h"
#include "attrib.h"
#include "combat.h"

// For queueing.
#include "queue.h"

// For channels
#include "comsys.h"

// Initial power settings.
#define PLAYER_POWINIT	25
#define	THING_POWINIT	10
#define ROOM_POWINIT	15
#define EXIT_POWINIT	5

// Object attribute definitions.
typedef struct attr_typ {
	int atype;			// An integer representation of the attribute.
	char *name;
	char *value;
	int flags;
	int powlvl;
	int dbref;

	struct attr_typ *next, *prev;
} attr, attr_list, ATTR, ATTR_LIST;

// Object command list structure. (for aliasing)
typedef struct clist_t {
	char *cmd;
	char *send;
	
	struct clist_t *next;
} CLIST;


// Content lists for objects.
typedef struct content_t {
	OBJECT *content;
	
	struct content_t *next;
} CONTENTS;

/** MAIL SYSTEM **/
#include "mail.h"

/** CHANNEL SYSTEM **/
#include "channels.h"


/** Parent structure **/
typedef struct parent_t {
	object *what;		// what object is part of this list?
	
	struct parent_t *next;	// er, uh, gee, what's next?;)
} PARENT;

/** Bulletin Board System 
typedef struct bboard_t {
	char *topic;		// Topic/Title
	int c_ref;		// creator reference
	object *creator;
	char *message;
	time_t created;
	time_t expiration;	
	int num_read;
	
	struct bboard_t *next;
} BBOARD;
**/

/** Move later **/
#define MAX_REGISTERS		100
#define MAX_ENVGLOBAL_SIZE	64000
#define MAX_RECURSION		500

/** Variable scope **/
#define SCOPE_LOCAL		0
#define SCOPE_GLOBAL		1

typedef struct dref_t {
  char *label;
  
  struct dref_t *next, *prev;
} DREF;

typedef struct data_t {
  char *label;
  unsigned int len;
  unsigned int scope;
  
  char *data;
  
  DREF *references, *rtail;
  
  struct data_t *next, *prev;
} DATA;

typedef struct env_t {
  unsigned int max_size;
  unsigned int cur_size;
  
  char *registers[MAX_REGISTERS];
  DATA *variables, *vtail;
} ENV, ENVIRONMENT;
	
// The object type. ( ** Our main database structure ** )
typedef struct object_t {
	char *name;			// name of object.
	char *desc;			// description of object.
	char *pass;			// password of object.
	ptype *pows;			// what powers we got goin' on here?
	dbref ref;			// database reference number.
	dbref owner;			// owner priveleges of object.
	dbref loc;			// location of object.
	dbref link;			// this points to the home of an object.
	int powlvl;			// power level of object (probably changing)
	unsigned long long flags;	// object flags.
	int script_lines;		// number of script lines.
	int qlev;			// we don't save this.
	int clev;			// nor this.


	// Pointers...
	MAIL *mail;			// Our mail system
	CLIST *cmdlist;			// object command list.
	CHANNEL *channels;		// channel list.
	CONTENTS *contents;		// a list of objects in contents.
	ATTR *alist;			// Object's attribute list.
	ATTR *atail;			// Tail of the object's attribute list. NULL on creation
	
	// COMBAT -->
	CSTATS *combat;			// Combat attributes.
	ITEMLIST *items, *itemstail;		
	ITEMLIST *equipped, *equippedtail;
        /*CREATURE *creature;		// Creature type, flags, and specifics.*/

	// <-- COMBAT
	
	/** Softcode-needed pointers:
	  Children & Parents should be saved in this order:
	  dbref:dbref:dbref:dbref:dbref:dbref:dbref
	**/
	PARENT *parent;			// Parents of this object.
	PARENT *children;		// Children of this object.
	DATA *list;			// We're going to try this out.
	ENV *environment;		// The environment the user's functions take place in.
	
	// pointers to objects.
	struct object_t *ownit;
	struct object_t *location;
	struct object_t *linkto;
	struct object_t *zone;

	// pointer to next object in list. 
	struct object_t *next;
	struct object_t *prev;		// we'll use this later when we revise the db format.
} OBJ;

typedef struct func_call_list_t {
  char *name;
  
  void (*func)();
  
  struct func_call_list_t *next, *prev;
} FLIST;

// Assign external/global object lists.
extern char *gvar[10];
extern object *genactor;
extern int dbtop;
extern int db_first_free;
extern object *exit_list;
extern object *exitlist_tail;
extern object *thing_list;
extern object *thinglist_tail;
extern object *room_list;
extern object *roomlist_tail;
extern object *player_list;
extern object *playerlist_tail;
extern object *rootobj;
extern object **db;
extern object *cmd_player;
extern object *global_match;      // explained in misc/command.c
extern ATTR_LIST myth_defined[];
extern ATTR *alist;
extern FLIST *flist, *ftail;
extern ENV *envglobal;

// Possibly shadowed functions for plugins
extern void (*match_twice)(void);
//extern BBOARD *bblist;
