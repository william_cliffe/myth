/***********************************
 MYTH - Configuration Tool Header
 Coded by Saruk (01/20/99)
 ---
 ***********************************/

struct cfg_typ {
	char *cfg_default;
	char *cfg_prompt;
	char *cfg_designation;
};

struct cfg_typ config_table[]={
	{"NewMYTH", "MYTH Name", "myth_name"},
	{"5000", "MYTH Port", "port"},
	{"Saruk", "MYTH Admin", "admin_name"},
	{"mdb", "Name of initial database?", "db_name"},
	{"maildb", "Name of initial mail database?", "maildb"},
	{"comdb", "Name of channel database?", "comdb"},
	{"MYTH undescribed object.", "Initial object description?", "default_description"},
	{"myth", "What should the default password be?", "default_password"},	
	{"msgs/welcome.txt", "Where and what is your welcome file?", "welcome_file"},
	{"msgs", "Where are your *.txt files kept?", "text_path"},
	{"scriptdb", "What is your script database?", "scriptdb"},
	{"combatdb", "What is your combat database?", "combatdb"},
	{"credits", "What is your default currency type?", "myth_currency"},
	{"item.db", "Default items db?", "item_db"},
	{"item.templates", "Templates db?", "item_template"},
	{NULL}};
