/****************************************************************************
 MYTH - Queue Header
 Coded by Jagged (01/14/01)
 ---
 ****************************************************************************/

#define NUMARGS		10 // only allow 10 substitue variables.
#define NUM_FUN_ARGS	NUMARGS

typedef struct var_list_t {
	char *var[NUMARGS];
} VARLIST;

 
typedef struct queue_t {
	char *action;
	object *activator; 	// this refers to the object that triggered the queued event.
	object *enactor;	// this refers to the enactor of the queued event.
	time_t seconds;
	time_t timestamp;
	char *var[NUMARGS];

	struct queue_t *next;
	struct queue_t *prev;
} QUEUE;

extern QUEUE *queue;
extern QUEUE *queuetail;
extern QUEUE *qnow;
extern QUEUE *qnowtail;

