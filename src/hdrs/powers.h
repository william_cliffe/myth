/****************************************************************************
 MYTH - Powers Header
 Coded by Saruk (01/29/03)
 ---
 This holds the power structure.
 ****************************************************************************/


/* Classes */
#define CLASS_GUEST		1
#define CLASS_CITIZEN		2
#define CLASS_GUIDE		3
#define CLASS_BUILDER		4
#define CLASS_OFFICIAL		5
#define CLASS_ADMINISTRATOR	6
#define CLASS_DIRECTOR		7


#define PW_NO		1
#define PW_YESLT	2
#define PW_YESEQ	3
#define PW_YES		4

#define NUM_CLASSES     	8
#define NUM_LIST_CLASSES 	7


/*

possibly something like:

   ptype *pows;
   
   pows = malloc(sizeof(ptype)*3);
   pows[0] = pow;
   pows[1] = YES/YESEQ/YESLT  ; no reason to add a NO power.
   so like... pow[0] == 5, then it's POW_SUMMON
   and then pow[1] == YES would let them summon ANYONE.
   but...
   we need to do it all so, then...
   
   malloc(nlist * sizeof(ptype));
   where nlist +=2 for each power...
   
   
   hmm.. i think i got it sorta now.
*/

typedef char ptype;

extern struct pow_list {
	char *name;
	int pnum;
	char *desc;

	int init_pow[NUM_LIST_CLASSES];
	int max_pow[NUM_LIST_CLASSES];	
} powers[];

#define POW_FREE		1
#define POW_BOOT		2
#define POW_SHUTDOWN		3
#define POW_JOIN		4
#define POW_SUMMON		5
#define POW_PRECYCLE		6
#define POW_PCREATE		7
#define POW_CLASS		8
#define POW_SETPOW		9
#define POW_REMOTE		10
#define POW_WIZADD		11
#define POW_MODIFY		12
#define POW_SEEATR		13
#define POW_COMBAT		14
#define POW_SECURITY		15
#define NUM_POWS		15
#define MAX_POWERNAMELEN	16
