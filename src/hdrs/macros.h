/* MACROS - Move these */
#define ISNULL(x) { \
	if(!(x) || !*(x)) \
	  return 1; \
	}

#define IS_ARGUMENT(o, x) { \
	if(!(x) || !*(x)) { \
		error_report(o, M_EINVARG); \
		return; \
	} \
	}

#define IS_ANUMBER(o, x) { \
	if(!isanumber(x)) { \
		error_report(o, M_ENOTANUM); \
		return; \
	} \
	}

#define ISWIZARD(o) ((o->flags & WIZARD))

#define DEBUG(str) { \
	log_channel("log_debug", str); \
	} 
