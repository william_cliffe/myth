/****************************************************************************
 MYTH - IO Header
 Coded by Saruk (03/26/99)
 ---
 Declare all the (local-to) io.c procedures/functions.
 ****************************************************************************/

/** MYTH Declared Types **/
TQUEUE *TQUEUE_FREE P((TQUEUE *));
TQUEUE *add_to_tqueue P((TQUEUE *, char *));
DESCRIPTOR *new_connection P((int));
DESCRIPTOR *initializesock P((int, struct sockaddr_in6 *, char *, int));

/** Voids **/
#ifdef CATCHSEG
void panic_list P((void));
#endif /*CATCHSEG*/
void attach_players P((void));
void do_game_state P((void));
void echo_off P((DESCRIPTOR *, char *));
void echo_on P((DESCRIPTOR *, char *));
void dfree P((int));
void DeleteDescriptor P((struct descriptor_data *));
void AppendDescriptor P((struct descriptor_data *));
void parse_connect P((struct descriptor_data *, char *));
void redirect_io P((void));
void make_nonblocking P((int));
void process_commands P((void));
void do_command P((struct descriptor_data *, char *));
void dwrite P((struct descriptor_data *, char *));
void report_online P((void));
void close_sockets P((void));
void open_sockets P((void));
void parse_cmd P((char **));

/** Statics **/
static int make_socket P((int));
static int process_input P((struct descriptor_data *));
static void set_signals P((void));
