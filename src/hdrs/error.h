/****************************************************************************
 MYTH - Error exceptions.
 Coded by Saruk (02/06/99)
 ---
 Error numbers.
 ****************************************************************************/
 
// M_E<ERROR> will return from -1001 and lower.
// If you reference this with the error_table in error.c you will find out what they do, 
// since there's no need for comments here.
#define M_EILLATTR	-1001
#define M_EILLNAME	-1002
#define M_EUNKCMD	-1003
#define M_EBADPOW	-1004
#define M_ENOTWIZ	-1005
#define M_EBADFLG	-1006
#define M_EBADAFLG	-1007
#define M_ENOBLTIN	-1008
#define M_ENOFILE	-1009
#define M_ENOPLAY	-1010
#define M_ENOTHERE	-1011
#define M_EWAYBAD	-1012
#define M_ENOCANDO	-1013
#define M_EDELEXIT	-1014
#define M_EBADPASS	-1015
#define M_EILLSTRING	-1016
#define M_ENOQUOTA	-1017
#define M_ETWOEXIT	-1018
#define M_EBADLNK	-1019
#define M_ENOTEXIT	-1020
#define M_EINVARG	-1021
#define M_ESHDBLT	-1022
#define M_ENOCNTRL	-1023
#define M_ESHDNAME	-1024
#define M_EISLOCKED	-1025
#define M_EREPORT	-1026
#define M_ERECURSE	-1027
#define M_EINUSE	-1028
#define M_EEXCESS	-1029
#define M_EFLGSET	-1030
#define M_ENOROOM	-1031
#define M_EINSUFF	-1032
#define M_ENOTFOUND	-1033
#define M_ENOMSG	-1034
#define M_EILLCHAR	-1035
#define M_EUNSET	-1036
#define M_EREMOTE	-1037
#define M_ENOTHELD	-1038
#define M_EOBJID	-1039
#define M_EBADEXIT	-1040
#define M_EPROTECTED	-1041
#define M_ENOTALK	-1042
#define M_ECHALSHDW	-1043
#define M_ENOTANUM	-1044
#define M_ECHNOTDONE	-1045
#define M_ECHINVSTAT	-1046
#define M_ECHINVDEF	-1047
#define M_ECHNOTFOUND	-1048
#define M_ECHNOQUOTA	-1049
#define M_ECHSHDW	-1050
#define M_ECHEMPTY	-1051
#define M_ECHNOJOIN	-1052
#define M_ECHNOTON	-1053
#define M_ECHNODFLT	-1054
#define M_ECHALREADY	-1055
#define M_ECHLOCPLY	-1056
#define M_EPAGELOCK	-1057
#define M_ETOOBOARD	-1058
#define M_ENODIG	-1059
#define M_ENOENTER	-1060
#define M_ENOSWITCH	-1061

typedef struct error_t {
	char *errmsg;
	int errornum;
} ERROR_T;

extern ERROR_T error_table[];
