/*** creature.h $ havencrag (11/28/2004)

  The creature struct goes on OBJECTs.
  
  Creatures will typically only go N, E, S, W, NE, NW, SE, SW, D, or U.
  
***/

#define MAX_CREATURE			250	// Maximum number of creatures of a specific type.
#define MAX_CREATURE_GESTATION		31536000

// CREATURE FLAGS
#define CREATURE_BREED			(1 << 0)
#define CREATURE_RESPAWN		(1 << 1)
#define CREATURE_AGGRESSIVE		(1 << 2)
#define CREATURE_MASTER			(1 << 3)
#define CREATURE_BOSS			(1 << 4)
#define CREATURE_WILLFLEE		(1 << 5)
#define CREATURE_PACKMENTALITY		(1 << 6)
#define CREATURE_WANDER			(1 << 7)

// CREATURE INTELLIGENCE
#define CREATURE_MORON			0
#define CREATURE_STUPID			1
#define CREATURE_AVERAGE		2
#define CREATURE_INTELLIGENT		3
#define CREATURE_GENIUS			4
#define CREATURE_SUPERGENIUS		5

// Creature Modify Tags
#define CMOD_MAXHP			1
#define CMOD_MAXMP			2
#define CMOD_STR			3
#define CMOD_INT			4
#define CMOD_WIS			5
#define CMOD_STA			6
#define CMOD_CON			7
#define CMOD_DEX			8
#define CMOD_LCK			9
#define CMOD_CHA			10
#define CMOD_HEIGHT			11
#define CMOD_WEIGHT			12
#define CMOD_IQ				13
#define CMOD_GENDER			14
#define CMOD_PREFERENCE			15
#define CMOD_GESTATION			16
#define CMOD_AREA			17
#define CMOD_CLIMATE			18
#define CMOD_ALIGNMENT			19
#define CMOD_LEVEL			20
#define CMOD_LIFESPAN			21                                                                                                                                              

typedef struct creature_template_t {
  char *name;
  unsigned int id;				// Unique ID for the creature.
  unsigned long long flags;			// Creature flags.
  unsigned int iq;				// 0 - 5, How intelligent is the creature.
  unsigned int climate;				// What type of climate does this creature prefer?
  unsigned int area;				// What area does this creature prefer?
  unsigned int lifespan;			// How long can the creature live?
  short gender;					// Male or female? 
  short preference;				// 0 - Asexual, 1 - Mate
  time_t gestation;				// How long before creature gives birth?
  
  CSTATS *stats;				// Creature's combat stats.

  struct creature_template_t *next, *prev;
} CRTEMPLATE;

typedef struct creature_object_t {
  unsigned int id;
  unsigned int t_id;				// Template ID
  unsigned long long flags;			// Instance Flags
  unsigned int age;				// How old is this creature?  
  
  struct creature_object_t *next, *prev;
} CREATURE;

extern CRTEMPLATE *crtemplates, *crtemplatestail;
extern CREATURE *creatures, *creaturestail;
extern CREATURE **creaturedb;
extern COMBATFLAGLIST creatureflag_list[];
/** END OF CREATURES.H **/
