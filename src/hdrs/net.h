/****************************************************************************
 MYTH - Net Header
 Coded by Saruk (01/10/99)
 ---
 Our network header file. For descriptor list and text queues.
 ***************************************************************************/

#include <sys/types.h>
#include <netinet/in.h>

#define MAX_OUTPUT	300

#define WAITCONNECT	0
#define WAITPASS	1
#define CONNECTED	2
#define RELOADCONNECT	3
#define SETNAME		4
#define SETPASS		5
#define SETEMAIL	6
#define BOOTITOFF	7
#define PASTE_MODE	8

// This is for combat.
#define SETRACE		20
#define SETCLASS	21
#define SETSTATS	22
#define WAITPROMPT	23

#define SET_DONE	0

typedef struct txt_queue {
	char *buffer;
	int num_lines;
	
	struct txt_queue *next;
	struct txt_queue *prev;
} TQUEUE;

typedef struct descriptor_data {
 	int active;
	int ref;
	int eor;
	int savestate;			// This is for WAITPROMPT.
	int state;
	int descriptor;
	int internal;
	int input_flag;
	int paste_flag;
	int output_flag;
	int output_lines;
	int paste_lines;
	int raw_input_len;
	int term_width;
	int term_height;
	int stats[10];			// this is for combat stats.

	long connected_at;
	long last_time;
		
	char addr[INET6_ADDRSTRLEN];
	char *paste_cmd;
	unsigned char *raw_input;
	unsigned char *raw_input_at;
	unsigned long input_chars;
	unsigned long output_chars;
	
	object *player;
	TQUEUE *input_queue;
	TQUEUE *paste_queue;
	TQUEUE *output_queue;
		
	struct sockaddr_in6 address;
	struct descriptor_data *next;
	struct descriptor_data *prev;
} DESCRIPTOR, DDATA;

extern struct descriptor_data *descriptor_list, *descriptor_tail;
extern int maxd;
