/****************************************************************************
 MYTH v1.00 Copyright Information 
 Coded by Saruk (01/20/99)
 ---
 
       MYTH is Copyright (C) 1999 by Will Cliffe. All Rights Reserved. 

 [ARTICLES OF COPYRIGHT]
 	
 1.) This project offers no warranty, either expressed or implied. The author(s)
 of this project are not required to support this project in any way, shape, or
 form.
 
 2.) This project is a free project, directly forbidding anyone to use code from 
 this project in any commercial venture without first contacting the author of
 the code. 
 
 3.) Many thanks to creators of several styles of online games out there that have
 inspired MYTH or been a direct influence. TinyMUSE, TinyMUSH, PennMUSH, MOO, and
 several versions of MUD have been routinely visited for any possible ideas. The
 attempt with MYTH is to make a versatile server for the purposes of commercial 
 entertainment.
 
 ****************************************************************************/

/* The copyright date of this MYTH */
#define COPYRIGHT_DATE		"January 10th, 1999"

/* Current MYTH developer */
#define DEVELOPER		"Saruk"

/* Whichever names are in the PREV_DEVELOPERS should be kept, and your name should 
   preceed them (if you are developing MYTH source). */
#define PREV_DEVELOPERS		"Saruk" 
