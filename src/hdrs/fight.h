/*** fight.h $ havencrag ***/

#define FIGHT_INTERVAL		2	// Every 3 seconds.

typedef struct cevent_list {
  CEVENT *event;
  
  struct cevent_list *next, *prev;
} CEVENTLIST;

typedef struct fight_action_t {
  object *attacker;			// Who initiated the fight.
  object *target;			// Who is being attacked.
  
  // This is for turns.
  object *turn_attack;			// Who's turn is it to attack?
  object *turn_defend;			// Who's turn is it to defend?
  
  // Time stuff.
  time_t started;			// Start time of the fight.
  time_t nextround;			// Next time to do a round.

  int a_blocks, a_hits;			// How many times the attacker blocked or hit.
  int t_blocks, t_hits;			// How many times the defender blocked or hit.

  int a_weapon, a_armour;		// Attacker's weapon and armour modifiers.
  int t_weapon, t_armour;		// Target's weapon and armour modifiers.
  
  CEVENTLIST *eventlist, *eventtail;	// There may be events to consider.
/* When completed, this will give us a list of active spells and skills
   associated with this fight sequence.
   
  SPELLACTION *spells;  
  SKILLACTION *skills;
*/

  struct fight_action_t *next;
  struct fight_action_t *prev;
} FIGHTQUEUE;

extern FIGHTQUEUE *fightlist, *fightlisttail;
