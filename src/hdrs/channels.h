/****************************************************************************
 MYTH - Channel System Header
 Coded by Saruk (04/09/99)
 ---
 This will hold the channel structures and definitions.
 ****************************************************************************/

#define CHANNEL_OFF		FALSE
#define CHANNEL_ON		TRUE
#define CHANNEL_CONSTRUCTION	2

typedef struct channel_t {
	int ref;
	char *name;
	char *desc;
	char *title;
	char *locks;
	char *alias;
	char *colour;
	int oref;
	int flags;
	int powlvl;
	int status;
	int chdef;
	
	OBJECT *owner;
	OBJECT *player;
	struct channel_t *chobj;
	struct channel_t *next;
} CHANNEL;

extern CHANNEL *channel_list;
