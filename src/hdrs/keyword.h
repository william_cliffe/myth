/****************************************************************************
 MYTH - Keyword List
 Coded by Saruk (06/24/99)
 ---
 This is the list of keywords for the actual script section. The actual stru-
 cture for the list is in script.h. This file should be included in tables.h
 ****************************************************************************/



KEYWORDS keywords[]={
	{"var", 1, mc_var, 25, 0},
	{"new", 1, mc_new, 25, 0},
	{NULL}};