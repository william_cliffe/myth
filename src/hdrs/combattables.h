/*** combattables.h $ havencrag (11/28/2004)
***/

/** Materials List **/
MATERIALFLAGLIST materials_list[] = {
  {"Cloth", MAT_CLOTH, ARMOUR},
  {"Wool", MAT_WOOL, ARMOUR},
  {"Leather", MAT_LEATHER, ARMOUR},
  {"Chainmaille", MAT_CHAINMAILLE, ARMOUR},
  {"Wood", MAT_WOOD, WEAPON|SHIELD},
  {"Iron", MAT_IRON, WEAPON|SHIELD},
  {"Steel", MAT_STEEL, WEAPON|SHIELD},
  {"Cobalt", MAT_COBALT, WEAPON|SHIELD},
  {"Ruby", MAT_RUBY, SPECIAL},
  {"Emerald", MAT_EMERALD, SPECIAL},
  {"Sapphire", MAT_SAPPHIRE, SPECIAL},
  {"Pearl", MAT_PEARL, SPECIAL},
  {"Diamond", MAT_DIAMOND, SPECIAL|WEAPON|SHIELD},
  {"Sacredium", MAT_SACREDIUM, SPECIAL|ARMOUR|WEAPON|SHIELD},
  {NULL}};

/** Combat Flags **/
COMBATFLAGLIST combatflag_list[]= {
  {"Unfinished", UNFINISHED},
  {"Weapon", WEAPON},
  {"Armour", ARMOUR},
  {"Shield", SHIELD},
  {"Food", FOOD},
  {"Drink", DRINK},
  {"Drug", DRUG},
  {"Special", SPECIAL},
  {"Breakable", BREAKABLE},
  {"Unmodifiable", UNMODIFIABLE},
  {"Noclone", NOCLONE},
  {"Potion", POTION},
  {NULL}};

/** Affect List **/
COMBATFLAGLIST affect_list[]={
  {"Vision", AFFECT_VISION},
  {"Walking", AFFECT_WALKING},
  {"Standing", AFFECT_STANDING},
  {"Fighting", AFFECT_FIGHTING},
  {"Hunger", AFFECT_HUNGER},
  {"Thirst", AFFECT_THIRST},
  {"Health", AFFECT_HEALTH},
  {"Magic", AFFECT_MAGIC},
  {NULL}};

/** Effect List **/
COMBATFLAGLIST effect_list[]={
  {"Assault", EFFECT_ASSAULT},
  {"Protect", EFFECT_PROTECT},
  {"Quench", EFFECT_QUENCH},
  {"Quell", EFFECT_QUELL},
  {"Increase", EFFECT_INCREASE},
  {"Decrease", EFFECT_DECREASE},
  {NULL}};

/** Wear list **/
COMBATFLAGLIST wear_list[]={
  {"None", LOC_NONE, 0},
  {"Head", LOC_HEAD, ARMOUR},
  {"Hands", LOC_HANDS, ARMOUR},
  {"Torso", LOC_TORSO, ARMOUR},
  {"Legs", LOC_LEGS, ARMOUR},
  {"Feet", LOC_FEET, ARMOUR},
  {"Cloak", LOC_CLOAK, ARMOUR},
  {"Dualwield", LOC_DUALWIELD, WEAPON},
  {"Shield", LOC_SHIELD, SHIELD},
  {"Neck", LOC_NECK, ARMOUR},
  {NULL}};

ITEMATTRIBUTELIST iattr_list[]={
  {"Strength", MOD_STRENGTH},
  {"Intelligence", MOD_INTELLIGENCE},
  {"Wisdom", MOD_WISDOM},
  {"Dexterity", MOD_DEXTERITY},
  {"Constitution", MOD_CONSTITUTION},
  {"Charisma", MOD_CHARISMA},
  {"Stamina", MOD_STAMINA},
  {"Luck", MOD_LUCK},
  {"Softcode", INITIATE_SOFTCODE},
  {NULL}};

COMBATFLAGLIST creatureflag_list[]={
  {"Breed", CREATURE_BREED},
  {"Respawn", CREATURE_RESPAWN},
  {"Aggressive", CREATURE_AGGRESSIVE},
  {"Master", CREATURE_MASTER},
  {"Boss", CREATURE_BOSS},
  {"WillFlee", CREATURE_WILLFLEE},
  {"PackMentality", CREATURE_PACKMENTALITY},
  {"Wander", CREATURE_WANDER},
  {NULL}};

COMBATFLAGLIST creatureiq_list[]={
  {"Moron", CREATURE_MORON},
  {"Stupid", CREATURE_STUPID},
  {"Average", CREATURE_AVERAGE},
  {"Intelligent", CREATURE_INTELLIGENT},
  {"Genius", CREATURE_GENIUS},
  {"SuperGenius", CREATURE_SUPERGENIUS},
  {NULL}};

/** END OF COMBATTABLES.H **/
