/****************************************************************************
 MYTH - Table header.
 Coded by Saruk (02/11/99)
 ---
 This goes in io/table.c where all external tables are sent to. It allows for
 easy addition.
 ****************************************************************************/

flags flag_table[] = {
	{"Player", 'P', TYPE_PLAYER, -1, TYPE_PLAYER},
	{"Room", 'R', TYPE_ROOM, -1,TYPE_ROOM},
	{"Exit", 'E', TYPE_EXIT, -1,TYPE_EXIT},
	{"Thing", 'T', TYPE_THING, -1, TYPE_THING},
	{"Can_Tel",'t', CAN_TEL, NOMASK,NOMASK},
	{"Wizard",'W', WIZARD, WIZARD,NOMASK},
	{"Invisible",'i', INVISIBLE, WIZARD,NOMASK},
	{"Connect",'c', CONNECT, WIZARD,TYPE_PLAYER},
	{"Locked",'l', LOCKED, NOMASK,NOMASK},
	{"Visible",'v',VISIBLE, NOMASK, NOMASK},
	{"Guest", 'G', GUEST, WIZARD, TYPE_PLAYER},
	{"Quiet", 'q', QUIET, NOMASK, TYPE_PLAYER|TYPE_ROOM},
	{"Nospoof", 'N', NOSPOOF, NOMASK, TYPE_PLAYER},
	{"Hidden", 'H', HIDDEN, NOMASK, TYPE_EXIT},
	{"Ansi", 'A', ANSI, NOMASK, TYPE_PLAYER},
	{"No_Walls", 'n', NO_WALLS, NOMASK, TYPE_PLAYER},
	{"Suspect", 'S', SUSPECT, WIZARD, NOMASK},	
	{"Link_Ok", 'L', LINK_OK, NOMASK, TYPE_ROOM},
	{"Enter_Ok", 'e', ENTER_OK, NOMASK, TYPE_THING},
	{"Inherit", 'I', INHERIT, NOMASK, NOMASK},
	{"Puppet", 'p', PUPPET, NOMASK, TYPE_THING},
	{"Zone", 'Z', ZONE, NOMASK, TYPE_THING}, 
	{"Jump", 'J', JUMP_OK, NOMASK, TYPE_ROOM},
	{"Haven", 'h', HAVEN, NOMASK, NOMASK},
	{"Shop", '@', SHOP, WIZARD, TYPE_ROOM},
	{NULL}};

flags chflag_table[] = {
	{"Wizard", 'W', WIZARD, NOMASK, NOMASK},
	{"Locked", 'L', LOCKED, NOMASK, NOMASK},
	{NULL}};
	
flags aflag_table[] = {
	{"AWizard",'W', AWIZARD, WIZARD},
	{"AInherit",'I', AINH, NOMASK},
	{"ASee",'V', ASEE, NOMASK},
	{"ADark",'D', ADARK, NOMASK},
	{"AImp",'i', AIMP, WIZARD},
	{"ASolid",'S', ASOLID, WIZARD},
	{"ACheck",'C', ACHECK, WIZARD},
	{"AExit", 'E', AEXIT, WIZARD},
	{"AProgram", 'P', APROGRAM, NOMASK},
	{"ANochange", '!', ANOCHANGE, WIZARD},
	{"AFunction", 'F', AFUNC, NOMASK},
	{"AUser", 'U', AUSER, NOMASK},
	{NULL}};

#include "combattables.h"
#include "attrtable.h"


LOCKS lock_table[]={
	{"power", lock_power},
	{"flag", lock_flag},
	{NULL}};
		
#include "errortable.h"
