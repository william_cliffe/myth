/****************************************************************************
 MYTH - Text Handling Header
 Coded by Saruk (03/24/99)
 ---
 A header file that intends to process text script from files.
 ****************************************************************************/

#define PLAIN		0
#define TOPIC		1
#define SEE_ALSO	2
#define INCLUDE_FILE	3
#define PARSE		4
