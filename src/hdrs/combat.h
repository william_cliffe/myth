/*** combat.h $ havencrag (10/27/2004) ***/

#define COMBAT_ATTRS		8
#define MAX_INSHOP_ITEM		1000
#define REGEN_INTERVAL		120		// Regen every 2 minutes.

// Combat flags
#define	NONCOMBATABLE		(1 << 0)
#define DEAD			(1 << 1)
#define FIGHT			(1 << 2)
#define INREGEN			(1 << 3)
#define FLEE			(1 << 4)

// Combat Event Types
#define SOFTCODE_EVENT		1		// This will perform a softcode event.
#define DELIMODIFIER_EVENT	2		// This will drop an item modifier.
#define ADDIMODIFIER_EVENT	3		// This will add an item modifier.
#define SPELL_EVENT		4		// This is a spell event.
#define SKILL_EVENT		5		// A skill event.
#define REGEN_EVENT		6		// Separate from the regen queue, will regenerate something.

// Combat Event Flags
#define EVENT_NEVERENDING	(1 << 1)
#define EVENT_INFIGHT		(1 << 2)

/** PUT SOMETHING HERE **/

#include "items.h"
#include "fight.h"

typedef struct material_flaglist_t {
  char *name;
  unsigned long long material;
  unsigned long long flag;
} MATERIALFLAGLIST;

typedef struct combat_flaglist_t {
  char *name;
  unsigned long long flag;
  unsigned long long check;
} COMBATFLAGLIST;

typedef struct regen_queue_t {
  object *player;
  
  struct regen_queue_t *next, *prev;
} REGENQUEUE;

typedef struct combat_classrace_table {
  char *name;
  char *key;
  int str;
  int intel;
  int wis;
  int dex;
  int con;
  int cha;
  int sta;
  int lck;
  int mp;
  int hp;
  
  float weight;
  float height;

  struct combat_classrace_table *next;
} RACETABLE, CLASSTABLE;

typedef struct combat_stats {
  unsigned int level;
  int str;
  int intel;
  int wis;
  int dex;
  int con;
  int cha;
  int sta;
  int lck;
  int mp;
  int maxmp;
  int hp;
  int maxhp;
  unsigned long exp;
  int strmod;
  int intmod;
  int wismod;
  int dexmod;
  int conmod;
  int stamod;
  int lckmod;
  int chamod;
  
  float weight;
  float height;
  float carry;
  
  int alignment;			// Alignment will be -256 to +256

  // Other cool combat stats.
  unsigned int kills;			// How many kills you've made.
  unsigned int deaths;			// How many times you've died.
  unsigned int blocks;			// How many blocks you've made.
  unsigned long long cflags;		// Combat flags.  
  unsigned int skillpoints;		// Skill points work for magic or skills.
  
} CSTATS;


/** Combat Events Queue **/
struct combat_event_queue_t {
  unsigned int id;			// Specific ID for the event. May not be needed.
  int type;				// Event type
  unsigned long long flags;		// Are there flags associated with this event?
  unsigned int num_fights;		// Mainly used for DELIMODIFIER events where a 
                                        // combatant is in multiple fights. This will
                                        // denote the number of fights so that DELIMODIFIERS
                                        // will only be parsed upon completion of the final
                                        // fight.
  
  object *thing;			// What does it affect?
  FIGHTQUEUE *fight;			// Is there something in the fight queue associated with this?
  void (*func)();			// Is there a function associated with this event?
                                        // Functions, if associated are linked through a table of allowed
                                        // functions.
  char *softcode;			// Is there softcode associated with this event?
  
  time_t started, endtime;		// When the event was started, and when it's supposed to end.
                                        // if endtime is -1, then the event never ends.
  
  ITEMLIST *item;			// Is there an item associated with this event?
  
  /** 
  SPELLACTION *spell;
  SKILLACTION *skill.
  **/
  
  struct combat_event_queue_t *next, *prev;
};

#include "creature.h"

extern RACETABLE *racetable;
extern CLASSTABLE *classtable;
extern ITEMLIST *itemlist, *itemlisttail;
extern MATERIALFLAGLIST materials_list[];
extern COMBATFLAGLIST combatflag_list[], affect_list[], effect_list[], wear_list[];
extern REGENQUEUE *regenlist, *regenlisttail;
extern ITEMATTRIBUTELIST iattr_list[];
extern ITEMLIST **itemdb;

extern CEVENT *cevents, *ceventtail;

// Other externs
extern time_t combat_last_regen;
extern int itemtop;
