/****************************************************************************
 MYTH - Memory Allocation Header
 Coded by Saruk (04/10/99)
 ---
 Header for the stack system
 ****************************************************************************/
#define VOLATILE	0
#define SOLID 		1
#define BLOCKFREED	1

#define MAX_FRAMES	75

typedef struct mstack_t {
	void *ptr;
	size_t size;
	int instance;		// has it been freed or not? if it has, we can reuse it in stack_em
		
	struct mstack_t *next;
} MSTACK;

struct VMHDR {
  char type;
  
};

