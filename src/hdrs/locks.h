/****************************************************************************
 MYTH - Locks Header
 Coded by Saruk (04/01/99)
 ---
 This holds the lock structure.
 ****************************************************************************/

typedef struct lock_func_t {
	char *name;
	int (*func)();
} LOCKS;

extern LOCKS lock_table[];
