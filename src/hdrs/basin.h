/****************************************************************************
 MYTH - Memory Basin Header
 Coded by sarukie (05/31/04)
 ---
 MYTH's own memory allocation subsystem.
 
 We want a heap of memory in which the server can control, actively
 moving memory, freeing, and allocating more. We should also have intervals
 in which the memory is realigned, thus creating faster processing times.
 
 We do not want to overload the server with basin_align() commands, so we'll 
 set in a TICK system whereby memory alignment can be done to a small number
 of freed allocations.
 
 Ie:
 1.   [0xAA00000]
 2.   [0xBB00000] (FREE)
 3.   [0xCC00000] 
 4.   [0xDD00000] (FREE)
 5.   [0xEE00000] (ADDRESSED) --^0xBB00000
 6.   [0xFF00000] (ADDRESSED) --^0xDD00000

   5 is swapped with 2
   6 is swapped with 4

 We'll need to allocate data, represent it in the basin, and continue...     
 ****************************************************************************/

#define		B_STBUF		32	// super tiny buffer
#define		B_SlBUF		64	// super little buffer
#define		B_SSBUF		128	// super small buffer
#define 	B_TBUF		256	// tiny buffer
#define		B_lBUF		512	// little buffer
#define		B_SBUF		1024	// small buffer
#define		B_MBUF		2048	// medium buffer
#define		B_LBUF		4096	// large buffer
#define		B_HBUF		8192	// huge/heavy buffer


#define 	ALIGN_TICK	150	// Every 2.5 minutes, align some allocs
#define		NUM_TO_ALIGN	10	// Align 10 allocs.

typedef struct BASINHDR_T {
	int ref;		
	int size;
	struct BASINHDR_T *next;
	struct BASINHDR_T *nextfree;
	
	char *b_type;
} BASINHDR;

typedef struct BASIN_T {
        
	BASINHDR *chain_head;
	BASINHDR *free_head;
	
}	