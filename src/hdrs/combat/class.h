/****************************************************************************
 MYTH - Class Structure
 Coded by Saruk (03/16/99)
 ---
 This stores the class structure... whee!
 ****************************************************************************/

typedef struct classes_t {
	char *name;
	char *key;
	int str;
	int dex;
	int intel;
	int wis;
	int vir;
	int sta;
	int hp;
	int mp;
	int lck;
} CLASSES;

extern CLASSES class_table[];

