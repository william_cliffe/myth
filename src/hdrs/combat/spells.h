/****************************************************************************
 MYTH - Spell Header
 Coded by Saruk (03/22/99)
 ---
 Spell structure definitions, etc.
 ****************************************************************************/
/** BASE FLAGS FOR S(KI|PE)LLS */
#define AGGRESSIVE		0x1
#define NEUTRAL			0x2

/** TYPES OF S(KI|E)LLS **/
#define MAGE			0x1
#define CLERIC			0x2
#define WARLOCK			0x4
#define NECROMANCER		0x8
#define PSIONICIST		0x10
#define BARBARIAN		0x20
#define KNIGHT			0x40
#define NINJA			0x80
#define MERCENARY		0x100
#define FIGHTER			0x200
#define HISTORIAN		0x400
#define MERCHANT		0x800
#define THIEF			0x1000
#define POLITICIAN		0x2000
#define SWASHBUCKLER		0x4000

/*** SPELLS/SKILLS 
(NOTE: I use skill to refer to skill and/or spell)

	In order to measure a skill's level, we have to rely solely on the skill itself - not on any of the
players attributes (except in the case of skill type -vs- player class). One focus could take the amount of 
successful casts of the skill divided by some equated formula. Then, of course, comes the question of "which
formula?".

	Level = Level + (Success/Cost)
----

	The ability to use the skill is calculated within the skill-function itself. A warrior can learn of
the skill "Heal", but good luck using it without the right type and/or attributes. All of this is calculated
at skill runtime.
***/
typedef struct skills_t {
	char *name;		// Name
	char *desc;		// Description
	void (*spfunc)();	// Spell/skill function
	int spflags;		// Spell/skill base flag.
	
	/* Basics */
	int base;		// Base cost of skill
	int type;		// Skill type (use ONE of the defines as the primary type)
	int flags;		// Skill flags
	int cost;		// Current cost of skill
	int level;		// Level of skill
	int success;		// # of successful casts/uses.
	int uses;		// # of casts/uses.
	 
	
	struct skills_t *next;	// Next skill in list
} SKILLS, SKILL, SPELLS, SPELL;

extern SKILLS skill_table[];
extern SKILLS spell_table[];
