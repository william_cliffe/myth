/****************************************************************************
 MYTH - Combat Tables
 Coded by Saruk (03/16/99)
 ---
 This will store all the nifty neat combat tables.
 ***************************************************************************/

CTABLE race_table[]={
	/**NAME-KEY-STR-DEX-INT-WIS-VIR-STA-HP-MP-LCK**/
	{"Qualth", "Qua", 1, -1, 1, 0, 0, 2, 10, -5, 2, 340.00, 78.00 },
	{"Inyali", "Iny", -1, 1, 1, 2, 0, 1, -3, 5, 4, 145.00, 71.00},
	{"Roja", "Roj", 0, 0, 3, -2, 0, 4, 0, 10, -5, 85.00, 53.00},
	{"Drelth", "Dre", 2, 1, -1, 1, 0, 4, 3, -5, 5, 160.00, 69.00 },
	{"Makin", "Mak", 6, -3, -1, 3, 0, 3, 12, -5, -5, 420.00, 102.00},
	{"Zorn", "Zor", -5, 2, 1, -2, -50, 4, 5, 5, 0, 155.00, 67.00},
	{"Archin", "Arc", -3, -3, 7, 4, 0, -2, -5, 10, 2, 140.00, 65.00},
	{NULL}};

CTABLE class_table[]={
	/** Magick Users **/
	/**NAME-KEY-STR-DEX-INT-WIS-VIR-STA-HP-MP-LCK**/
	{"Mage", "Mag", 0, 0, 2, 1, 0, 1, 0, 5, 1},
	{"Cleric", "Cle", 1, 1, 1, 1, 0, 0, 2, 4, 0},
	{"Warlock", "War", 2, 0, 4, 1, -5, -1, 0, 5, -1},
	{"Necromancer", "Nec", 0, 0, 2, 2, -5, 1, 0, 4, 1},
	{"Psionicist", "Psi", 0, 0, 1, 3, 1, 1, 0, 5, 0},
	/** Fighters **/
	{"Barbarian", "Bar", 5, 5, -1, -2, 0, 1, 5, -5, 2},
	{"Knight", "Kni", 3, 3, 1, -1, 5, 0, 2, 0, 2},
	{"Ninja", "Nin", 0, 5, 5, 0, 0, 5, 0, -10, 5},
	{"Mercenary", "Mrc", 2, 1, -1, 0, -5, 3, 5, -1, 0},
	{"Fighter", "Fig", 1, 1, 1, 0, 1, 1, 5, 0, 1},
	/** Adventurers **/
	{"Historian", "His", -1, -1, 4, 5, 0, -2, 1, 2, 2},
	{"Merchant", "Mer", 1, 0, 3, 2, 5, 1, 1, 0, 2},
	{"Thief", "Thi", -1, 3, 1, -2, -5, 2, 5, 0, 2},
	{"Politician", "Pol", 0, -2, 5, 0, 0, 5, 2, 0, 0},
	{"Swashbuckler", "Swa", 1, 1, 5, 1, 1, 1, 2, 0, 0},
	{NULL}};

SKILLS spell_table[]={
	{"Heal", "Casts a healing spell...", sp_heal, NEUTRAL, 15, CLERIC, MAGE|CLERIC|PSIONICIST},
	{"Fireball", "Casts a fireball...", sp_fireball, AGGRESSIVE, 15, MAGE, MAGE|WARLOCK|CLERIC|PSIONICIST|NECROMANCER},
	{NULL}};
	
SKILLS skill_table[]={
	{NULL}};

/* For locations, only put ONE location so that we can actually calculate it
   better */
ITEMS item_table[]={
	/** DRINKS - 1..150 **/
	{"Water Flask", 1, "A flask of water.", 1, DRINK, CARRIED,
		2, 0, 0.30},
	{"Citrus Drink", 2, "A refreshing fruit drink.", 5, DRINK, CARRIED,
		3, 0, 0.66},
	{"Bourbon Flask", 3, "Alcoholic drink.", 15, DRINK|DRUG, CARRIED,
		4, 7, 1.00},
	/** FOOD - 151..300 **/
	{"Bread", 151, "A loaf of bread", 3, FOOD, CARRIED,
		4, 0, 0.44},
	{"Salted Meat", 152, "Salted meat for travellers", 5, FOOD, CARRIED,
		3, 0, 0.86},
	{"Fruit", 153, "A sack of fruit", 8, FOOD, CARRIED,
		7, 0, 4.00},
	/** WEAPON - 301..550 **/
	{"Dagger", 301, "Your basic steel dagger.", 15, WEAPON, RIGHTHAND,
		1, 0, 1.00},
	{"Short Sword", 302, "A short sword.", 25, WEAPON, LEFTHAND,
		2, 0, 5.00},
	{"Sword", 303, "An ordinary looking sword.", 40, WEAPON, LEFTHAND,
		2, 0, 6.50},
	{"Broad Sword", 304, "A large sword.", 50, WEAPON, RIGHTHAND,
		5, 0, 15.25},
	{"Serpent Ring", 305, "A serpent ring.", 100, WEAPON, RIGHTRING, 
		10, 0, 0.5},
	/** ARMOR - 551..700 **/
	{"Cloth Shoes", 551, "A pair of cloth shoes.", 5, ARMOR, FEET,
		1, 0, 0},
	{"Cloth Shirt", 552, "A wool shirt.", 10, ARMOR, TORSO,
		1, 0, 0},
	{"Cloth Pants", 553, "A pair of cloth pants.", 5, ARMOR, LEGS,
		2, 0, 0},
	{"Cloth Helmet", 554, "A cloth hat.", 10, ARMOR, HEAD, 2, 0, 0},
	{NULL}};

