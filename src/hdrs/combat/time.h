#define TIME_RATIO	30	// 1/30th of a RL second.
#define MYTH_INTERVAL	120	// Check every 2 minutes (1 MYTH hour)

struct DAYNIGHT_T {
	char *message;
	float timeofday;
};

typedef struct myth_tm_t {
	float timeofday;
	float age_world;
	int   weather_counter;
} MYTH_TM;

/* External combat calls */
extern MYTH_TM myth_time;
extern struct DAYNIGHT_T daynight[];

