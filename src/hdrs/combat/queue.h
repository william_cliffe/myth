/*****************************************************************************
 MYTH - Combat Queue System
 Coded by Saruk (04/20/99)
 ---
 This is the combat queues system header file.
 *****************************************************************************/

#define MAX_IDLE_FIGHT	30	// Maximum 30 seconds idle time (should be ample)

#define ATTACKER_A	0x1	// Attacker Attack
#define ATTACKER_D	0x2	// Attacker Defend
#define VICTIM_A	0x4	// Victim Attack
#define VICTIM_D	0x8	// Victim Defend
#define FIGHTDONE	0x10	// Fight is over.

typedef struct fight_queue_t {
	object *attacker;
	object *victim;
	object *turn;
	time_t start_time;
	time_t last_time;
	int flags;
	int id;
	
	int offense;
	int defense;
	int victim_success;
	int attacker_success;
	int num_turns;
	struct fight_queue_t *next;
} FIGHT;

typedef struct spell_queue_t {
	object *enactor;
	object *victim;
	SPELL *spell;
	time_t can_do;
	
	struct spell_queue_t *next;
} SPQUEUE;
	
extern FIGHT *fight_queue;
extern SPQUEUE *spell_queue;

