/****************************************************************************
 MYTH Combat - Weather Control Header
 Coded by Saruk (03/06/99)
 ---
 This is the weather control definition header. Structures for weather systems,
 climate designations, and the like are in here.
 ****************************************************************************/

/* Climates dictate what type of weather will fall on said area. */
#define CLIMATE_FRIGID		0
#define CLIMATE_COLD		1
#define CLIMATE_MILD		2
#define CLIMATE_HOT		3
#define CLIMATE_BLISTERING	4

#define RAIN			0
#define WIND			1
#define STORM			2
#define NORMAL			3

struct WEATHER_SYSTEM_T {
	char *message;	// displayed message from weather table.
	int climate;	// climate this weather is useful in.
	int type;	// what type of weather is it?
};

extern struct WEATHER_SYSTEM_T weather_system[];
