/****************************************************************************
 MYTH - Combat Flags System Header
 Coded by Saruk (04/07/99)
 ---
 Specific flags for combat.
 ****************************************************************************/

/** Combat structure flags **/ 
#define NPC		0x1
#define MONSTER		0x2
#define SHOP		0x4
#define CAN_FIGHT	0x8
#define WANDER		0x10

/** Item List Flags **/
#define DRINK		0x1
#define FOOD		0x2
#define DRUG		0x4
#define WEAPON		0x8
#define ARMOR		0x10
#define ENCHANTED	0x20

/** Location Flags **/
#define UNEQUIPPED	0x1
#define EQUIPPED	0x2
#define RIGHTHAND	0x4
#define LEFTHAND	0x8
#define RIGHTRING	0x10
#define LEFTRING	0x20
#define RIGHTARM	0x40
#define LEFTARM		0x80
#define NECK		0x100
#define FEET		0x200
#define LEGS		0x400
#define TORSO		0x800
#define HEAD		0x1000
#define CARRIED		0x2000

