/****************************************************************************
 MYTH - Combat System
 Coded by Saruk (03/04/99)
 ---
 This is the MYTH combat system, compliments of my wife.
 ****************************************************************************/

#include "weather.h"
#include "time.h"
#include "spells.h"
#include "flags.h"
#include "queue.h"

#define COMBAT_UPDATE	300	// Every two minutes, do a HP update
#define MIN_EXP		2	// minimum of 2 experience.
#define DEATH_TIME	150	// 2 1/2 minutes of death time.
#define WANDER_TIME 	30	// 2 1/2 minutes before a monster wanders.
#define MAX_MONSTERS	150	// 150 monsters of each type - MAX

/* This is our table stat struct */
typedef struct ctable_t {
	char *name;
	char *key;
	int str;
	int dex;
	int intel;
	int wis;
	int vir;
	int sta;
	int hp;
	int mp;
	int lck;
	float weight;
	float height;
} CTABLE;


/* This is for the stats such as HP, VIRTUE, etc. */
typedef struct stats_t {
	int mp;		// magic points
	int hp;		// hit points
	int str;	// strength
	int sta;	// stamina
	int wis;	// wisdom
	int intel;	// intelligence
	int dex;	// dexterity
	int lck;	// luck
	int vir;	// virtue
	int exp;	// experience
	int level;	// level
	int maxmp;	// max magic
	int maxhp;	// max hitpoints
	
	float carry;	// max carry weight.
	float weight;
	float height;
} STATS;

typedef struct itemlist_t {
	char *name;
	int id;
	char *desc;
	int cost;
	int flags;
	int location;
	
	/** The following are basics, some used, some not - depending on flags **/
	int add;	// 
	int sub;	// 
	float weight;	// how much does the item weigh.
	
	int loc;	// this is where you end up putting the equipment.
	struct itemlist_t *next;
} ITEMS, ITEM;

typedef struct combat_t {
	long flags;		// all the combat-oriented flags can be moved in here.
	time_t dead;		// when you were pronounced dead. elsewise -1
	time_t wander;		// last time something moved.
	time_t breed_time;	// when was the last breed time.
	long time_to_breed;	// how many seconds until breed time.
	
	/* I'm beginning to think it's best to include an NPC struct soon */
	/* we'll add this and we can use parts of it for players and rooms */
	ITEM *items;
	SKILLS *skills;
	STATS *stats;
} MCOMBAT;

extern CTABLE race_table[];
extern CTABLE class_table[];
extern ITEMS item_table[];

