/****************************************************************************
 MYTH - Header for trust list
 Coded by sarukie (10/15/2004)
 ---
 ****************************************************************************/

// Define our access lists
#define ROOT_LIST		0
#define ADMINISTRATOR_LIST	1
#define PROGRAMMER_LIST		2
#define BUILDER_LIST		3
#define PLAYER_LIST		4
#define GUEST_LIST		5

// Define our permission variable
#define PERM_NO			0
#define PERM_ALL		1
#define PERM_ALLEQ		2
#define PERM_ALLLT		3
#define PERM_EXCLUSIVE		4	// Please note, exclusive marked with no list
                                        // defaults to player + owned objects.

// Define our permissions here
#define PERM_SHUTDOWN		1
#define PERM_REBOOT		2
#define PERM_PCREATE		3
#define PERM_NUKE		4
#define PERM_BOOT		5
#define PERM_REMOTE		6
#define PERM_JOIN		7
#define PERM_SUMMON		8
#define PERM_MODIFY		9
#define PERM_EXAMINE		10
#define PERM_ATTRSET		11

#define PERM_TOTAL		11

typedef struct permission_t {
	int perm_type;		// permission type (PERM_SHUTDOWN, etc.)
	char *key;		// This is the keyword that will assist in setting a permission.
	char *perm_desc;	// description of permission
} PERMISSION;

struct permission_grant {
	int perm_type;			// What type of permission?
	char permission;		// Default grant (ALL through EXCLUSIVE)
};

typedef struct access_list_t {
	int ref;			// Access list reference.
	char *name;			// Name of the list
	struct permission_grant *perm;	// List of grantable permissions.
} ACCESSLIST;


extern PERMISSION permissions[];
extern struct permission_grant default_root_perms[];
extern ACCESSLIST default_root_list;

