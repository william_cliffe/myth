/****************************************************************************
 MYTH - Paste header
 Coded by Saruk (02/02/99)
 ---
 Do paste stuffies.
 ****************************************************************************/

struct paste_struct {
	PID owner;
	
	char *paste_buffer;
	char *paste_cmd;
	
	struct paste_struct *next;
};

#define PASTE_ALL	1
#define PASTE_ME	2
#define PASTE_ROOM	3
