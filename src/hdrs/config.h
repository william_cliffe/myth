/****************************************************************************
 MYTH - Configuration Header
 Coded by Saruk (01/10/99)
 ---
 Holds general information on MYTH structures and static variables.
 ****************************************************************************/

#include "credits.h"

// Macro Definitions
#define P(x)		x
#define DOEXTERN(x,y)	extern x y
#define GENERATE(a, b)	(a = (b *)stack_em(sizeof(b), SOLID))
#define SET(a,b)	a = strset(b)

// LIST MACROS: Credits to itsme @ TinyMAZE for kindly allowing us to just copy/paste them.
// Were used for first integration of a cross-MU* plugin for MAZE and MYTH.

#define LIST_INIT(head, node) \
   do { (node)->next = (node)->prev = (head) = (node); } while(0)

#define LIST_TAIL(head) ((head)->prev)

#define LIST_NEXT(head, l) \
   ((l)?(((l)->next == (head))?NULL:(l)->next):NULL)

#define LIST_PREV(head, l) \
   ((l)?(((l) == (head))?NULL:(l)->prev):NULL)

#define LIST_REM_NODE(head, node) \
   do { if((head)) { (node)->prev->next = (node)->next; \
        (node)->next->prev = (node)->prev; \
        if((node) == (head)) (head) = LIST_NEXT((head), (head)); \
      }} while(0)

#define LIST_INSERT_NODE(which, node) \
   do { (node)->next = (which); (node)->prev = (which)->prev; \
        (which)->prev->next = (node); (which)->prev = (node); \
      } while(0)

#define LIST_ADD_NODE_TAIL(head, node) \
   do { if(!(head)) { LIST_INIT((head), (node)); } \
        else { LIST_INSERT_NODE((head), (node)); } \
      } while(0)

#define LIST_ADD_NODE_HEAD(head, node) \
   do { LIST_ADD_NODE_TAIL((head), (node)); (head) = (node); } while(0)

#define LIST_ADD_NODE_PREV(head, which, node) \
   do { if(!(head)) LIST_INIT((head), (node)); \
        else LIST_INSERT_NODE((which), (node)); \
      } while(0)

#define LIST_EXTRACT_LIST(start, end) \
   do { (start)->prev->next = end->next; \
        (end)->next->prev = (start)->prev; (start)->prev = (end); \
        (end)->next = (start); \
      } while(0)

#define LIST_INSERT_LIST_PREV(where, list, temp) \
   do { (where)->prev->next = (list); (list)->prev->next = (where); \
        (temp) = (where)->prev; (where)->prev = (list)->prev; \
        (list)->prev = (temp); \
      } while(0)

#define LIST_CYCLE(head, l) \
   for((l) = (head);(l);(l) = LIST_NEXT((head), (l)))

#define LIST_CYCLE_SAFE(head, l, lnext) \
   for((l) = (head), (lnext) = LIST_NEXT((head), (l));(l); \
     (l) = (lnext), (lnext) = LIST_NEXT((head), (l)))

// Basic Definitions
// Only root can do this:
#define ROOTONLY_SETWIZ

#define FALSE		0
#define TRUE		1

#define ENABLE_NEW	1	// Set this to 0 if you don't want to allow players to create themselves.
#define GUEST_PREFIX	"Guest"	// Prefix for the guest name.
#define ROOTOBJ		1	// This determines which object to assign as the rootobj. 
#define INIT_QUOTA	10	// Enough quota to build a room and a couple of objects.
#define INIT_CHQUOTA	3	// Enough quota to create a personal channel.
#define MIN_POWER	-100	// set the minimum powerlevel.
#define MAX_POWER	105	// set the maximum powerlevel.
#define REG_BUFSIZE	1024	// Regular buffer size.
#define MIN_BUFSIZE	2048	// Minimum buffer size.
#define MED_BUFSIZE	4096	// Medium buffer size.
#define MAX_BUFSIZE	8192	// Maximum buffer size.
#define HUGE_BUFSIZE	10000	// HUGE buffer size.
#define MAX_IOINPUT	1000	// Maximum of 1000 chars at a time, to reduce overflow chances.
#define MAX_GUESTS	0
#define NEXT_LOOKUP	150	// Lookup a host.
#define CAN_WALLOFF	75	// powerlevel to wall official something.
#define DEF_BBEXP	60	// Expiration of Bulletin Board items in days.
#define BOARD_MAX	10	// Maximum of 10 board messages per player.
#define NOTIFY_IDLE	1800	// Default idle time. (30 Minutes)
#define MAX_CLEV	1000	// Maximum 1000 command interpersing.
#define MAX_QLEV	1000	// Maximum amount of commands in the queue.

// Speech definitions/tokens.
/** We have to change these for flags in cmdlist.h **/
#define SPEAK_SAY	0x8
#define SPEAK_POSE	0x10
#define SPEAK_POSS	0x20
#define SPEAK_THINK 	0x40
#define SPEAK_TO	0x80

#define POSS_TOKEN	';'
#define THINK_TOKEN	'.'
#define CHAN_TOKEN	'='
#define POSE_TOKEN	':'
#define TO_TOKEN	'\''
#define SAY_TOKEN	'"'

#define MUD_SAY_TOKEN	'\''
#define MUD_TO_TOKEN	'>'
#define MUD_CHAN_TOKEN	'-'

// Special considerations
#define MAX_VARNOM_LEN		18

// Type definitions.
typedef int dbref;
typedef dbref DBREF;


// External variables.
extern int clev; 	// command level. (MAX: 1000)
extern char *wildptr[];
extern char wildbuff[];
extern int wildlen[];
#include "extvars.h"
