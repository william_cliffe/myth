/*** items.h $ Havencrag ***/

// Locations
#define LOC_NONE		(1 << 0)
#define LOC_HEAD		(1 << 1)
#define LOC_HANDS		(1 << 2)
#define LOC_TORSO		(1 << 3)
#define LOC_LEGS		(1 << 4)
#define LOC_FEET		(1 << 5)
#define LOC_CLOAK		(1 << 6)
#define LOC_DUALWIELD		(1 << 7)	
#define LOC_SHIELD		(1 << 8)
#define LOC_NECK		(1 << 9)

// Flags
#define UNFINISHED		(1 << 0)
#define	WEAPON			(1 << 1)
#define ARMOUR			(1 << 2)	
#define SHIELD			(1 << 3)
#define FOOD			(1 << 4)
#define DRINK			(1 << 5)
#define DRUG			(1 << 6)
#define SPECIAL			(1 << 7)
#define BREAKABLE		(1 << 8)
#define UNMODIFIABLE		(1 << 9)
#define NOCLONE			(1 << 10)
#define POTION			(1 << 11)

// Affect 
#define AFFECT_VISION		(1 << 0)
#define AFFECT_WALKING		(1 << 1)
#define AFFECT_STANDING		(1 << 2)
#define AFFECT_FIGHTING		(1 << 3)
#define AFFECT_HUNGER		(1 << 4)
#define AFFECT_THIRST		(1 << 5)
#define AFFECT_HEALTH		(1 << 6)
#define AFFECT_MAGIC		(1 << 7)

// Effect
#define EFFECT_ASSAULT		(1 << 0)
#define EFFECT_PROTECT		(1 << 1)
#define EFFECT_QUENCH		(1 << 2)
#define EFFECT_QUELL		(1 << 3)
#define EFFECT_INCREASE		(1 << 4)
#define EFFECT_DECREASE		(1 << 5)

// Material
#define MAT_CLOTH		(1 << 0)
#define MAT_WOOL		(1 << 1)
#define MAT_LEATHER		(1 << 2)
#define MAT_CHAINMAILLE		(1 << 3)
#define MAT_WOOD		(1 << 4)
#define MAT_IRON		(1 << 5)
#define MAT_STEEL		(1 << 6)
#define MAT_COBALT		(1 << 7)
#define MAT_RUBY		(1 << 8)
#define MAT_EMERALD		(1 << 9)
#define MAT_SAPPHIRE		(1 << 10)
#define MAT_PEARL		(1 << 11)
#define MAT_DIAMOND		(1 << 12)
#define MAT_SACREDIUM		(1 << 13)

// Item types
#define MOD_STRENGTH		1
#define MOD_INTELLIGENCE	2
#define MOD_WISDOM		3
#define MOD_DEXTERITY		4
#define MOD_CONSTITUTION	5
#define MOD_CHARISMA		6
#define MOD_STAMINA		7
#define MOD_LUCK		8
#define INITIATE_SOFTCODE	9

/*
  The idea behind item attributes is this:
  
  When creating another item attribute, a corresponding type is affixed.
  
  Examples: ADD_STRENGTH, DEL_STRENGTH, INITIATE_SOFTCODE
  
  All the types would have hardcoded values and a table to associate them.
  That way if list.type = INITIATE_SOFTCODE and list.value.ch = "@pe %#=Whee!"
  the hardcode would send list.value.ch through the parser.
  
  Or if list.type == ADD_STRENGTH, list.value.i would modify the strength.
  We could have any number of attributes, therefore making an item nearly
  omnipotent.
  
  Each type, in code will designate whether the value will be an int, 
  a long, a long long, a float, or a char.
*/
typedef struct item_attribute_t {
  unsigned int type;
  union item_datatype {
    int i;
    long l;
    long long ll;
    float f;
    char *ch;
  } value;
  
  struct item_attribute_t *next, *prev;
} ITEMATTR;

typedef struct item_attribute_list_t {
  char *name;
  int type;
} ITEMATTRIBUTELIST;

typedef struct combat_itemtable_t {
  // ESSENTIAL DEFAULT VALUES
  unsigned int id;			// The ID of the item.
  char *name;				// What is this item?
  char *desc;				// Describe this item.
  long cost;				// How much does this item cost, if purchased?
  
  unsigned long long wear;		// What body location will this item reside?
  unsigned long long flags;		// Special flags for the item.
  unsigned long long affect;		// What does it affect?
  unsigned long long effect;		// What is its effect?
  unsigned long long material;		// If this is armour or weapon, there is a material associated with the item.
                                        // This will affect the ability to forge or fortify the item.
                                        
  // If it's armour, weapons, or whatnot.
  unsigned int value;			// What is its affecting value?
  int alignment;			// Alignment can be -1 (Evil), 0 (Neutral), or 1 (Good). 
  
  float weight;				// How much does it weigh?
  unsigned int stresspoint;		// At what point of accumulated damage will this item break, if flagged?
                                        // On a created item, stresspoint will decrease with each damage. Many
                                        // items will be breakable, though some can be enchanted to not break.
                                        // Breakable items can also be strengthened or fortified by special 
                                        // forging. 
      
  // ** NON DEFAULT VALUES **
  int equipped;				// Is it equipped or not (WEAPON or ARMOUR)
  
  // WEAPONS
  unsigned int kills;
  unsigned int uses;
  unsigned int proficiency;  

  // ARMOUR or SHOPS
  unsigned int blocks;			
  unsigned int onhand;
  unsigned long long worn;
   
  // Item Attributes
  ITEMATTR *list, *listtail;		// A list of modified attributes.  
  
  struct combat_itemtable_t *next, *prev;
} ITEMLIST;
