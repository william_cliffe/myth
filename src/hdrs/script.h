/****************************************************************************
 MYTH - Script Header
 Coded by Saruk (06/22/99)
 ---
 A structure added onto the main OBJECT structure in order to allow multiple
 lines of softcode to be used in the new softcode system.
 ****************************************************************************/
#define SCRIPT_VERSION	1
#define SCRIPT_NAME	"MScript"

#define M_FUNCTION	0
#define M_COMMAND	1

typedef int MTYPE;	// code types are actually integers.

typedef struct myth_code_block {
	char *line;	// a line of compiled code.
	
	struct myth_code_block *next;	// pointer to next line.
} MBLOCK;

typedef struct object_t OBJ2;

typedef struct compiled_block_t {
	char *name;	// name of compiled block
	MTYPE type;	// type of compiled block
	MBLOCK *code;	// lines of compiled code.
	
	int oref;	// owner of compiled block. (saveable)
	OBJ2 *owner;	// memory address of above.
	
	struct compiled_block_t *next;
} COMPILE_BLOCK, CBLOCK;

typedef struct functions_t {
	char *name;
	int args;
	void (*func)();
} FUNC;

typedef struct script_t {
	char *line;
	
	struct script_t *next;
} SCRIPT;

typedef struct keyword_t {
	char *name;	// name of keyword.
	int args;	// number of arguments.
	void *(*proc)();	// procedure to call on keyword.
	int perms;	// powerlevel of permission needed.
	int flag;	// flags needed to call keyword.
} KEYWORDS;

extern FUNC functions[];
extern KEYWORDS keywords[];
extern CBLOCK *myth_code;
