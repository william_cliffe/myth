/*****************************************************************************
 attrib.h - set the hardcoded attributes fast lookup.
 *****************************************************************************/

#define A_AUTOOWN	1
#define A_RAINBOW	2
#define A_CREATED	3
#define A_QUOTA		4
#define A_ALIAS		5
#define A_SEX		6
#define A_ARRIVE	7
#define A_LEAVE		8
#define A_THROUGH	9
#define A_STHROUGH	10
#define A_FTHROUGH	11
#define A_TAKE		12
#define A_STAKE		13
#define A_FTAKE		14
#define A_NORTH		15
#define A_SOUTH		16
#define A_WEST		17
#define A_EAST		18
#define A_SOUTHEAST	19
#define A_SOUTHWEST	20
#define A_NORTHEAST	21
#define A_NORTHWEST	22
#define A_UP		23
#define A_DOWN		24
#define A_OUT		25
#define A_LASTPAGETO	26
#define A_CHANNELQUOTA	27
#define A_PLAYERLOCK	28
#define A_DOING		29
#define A_STARTUP	30
#define A_KILLS		31
#define A_SKILLPOINTS	32
#define A_LEVEL		33
#define A_GOLD		34
#define A_LASTON	35
#define A_LASTOFF	36
#define A_WHOFLAGS	37
#define A_POSITION	38
#define A_ATALK		39
#define A_COMMENT	40
#define A_PARENT	41
#define A_CHILDREN	42
#define A_ZONE		43
#define A_ADESC		44
#define A_GROUP		45
#define A_CAPTION	46
#define A_MUTYPE	47
#define A_RACE		48
#define A_CLASS		49
#define A_EQHD		50	// Head
#define A_EQHANDS	51	// Hands
#define A_EQCLOAK       52
#define A_EQTORSO	53	// Torso
#define A_CFLAGS	54	// Combat flags.
//#define ???
#define A_EQLEGS	56	// Legs (Pants, etc)
#define A_EQFEET	57
//#define ???
#define A_WIELDRH	59	// Right-hand wield
#define A_WIELDLH	60	// Left-hand wield
#define A_WIELDDH	61	// Double-hand wield
#define A_TITLE		62	// What combat title doth they have?
#define A_DEFCHANNEL	63	// Default channel
#define A_DESC		64	// Description of object.

// Combat attributes
#define A_HP		100	// Hitpoints
#define A_MAXHP		101	// Max Hitpoints
#define A_MP		102	// Magicpoints
#define A_MAXMP		103	// Max Magicpoints
#define A_STRENGTH	104
#define A_DEXTERITY	105
#define A_CONSTITUTION	106
#define A_CHARISMA	107
#define A_INTELLIGENCE	108
#define A_WISDOM	109
#define A_STAMINA	110
#define A_LUCK		111
#define A_VIRTUE	112
#define A_HEIGHT	113
#define A_WEIGHT	114
#define A_CARRY		115
#define A_EXP		116

// Combat attribute modifiers
#define A_STRMOD	200
#define A_DEXMOD	201
#define A_CONMOD	202
#define A_CHAMOD	203
#define A_INTMOD	204
#define A_WISMOD	205
#define A_STAMOD	206
#define A_LCKMOD	207

// Programmable attributes
#define A_VA		500
#define A_VB		501
#define A_VC		502
#define A_VD		503
#define A_VE		504
#define A_VF		505
#define A_VG		506
#define A_VH		507
#define A_VI		508
#define A_VJ		509
#define A_VK		510
#define A_VL		511
#define A_VM		512
#define A_VN		513
#define A_VO		514
#define A_VP		515
#define A_VQ		516
#define A_VR		518
#define A_VS		519
#define A_VT		520
#define A_VU		521
#define A_VV		522
#define A_VW		523
#define A_VX		524
#define A_VY		525
#define A_VZ		526
#define A_V0		527
#define A_V1		528
#define A_V2		529
#define A_V3		530
#define A_V4		531
#define A_V5		532
#define A_V6		533
#define A_V7		534
#define A_V8		535
#define A_V9		536


// Extra Attributes
#define A_FIRSTNAME	1000
#define A_LASTNAME	1001
#define A_PCOMTITLE	1002
#define A_SCOMTITLE	1003
