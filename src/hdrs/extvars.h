/****************************************************************************
 MYTH - External Variables
 Coded by Saruk (03/22/99)
 ---
 Declare all the external variables.
 ****************************************************************************/

DOEXTERN(int, DEPTH);
DOEXTERN(int, resock); 
DOEXTERN(int, recursion);
DOEXTERN(int, dump_time);
DOEXTERN(int, myth_ticks);
DOEXTERN(int, db_version);
DOEXTERN(int, reboot_flag);
DOEXTERN(int, myth_reboots);
DOEXTERN(int, wizlock);
DOEXTERN(int, dbloading);
DOEXTERN(int, total_stack_calls);
DOEXTERN(int, total_stack_frees);
DOEXTERN(int, free_frames);
DOEXTERN(int, update_time);
DOEXTERN(int, shutdown_flag);
DOEXTERN(int, database_saves);
DOEXTERN(int, reboot_style);
DOEXTERN(int, MYTH_connected);
DOEXTERN(int, ConnectRecord);
DOEXTERN(int, hub_desc);
DOEXTERN(long, myth_uptime);
DOEXTERN(long, total_input);
DOEXTERN(long, total_output);
DOEXTERN(size_t, tot_mem_act);
DOEXTERN(time_t, last_update);
DOEXTERN(time_t, myth_reboot);
DOEXTERN(time_t, last_dump_time);
DOEXTERN(time_t, last_update_frames);
DOEXTERN(MSTACK *, MYTH_stack);
