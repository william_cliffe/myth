/** comsys.h $ MYTH **/

#define COM_DB_VERSION	1.01		// Version 1.1 of the com db.

#define MAX_COM_RECALL	100		// 100 Lines of recall, maximum, per channel.

#define COM_HOBGOBLIN	0x01		// Absolutely nothing.
#define COM_ADMIN	0x02		// Channel is an admin channel.
#define	COM_LOG		0x04		// Log the channel.
#define	COM_SERVER	0x08		// Server-only channel
#define COM_NOTALK	0x10		// Channel has been quieted.
#define COM_RECALL	0x20		// The channel can recall.
#define COM_DUMP	0x40		// Dump the channel recall on @reboot.

#define CHAN_STATE_OFF	0
#define CHAN_STATE_ON	1


typedef struct comrecall_t {
  char *buffer;
  unsigned int len;
  
  struct comrecall_t *next;
  struct comrecall_t *prev;
} COMRECALL;

typedef struct comban_t {
  object *player;		// Player to be banned.
  time_t expiry;		// If expiry is -1, they're forever banned.
  
  struct comban_t *next;
  struct comban_t *prev;	
} COMBAN;

typedef struct comwho_t {
  object *player;
  unsigned int state;		// Channel is ON or OFF?
  
  struct comwho_t *next;
  struct comwho_t *prev;
} COMWHO;

typedef struct comobject_t {
  unsigned int creator;		// Who created this channel?
  object *owner;		// Object representation of creator.
  time_t created;		// When did they create it?  
  char *name;			// channel name
  unsigned long flags;		// Channel flags
  unsigned long total_lines;	// All the lines that have passed over the channel.
  
  /** Players on the channel **/
  COMWHO *wholist;
  COMWHO *whotail;
  
  /** BAN specific **/
  COMBAN *banlist;		// Ban players from the channel.
  COMBAN *bantail;		// The tail of bans.
  
  /** RECALL specific **/
  unsigned int recall_lines;	// How many lines in recall?
  COMRECALL *recall;		// The recall buffer.
  COMRECALL *recalltail;	// The recall buffer tail.
  
  struct comobject_t *next;	
  struct comobject_t *prev;
} COMOBJECT;


typedef struct com_flags_t {
  char *name;
  unsigned long flag;
} com_flags; 

extern struct com_flags_t comflags_table[];                                     
extern COMOBJECT *comsys;
extern COMOBJECT *comsystail;
extern float com_db_version;

/** END OF COMSYS.H **/
