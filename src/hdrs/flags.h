/****************************************************************************
 MYTH - Flag settings, etc.
 Coded by Saruk (02/03/99)
 ---
 This file holds flags, flag tables, declarations pointing to external tables
 and such. Please note that to add a flag, you must first add it to the defi-
 nitions list below (remembering not to shadow another flag) and to the table
 defined in flaglist.h.
 NOTE: If you find yourself causing overflows in your flags attribute, you may
 want to consider assigning a flags2 to the database object, or to make it even
 more structured, assign a link list of flags. This may be incorporated here.
 ****************************************************************************/


// In order to assign types, you would need to define a new object list, as well as assign types
// all over the code. There should be no reason to assign new types - just flag certain objects,
// like TYPE_THING as SHIP, MONSTER, NPC, etc, etc.
#define TYPE_EXIT       0x1	// Denote an exit object.
#define TYPE_PLAYER     0x2	// Denote a player object.
#define TYPE_THING      0x4	// Denote a thing object.
#define TYPE_ROOM       0x8	// Denote a room object.
#define NOMASK		0xF	// this is used for permissions and such.

// The following flags should not be shadowed as they are applicable for every object.
#define CAN_TEL		0x10
#define VISIBLE		0x20
#define LOCKED		0x100
#define WIZARD          0x400
#define INVISIBLE       0x800
//#define SUSPECT		0x8000	// Object is on suspect.
#define INHERIT         0x10000 // Object can inherit powers.
#define HAVEN		0x20000 // Keep them objects stopped.
#define COMBAT		0x40000 // Object can participate in combat.

// These are flags that can be shadowed (A specific TYPE will be assigned beside them)
#define GUEST		0x40	// Player-specific, a flag for guests.
#define SHOP		0x40	// Room-specific, Room acts as a shop.
#define HIDDEN		0x80	// Exit-specific, a flag to hide exits. (Save for possible THING addition)
#define QUIET		0x80	// Player/Room-specific. Cannot hear messages.
#define ZONE		0x80	// Thing-specific, zone.
#define CONNECT         0x200	// Player-specific, only players should be set or unset connect.
#define LINK_OK		0x200   // Room-specific, allows a room to be linked to, no matter who the linker.
#define ENTER_OK	0x200   // Thing-specific, allows you to walk inside a thing.
#define NOSPOOF		0x1000	// Player-specific, appends some strings with dbref of activator.
#define PUPPET		0x1000  // Thing-specific, allows an object to become a "puppet".
#define ANSI		0x2000  // Player-specific, the ANSI flag.
#define NO_WALLS	0x4000	// Player-specific, allow you to do WALLs.
#define JUMP_OK		0x4000	// Room-specific, anyone can @tel there.
#define SUSPECT		0x8000  // Player-specific, they're set SUSPECT.

// Attribute flags.
// Attribute definition flags
#define AWIZARD         0x1
#define AINH            0x2
#define ADARK           0x4
#define ASEE            0x8
#define AIMP            0x10
#define ASOLID          0x20    // can't delete this attribute.
#define ACHECK          0x40    // Check all other attributes for same values. If there is, return 0.
#define AEXIT		0x80	// Attribute is an exit.
#define APROGRAM	0x100	// Attribute can be programmed as a trigger or softcode.
#define ANOCHANGE       0x200   // Attribute's value cannot change; server-only setting.
#define AFUNC		0x400   // Attribute is a function.
#define AUSER		0x800   // Attribute is user-defined.

typedef struct flag_type {
	char *name;
	char parsed;
	int flag;
	int oflags;
	int type;
} flags;

extern flags flag_table[];
extern flags chflag_table[];
extern flags aflag_table[];
extern flags mflag_table[];
