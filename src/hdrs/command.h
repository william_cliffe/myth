/****************************************************************************
 MYTH - Command List
 Coded by Saruk (03/26/99)
 ---
 Keep track of all the useabl commands.
 ****************************************************************************/

#define MAX_COMMANDS		37

#define PLUGIN			1	// for plugin command additions
 
#define PURITY 			0x1
#define TEXT			0x2
#define NOAMB			0x4	// No ambiguous matching.
// #define SPEAK_SAY 0x8
// #define SPEAK_POSE 0x10
// #define SPEAK_POSS 0x20
// #define SPEAK_THINK 0x40
// #define SPEAK_TO 0x80
// #define WIZARD 0x400 - this is defined in db.h, just to note that it is used here.
#define SWITCH			0x800

typedef struct cmd_list_t {
	char *command;
	void (*func)();
	int flags;
	int powerlevel;
	PHANDLE *handle;
		
	struct cmd_list_t *next;
	struct cmd_list_t *prev;
} CL;

typedef struct cmdhash_t {
	CL *cmdlist;
	CL *cmdtail;
	
} HASHCMD;

extern HASHCMD cmdhash[];		// Commands hash table.
