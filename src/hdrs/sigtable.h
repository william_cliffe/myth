/****************************************************************************
 MYTH - Signal Header
 Coded by Saruk (03/26/99)
 ---
 Keep a list of all the signals we want to mess or report.
 ****************************************************************************/

struct sigtable_t {
	char *retstr;
	int t_sig;
};

struct sigtable_t sig_table[]={
	{"(SIGHUP) Hangup detected on controlling terminal or death of controlling process.", SIGHUP},
	{"(SIGINT) Interrupt from keyboard.", SIGINT},
	{"(SIGQUIT) Quit from keyboard.", SIGQUIT},
	{"(SIGILL) Illegal Instruction.", SIGILL},
	{"(SIGABRT) Abort signal from abort().", SIGABRT},
	{"(SIGFPE) Floating point exception.", SIGFPE},
	{"(SIGKILL) Kill signal.", SIGKILL},
	{"(SIGSEGV) Segmentation fault / Invalid memory reference.", SIGSEGV},
	{"(SIGALRM) Timer signal from alarm().", SIGALRM},
	{"(SIGTERM) Termination signal.", SIGTERM},
	{"(SIGUSR1) Reboot MYTH server.", SIGUSR1},
	{"(SIGSTOP) Stop process.", SIGSTOP},
	{"(SIGTSTP) Stop typed at tty.", SIGTSTP},
	{NULL}};
