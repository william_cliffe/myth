/****************************************************************************
 MYTH - Function structure.
 Coded by r00t (02/04/03)
 ---
 Softcode function stuff.
 ****************************************************************************/

typedef struct fun_t {
	char *name;
	void (*func)();
	int nargs;

	struct fun_t *next;
} FUN, FUNC, FUNCTION;


extern FUN funlist[];
extern FUN *pfunlist;

