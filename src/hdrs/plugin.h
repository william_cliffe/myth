/****************************************************************************
 Dynamic Library Header 
 Coded by sarukie (08/06/04)
 ---------------------------
 We'll eventually do a lot more work to plugins as we progress.
 ****************************************************************************/

// Simple handle handler.
typedef struct handle_plugins_t {
	char *cmd;
	char *name;
	void *handle;
	unsigned long timestamp;
	
	void (*fselectchk)();
	int *(*freadset)();
	int *(*fwriteset)();
	
	struct handle_plugins_t *next;
	struct handle_plugins_t *prev;
} PHANDLE;

extern PHANDLE *phandle;
extern PHANDLE *phandletail;
