/****************************************************************************
 MYTH - Mail Header
 Coded by Saruk (03/27/99)
 ---
 Keep track of the mail structure and such.
 ****************************************************************************/

#define MAIL_TOO_OLD	432000	// After 5 days, old READ mail is deleted.
 
#define MAIL_NEW	0x1
#define MAIL_READ	0x2
#define MAIL_PROTECTED	0x4
#define MAIL_URGENT	0x8
#define MAIL_FORWARD	0x10

typedef struct mail_t {
	dbref fromref;
	time_t age;
	char *message;
	int flags;
	
	OBJECT *from;	
	struct mail_t *next;
} MAIL;
