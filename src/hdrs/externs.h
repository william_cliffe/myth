/****************************************************************************
 MYTH - External Calls
 Coded by Saruk (01/12/99)
 ---
 Keep all the external calls here. And include this file in all your files.
 ****************************************************************************/

/* For some versions of Linux, this gets rid of the implict warning */
#ifdef __GLIBC__
extern char *crypt P((char *, char *));
#endif


/* From admin.c */
extern void do_as P((OBJECT *, char *, char *));
extern void do_admin P((OBJECT *));
extern void do_su P((OBJECT *, char *, char *));
extern void do_empower P((OBJECT *, char *, char *));
extern void do_powers P((OBJECT *, char *, char *));
extern void do_class P((OBJECT *, char *, char *));
extern void do_force P((OBJECT *, char *, char *));
extern void do_swap P((object *, char *, char *));
extern void do_add P((object *, char *, char *, char *));
extern void do_del P((object *, char *, char *, char *));
extern void do_dpage P((object *, char *, char *, char *));
extern void log_channel P((char *, char *));
extern void AddFuncCallList P((char *, void *));
extern FLIST *CallFuncCall P((char *));

/* From ansi.c */
extern char *format_color P((char *, int));
extern char *strip_color P((char *));
extern char *color_name P((object *, int));
extern void do_ansi_toggle P((object *));

/* From attrib.c */
extern void do_alist_list P((object *));
extern void mkattr P((object *, char *));
extern void atr_examine P((object *, object *, char *));
extern int edit_atr_set P((object *, object *, char *, char *));
extern int atr_set P((object *, object *, int, char *));
extern void SET_ATR P((object *, int, char *));
extern char *parser_atr_get P((object *, char *));
extern char *atr_get P((object *, int));
extern void KILL_ATTRS P((object *));
extern int __is_builtin P((char *));
extern int __all_attribute_search P((char *));
extern void do_defattr P((object *, char *, char *));
extern void do_undefattr P((object *, char *));
extern void do_easyattr P((object *, char *));

/* From board.c 
extern void sort_bboard P((void));
extern void do_board P((object *, char *, char *));
extern BBOARD *add_to_board P((object *, char *, char *));
*/

/* From cevents.c */
extern int StartItemEvent P((object *, ITEMLIST *, int));
extern void ListEvents P((object *));
extern void CheckFightEvents P((object *, object *));
extern void ParseEvent P((CEVENT *));

/* From comsys.c */
extern COMOBJECT *FindChannel P((char *));
extern void ParseChannel P((object *, char *, char *, char *));
extern int ParseChannelShortcut P((object *, char *));
extern void DefaultChannel P((object *, char *));
extern void ComDBSave P((char *));
extern void ComDBLoad P((char *));
extern void LogChannel P((char *, char *));
extern int ChannelPasteHeader P((object *, char *, int));
extern int ChannelPaste P((object *, char *, char *));
extern void RemoveFromChannels P((object *));
extern void CheckChannels P((object *));

/* From combat.c */
extern int check_combat P((object *));
extern unsigned int die_roll P((int, int));
extern void setup_classtable P((void));
extern void setup_racetable P((void));
extern void do_score P((object *, char *));
extern void do_listrace P((object *));
extern void SAVE_COMBAT_OBJECT P((object *));
extern void do_combat_load P((void));
extern void SetCombat P((object *, char *, char *));
extern void CombatTimer P((void));
extern void NeedRegen P((object *));
extern void ReleaseFromRegen P((object *));
extern float Fatigue P((CSTATS *));
extern void CleanUpCombat P((void));

/* From creature.c */
extern void CreatureSetTemplate P((object *, char *, char *));
extern void CreateCreatureTemplate P((object *, char *));
extern void CreatureList P((object *, char *));

/* From items.c */
extern void ItemCreate P((object *, char *)); // For creating template items.
extern void SetItem P((object *, char *, char *));
extern void ShowItems P((object *, char *));
extern void ShowStoreMenu P((object *, char *));
extern void BuyItem P((object *, char *, char *));
extern void SellItem P((object *, char *, char *));
extern void AddToShop P((object *, char *, char *));
extern void DelFromShop P((object *, char *, char *));
extern void ShowEquipment P((object *, char *));
extern void WearItem P((object *, char *, char *));
extern void WieldItem P((object *, char *, char *));
extern void UnequipItem P((object *, char *));
extern void SaveTemplates P((char *));
extern void SaveUserItems P((char *));
extern void LoadItems P((char *));
extern void RemoveAllItems P((object *));
extern void RemoveTemplate P((object *, char *));
extern int AddItemModifiers P((object *));
extern void RemoveItem P((ITEMLIST **, ITEMLIST **, ITEMLIST *));
extern void ParseItemModifiers P((FIGHTQUEUE *, object *, ITEMATTR *));
extern void UseItem P((object *, char *, char *));
extern void LoadTemplates P((char *));

/* From comm.c */
extern void pronoun_substitute P((char *, object *, char *, object *));
extern char *substitute P((char *));
extern void notify P((object *, char *));
extern void cnotify P((object *, char *));
extern void ncnotify P((object *, char *));
extern void clear_exception P((CONTENTS *));
extern void add_exception P((CONTENTS **, object *));
extern void notify_in P((object *, CONTENTS *, char *));
extern void __notify P((object *, char *, int));
extern void perm_denied P((object *, char *));
extern void do_page P((object *, char *, char *));
extern void do_announce P((object *, char *));
extern void do_broadcast P((object *, char *));
extern void do_emit P((object *, char *));
extern void do_pemit P((object *, char *, char *));
extern void do_zemit P((object *, char *, char *));
extern void do_whisper P((object *, char *, char *));
extern char *color_eval P((char *));

// Separate because it takes up so much. 
extern void do_speak P((object *, char *, int));
extern char *speech_parse P((object *, char *));
#define DO_SAY(player, sendstr)	do_speak(player, (sendstr), SPEAK_SAY);
#define DO_POSE(player, sendstr) do_speak(player, sendstr, SPEAK_POSE);
#define DO_POSS(player, sendstr) do_speak(player, sendstr, SPEAK_POSS);
#define DO_THINK(player, sendstr) do_speak(player, sendstr, SPEAK_THINK);
#define DO_TO(player, sendstr) do_speak(player, sendstr, SPEAK_TO);

/* From command.c */
extern void remove_command P(( char * ));
extern void append_cmdlist P((CL *, void *));
extern void build_cmdlist P((void));
extern void build_hashcmd P((CL *, PHANDLE *));
extern CLIST *mkcmd P((CLIST *, char *));
extern void filter_command P((object *, char *, int));
extern void do_reboot P((object *, char *));
extern void do_shutdown P((object *));
extern void do_listcmd P((object *));
extern void do_alias P((object *, char *, char *));
extern void do_alias_list P((object *, char *));
extern void purge_commands P((PHANDLE *));

/* From create.c */
extern void do_roomjoin P((object *, char *, char *));
extern void do_open P((object *, char *, char *));
extern void do_create P((object *, char *, char *));
extern void do_dig P((object *, char *, char *));

/* From db.c */
extern void SET P((char **, char *));
extern object *linkit P((object *));
extern int calculate_dbsize P((void));
extern object *loc P((object *));
extern object *delete_object P((object **, object **, object *));
extern void load_database P((void));
extern void save_database P((char *));
extern void new_database P((void));
extern object *create_object P((dbref, int, char *));
extern void fork_and_dump P((void));
extern void do_dbcheck P((void));
extern void set_zones P((void));
extern void set_inherency P((void));

/* From encrypt.c */
extern void vigenere_print P((object *, char *, char *));
extern void vigenere_decrypt P((object *, char *, char *));

/* From softcode.c */
extern void displayEnvironmentStats P((object *));
//extern void evaluate P((char *, object *, char *, object *, unsigned long));

/* From error.c */
extern int call_error_report P((object *, int, int, char *));

// ERROR REPORT MACRO...
#define error_report(a, b) call_error_report(a, b, __LINE__, __FILE__)

/* From examine.c */
extern void do_examine P((object *, char *, char *));

/* From fight.c */
extern void Fight P((object *, char *, char *));
extern void Levels P((object *));
extern void Flee P((object *));
extern void FightAction P((void));

/* From fileutil.c */
extern int getint P((FILE *));
extern char *getstring P((FILE *));
extern long getlong P((FILE *));
extern long long getllong P((FILE *));
extern float getfloat P((FILE *));

/* From help.c */
extern void do_uptime P((object *));
extern void do_version P((object *));
extern void do_help P((object *, char *, char *));
extern void do_changelog P((object *, char *));
extern void do_motd P((object *));
extern void do_todo P((object *, char *, char *));

/* From hub.c */
/**extern void initialize_connection P((void));
extern void hub_read P((void));
extern void hub_rwho P((object *, char *));
extern void hub_rpage P((object *, char *, char *));
extern void hub_rping P((object *, char *));
extern void hub_rworld P((object *));
**/

/* From list.c */
extern void do_list P((object *, char *, char *));

/* From locks.c */
extern char *get_chunk P((char *, int, int));
extern int lock_race P((object *, char *));
extern int lock_class P((object *, char *));
extern int lock_power P((object *, char *));
extern int lock_flag P((object *, char *));
extern int parse_lock P((object *, char *));

/* From look.c */
extern void do_look P((object *, char *));
/**extern void do_who P((object *, char *));**/
extern void do_whois P((object *, char *));
extern void do_laston P((object *, char *));
extern char *who_flags P((object *));

/* From mail.c */
extern void free_mail P((object *));
extern void __announce_mail P((object *));
extern void save_mail P((char *));
extern void load_mail P((void));
extern void dump_mail P((char *));

/* From match.c */
extern int match_objnum P((char *));
extern long match_num P((char *));
extern object *match_combatable P((object *, char *));
extern object *match_special P((object *, char *));
extern object *match_partial P((object *, char *));
extern object *match_moveable P((char *));
extern object *match_thing P((char *));
extern object *match_player P((char *));
extern object *match_room P((char *));
extern object *match_exit P((char *));
extern object *match_object P((char *));
extern object *remote_match P((object *, char *));
extern object *match_partial_type P((object *, char *, int));
extern object *match_pseudoexit P((object *, char *));

/* From memory.c */
extern void block_free P((void *));
extern void *stack_em P((size_t, int));
extern void *stack_realloc P((void *, int, int));
extern void clear_stack P((void));

/* From objects.c */
extern int change_home P((object *, object *, char *));
extern int change_ownership P((object *, object *, char *));
extern char *unparse_object P((object *));
extern void do_drop P((object *, char *));
extern void do_take P((object *, char *));
extern void add_content P((object *, object *));
extern void del_content P((object *, object *));
extern void do_teleport P((object *, char *, char *));
extern char *flaglist P((object *));
extern char *aflaglist P((int));
extern void do_chownall P((object *, char *, char *));

/* From parent.c */
extern void add_parent P((object *, char *, int));
extern void do_addparent P((object *, char *, char *));
extern void do_listparent P((object *, char *, char *));
extern int DOPARENT P((object *, object *, char *, int));
extern void clear_children P((object *));
extern void clear_parent P((object *));
extern void do_delparent P((object *, char *, char *));
extern void update_parent_attribute P((object *, char *));
extern void update_child_attribute P((object *, char *));


/* From paste.c */
extern void do_paste P((DESCRIPTOR *, char *));
extern void start_paste P((object *, char *));

/* From player.c */
extern void do_home P((object *));
extern void do_remove_exit P((object *, char *));
extern void guest_precycle P((object *, object *));
extern void do_join P((object *, char *));
extern void do_summon P((object *, char *));
extern int move_to P((object *, object *));
extern void do_enter P((object *, char *));
extern void do_leave P((object *));
extern void do_recycle P((object *, char *, char *, char *));
extern void do_precycle P((object *, char *, char *));
extern object *create_player P((object *, char *, char *));
extern void do_pcreate P((object *, char *, char *));
extern int match_type P((object *, char *));
extern void do_cboot P((object *, char *));
extern void do_give P((object *, char *, char *));
extern void do_gold P((object *, char *));
extern int can_control P((object *, object *));
extern int connected_player P((object *));
extern void do_boot P((object *, char *, char *));
extern void do_foreach P((object *, char *, char *));
extern void do_trigger P((object *, char *, char *));
extern void do_switch P((object *, char *, char *));
extern void do_talk P((object *, char *, char *));
extern void do_mutype_toggle P((object *));
extern void do_inventory P((object *, char *));

/* From plugin.c */
extern void load_plugin P((object *, char *));
extern void unload_plugin P((object *, char *));
extern void UnloadPlugins P((void));
extern void list_plugins P((object *));
extern void plugin_selectchk P((fd_set, fd_set));
extern void plugin_readset P((fd_set *, int *));
extern void plugin_writeset P((fd_set *, int *));
extern void PluginCommand P((object *, char *, char *));

/* From powers.c */
extern void set_pow P((OBJECT *, ptype, ptype));
extern int get_pow P((OBJECT *, ptype));
extern int name_to_class P((char *));
extern int class_to_list_pos P((int));
extern char *class_to_name P((int));
extern void put_powers P((FILE *, object *));
extern void get_powers P((object *, char *));
extern int has_pow P((object *, object *, ptype));
extern int Level P((object *));

/* From report.c */
extern void do_connections P((object *));
extern void do_dbinfo P((object *));
extern void do_report P((object *, char *, char *));
extern void do_search P((object *, char *, char *));
extern void do_find P((object *, char *, char *));
extern void do_info P((object *, char *));

/* From src/softcode */
/* From softcode.c */
extern void softcode_startup P((void));

/* From parser.c */
extern void exec P((char **, char *, object *, object *, int));
extern void exp_parser P((object *, char *));
extern void do_funlist P((object *));
extern void func_zerolev P((void));
extern void do_atat P((void));
extern void init_startups P((void));
extern void insert_func_hash P((void));
extern FUN *addFunc P((FUN *));
extern void delFunc P((char *));

/* From stringutil.c */
extern int NULLIFIED P((char *));
extern char *mcrypt P((char *));
extern int dcrypt P((char *, char *));
extern char *lfof P((char *, int));
extern char *stringalloc P((char *));
#define stralloc stringalloc
extern char *strset P((char *));
extern int isanumber P((char *));
extern char *get_inside P((char *, int, int, int));
extern char *flip P((char *));
extern int ISNUM P((int));
extern char *rjust P((char *, int));
extern char *comma P((unsigned long long));
extern void make_header P((object *, char *));
extern int match_key P((char *, char *));
extern char *rparse P((char *, int));
extern char *fparse P((char *, int));
extern char *parse_it P((char **, int));
extern char *lower_case P((char *));
extern char to_upper P((int));
#define LC(x) lower_case(x)
extern char *make_ln P((char *, int));
extern char *cjust P((char *, int));
extern int string_prefix P ((char *, char *));
extern int strcmpm P((char *, char *));
extern char *parse_up P((char **, int));
extern int isalphanumeric P((char));
#ifndef NO_PROTO_VARARGS
extern char *pstr P((char *, ...))
#define tprintf pstr
#ifdef __GNUC__
__attribute__ ((format (printf, 1, 2)))
#endif
;
#endif

/* io.c */
extern TQUEUE *TQUEUE_FREE P((TQUEUE *));
extern void do_resock P((void));
extern void do_command P((DESCRIPTOR *, char *));
extern void connect_player P((DESCRIPTOR *));
extern void dwrite P((DESCRIPTOR *, char *));
extern void shutdownsock P((dbref, char *));
extern void boot_off P((dbref));
extern void dfree P((int));
extern char *strip_ansi P((const char *));
extern int process_output P((DESCRIPTOR *));

/* From iomisc.c */
extern void world_dwrite P((char *));
extern void world_dwrite_q P((char *));
extern int display_file P((DESCRIPTOR *, char *));
extern int show_welcome P((DESCRIPTOR *));
extern void do_showterm P((object *));

/* From mail.c */
extern void do_mail P((object *, char *, char *));

/* From softcode/queue.c */
extern QUEUE *delete_from_qnow P((QUEUE *));
extern QUEUE *delete_from_queue P((QUEUE *));
extern void set_inqueue P(( object *, char *, int, char *));
extern void parse_queue P(( object *, char *, int, char *));
extern void DO_QNOW P((void));
extern void remove_queue P((object *));

/* From set.c */
extern void do_edit P((object *, char *, char *));
extern int __find_player P((char *));
extern int __allowed_name P((char *));
extern int __name_player P((object *, object *, char *));
extern void do_set P((object *, char *, char *, char *));
extern ATTR *__local_attribute_search P((object *, char *));
extern int is_grouped P((object *, object *));

/* From telnet.c */
extern char *t_echo_on P((void));
extern char *t_echo_off P((void));
extern char *t_eor P((void));

/* From text.c */
extern void do_text P((object *, char *, char *));

/* From timer.c */
extern char *get_date P((time_t));
extern char *get_timestamp P((time_t));
extern char *time_format2 P((time_t));
extern char *time_format P((time_t));
extern char *count_idle P((time_t));
extern char *TimeFormat P((time_t));
extern void do_timer P((void));
extern void do_wait P((object *, char *, char *));
extern void do_halt P((object *, char *, char *));
extern void do_ps P((object *, char *, char *));

/* From tools.c */
extern void setup_myth P((void));
extern int load_config P((void));
extern void list_config P((object *));
extern char *return_config P((char *));

// these WILL be changed over to stringutil.c later
extern char *front P((char *));
extern char *restof P((char *));

/* From exittrigger.c */
extern char *show_pseudo_exit P((char *));
extern int match_pseudo_exit P((object *, char *));
extern int match_real_exit P((object *, char *));
extern void take_trigger P((object *, object *));
extern void exit_trigger P((object *, object *));

/* From trigger.c in softcode/ */
extern int match_trigger P((object *, char *));
extern void do_listen P((object *, char *));
extern int listen_match P((object *, char *, char *, int));

/* From who.c */
extern void Who P((object *));

/* From wild.c */
extern void set_args P(( void ));
extern void do_wildcard P((object *, char *, char *));
extern int wild P((char *, char *, VARLIST **));
extern int match_wild P((char *, char *));
extern int switch_match_wild P((char *, char *));
extern void cat_str P((char *, char *));
extern void math_twice1 P((void));

/* From zone.c */
extern void do_zlink P((object *, char *, char *));
extern void do_unzlink P((object *, char *));
extern int DOZONE P((object *, object *, char *));

/* Include World Macros */
#include "macros.h"
