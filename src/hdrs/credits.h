/****************************************************************************
 MYTH v1.0 - Credits
 Coded by Saruk (sarukie@willcliffe.com)
 ---
 Please read the copyright notice included in the src/hdrs directory. It is
 also included in the myth/docs section as well, but please include it anyway.
 ****************************************************************************/

/* Where should technical problems, bugs, questions be sent to for this version? */
#define TECH_EMAIL	"sarukie@willcliffe.com"

/* What is the base of this code? */
#define SERVER_BASE	"MYTH v1.0"

/* What is the current server code/version? */
#define SERVER_CODE	"TinyMYTH"

/* What is the database version? (This can be useful for backwards compatibility) */
#define DB_VERSION	1

/* Some copyright information */
#include "copyright.h"
#include "build.h"
