/****************************************************************************
 MYTH - Command structure, parsing, etc.
 Coded by Saruk (01/25/99)
 ---
 Command filtering, processing, etc.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <ctype.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "db.h"
#include "net.h"
#include "externs.h"
#include "cmdlist.h"

// command.c specifics
void do_alias P((object *, char *, char *));
void add_alias P((object *, char *, char *));
void del_alias P((object *, char *));
void _filter_command P((object *, char *, int));

// command variables
int recursion = 0;
int clev = 0;
object *genactor = 0;

/* global_match - 
     this should allow filter_command to figure out who you mean, no matter what 
     their location is and store them in global_match. global_match lasts as long as
     your current process level.
*/
object *global_match = NULL;

HASHCMD cmdhash[MAX_COMMANDS];

unsigned int hash_cmd_string(char *s)
{
	unsigned int h;
	
	h = *s;
	h += (h << 4) * 11;
	if(*s == '@' || *s == '+')
	  if(*(s+1))
	    h += h * 11 + *(s+1);
	
	h %= MAX_COMMANDS;
	
	return(h);
}

// If we're in here we're only searching @_, +_, or _ sets.
// where _ refers to a letter variable.
int hash_ambiguity(object *player, char *cmd, unsigned int h)
{
	char buf[8000];
	int amt = 0;
	CL *c;
	
	strcpy(buf, "");	
	for(c = cmdhash[h].cmdlist; c; c=c->next)
	  if(string_prefix(cmd, c->command)) {
	    if( (c->flags & NOAMB) || !strcmpm(c->command, cmd) ) {
	      if(!strcmp(cmd, "+") || !strcmpm(cmd, "@"))
	        amt++;
	      else
	        return 0;
	    }
	    amt++;
	    sprintf(buf + strlen(buf), "%s ", c->command);
	  }
	
	if(amt > 1) {
	  notify(player, pstr("Ambiguous commands: %s", buf));
	  return 1;
	}
	
	return 0;
}

CL *hash_match(object *player, char *string)
{
	CL *c = NULL;
	unsigned int h = hash_cmd_string(lower_case(string));
	
	if(h < 0)
	  return(NULL);
	
	for(c = cmdhash[h].cmdlist; c; c=c->next) {
	  if(string_prefix(string, c->command))
	    break;
	}
	
	if(!c)
	  return(NULL);
	  
	if( ((player->flags & TYPE_PLAYER)? (c->powerlevel <= player->powlvl):((player->flags & INHERIT)) ? (c->powerlevel <= player->ownit->powlvl) : (c->powerlevel <= player->powlvl))  &&
	  ( ((c->flags & WIZARD) && ( (player->flags & TYPE_PLAYER) ? (player->flags & WIZARD):(player->flags & INHERIT) ? (player->ownit->flags & WIZARD) : (player->flags & WIZARD))) || 
	  !(c->flags & WIZARD)) )
	  return (c);
	
	return(NULL);
}

void insert_hashcmd(CL *c, unsigned int h, PHANDLE *handle)
{
	CL *cnew;
	
	GENERATE(cnew, CL);
	cnew->command = NULL;
	SET(cnew->command, c->command);
	cnew->func = c->func;
	cnew->flags = c->flags;
	cnew->powerlevel = c->powerlevel;
	if(handle)
	  cnew->handle = handle;
	else
	  cnew->handle = NULL;
	  
	if(!cmdhash[h].cmdlist) {
	  cnew->next = NULL;
	  cnew->prev = NULL;
	  cmdhash[h].cmdlist = cnew;
	  cmdhash[h].cmdtail = cnew;
	} else {
	  cnew->next = NULL;
	  cnew->prev = cmdhash[h].cmdtail;
	  cmdhash[h].cmdtail->next = cnew;
	  cmdhash[h].cmdtail = cnew;
	}
}

void remove_hashcmd(CL *c)
{
	unsigned int h = hash_cmd_string(c->command);
	
	if(c->prev)
	  c->prev->next = c->next;
	
	if(c->next)
	  c->next->prev = c->prev;
	
	if(cmdhash[h].cmdlist == c)
	  cmdhash[h].cmdlist = c->next;

	if(cmdhash[h].cmdtail == c)
	  cmdhash[h].cmdtail = c->prev;	
}

/** Old plugin stuff. **/
void purge_commands(PHANDLE *p)
{
	int i;
	unsigned int h;
	CL *c, *r, *rnext = NULL;
	void *(*func)();
	
	if(!(func = dlsym(p->handle, "additional_command"))) {
	  printf("**PANIC ERROR *** DLSYM PROBLEM\n");
	  return;
	}
	if(!dlerror()) {
	  if( (c = func()) == NULL)
	    return;
	  for(i = 0; c[i].command; i++) {
	    h = hash_cmd_string(c[i].command);
	    for(r = cmdhash[h].cmdlist; r; r=rnext) {
	      rnext = r->next;
	      if(p->handle == r->handle)
	        remove_hashcmd(r);
	    }
	  }
	} else {
	  log_channel("log_sens", "* Error in purging commands from plugin handler.");
	  return;
	}
}
/****/

void build_hashcmd(CL *c, PHANDLE *handle)
{
	unsigned int h = hash_cmd_string(lower_case(c->command));
	
        insert_hashcmd(c, h, handle);
}

void build_cmdlist()
{
	int i;

	for(i = 0; cmdlist_default[i].command; i++) {
	  build_hashcmd(&cmdlist_default[i], NULL);
	}
}


void do_listcmd(player)
	object *player;
{
	CL *c;
	int i = 0;
	
	notify(player, "Available Commands:");
	for(i = 0; i < MAX_COMMANDS; i++) {
	  for(c = cmdhash[i].cmdlist; c; c=c->next) {
	    if( (player->powlvl >= c->powerlevel) && ( ((c->flags & WIZARD) && (player->flags & WIZARD)) ||
	        !(c->flags & WIZARD)) ) {
	      notify(player, pstr("[%-15.15s] [%3d/%3d]", c->command, c->powerlevel, player->powlvl));
	    }
	  }
	}
}

void do_reboot(player, style)
	object *player;
	char *style;
{
  DESCRIPTOR *d, *dnext = NULL;

  /** Check to make sure we haven't had the binary cleaned. If we have,
      we report that we are currently unable to do a reboot. **/

  if(!strcmpm(style, "quiet")) {
    notify(player, "[WIZARD]: Initiating a quiet reboot...");
    log_channel("log_io", pstr("* %s initiated a quiet reboot...", color_name(player, -1)));
    reboot_flag = 2;
  } else if (!strcmpm(style, "wizard")) {
    notify(player, "[WIZARD]: Initiating a wizard reboot...");
    log_channel("lo_io", pstr("* %s initiated a wizard reboot...", color_name(player, -1)));
    for(d=descriptor_list;d;d=dnext) {
      dnext = d->next;
      if(d->state == CONNECTED && d->player)
        if(!(d->player->flags & WIZARD)) {
	  dwrite(d, "[REBOOT]: Wizard reboot - sorry, disconnecting you...\n");
	  boot_off(d->player->ref);
	}
    }
    reboot_flag = 4;
  } else if (!strcmpm(style, "nosave")) {
    notify(player, "Initiating a NOSAVE reboot...");
    log_channel("log_io", pstr("* %s initiated a NOSAVE reboot, database will NOT be saved...", color_name(player, -1)));
    reboot_flag = 5;
  } else {
    notify(player, "[WIZARD]: Initiating a reboot...");
    log_channel("log_io", pstr("* %s initiated a standard reboot...", color_name(player, -1)));
    reboot_flag = 1;
  }
		
  shutdown_flag = 1;
}

void do_shutdown(player)
	object *player;
{
	DESCRIPTOR *d;
	
	for(d=descriptor_list;d;d=d->next)
	  if(d->state == CONNECTED||d->state==PASTE_MODE)
	    dwrite(d, pstr("[SHUTDOWN]: %s caused a shutdown!\n", color_name(player, -1)));
	shutdown_flag = 1;
}


char *check_switch(char *instr, char delimiter)
{
	char *p;
	
	for(p = instr; *p; p++) {
	  if(*p == ' ')
	    return(NULL);
	  else if(*p == delimiter)
	    break;
	}
	
	if(!*p)
	  return(NULL);
	
	return(p+1);
}

/* 
  We've done a rudimentary command matching hash table. It is not quiet as speedy as we'd like because
  there's no way to get the correct hash if you type 'l' and 'look' has been hashed. So we hash the 
  first char (except on @ and + where we hash the first and second chars)...
  
  There are numerous collisions in each table, but it works faster than a normal string matching of 
  every command.
  
  Our only other problem is the fact that checking properly for ambiguity necessitates matching strings
  twice. Unfortunately, removing ambiguity checking is not a smart idea.
    
  Command parsing after matching could really use some touch ups, as well.
*/

void filter_command(player, instr, direct)
  object *player;
  char *instr;
  int direct;
{
  _filter_command(player, instr, direct);
  if(!direct) { player->clev--; }
}

void _filter_command(player, instr, direct)
	object *player;
	char *instr;
	int direct;
{
	struct timeval tv1, tv2;
	CLIST *clist;
	CL *fp;
	char *p, *parse_1, *parse_2;
	char command[8192], cmdswitch[8192], arg1[8192], arg2[8192];
	char parsed_arg1[8192], parsed_arg2[8192], pure[8192];
	int index = 0;

	p = instr + strlen(instr)-1;
	while(*p && isspace(*p)) *p-- = 0;
	
//	printf("_filter_command (instr): %s\n", instr);
	
	// cmd_player is used for QUEUED events.
	cmd_player = player;
	func_zerolev();
	
        *command = *cmdswitch = *arg1 = *arg2 = *parsed_arg1 = *parsed_arg2 = *pure = '\0';
  
	if( (player->flags & HAVEN) && !direct )
	  return;
	
	//printf("(%s) %s\n", player->name, instr);
	//DEBUG(pstr("CLEV for %s: %d", player->name, player->clev));
	/** Not sure how this will work. Lot of work to do here. **/
	if(!direct && (player->clev++ > 100) ) {
	  player->clev = 0;
	  player->flags |= HAVEN;
	  DEBUG(pstr("%s caused a command overflow", player->name));
	  DEBUG(pstr("COMMAND: %s", instr));
	  return;
	} else if (player->clev < 0)
	  player->clev = 0;
	
	//DEBUG(pstr("CMDLVL(%s): %d", player->name, player->clev));
	if(direct) { // direct
		set_args();
		player->clev = 0;
		genactor = player;
	} else {
		if(!genactor)
		  genactor = player;
		else
		  genactor = genactor;
	}
	
	if(player->flags & SUSPECT)
		log_channel("log_suspect", pstr("* (%s) %s->: %s", 
			get_date(time(0)), color_name(player, -1), instr));
	
	if(player->flags & GUEST)
		log_channel("log_guest", pstr("* (%s) %s->: %s",
                        get_date(time(0)), color_name(player, -1), instr));	
        
        while(*instr && isspace(*instr))
		instr++;
	
	if(!*instr) {
	  notify(player, "Were you attempting some form of telepathic communication?");
	  return;
	}
	
	// This is our token matching. We've created a switch for MUD users
	// so that they're not stucking attempting to learn MUSH-ish tokens.
	// 1 is MUD, 0 is MUSH.
	// We're going to take this out _real_ soon.
	if(player->powlvl > 5) {
	  if(!atoi(atr_get(player, A_MUTYPE))) { // **** MUSH ****
	    if(*instr == SAY_TOKEN) {
	      do_speak(player, instr + 1, SPEAK_SAY);
	      return;
	    } else if (*instr == POSE_TOKEN) {
	      DO_POSE(player, instr + 1);	
	      return;	
	    } else if (*instr == POSS_TOKEN) {
	      DO_POSS(player, instr + 1);
	      return;
	    } else if (*instr == THINK_TOKEN) {	
	      DO_THINK(player, instr + 1);
	      return;
	    } else if (*instr == TO_TOKEN) {
	      DO_TO(player, instr + 1);
	      return;
	    } else if (*instr == CHAN_TOKEN) {	
	      DefaultChannel(player, instr + 1);
	      return;
	    }
	  } else { // **** MUD ****
	    if(*instr == MUD_SAY_TOKEN) {
	      do_speak(player, instr + 1, SPEAK_SAY);
	      return;
	    } else if (*instr == POSE_TOKEN) {
	      DO_POSE(player, instr + 1);	
	      return;	
	    } else if (*instr == POSS_TOKEN) {
	      DO_POSS(player, instr + 1);
	      return;
	    } else if (*instr == THINK_TOKEN) {	
	      DO_THINK(player, instr + 1);
	      return;
	    } else if (*instr == MUD_TO_TOKEN) {
	      DO_TO(player, instr + 1);
	      return;
	    } else if (*instr == MUD_CHAN_TOKEN) {	
	      DefaultChannel(player, instr + 1);
	      return;
	    }
	  }
	}


	strcpy(command, "");
	strcpy(cmdswitch, "");
	strcpy(arg1, "");
	strcpy(arg2, "");

	
	if((p = check_switch(instr, '/'))) {
	  strcpy(cmdswitch, front(p));
	  strcpy(command, fparse(instr, '/'));
	  strcpy(arg1, restof(p));
	} else {
	  strcpy(command, front(instr));
	  strcpy(arg1, restof(instr));
	}
	
	if(strchr(arg1, '=')) {
	  strcpy(arg2, rparse(arg1, '='));
	  strcpy(arg1, fparse(arg1, '='));
	}
	
	if(*arg1) {
	  while(arg1[strlen(arg1)-1] == ' ')
	    arg1[strlen(arg1)-1] = '\0';
	}
  
	strncpy(pure, restof(instr), sizeof pure);
	
	/* MATCHING EXITS */
	// Pseudo Exits
	if( (index = match_pseudo_exit(player, command)) == 1)
	  return;
	
	// Object Exits
	if(match_real_exit(player, instr))
	  return;
	
	if(index < 0) {
	  notify(player, "* You can't go that way...");
	  return;
	}
		
	/*if(match_channels(player, command, restof(instr)))
	  return;*/
	for(clist = player->cmdlist; clist; clist=clist->next) {
	  if(!strcmpm(clist->cmd, command) ) {
	    /*if((recursion++) > (15)) {
              error_report(player, M_ERECURSE);
	      recursion = 0;
	      return;
            }*/
 	    strcpy(command, clist->send);
	    if(strlen(restof(instr)) > 0)
	      sprintf(command, "%s %s", command, restof(instr));
	    filter_command(player, command, 0);
	    return;
	  }
        }
	/** Extra precaution **/
	if( (*command == '@' && !*(command+1)) || (*command == '+' && !*(command+1)) ) {
	  notify(player, "Too many ambiguities.");
	  return;
	}
	
	if(hash_ambiguity(player, command, hash_cmd_string(lower_case(command))))
	  return;

	if( (fp = hash_match(player, command)) ) {
	  if( (fp->flags & TEXT) ) {
	    fp->func(player, command, arg1);
	  } else if( (fp->flags & PURITY) ) {
	    fp->func(player, pure);
	  } else if( (fp->flags & SPEAK_SAY) ) {
	    DO_SAY(player, pure);
	  } else if( (fp->flags & SPEAK_POSE) ) {
	    DO_POSE(player, pure);
	  } else if( (fp->flags & SPEAK_THINK) ) {
	    DO_THINK(player, pure); 
	  } else if( (fp->flags & SWITCH) ) {
	    fp->func(player, cmdswitch, arg1, arg2);
	  } else {
	    // Parse the functions into the arguments
	    // so we can use: @fo [lwho()]=...
	    //DEBUG(pstr("arg1: %s, arg2: %s", arg1, arg2));
            parse_1 = arg1;
            exec(&parse_1, parsed_arg1, cmd_player, player, 0);
            parse_2 = arg2;
            exec(&parse_2, parsed_arg2, cmd_player, player, 0);
	    fp->func(player, parsed_arg1, parsed_arg2);
	  }
  
	  recursion = 0;
	  return;
	}	
	
	// since we're not seeing a command, exit, or channel, let's check for attributes.
	// if they create an attribute that matches a command, they won't get to set the attribute by way
	// of the shortcut. that'll teach them!
	// We're looking at @<attr> <target>=<value>
	if(*instr == '@') {
		if( (__all_attribute_search(front(instr)+1)) || (__is_builtin(front(instr)+1)) ) {
			// Let's chop it up.
			/* pseudo code.
				@<attr> <target>=<value>
				@set/attr b =a:c
				---
				a = front(instr+1)
				temp = restof(instr)
				b = fparse(temp, '=');
				c = rparse(temp, '=');
				sprintf(buf, "@set/attr %s=%s:%s", b, a, c);
			*/
			sprintf(pure, "@set/attribute %s=%s:%s", arg1, front(instr + 1), arg2);
			filter_command(player, pure, 0);
			return;
		}
	}
	
	if(*instr == '&') {
	  do_easyattr(player, instr + 1);
	  return;
	}
	
	/** Let's match for channels **/
	if(*instr == '+') {
	  if(ParseChannelShortcut(player, instr + 1))
	    return;
	}
	
	// Now it's time to match for triggers.
	if(match_trigger(player, instr))
		return;
	
	notify(player, "Huh? (Type 'help' for help.)");
}


/***
 command_parse();
 -- This function will parse the incoming string into the appropriate
 sectioned chunks (ie: COMMAND, CMDSWITCH, ARGUMENT1, ARGUMENT2, and PURE)
 
 * COMMAND - Obviously the command
 * CMDSWITCH - Lots of commands will have switches
 * ARGUMENT1 - Before the = delimiter.
 * ARGUMENT2 - Proceding the = delimiter.
 * PURE - Anything after COMMAND or COMMAND + CMDSWITCH
 
int command_parse(char *instr;)
{
	char command[1024], cmdswitch[1024], arg1[1024], arg2[1024];
	char pure[2048];
	
	strcpy(command, "");
	cmdswitch = arg1 = arg2 = command;
	if((p = *strchr(instr, '/'))) {
	  strcpy(cmdswitch, front(p));
	  strcpy(command, fparse(instr, '/'));
	} else {
	  strcpy(command, front(instr));
	  strcpy(arg1, restof(instr));
	}
	
	if(strchr(arg1, '=')) {
	  strcpy(arg2, rparse(arg1, '='));
	  strcpy(arg1, fparse(arg1, '='));
	}
	
	strcpy(pure, restof(instr));
	
}
***/

// seen as how command aliasing is a command function...
void do_alias(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	OBJECT *who;
	CLIST *tmp;
	CL *c;
	
	if(!*arg1) {
		do_alias_list(player, player->name);
		return;
	} else if (*arg1 && !*arg2) {
		if(!(who = match_object(arg1))) {
			error_report(player, M_ENOPLAY);
			return;
		}
		do_alias_list(player, who->name);
		return;
	}
		
	if(!*arg1 || !*arg2) {
		error_report(player, M_EINVARG);
		return;
	}			
	
	if(!strcmpm(arg1, "delete")) {
		for(tmp = player->cmdlist;tmp; tmp=tmp->next)
			if(!strcmpm(arg2,tmp->cmd)) {
				notify(player, pstr("[DELETE]: %s was deleted.",
					tmp->cmd));
				del_alias(player, tmp->cmd);
				return;
			}
		
		error_report(player, M_EILLSTRING);
		return;
	}	
	
	for(c = cmdhash[hash_cmd_string(lower_case(arg1))].cmdlist; c; c=c->next) {
	  if(string_prefix(arg1, c->command)) {
	    error_report(player, M_ESHDBLT);
	    return;
	  }
	}
	
	add_alias(player, arg1, arg2);
}

void add_alias(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	CLIST *tmp;
	
	for(tmp = player->cmdlist; tmp; tmp=tmp->next)
		if(!strcmpm(tmp->cmd, arg1)) {
			error_report(player, M_ESHDBLT);
			return;
		}
	

	GENERATE(tmp, CLIST);
	tmp->cmd = NULL;
	tmp->send = NULL;
	SET(tmp->cmd, arg1);
	SET(tmp->send, arg2);

	tmp->next = player->cmdlist;
	player->cmdlist = tmp;
}

void del_alias(player, todel)
	object *player;
	char *todel;
{
	CLIST *c, *cprev = NULL;
	
	for(c = player->cmdlist; c; c=c->next) {
		if(!strcmpm(c->cmd, todel))
			break;
		cprev = c;
	}
	
	if(!c)
		return;
	
	if(!cprev)
		player->cmdlist = c->next;
	else
		cprev->next = c->next;
	
	if(c->cmd)
		block_free(c->cmd);
	if(c->send)
		block_free(c->send);
	block_free(c);
}

void do_alias_list(player, arg1)
	object *player;
	char *arg1;
{
	object *who;
	CLIST *c;
	
	if(*arg1) {
		if(!(who=match_player(arg1))) {
			error_report(player, M_ENOPLAY);
			return;
		}
	} else
		who = player;

	if(!can_control(player, who)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	notify(player, pstr("%s's Commands:", color_name(who, -1)));
	
	if(!who->cmdlist) {
		notify(player, "* You have no command aliases.");
		return;
	}
	
	for(c=who->cmdlist;c;c=c->next)
		notify(player, pstr("[%-12.12s] represents: %s",
			c->cmd, c->send));
		
}

CLIST *mkcmd(cmdlist, tmpstr)
	CLIST *cmdlist;
	char *tmpstr;
{
	CLIST *c;
	char buf[2048];
	
	strcpy(buf, tmpstr);
	GENERATE(c, CLIST);
	c->cmd = NULL;
	c->send = NULL;
	SET(c->cmd, front(buf));
	SET(c->send, restof(buf));

	c->next = cmdlist;
	cmdlist = c;
	return(cmdlist);
}
