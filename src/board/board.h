/*** board.h ***/

typedef struct bboard_t {
        char *topic;            // Topic/Title
        int c_ref;              // creator reference
        object *creator;
        char *message;
        time_t created;
        time_t expiration;
        int num_read;

        struct bboard_t *next;
} BBOARD;


extern BBOARD *bblist;                                                                

