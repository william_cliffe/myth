/****************************************************************************
 MYTH - Help System
 Coded by Saruk (02/06/99)
 ---
 The goal is to use this system like man pages. In fact, possibly using the 
 syntax as man pages.
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"

void do_version(player)
	object *player;
{
	make_header(player, "--- Version Information ---");
	notify(player, pstr("\tCode Based Off........: %s\n", SERVER_BASE));
	notify(player, pstr("\tCurrent Code Version..: %s v%d.%d.%d (Build #%d)", SERVER_CODE, VER_MAJOR, VER_MINOR, VER_TINY, BUILD_NUM));
	notify(player, pstr("\tDatabase Version......: MYTH DB v%d", DB_VERSION));
	notify(player, pstr("\tMain Developer........: %s\n", DEVELOPER));
	notify(player, pstr("\tPrevious Developers...: %s", PREV_DEVELOPERS));
	notify(player, pstr("%s", make_ln("-", 78)));
	notify(player, pstr("\tCompiler Options......: %s",
#ifdef CATCHSEG
	"CATCHSEG "
#else
	""
#endif
	));
	notify(player, pstr("\tLast Code Upgrade.....: %s", get_date(LAST_BUILD_DATE)));
	notify(player, pstr("\tCopyright Date........: %s", COPYRIGHT_DATE));
	notify(player, pstr("%s", make_ln("=", 78)));
}

void do_uptime(player)
	object *player;
{
	float throughput = 0, throughget = 0;
	time_t next_dump_time = 0;
	
	// Calculate throughput.	
	throughput = (float)total_input / (float)(time(0)-myth_reboot);
	throughget = (float)total_output / (float)(time(0)-myth_reboot);
	
	// Calculate next database dump.
	next_dump_time = last_dump_time + dump_time;

	make_header(player, "--- Uptime Information ---");
	notify(player, pstr("\tInitial Startup.......: %s\n", get_date(myth_uptime)));
	if(myth_reboots) {
		notify(player, pstr("\tLast Reboot At........: %s", get_date(myth_reboot)));
		notify(player, pstr("\tTotal Reboots.........: %d", myth_reboots));
	}
	notify(player, pstr("\tCurrent Time..........: %s", get_date(time(0))));
	notify(player, pstr("\tLast Database Save....: %s", get_date(last_dump_time)));
	notify(player, pstr("\tNext Database Number..: %d", database_saves+1));
	notify(player, pstr("\tNext Database Save....: %s\n", get_date(next_dump_time)));
	notify(player, pstr("\tTotal Input/Second....: %s bytes (%.2f/sec)",
		comma((int)total_input), throughput));
	notify(player, pstr("\tTotal Output/Second...: %s bytes (%.2f/sec)",
		comma((int)total_output), throughget));
	notify(player, pstr("%s", make_ln("-", 78)));
	notify(player, pstr("%s",cjust("Elapsed Time", 78)));
	notify(player, pstr("%s", cjust(time_format2(time(0) - myth_uptime),78)));
	notify(player, pstr("%s", cjust("Next Database Save", 78)));
	notify(player, pstr("%s", cjust(time_format2(next_dump_time - time(0)), 78)));
	notify(player, pstr("%s", make_ln("=", 78)));
}

void do_help(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	FILE *fp;
	int show_lines = FALSE;		// whether or not to show lines. (default is false)
	int found_topic = FALSE;	// Error report on FALSE.
	char filename[MAX_BUFSIZE];	// filename to get.
	char buf[MAX_BUFSIZE];		// store buffer.
	char lookfor[MAX_BUFSIZE];	// what we're looking for.
		
	sprintf(filename, "%s/%s.txt", return_config("text_path"), arg1);
	
	if(!(fp = fopen(filename, "r"))) {
		error_report(player, M_ENOFILE);
		return;
	}

	strcpy(lookfor, (!*arg2) ? arg1 : arg2);
		
	while(fgets(buf,sizeof(buf), fp)) {
		if(buf[strlen(buf)-1]=='\n')
			buf[strlen(buf)-1] = '\0';
			
		if(buf[0] == '&' && found_topic == FALSE) {
			if(string_prefix(lookfor, buf + 1)) {
				show_lines = TRUE;
				notify(player, pstr("TOPIC: %s", buf + 1));
			} else
				show_lines = FALSE;
			strcpy(buf, "");
		} else if (buf[0] == '&' && found_topic == TRUE) {
			show_lines = FALSE;
		}

		if(show_lines) {
			found_topic = TRUE;
			if(buf[0] == '.') 
				notify(player, pstr("See Also: %s", buf + 1));
			else
				notify(player, buf);
		}

	}

	if(!found_topic) 
		error_report(player, M_ENOTFOUND);

	fclose(fp);
}

void do_changelog(player, arg1)
	object *player;
	char *arg1;
{
	FILE *fp;
	char buf[MAX_BUFSIZE];
	
		
	if(!(fp=fopen("msgs/changelog", "a+"))) {
		error_report(player, M_ENOFILE);
		return;
	}
	
	if( (*arg1) && has_pow(player, player, POW_WIZADD)) {
		notify(player, pstr("%s added to changelog.", arg1));
		fprintf(fp, "%s - %s\n", get_date(time(0)), arg1);
		fclose(fp);
		return;
	} else if (*arg1 && !has_pow(player, player, POW_WIZADD)) {
		notify(player, "Sorry. You do not have the necessary power to make changes.");
		fclose(fp);
		return;
	}
	fclose(fp);

	if(!(fp=fopen("msgs/changelog", "r"))) {
		error_report(player, M_ENOFILE);
		return;
	}
		
	while(fgets(buf, sizeof(buf), fp))
		__notify(player, buf, 0);
	
	fclose(fp);
}

void do_motd(player)
	object *player;
{
	DESCRIPTOR *d = NULL;
	
	for(d=descriptor_list;d;d=d->next)
		if(d->player == player)
			break;	
	
	if(!d)  {
		error_report(player, M_EILLNAME);
		return;
	}
	
	display_file(d, "msgs/motd");
}


typedef struct todo_t {
        char *buf;
      	int ref;
      	
      	struct todo_t *next;
} TODO;

static TODO *todo=NULL;

void save_todo(str)
	char *str;
{
	FILE *fp;
	
	if(!(fp = fopen("msgs/todo", "a+"))) {
		log_channel("log_imp", "**ERROR** problem saving to msgs/todo");
		return;
	}
	
	fprintf(fp, "%s\n", str);
	fclose(fp);
}

void __add_to_todo(str, ref)
	char *str;
	int ref;
{
	TODO *tnew, *tprev = NULL;

	if(todo)
	{
		for(tnew = todo; tnew; tnew=tnew->next)
			tprev = tnew;
	}	
	
	GENERATE(tnew, TODO);
	tnew->buf = NULL;
	if(str[strlen(str)-1] == '\n')
		str[strlen(str)-1] = '\0';
	SET(tnew->buf, str);
	tnew->ref = ref;
	
	if(!tprev)
		todo = tnew;
	else
		tprev->next = tnew;
}

TODO *delete_todo_list()
{
	TODO *t, *tnext;
	
	for(t = todo; t; t=tnext)
	{
		tnext = t->next;
		if(t->buf)
			block_free(t->buf);
		block_free(t);
	}
	
	return(NULL);
}

void clear_todo(ref, pos, str)
	int ref;
	int pos;    // in the case of {insert}, save str here. 
	char *str;  // what pos says.
{
	FILE *fp;
	TODO *t;
	
	if(!(fp = fopen("msgs/todo", "w+"))) {
		log_channel("log_imp", "** ERROR ** can't open msgs/todo for write");
		return;
	}
	
	for(t = todo; t; t=t->next)
	{
		if((pos == (t->ref+1)) && *str) {
			fprintf(fp, "%s\n", str);
			fprintf(fp, "%s\n", t->buf);
		} else if((ref-1) != t->ref) {
			fprintf(fp, "%s\n", t->buf);
		}
	}
	
	fclose(fp);
	
	todo = delete_todo_list();
}

// First attempt at the MYTH ToDo system.
void do_todo(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	FILE *fp;
	TODO *t;
	static char buf[MAX_BUFSIZE];
	char *x, *p;
	int cnt = 0, restricted = 0;
	
	if(!(fp=fopen("msgs/todo", "r"))) {
		error_report(player, M_ENOFILE);
		return;
	}
	
	while(fgets(buf, sizeof(buf), fp))
	{
		__add_to_todo(buf, cnt++);
	}
	
	fclose(fp);

	if(!has_pow(player, player, POW_WIZADD))
		restricted++;


	if(!*arg1) {
		for(t = todo; t; t=t->next)
			notify(player, pstr("%d.) %s", t->ref+1, t->buf));
	} else if(!strcmpm(arg1, "add") && !restricted) {
		notify(player, pstr("Adding: %s", arg2));
		clear_todo(-1, -1, NULL);
		save_todo(arg2);
		return;
	} else if(!strcmpm(arg1, "del") && !restricted) {
		if(!isanumber(arg2))
		{
			notify(player, "I don't recognize that.");
			clear_todo(-1, -1, NULL);
			return;
		}
		
		if(atoi(arg2) > cnt || atoi(arg2) < 1)
			notify(player, "What +todo message are you talking about?");
		else
			notify(player, pstr("Deleting +todo %d", atoi(arg2)));
		clear_todo(atoi(arg2), -1, NULL);	
		return;

	} else if(!strcmpm(arg1, "insert") && !restricted) {
		if(!strchr(arg2, ',')) {
			notify(player, "Invalid use of +todo insert. Try: +todo insert=<#>,<text>");
			clear_todo(-1, -1, NULL);
			return;
		}		
		x = fparse(arg2, ',');	p = rparse(arg2, ',');
		if(!isanumber(x)) 
		{
			notify(player, "Just where are you trying to insert what!?");
			clear_todo(-1, -1, NULL);
			return;
		}
		
		if(atoi(x) > cnt || atoi(x) < 1) 
		{
			notify(player, "Invalid range in insert.");
			clear_todo(-1, -1, NULL);
			return;
		}
		
		notify(player, pstr("Inserting...: %s", p));
		clear_todo(-1, atoi(x), p);
		return;
	} else if (*arg1 && restricted) {
		notify(player, "Sorry, you do not have the necessary powers!");
	}
	
	clear_todo(-1);	
}

