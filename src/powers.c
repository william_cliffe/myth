/****************************************************************************
 MYTH v1.01 - Powers
 Coded by Saruk (1/29/03)
 ---
 List the powers here.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

#define YES PW_YES
#define YESLT PW_YESLT
#define YESEQ PW_YESEQ
#define NO PW_NO


static ptype powlist[NUM_POWS * 2 + 2];


	 /* DIR  ADMIN  OFF  BLDR  GDE  CIT  GUEST */ 
struct pow_list powers[]={
	{
	 "Free", POW_FREE, "Ability to build, etc., for free.",
	 { YES,  YES,  YES,  YES,  NO,  NO,  NO},
	 { YES,  YES,  YES,  YES,  YES,  NO,  NO}},
	{
	 "Boot", POW_BOOT, "Ability to boot players/connections off.",
	 { YES,  YESEQ,  YESLT,  NO,  NO,  NO,  NO},
	 { YES,    YES,  YESEQ,  NO,  NO,  NO,  NO}},
	{
	 "Shutdown", POW_SHUTDOWN, "Ability to reboot or shutdown the MYTH.",
	 { YES,  NO,  NO,  NO,  NO,  NO,  NO},
	 { YES,  NO,  NO,  NO,  NO,  NO,  NO}},
	{
	 "Join", POW_JOIN, "Ability to join players.",
	 { YES,  YES,  YESEQ,  NO,  YESLT,  NO,  NO},
	 { YES,  YES,    YES,  NO,  YESEQ,  NO,  NO}},
	{
	 "Summon", POW_SUMMON, "Ability to summon players.",
	 { YES,  YES,  YESEQ,  NO,  NO,  NO,  NO},
	 { YES,  YES,    YES,  NO,  NO,  NO,  NO}},
	{
	 "Precycle", POW_PRECYCLE, "Ability to recycle (delete) players.",
	 {  YESLT,  NO,  NO,  NO,  NO,  NO,  NO},
	 {  YESLT,  NO,  NO,  NO,  NO,  NO,  NO}},
	{
	 "Pcreate", POW_PCREATE, "Ability to create players.",
	 { YES,  YES,  NO,  NO,  NO,  NO,  NO},
	 { YES,  YES, YES,  NO,  NO,  NO,  NO}},
	{
	 "Class", POW_CLASS, "Ability to @class players.",
	 { YESLT,    NO,  NO,  NO,  NO,  NO,  NO},
	 {   YES, YESLT,  NO,  NO,  NO,  NO,  NO}},
	{
	 "Power", POW_SETPOW, "Ability to @empower and see @powers on players.",
	 {  YESEQ,  YESLT,  NO,  NO,  NO,  NO,  NO},
	 {    YES,  YESLT,  NO,  NO,  NO,  NO,  NO}},
	{
	 "Remote", POW_REMOTE, "Ability to do remote commands to objects.",
	 {    YES,    YES,    YES,    YES,    YES,    NO,   NO},
	 {    YES,    YES,    YES,    YES,    YES,   YES,   NO}},
	{
	 "Wizadd", POW_WIZADD, "Ability to add to +todo, +changelog, etc.",
	 {    YES,    YES,     NO,     NO,     NO,    NO,   NO},
	 {    YES,    YES,     NO,     NO,     NO,    NO,   NO}},
	{
	 "Modify", POW_MODIFY, "Ability to modify objects.",
	 {    YES,    YES,     NO,     NO,     NO,    NO,   NO},
	 {    YES,    YES,     NO,     NO,     NO,    NO,   NO}},
	{
	 "Seeatr", POW_SEEATR, "Ability to see other objects' attributes.",
	 {    YES,    YES,  YESEQ,  YESLT,     NO,    NO,   NO},
	 {    YES,    YES,    YES,  YESEQ,  YESLT,    NO,   NO}},
	{
	 "Combat", POW_COMBAT, "Ability to modify combat properties.",
	 {    YES,     NO,     NO,     NO,     NO,    NO,    NO},
	 {    YES,    YES,     NO,     NO,     NO,    NO,    NO}},
	{
	 "Security", POW_SECURITY, "Ability to perform security-related procedures.",
         {    YES,     NO,     NO,     NO,     NO,    NO,    NO},
         {    YES,    YES,     NO,     NO,     NO,    NO,    NO}},
};


int Level(player)
	object *player;
{
	
	if(!(player->flags & TYPE_PLAYER))
	{
	  if(player->flags & INHERIT)
	    return(Level(player->ownit));
          else
	    return CLASS_GUEST;
	}
	
	if(player->flags & GUEST)
		return CLASS_GUEST;
	
	return (*player->pows);
}


int get_pow(player, pow)
	object *player;
	ptype pow;
{
	ptype *pows;
	
	if(player == rootobj)
		return PW_YES;
	
	if(!(player->flags & TYPE_PLAYER))
		if(!(player->flags & INHERIT))
			return PW_NO;
	
	if(!player->pows)
		return PW_NO;
		
	pows = player->pows;
	
	if(!pows) 
		return PW_NO;
	
	if(player->flags & TYPE_PLAYER)
		pows++;
	
	for(;*pows;pows+=2)
		if(*pows==pow) {
			return pows[1];
		}
		
	return PW_NO;	
}

void del_pow(player, pow)
	object *player;
	ptype pow;
{
	ptype *x;
	
	x = player->pows;
	if(!x)
		return;
	if(player->flags & TYPE_PLAYER) x++;
	for(;*x;x+=2)
		if(*x==pow) {
			while(*x) {
				if(x[2]) {
					*x = x[2];
					x[1] = x[3];
					x+=2;
				} else {
					*x = x[2];
				}
			}
			return;
		}
}

int has_pow(player, who, power)
	object *player, *who;
	ptype power;
{
	ptype pows;
	
	if(player == rootobj)
		return 1;
	
	if((player->flags & GUEST))
		return 0;
	
	if(player->flags & INHERIT)
		player = player->ownit;
	
	pows = get_pow(player, power);
	
	if((pows == PW_YES) || 
		(who == NULL && (pows == PW_YESLT || pows == PW_YESEQ)) ||
		(pows == PW_YESLT && Level(who)<Level(player)) || 
		(pows == PW_YESEQ && Level(who)<=Level(player)))
			return 1;

	return 0;
}

void set_pow(player, power, val)
	object *player;
	ptype power;
	ptype val;
{
	ptype *x;
	int nlist;
	
	del_pow(player, power);
	if(val == PW_NO)
		return;
	
	if(!player->pows)
	{
		player->pows = malloc(sizeof(ptype) * 3);
		player->pows[0] = power;
	}
	
	nlist = 0;
	x = player->pows;
	if(player->flags & TYPE_PLAYER) x++,nlist++;
	for(;*x;x+=2,nlist+=2);
	bcopy(player->pows, powlist, nlist * sizeof(ptype));
	free(player->pows);
	x = powlist + nlist;
	*x = power;
	x++, nlist++;
	*x = val;
	x++, nlist++;
	*x = 0;
	nlist++;
	player->pows = malloc(nlist * sizeof(ptype));
	bcopy(powlist, player->pows, nlist*sizeof(ptype));
}


void get_powers(o, str)
	object *o;
	char *str;
{
	int pos = 0;
	
	
	for(;;) 
	{
	  if(!strchr(str,'/') || *str=='0') {
	  	powlist[pos++] = 0;
	  	o->pows = malloc(pos*sizeof(ptype));
	  	bcopy(powlist, o->pows, pos*sizeof(ptype));
	  	return;
	  }
	  if(isdigit(*str)) {
	  	powlist[pos++]=atoi(str);
	  } else {
	    switch(*str) 
	    {
	      case '<':
	        powlist[pos++] = PW_YESLT;
	        break;
	      case '=':
	        powlist[pos++] = PW_YESEQ;
	        break;
	      case 'y':
	      case 'Y':
	        powlist[pos++] = PW_YES;
	        break;
	      default:
	        powlist[pos++] = PW_NO;
	        break;
	    }
	  }
	  str = strchr(str, '/')+1;
	}
}

void put_powers(f, o)
	FILE *f;
	OBJECT *o;
{
	ptype *pows;
	
	if(!o->pows) {
		fputs("\n", f);
		return;
	}
	
	if(o->flags & TYPE_PLAYER) 
	{
		fprintf(f, "%d/", *(o->pows));
		pows = o->pows +1;
	} else {
		pows = o->pows;
	}
	
	for(;;)
		if(*pows) {
		  fprintf(f, "%d/", *(pows++));
		  switch(*(pows++)) {
		    case PW_YESLT: 
		    	fputc('<', f);
		    	break;
		    case PW_YESEQ:
		    	fputc('=', f);
		    	break;
		    case PW_YES:
		    	fputc('y', f);
		    	break;
		    default:
		    	fputc('.', f);
		    	fprintf(f, "%d", *(pows-1));
		    	break;
		    }
		    fputc('/', f);
		  } else {
		  	fputs("0\n", f);
		  	return;
		  }
}


// POWER FUNCTIONS
static char *classnames[]=
	{
		" ?", "Guest", "Citizen", "Guide", 
		"Builder", "Official", "Administrator", 
		"Director", NULL
	};

//static char *typenames[]=
//	{
//		"Room", "Thing", "Exit", "Player", NULL
//	};

int name_to_class(name)
	char *name;
{
	int k;
	
	for(k = 0; classnames[k];k++)
		if(!strcmpm(name, classnames[k]))
			return k;
	
	return 0;
}

char *class_to_name(class)
	int class;
{
	if(class >= NUM_CLASSES || class <=0) return NULL;
	
	return(classnames[class]);
}

int class_to_list_pos(class)
	int class;
{
	switch(class)
	{
		case CLASS_DIRECTOR:
			return 0;
		case CLASS_ADMINISTRATOR:
			return 1;
		case CLASS_OFFICIAL:
			return 2;
		case CLASS_BUILDER:
			return 3;
		case CLASS_GUIDE:
			return 4;
		case CLASS_CITIZEN:
			return 5;
		case CLASS_GUEST:
			return 6;
		default:
			return 5;
	}
}

