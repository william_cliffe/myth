/** fight.c $ havencrag (11/18/2004)

  - We must remember that TYPE_THINGs are destroyed upon combat death, whereas
    players will get transferred to the Netherworld.
  - !! Remember to add in special defense and attack extra in rounds to liven up
    the play. I mean, people _ARE_ coming here to read text!
  - We must remember to either disallow @destroying and @nuking while infight, or make
    both those functions remove all instances of fighting in the fight queue.  
  - Remember when fighting to make the flags, materials, affects, effects, etc. on items
    do something special.
  - !!! NOTE !!! Make a check on fights to see if the fight has been going on for awhile. If 
    it has and the blocks are high and hits very low we'll have to intervene :)
  - Remember to add other attributes and modifiers to balance out the fighting more.
  - Skill points need to buy extra attributes, hitpoints, etc.
  - __ADD IN THE FLEE CODE!__
**/
#include <stdlib.h> 
#include <string.h> 
#include <stdio.h> 
#include <time.h>
#include <math.h>

#include "db.h"
#include "net.h"
#include "externs.h"

FIGHTQUEUE *fightlist = NULL;
FIGHTQUEUE *fightlisttail = NULL;

/** MOVE THIS **/
#define BLOCK		(1 << 0)
#define HIT		(1 << 1)
#define HASSHIELD	(1 << 2)
#define NOSHIELD	(1 << 3)
#define HASWEAPON	(1 << 4)
#define NOWEAPON	(1 << 5)

/* Weapon Codes
  $p - his/her
  $w - weapon
  $! - shield
  $o - him/her
  $s - he/she
  
  For type, we could do stuff like:
  BLOCK - it's a block
  HIT - it's a hit.

    
*/
struct battle_list_t {
  char *attack;
  char *defend;
  int type;
} battle_list_text[]= {
  {"swings $p $w at you!", "blocks your swing with $p $!!", BLOCK|HASSHIELD|HASWEAPON},
  {"thrusts forward with $p $w, slicing through your torso!", "stumbles backwards as your $w slices through $p torso!", HIT|HASWEAPON},
  {"roars with fury as $s raises $p $w over $p head and smashes it down upon your skull!", 
  "drops to a knee as the powerful blow hits $o on the noggin!", HIT|HASWEAPON},
  {"brings $p $w to $p side and lunges, bringing $p $w in an arc towards you!", "rolls easily to one side, coming back up with $p $w ready!", BLOCK|HASSHIELD|HASWEAPON},
  {"hits you with $p fist.", "staggers backwards in pain as your fist connects solidly with $p chin.", HIT},
  {NULL}};


/** QueueFight():
**/
FIGHTQUEUE *QueueFight(object *player, object *target)
{
  FIGHTQUEUE *f;
  
  GENERATE(f, FIGHTQUEUE);
  f->attacker = player;
  f->target = target;
  f->started = time(0);
  f->nextround = 0;
  f->turn_attack = f->attacker;
  f->turn_defend = f->target;
  f->a_hits = f->a_blocks = f->t_hits = f->t_blocks = 0;
  f->a_weapon = f->a_armour = f->t_weapon = f->t_armour = 0;
  f->eventlist = f->eventtail = NULL;
  
  if(!fightlist) {
    f->next = f->prev = NULL;
    fightlist = fightlisttail = f;
  } else {
    f->next = NULL;
    f->prev = fightlisttail;
    fightlisttail->next = f;
    fightlisttail = f;
  }
  
  return(f);
}

/** RemoveFight():
**/
void RemoveFight(FIGHTQUEUE *f)
{
  if(f->next)
    f->next->prev = f->prev;
  
  if(f->prev)
    f->prev->next = f->next;
  
  if(fightlist == f)
    fightlist = f->next;
  
  if(fightlisttail == f)
    fightlisttail = f->prev;
  
  CheckFightEvents(f->attacker, f->target);
  
  block_free(f);
}

int CheckFighting(object *fighter, object *other)
{
  FIGHTQUEUE *f = NULL;
  
  for(f = fightlist; f; f=f->next)
    if((f->attacker == fighter && f->target != other) || (f->attacker != other && f->target == fighter)) {
      return TRUE;
    }
  
  fighter->combat->cflags &= ~FIGHT;
}

/** DoRoundKill():
**/
void DoRoundKill(object *killer, object *victim)
{
  FIGHTQUEUE *f = NULL;
  CONTENTS *exception = NULL;
  
  victim->combat->cflags &= ~FIGHT;
  victim->combat->cflags |= DEAD;
  add_exception(&exception, killer);
  add_exception(&exception, victim);
  notify(killer, pstr("You killed %s!", color_name(victim, -1)));
  notify(victim, pstr("%s killed you!", color_name(killer, -1)));
  notify_in(killer->location, exception, pstr("%s killed %s!", color_name(killer, -1), color_name(victim, -1)));
  clear_exception(exception);
  killer->combat->kills++;
  
  for(f = fightlist; f; f=f->next)
    if((f->attacker == killer && f->target != victim) || (f->target == killer && f->attacker != victim))
      break;
  
  if(f) 
    return;
  
  killer->combat->cflags &= ~FIGHT;
  
  if(killer->combat->mp < killer->combat->maxmp || killer->combat->hp < killer->combat->maxhp)
    NeedRegen(killer);
}

/** LevelUp():
**/
void LevelUp(object *thing, CSTATS *c)
{
  int skillpoints;
  
  c->level += 1;
  
  skillpoints = (5*(c->level * .2))*5;
  skillpoints += (c->exp / (c->exp / skillpoints));
  skillpoints += random() % skillpoints;
  c->skillpoints += skillpoints;
  
  notify(thing, pstr("You have risen to level %d!", c->level));
  notify(thing, pstr("You are awarded %d skillpoints!", skillpoints));
}

/** CheckLevel():
**/
void CheckLevel(object *thing)
{
  float cnt = 1;
  CSTATS *c = thing->combat;
  
  if(!c)
    return;
  //(((5x*2x)+x)/x^-1)-11)
  //(((5*cnt)*(2*cnt))/(cnt**-1))-11)
  for(;;) {
    if( (c->exp) > (((((float)(10*(cnt*cnt))+cnt)/(float)(1/(float)cnt))-(float)11) )) { /*( 5 * (cnt * .45) * (cnt * cnt))) {*/
      if(c->level < cnt)
        LevelUp(thing, c);
    } else {
      break;
    }
    cnt++;
  }
  
}

/** Levels():
**/
void Levels(object *player)
{
  CSTATS *c;
  int i;
  char cc = 'c';
  
  if(!(c=player->combat)) {
    notify(player, "You need to have combat stats.");
    return;
  }
  
  if( (i = c->level - 10) < 1)
    i = 1;
  
  notify(player, "^ccNearest ^ch20^cn^cc levels to you^ch^cw:");
  notify(player, "^ch------------------------");
  
  for(; i < (c->level + 10); i++) {
    if(c->level == i)
      cc = 'r';
    else
      cc = 'c';
    notify(player, pstr("^c%cLevel ^ch^cy%d ^cn^c%cat ^ch^cy%s ^cn^c%cexperience.",
      cc, i, cc, comma( (((float)(10*(i*i))+i)/(float)(1/(float)i))-(float)11 ), cc));
  }
}

void GiveExperience(FIGHTQUEUE *f, object *attacker, object *defender)
{
  CSTATS *a, *d;
  int a_blocks, a_hits, d_blocks, d_hits;
  int bonus, hitratio, exp;
  time_t fightlength = time(0) - f->started;
  
  a = attacker->combat;
  d = defender->combat;
  
  // Sort out the hit/block count.
  if(f->attacker == attacker) {
    a_blocks = f->a_blocks;
    a_hits = f->a_hits;
    d_blocks = f->t_blocks;
    d_hits = f->t_hits;
  } else {
    a_blocks = f->t_blocks;
    a_hits = f->t_hits;
    d_blocks = f->a_blocks;
    d_hits = f->a_hits;
  }

  if( d->exp < 1)
    d->exp = 1;
  if( a->exp < 1)
    a->exp = 1;

  exp = (d->exp * .0099) + (a_hits * .7) + 5;
  
  notify(attacker, pstr("You gain %d experience points.", exp));
  a->exp += exp;
  
  CheckLevel(attacker);
}


/** DoRoundDamage():
**/
int DoRoundDamage(object *attacker, object *defender, int attack, int defend)
{
  CONTENTS *exception = NULL;
  CSTATS *a, *d;
  int damage = attack - defend;
  float exp;
  
  add_exception(&exception, attacker);
  add_exception(&exception, defender);
  a = attacker->combat;
  d = defender->combat;
  notify(attacker, pstr("You inflict %d damage on %s!", damage, color_name(defender, -1)));
  notify(defender, pstr("%s inflicts %d damage on you!", color_name(attacker, -1), damage));
  notify_in(attacker->location, exception, pstr("%s inflicts %d damage on %s!", color_name(attacker, -1), damage, color_name(defender, -1)));
  clear_exception(exception);

  

  if( (d->hp -= damage) <= 0) {
    DoRoundKill(attacker, defender);
    return FALSE;
  }
  
  if( (exp = ((float)damage / (float)d->maxhp) * (float)(d->exp * .002)) < 1)
    exp = 1;
  
  a->exp += (int)exp;
  notify(attacker, pstr("* You gain %d experience from that hit.", (int)exp));
  
  return TRUE;
}

/** DoRound():
**/
int DoRound(FIGHTQUEUE *f)
{
  ITEMLIST *i;
  CONTENTS *exception = NULL;
  object *temp;			// Temporary holder for swapping attacker/defender.
  CSTATS *a, *d;		// a - attacker, d - defender
  int attack_roll, defend_roll;
  
  a = f->turn_attack->combat;
  d = f->turn_defend->combat;
  
  // Do some simple checks because of spells.
  if( (a->cflags & DEAD) || (d->cflags & DEAD) ) {
    CheckFighting(f->turn_attack, f->turn_defend);
    CheckFighting(f->turn_defend, f->turn_attack);
    return 0;
  }
  
  /** The attacking formula states STR + DEX + CON + STA 
      -vs- STR + DEX + CON + STA **/

  // REMEMBER: Before too long, calculate the attribute modifiers for any weapons
  attack_roll = a->str + a->dex + a->con + a->sta + a->lck;
  attack_roll += a->strmod + a->dexmod + a->conmod + a->stamod + a->lckmod;
  // REMEMBER: Before too long, calculate the attribute modifiers for any armour & shield
  defend_roll = d->str + d->dex + d->con + d->sta + d->lck;
  defend_roll += d->strmod + d->dexmod + d->conmod + d->stamod + d->lckmod;
  
  // Find the mean
  attack_roll /= 5;
  defend_roll /= 5;
  
  attack_roll = random() % attack_roll;
  defend_roll = random() % defend_roll;



  if(f->turn_attack == f->attacker) {
    attack_roll += f->a_weapon;
    defend_roll += f->t_armour;
  } else {
    attack_roll += f->t_weapon;
    defend_roll += f->a_armour;
  }
 
  if(Fatigue(a) > 0)
    attack_roll = attack_roll - (attack_roll * (Fatigue(a)/100));
  if(Fatigue(d) > 0)
    defend_roll = defend_roll - (defend_roll * (Fatigue(d)/100));

  if(attack_roll > defend_roll) {
    if(f->turn_attack == f->attacker)
      f->a_hits += 1;
    else
      f->t_hits += 1;
    
    if(!DoRoundDamage(f->turn_attack, f->turn_defend, attack_roll, defend_roll)) {
      GiveExperience(f, f->turn_attack, f->turn_defend);
      return FALSE;
    }
    
  
    f->nextround = time(0) + FIGHT_INTERVAL;
    temp = f->turn_attack;
    f->turn_attack = f->turn_defend;
    f->turn_defend = temp;
    return TRUE;
  }

  add_exception(&exception, f->turn_attack);
  add_exception(&exception, f->turn_defend);
  notify(f->turn_attack, pstr("%s blocked your attack!", color_name(f->turn_defend, -1)));
  notify(f->turn_defend, pstr("You block %s's attack!", color_name(f->turn_attack, -1)));
  notify_in(f->turn_attack->location, exception, pstr("%s blocks %s's attack!", color_name(f->turn_defend, -1),
    color_name(f->turn_attack, -1)));
  clear_exception(exception);


  if(f->turn_defend == f->target)
    f->t_blocks += 1;
  else
    f->a_blocks += 1;
    
  // Switch turns.
  f->nextround = time(0) + FIGHT_INTERVAL;
 
  temp = f->turn_attack;
  f->turn_attack = f->turn_defend;    
  f->turn_defend = temp;
  
  return TRUE;
}

/** FightAction():
**/
void FightAction()
{
  FIGHTQUEUE *f, *fnext = NULL;
  
  if(!fightlist)
    return;
  
  for(f = fightlist; f; f=f->next) {
    fnext = f->next;
    if(f->nextround <= time(0)) {
      if(!DoRound(f)) { // Fight is over.
        if( !(f->attacker->combat->cflags & DEAD) )
          NeedRegen(f->attacker);
        else if (!(f->target->combat->cflags & DEAD) )
          NeedRegen(f->target);
        // Here is where we destroy NPCs.
        RemoveFight(f);
      }
    }
  }
}

/** Fight():
**/
void Fight(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  ITEMLIST *i;
  CONTENTS *exception = NULL;
  object *target;
  CSTATS *a, *t, *r;
  CEVENT *event = NULL;
  FIGHTQUEUE *fight;
  
  if(!(target=match_partial_type(player->location, arg1, TYPE_THING|TYPE_PLAYER))) {
    notify(player, "You cannot fight that!");
    return;
  }
  
  if( (target->flags & TYPE_PLAYER) && !(target->flags & CONNECT)) {
    notify(player, "That player is not connected!");
    return;
  }
  
  if(target == player) {
    notify(player, "What did you expect to do? Kick yourself in the butt?");
    return;
  }
  
  if(target->location != player->location) {
    notify(player, "You cannot fight remotely!");
    return;
  }
  
  for(fight = fightlist; fight; fight=fight->next)
    if( ( (fight->attacker == player) && (fight->target == target) ) || ( (fight->attacker == target) && (fight->target == player) ) ) {
      notify(player, "You're already in combat with them!");
      return;
    }
  
  if(!(a = player->combat)) {
    notify(player, "Your combat stats are not set up.");
    return;
  }
  
  if(!(t=target->combat)) {
    notify(player, pstr("Combat stats for %s need to be set up first.", color_name(target, -1)));
    return;
  }

  if(!(r=player->location->combat)) {
    notify(player, "This room cannot accept combat.");
    return;
  }

  if( (a->cflags & DEAD) ) {
    notify(player, "You cannot fight while dead.");
    return;
  }
  
  if ((t->cflags & DEAD) ) {
    notify(player, "Your target is already dead.");
    return;  
  }

  /* Has this room been combat-enabled? */
  if( (r->cflags & NONCOMBATABLE)  || (player->location->flags & SHOP) ) {
    notify(player, "This is a safe room. No fighting allowed.");
    return;
  }
  
  // We shouldn't have to search the regen queue because all regeneration
  // should be flagged as INREGEN. All we need to do is check that.
  if( (a->cflags & INREGEN) )
    ReleaseFromRegen(player);
  
  if( (t->cflags & INREGEN) )
    ReleaseFromRegen(target);
  
  a->cflags |= FIGHT;
  t->cflags |= FIGHT;
 
  fight = QueueFight(player, target);
  fight->nextround += time(0) + FIGHT_INTERVAL;

  for(event = cevents; event; event=event->next) {
    if( (event->flags & EVENT_INFIGHT) && (event->type == DELIMODIFIER_EVENT) && ((event->thing == player) || (event->thing == target)) )
      event->num_fights++;
  }
  
  // Start up all the equipped item modifiers for fights.
  for(i = player->equipped; i; i=i->next) {
    if( (i->flags & WEAPON) && (i->equipped) )
      fight->a_weapon += i->value;
    else if( ((i->flags & ARMOUR) || (i->flags & SHIELD)) && (i->equipped) )
      fight->a_armour += i->value;
    
    if(!i->list)
      continue;

    if(StartItemEvent(player, i, DELIMODIFIER_EVENT))
      ParseItemModifiers(fight, player, i->list);
  }

  
  for(i = target->equipped; i; i=i->next) {
    if( (i->flags & WEAPON) && (i->equipped) )
      fight->t_weapon += i->value;
    else if( ((i->flags & ARMOUR) || (i->flags & SHIELD)) && (i->equipped) )
      fight->t_armour += i->value;
    if(!i->list)
      continue;

    if(StartItemEvent(target, i, DELIMODIFIER_EVENT))
      ParseItemModifiers(fight, target, i->list);
  }

  add_exception(&exception, player);
  add_exception(&exception, target);
  notify(player, pstr("You begin attacking %s.", color_name(target, -1)));  
  notify(target, pstr("%s begins attacking you.", color_name(player, -1)));
  notify_in(player->location, exception, pstr("%s begins attacking %s.", color_name(player, -1), color_name(target, -1))); 
  clear_exception(exception);
}


/** Flee(): You may only flee in compass directions.
**/
void Flee(object *player)
{
  FIGHTQUEUE *f, *fnext= NULL;
  CSTATS *c;
  char *dir[]={"n","s","e","w","ne","nw","se","sw"};
  char *exit;
  
  if(!(c = player->combat)) {
    notify(player, "You're not set up for combat.");
    return;
  }

  if( !(c->cflags & FIGHT)) {
    notify(player, "What are you fleeing from? Yourself?");
    return;
  }

  /** Check stats roll **/
  /** Flee has to be completely rewritten because we have to take into consideration
      the stats of the player -vs- who they are fighting. **/
      
  DEBUG(pstr("%f", Fatigue(c)));
  for(f = fightlist; f; f=f->next)
    if( f->attacker == player) {
      DEBUG(pstr("%f", Fatigue(f->target->combat)+1));
      DEBUG(pstr("Player: %f  Attacker: %f", ( (c->dex + c->lck + c->sta) - die_roll(6, 9) ) * (Fatigue(c)+1/100), ((f->target->combat->dex + f->target->combat->lck + f->target->combat->sta) - die_roll(6, 9)) * (float)(Fatigue(f->target->combat)+1/100)));
    } else if (f->target == player) {
      DEBUG(pstr("%f", Fatigue(f->attacker->combat)+1));
      DEBUG(pstr("Player: %f  Attacker: %f", ( (c->dex + c->lck + c->sta) - die_roll(6, 9)) * (Fatigue(c)+1/100), ((f->attacker->combat->dex + f->attacker->combat->lck + f->attacker->combat->sta) -  die_roll(6, 9)) * (float)(Fatigue(f->attacker->combat)+1/100)));
    }

  return;
  
  

  c->cflags |= FLEE;

  exit = dir[random() % 8];
  
  
  DEBUG(pstr("Exit: %s", exit));

  if( (match_pseudo_exit(player, exit)) == -1) {
   if(!match_real_exit(player, exit)) {
      notify(player, "You try to flee but cannot!");
      c->cflags &= ~FLEE;
      return;
    }
  }
  
  
  for(f = fightlist; f; f=fnext) {
    fnext = f->next;
    if(f->attacker == player || f->target == player)
      RemoveFight(f);
  }

  c->cflags &= ~FIGHT;
  c->cflags &= ~FLEE;
  
  notify(player, "Run away! Run away!!!");
}    
/** END OF FIGHT.C **/

