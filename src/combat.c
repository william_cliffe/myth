/** combat.c $ havencrag (10/27/2004)
  
  - Make it so that you can create races like you can create item templates:
    @create/race Race and @set/race Race=<arguments>
  - !!! We have to make sure in the future that all DELIMODIFIER events, and 
    rather, any attribute modifier combat events get reversed before a @reboot
    or @shutdown. 
**/

#include <stdlib.h> 
#include <string.h> 
#include <stdio.h> 
#include <time.h>

#include "db.h"
#include "net.h"
#include "externs.h"

RACETABLE *racetable = NULL;
CLASSTABLE *classtable = NULL;
ITEMLIST *itemlist = NULL;	// this is where all the item templates are loaded.
ITEMLIST *itemlisttail = NULL;
REGENQUEUE *regenlist = NULL;
REGENQUEUE *regenlisttail = NULL;

time_t combat_last_regen = 0;

//NAME,  KEY,          STR, INT, WIS, DEX, CON, CHA, STA, LCK, MP, HP, WEIGHT, HEIGHT  
RACETABLE racetable_list[]={
  {"Raumgar", "RAU",     3,   1,   1,  -3,   1,  -2,   2,  -1,  0,  5, 300.0, 144.0}, // 7 <--
  {"Fos", "FOS",        -2,   4,   1,   4,  -1,   3,  -2,   2,  2, -4, 30.0, 32.0},   // 7 <--
  {"Zorn", "ZOR",       -1,   2,  -2,   1,  -1,   4,   0,   2,  3, -1, 80.0, 60.0},   // 7 <--
  {"Dungatti", "DUN",    1,   2,   1,   0,   0,   1,   1,  -2,  2,  1, 80, 60.0},     // 7 <-- 
  {"Kalaan", "KAL",      3,   2,  -1,   1,   3,  -3,   2,  -2,  0,  3, 180, 86.0},    // 7 <--
  {"Daask Voon", "DAV", -2,   3,   1,   0,  -2,   3,   1,   0,  1,  2, 150.0, 68.0},  // 7 <--
  {NULL}};

CLASSTABLE classtable_list[]={
//NAME,  KEY,           STR, INT, WIS, DEX, CON, CHA, STA, LCK, MP, HP, WEIGHT, HEIGHT
        /** Magick Users **/
  {"Mage",        "Mag",  0,   1,   1,   0,   0,   1,   0,   1,  1,  0},  // 5
  {"Cleric",      "Cle", -1,   1,   1,   0,   0,   0,   2,   0,  2,  0},  // 5
  {"Warlock",     "War",  0,   2,   2,   0,  -1,   1,  -1,  -1,  4, -1},  // 5
  {"Necromancer", "Nec",  0,   3,   2,   0,  -3,  -2,   1,   0,  3,  1},  // 5
  {"Psionicist",  "Psi",  0,   2,   1,   0,   0,   1,  -1,   1,  2, -1},  // 5
        /** Fighters **/ 
  {"Barbarian", "Bar",    2,  -2,  -1,   2,   0,   0,   1,   0,  0,  3},  // 5
  {"Knight", "Kni",       2,   1,   1,  -1,   2,   2,  -1,  -1,  0,  0},  // 5
  {"Ninja", "Nin",        0,   1,   1,   2,   0,   1,   0,   0,  0,  0},  // 5
  {"Mercenary", "Mrc",    2,   1,  -1,   0,   0,   1,   2,  -1,  0,  1},  // 5
  {"Fighter", "Fig",      1,   1,   0,   1,   1,   0,   1,   0,  0,  0},  // 5
        /** Adventurers **/
  {"Historian", "His",   -1,   2,   3,   0,   1,   0,   0,   0,  0,  0},  // 5
  {"Merchant", "Mer",     0,   2,   1,   0,   0,   2,   0,   0,  0,  0},  // 5
  {"Thief", "Thi",       -1,   2,   2,   2,  -2,   2,   0,   0,  0,  0},  // 5
  {"Politician", "Pol",   0,   3,   1,   0,   0,   3,   0,  -2,  0,  0},  // 5
  {"Swashbuckler", "Swa", 1,   0,   0,   1,   1,   2,   0,   0,  0,  0},  // 5
  {NULL}};


// Set up racetable
RACETABLE *add_raceclass()
{
  RACETABLE *rnew;
  
  GENERATE(rnew, RACETABLE);
  rnew->name = NULL;
  rnew->key = NULL;
  
  return(rnew);
}

void setup_racetable()
{
  RACETABLE *r;
  int i;
  
  for(i = 0; racetable_list[i].name; i++) {
    r = add_raceclass();
    SET(r->name, racetable_list[i].name);
    SET(r->key, racetable_list[i].key);
    r->str = racetable_list[i].str;
    r->intel = racetable_list[i].intel;
    r->wis = racetable_list[i].wis;
    r->dex = racetable_list[i].dex;
    r->con = racetable_list[i].con;
    r->cha = racetable_list[i].cha;
    r->sta = racetable_list[i].sta;
    r->lck = racetable_list[i].lck;
    r->mp = racetable_list[i].mp;
    r->hp = racetable_list[i].hp;
    r->weight = racetable_list[i].weight;
    r->height = racetable_list[i].height;
    r->next = racetable;
    racetable = r;
  }
}

// Set up classtable
void setup_classtable()
{
  RACETABLE *r;
  int i;
  
  for(i = 0; classtable_list[i].name; i++) {
    r = add_raceclass();
    SET(r->name, classtable_list[i].name);
    SET(r->key, classtable_list[i].key);
    r->str = classtable_list[i].str;
    r->intel = classtable_list[i].intel;
    r->wis = classtable_list[i].wis;
    r->dex = classtable_list[i].dex;
    r->con = classtable_list[i].con;
    r->cha = classtable_list[i].cha;
    r->sta = classtable_list[i].sta;
    r->lck = classtable_list[i].lck;
    r->mp = classtable_list[i].mp;
    r->hp = classtable_list[i].hp;
    r->weight = classtable_list[i].weight;
    r->height = classtable_list[i].height;
    r->next = classtable;
    classtable = r;
  }
}

// All this does is return TRUE if the player has been
// set up for combat, FALSE if not.
int check_combat(object *player)
{
  if(*atr_get(player, A_RACE) && *atr_get(player, A_CLASS))
    return TRUE;
  
  return FALSE;     
}

int rroll(int num)
{
  return( (1 + (random() % ((num) ? num : 6))));
}

unsigned int die_roll(int die, int num)
{
  unsigned int total;
 
  if(die <= 0 || num <= 0)
    return 0;
   
  if(die < 2 || num < 1)
    return(die);
  
  while( (total = (random() % (die * num)) + 1) < num);  
  
  return(total);
}


// Do not let attributes roll below 6.
int attribute_roll(int num, int modifier)
{
  int ret;
  
  while((ret = die_roll(6, 4) + modifier) < 6);
  
  return(ret);
}

/***** When I code the height+weight+carry function, these are the formulas...

*****/

/** Fatigue():
**/
float Fatigue(CSTATS *c)
{
  float ret;
  
  if(c->hp <= 0)
    return (float)100;
  
  ret = (float)c->hp / c->maxhp;
  ret *= (float) 100;
  ret = (float) 100 - ret;
  
  return(ret);
}

char *ShowModifier(int modifier)
{
  char buf[512];
  
  sprintf(buf, "^ch^cm(^cy%c^cg%d^cm)", ((modifier < 0) ? '-' : (modifier > 0) ? '+' : ' '), ((modifier >= 0) ? modifier : modifier * -1));
  
  return stralloc(buf);
}


// SCORE TALLY SHEET
void do_score(player, arg1)
  object *player;
  char *arg1;
{
  CSTATS *c;
  object *who;
  
  if(!*arg1)
    who = player;
  else {
    if(!(who = remote_match(player, arg1))) {
      notify(player, "I'm not sure who you're referring to!");
      return;
    }
  }
  
  if( !(who->flags & TYPE_THING) && !(who->flags & TYPE_PLAYER) ) {
    notify(player, "That does not have a score.");
    return;
  }
  
  if(player != who && !has_pow(player, who, POW_SEEATR)) {
    notify(player, "You cannot see that.");
    return;
  }
 
  if(!who->combat) {
    notify(player, "Sorry, but their combat stats are not set up.");
    return;
  }
 
/***
  /----------------------------------------------------------------------\
  | sarukie   (Daask Voon) | Untrained Mage         |           Level: 1 |
  |----------------------------------------------------------------------|
  | Experience:     1,123,492 | Skill Points:      3,005 | Fatigue:  32% |
  |----------------------------------------------------------------------| 
  | Hit Points:    235/400 | Magic Points:    300/599 | Kills:    30,005 |
  |----------------------------------------------------------------------|
  | Strength      (12): _______              [ 38%] ( 0)  |              |
  | Dexterity     (19): ___________          [ 59%] ( 0)  |              |
  | Will Power    (14): ________             [ 44%] ( 0)  |              |
  | Intelligence  (23): ______________       [ 72%] ( 0)  |              |
  | Wisdom        (08): _____                [ 25%] ( 0)  |              |
  | Stamina       (18): ___________          [ 56%] ( 0)  |              |
  | Charisma      (10): ______               [ 31%] ( 0)  |              |
  | Luck          (08): _____                [ 25%] ( 0)  |              |
  |----------------------------------------------------------------------|
  | Height:  9.08ft | Weight:  412lbs | Carry Status:   412lbs of 505lbs |    
  |----------------------------------------------------------------------|
  | You have completed 10 of 30 quests.  | Quest Points:    232 | ****** |
  \----------------------------------------------------------------------/  
***/






  c = who->combat;  
  notify(player, pstr("^ch^cw/%s\\", make_ln("-", 70)));
  notify(player, pstr(" %s ^ch^cw(^cy%s^cw) - ^cr%s ^cb%s            ^crExp^cw: ^cg%ld  ^cn^ccLevel^ch^cw: ^cy%d", 
    atr_get(who, A_RAINBOW), atr_get(who, A_RACE), (*atr_get(who, A_TITLE)) ? atr_get(who, A_TITLE) : "Untrained",
      atr_get(who, A_CLASS), c->exp, c->level));
  notify(player, pstr(" ^ch^cc%s ", make_ln("=", 70)));
  notify(player, pstr(" ^ch^cgHP^cw: ^cy%d^cw/^cr%d     ^cgMP^cw: ^cy%d^cw/^cr%d     ^cgFatigue: ^cy%1.0f^cr%% %s", 
    c->hp, c->maxhp, c->mp, c->maxmp, Fatigue(c), ((c->cflags & DEAD)) ? "^ch^cw(^crDEAD^cw)^cn" : "")); 
  notify(player, pstr(" ^ch^cw%s ", make_ln("-", 70))); 

  if(!(player->flags & ANSI)) { // Original score code.
    notify(player, pstr(" STR: %d (%d)     INT: %d (%d)    WIS: %d (%d)    DEX: %d (%d)",
      c->str, c->strmod, c->intel, c->intmod, c->wis, c->wismod, c->dex, c->dexmod));
    notify(player, pstr(" CON: %d (%d)     CHA: %d (%d)    STA: %d (%d)    LCK: %d (%d)\n",
      c->con, c->conmod, c->cha, c->chamod, c->sta, c->stamod, c->lck, c->lckmod));
  } else {
    notify(player, pstr(" ^ccStrength      ^ch^cy(^cr%-2.2d^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s", 
      c->str, format_color(pstr("^cR%s^cn", make_ln("_", (((float)c->str/32)*20))),20), ((float)c->str/32)*100, ShowModifier(c->strmod)));
    notify(player, pstr(" ^ccDexterity     ^ch^cy(^cr%.2d^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s", 
      c->dex, format_color(pstr("^cG%s^cn", make_ln("_", (((float)c->dex/32)*20))),20), ((float)c->dex/32)*100, ShowModifier(c->dexmod)));
    notify(player, pstr(" ^ccWill Power    ^ch^cy(^cr%.2d^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s",
      c->con, format_color(pstr("^cB%s^cn", make_ln("_", (((float)c->con/32)*20))), 20), ((float)c->con/32)*100, ShowModifier(c->conmod)));
    notify(player, pstr(" ^ccIntelligence  ^ch^cy(^cr%.2d^ch^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s",
      c->intel, format_color(pstr("^cM%s^cn", make_ln("_", (((float)c->intel/32)*20))), 20), ((float)c->intel/32)*100, ShowModifier(c->intmod)));
    notify(player, pstr(" ^ccWisdom        ^ch^cy(^cr%.2d^ch^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s",
      c->wis, format_color(pstr("^cC%s^cn", make_ln("_", (((float)c->wis/32)*20))), 20), ((float)c->wis/32)*100, ShowModifier(c->wismod)));
    notify(player, pstr(" ^ccStamina       ^ch^cy(^cr%.2d^ch^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s",
      c->sta, format_color(pstr("^cY%s^cn", make_ln("_", (((float)c->sta/32)*20))), 20), ((float)c->sta/32)*100, ShowModifier(c->stamod)));
    notify(player, pstr(" ^ccCharisma      ^ch^cy(^cr%.2d^ch^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s",
      c->cha, format_color(pstr("^cW%s^cn", make_ln("_", (((float)c->cha/32)*20))), 20), ((float)c->cha/32)*100, ShowModifier(c->chamod)));
    notify(player, pstr(" ^ccLuck          ^ch^cy(^cr%.2d^ch^cy)^ch^cw: %s ^ch^cw[^cy%3.0f%%^cw] %s\n",
      c->lck, format_color(pstr("^cG%s^cn", make_ln("_", (((float)c->lck/32)*20))), 20), ((float)c->lck/32)*100, ShowModifier(c->lckmod)));
  }
  
  notify(player, pstr(" ^ccYou have a total of ^ch^cy%d ^cn^ccskillpoints.   You have made ^ch^cy%d ^cn^cckill%s.", c->skillpoints, c->kills, (c->kills == 1) ? "" : "s"));
  notify(player, pstr(" ^ccYou're ^ch%1.0f ^cn^ccfeet tall^cy, ^cn^ccweigh ^ch%1.0f^cn^cclbs, and can carry ^ch^cy%1.0f^cn^cclbs.",
    c->height/12, c->weight, c->carry));
  notify(player, pstr("^cw^ch\\%s/", make_ln("-", 70)));
}

void do_listrace(player)
  object *player;
{
  RACETABLE *r;
  
  for(r = racetable; r; r=r->next)
    notify(player, pstr("%-14.14s", r->name));
}

/** CheckRace():
**/
RACETABLE *CheckRace(object *target)
{
  RACETABLE *r = NULL;
  char *race;
  
  if(!*(race = atr_get(target, A_RACE))) 
    return(NULL);
    
  for(r = racetable; r; r=r->next)
    if(!strcmpm(r->name, race))
      break;
  
  return(r);
}

/** CheckClass():
**/
CLASSTABLE *CheckClass(object *target)
{
  CLASSTABLE *c = NULL;
  char *clas;
  
  if(!*(clas = atr_get(target, A_CLASS)))
    return(NULL);
  
  for(c = classtable; c; c=c->next)
    if(!strcmpm(c->name, clas))
      break;
  
  return(c);
}

/** RollStats():
**/
void RollStats(object *player, object *target)
{
  CSTATS *stats;
  RACETABLE *r;
  CLASSTABLE *c;
  
  if( (target->flags & TYPE_ROOM) || (target->flags & TYPE_EXIT) ) {
    notify(player, "You cannot roll combat stats on that!");
    return;
  }
  
  // We check to make sure the race and class are properly set.
  if(!(r = CheckRace(target))) {
    notify(player, pstr("%s has an invalid race.", color_name(target, -1)));
    return;
  }
  
  if(!(c = CheckClass(target))) {
    notify(player, pstr("%s has an invalid class.", color_name(target, -1)));
    return;
  }
  
  if( !(stats = target->combat) )  {
    GENERATE(target->combat, CSTATS);
    stats = target->combat;
    stats->cflags = 0;
  }
  
  /* Zero out all modifiers */
  stats->strmod = stats->intmod = stats->dexmod = stats->wismod = stats->dexmod = 0;
  stats->chamod = stats->lckmod = stats->conmod = 0;
  stats->level = 0;
  stats->exp = 1;
  stats->str = attribute_roll(0, r->str + c->str);
  stats->intel = attribute_roll(0, r->intel + c->intel);
  stats->dex = attribute_roll(0, r->dex + c->dex);
  stats->wis = attribute_roll(0, r->wis + c->wis);
  stats->con = attribute_roll(0, r->con + c->con);
  stats->cha = attribute_roll(0, r->cha + c->cha);
  stats->lck = attribute_roll(0, r->lck + c->lck);
  stats->sta = attribute_roll(0, r->sta + c->sta);
  stats->maxmp = attribute_roll(0, r->mp + c->mp);
  stats->maxmp += die_roll(stats->wis, 1);
  stats->maxhp = attribute_roll(0, r->hp + c->hp);
  stats->maxhp += die_roll( (stats->str + stats->dex)/2, 1);
  stats->mp = stats->maxmp;
  stats->hp = stats->maxhp;

  stats->height = (float)r->height + rroll(r->height * .29);
  stats->weight = (float)((stats->height/12) * 25) + rroll(r->weight * .49);
  stats->carry = r->weight * ( ((stats->str * 1.69)/100) + 1);
  notify(player, pstr("Stats for %s set.", color_name(target, -1)));
  notify(target, pstr("Your stats have been reset by %s.", color_name(player, -1)));    
}



/** CombatSetUndead():
**/
void CombatSetUndead(player, target)
  object *player, *target;
{
  if(!target->combat) {
    notify(player, "They cannot be set undead. They don't have any combat stats.");
    return;
  }
  
  target->combat->cflags &= ~DEAD;
  target->combat->mp = target->combat->maxmp;
  target->combat->hp = target->combat->maxhp;
}

/** CombatRoomSetup():
**/
void CombatRoomSetup(object *player, object *target)
{
  CSTATS *stats;
  
  if(!(target->flags & TYPE_ROOM)) {
    notify(player, "You can only set up rooms with this command.");
    return;
  }
  
  if(!(stats = target->combat)) {
    GENERATE(target->combat, CSTATS);
    stats = target->combat;
  }
  
  notify(player, pstr("%s set up.", color_name(target, -1)));
}

/** ModifyCombat():
**/
void ModifyCombat(object *player, char *arg1)
{

}


/** SetCombat(): 

    Proper syntax is: @set/combat <object>=<argument>:<data>
    (OR)............: @set/combat COMBAT=<argument>:<data>
    -- The colon is not always necessary.
    
    SetCombat will check for an ALL UPPERCASE 'COMBAT' first argument.
    It will then check for objects and stuff if it doesn't match to: COMBAT
**/
void SetCombat(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  object *target;
  char *data, *value = NULL;
  
  if(!*arg1 || !*arg2) {
    notify(player, "Not enough information to proceed.");
    return;
  }
  
  if(!has_pow(player, player, POW_COMBAT)) {
    notify(player, "You do not have the necessary powers to manipulate combat.");
    return;
  }
  
  if(!strcmp(arg1, "COMBAT")) {
    ModifyCombat(player, arg2);
    return;
  }
  
  if(!(target = remote_match(player, arg1))) {
    notify(player, "I'm not sure what you want to modify.");
    return;
  }
  
  // Separate our arguments.
  if(strchr(arg2, ':')) {
    data = fparse(arg2, ':');
    value = rparse(arg2, ':');
  } else {
    data = arg2;
  }
  
  
  if(string_prefix(data, "roll")) {
    RollStats(player, target);
  } else if (string_prefix(data, "room")) {
    CombatRoomSetup(player, target);
  } else if (string_prefix(data, "undead")) {
    CombatSetUndead(player, target);
  }
  
  
}

/** NeedRegen()
**/
void NeedRegen(object *player)
{
  REGENQUEUE *r;
  CSTATS *c;
  
  if(!(c = player->combat))
    return;
  
  if((c->cflags & INREGEN))
    return;

  if((c->cflags & FIGHT))
    return;
    
  // Make sure they REALLY NEED regen.  
  if(c->hp >= c->maxhp && c->mp >= c->maxmp)
    return;
  
  c->cflags |= INREGEN;
  
  GENERATE(r, REGENQUEUE);
  r->player = player;
  
  if(!regenlist) {
    r->next = r->prev = NULL;
    regenlist = regenlisttail = r;
  } else {
    r->next = NULL;
    r->prev = regenlisttail;
    regenlisttail->next = r;
    regenlisttail = r;
  }
}

/** DoneRegen():
**/
void DoneRegen(REGENQUEUE *r)
{
  if(r->next)
    r->next->prev = r->prev;
  
  if(r->prev)
    r->prev->next = r->next;
  
  if(regenlist == r)
    regenlist = r->next;
  
  if(regenlisttail == r)
    regenlisttail = r->prev;
  
  r->player->combat->cflags &= ~INREGEN;
  block_free(r);
}

/** ReleaseFromRegen():
**/
void ReleaseFromRegen(object *target)
{
  REGENQUEUE *r, *rnext = NULL;
  
  for(r = regenlist; r; r=rnext) {
    rnext = r->next;
    if(r->player == target)
      DoneRegen(r);
  }
}

/** CombatRegen():
**/
void CombatRegen()
{
  REGENQUEUE *r, *rnext = NULL;
  CSTATS *c;
  
  if(!regenlist)
    return;
  
  for(r = regenlist; r; r=rnext) {
    rnext = r->next;
    c = r->player->combat;
    
    if(c->hp >= c->maxhp && c->mp >= c->maxmp) {
      notify(r->player, "^ch^cy* ^cnAll hit and magic points have been regenerated.");
      DoneRegen(r);
      continue;
    }
      
    if(c->mp < c->maxmp) {
      c->mp += 1; // Later we'll do a percentage increase
    }
    if(c->hp < c->maxhp)
      c->hp += 1;
  }
  
  combat_last_regen = time(0);  
}

/** CombatTimer():
**/
void CombatTimer()
{
  if(!regenlist)
    return;
  if(regenlist) {
    if(combat_last_regen + REGEN_INTERVAL <= time(0)) {
      CombatRegen();
    }
  }
  
}


/** CLEAN UP COMBAT **/

/** CleanUpCombat():
**/
void CleanUpCombat()
{
  CEVENT *e, *enext = NULL;
  
  for(e = cevents; e; e=enext) {
    enext = e->next;
    ParseEvent(e);
  }

        
}

/*** COMBAT DATABASE LOAD/SAVE ROUTINES ***/
/** Data is stored on object attributes. **/

// Load a combat object.
// Loads only those that are combatable.
void LOAD_COMBAT_OBJECT(OBJECT *thing)
{
  CSTATS *c;

  GENERATE(c, CSTATS);  
  c->level = atoi(atr_get(thing, A_LEVEL));
  c->str = atoi(atr_get(thing, A_STRENGTH));
  c->intel = atoi(atr_get(thing, A_INTELLIGENCE));
  c->dex = atoi(atr_get(thing, A_DEXTERITY));
  c->wis = atoi(atr_get(thing, A_WISDOM));
  c->con = atoi(atr_get(thing, A_CONSTITUTION));
  c->cha = atoi(atr_get(thing, A_CHARISMA));
  c->sta = atoi(atr_get(thing, A_STAMINA));
  c->lck = atoi(atr_get(thing, A_LUCK));
  c->mp = atoi(atr_get(thing, A_MP));
  c->hp = atoi(atr_get(thing, A_HP));
  c->maxmp = atoi(atr_get(thing, A_MAXMP));
  c->maxhp = atoi(atr_get(thing, A_MAXHP));
  c->weight = atof(atr_get(thing, A_WEIGHT));
  c->height = atof(atr_get(thing, A_HEIGHT));
  c->carry = atof(atr_get(thing, A_CARRY));
  c->strmod = atoi(atr_get(thing, A_STRMOD));
  c->dexmod = atoi(atr_get(thing, A_DEXMOD));
  c->intmod = atoi(atr_get(thing, A_INTMOD));
  c->wismod = atoi(atr_get(thing, A_WISMOD));
  c->chamod = atoi(atr_get(thing, A_CHAMOD));
  c->stamod = atoi(atr_get(thing, A_STAMOD));
  c->lckmod = atoi(atr_get(thing, A_LCKMOD));
  c->exp = atol(atr_get(thing, A_EXP));
  c->cflags = atoll(atr_get(thing, A_CFLAGS));
  c->skillpoints = atoi(atr_get(thing, A_SKILLPOINTS));
  c->kills = atoi(atr_get(thing, A_KILLS));

  thing->combat = c;

  if((thing->combat->cflags & FIGHT))
    thing->combat->cflags &= ~FIGHT;
  
  if((thing->flags & TYPE_PLAYER) || (thing->flags & TYPE_THING) ) {
    if( ( (c->hp < c->maxhp || c->mp < c->maxmp) || (c->cflags & INREGEN) ) && !(c->cflags & DEAD)) {
      thing->combat->cflags &= ~INREGEN; // let NeedRegen reset this.
      NeedRegen(thing);
    }
  }
  
}

void SAVE_COMBAT_OBJECT(object *thing)
{
  CSTATS *c;
  
  // So far, exits do not need ANY combat attributes.
  if( (thing->flags & TYPE_EXIT) )
    return;
  
  if(!thing->combat)
    return;

  c = thing->combat;
  if((thing->flags & TYPE_THING)||(thing->flags & TYPE_PLAYER))
    if(!check_combat(thing))
      return;

  // All we need to save for rooms (ATM) is the combat flags
  if( (thing->flags & TYPE_ROOM) ) {
    SET_ATR(thing, A_CFLAGS, pstr("%llu", c->cflags));
    return;
  }
  
  SET_ATR(thing, A_LEVEL, pstr("%d", c->level));
  SET_ATR(thing, A_STRENGTH, pstr("%d", c->str));
  SET_ATR(thing, A_INTELLIGENCE, pstr("%d", c->intel));
  SET_ATR(thing, A_DEXTERITY, pstr("%d", c->dex));
  SET_ATR(thing, A_WISDOM, pstr("%d", c->wis));
  SET_ATR(thing, A_CONSTITUTION, pstr("%d", c->con));
  SET_ATR(thing, A_CHARISMA, pstr("%d", c->cha));
  SET_ATR(thing, A_STAMINA, pstr("%d", c->sta));
  SET_ATR(thing, A_LUCK, pstr("%d", c->lck));
  SET_ATR(thing, A_MP, pstr("%d", c->mp));
  SET_ATR(thing, A_HP, pstr("%d", c->hp));
  SET_ATR(thing, A_MAXMP, pstr("%d", c->maxmp));
  SET_ATR(thing, A_MAXHP, pstr("%d", c->maxhp));
  SET_ATR(thing, A_WEIGHT, pstr("%f", c->weight));
  SET_ATR(thing, A_HEIGHT, pstr("%f", c->height));
  SET_ATR(thing, A_CARRY, pstr("%f", c->carry));
  SET_ATR(thing, A_STRMOD, pstr("%d", c->strmod));
  SET_ATR(thing, A_DEXMOD, pstr("%d", c->dexmod));
  SET_ATR(thing, A_INTMOD, pstr("%d", c->intmod));
  SET_ATR(thing, A_WISMOD, pstr("%d", c->wismod));
  SET_ATR(thing, A_CHAMOD, pstr("%d", c->chamod));
  SET_ATR(thing, A_STAMOD, pstr("%d", c->stamod));
  SET_ATR(thing, A_LCKMOD, pstr("%d", c->lckmod));
  SET_ATR(thing, A_EXP, pstr("%ld", c->exp));
  SET_ATR(thing, A_CFLAGS, pstr("%lld", c->cflags));
  SET_ATR(thing, A_SKILLPOINTS, pstr("%d", c->skillpoints));
  SET_ATR(thing, A_KILLS, pstr("%d", c->kills));
}

void do_combat_load() 
{
  object *o;
  int i;
  
  for(i = 0; i < dbtop; i++) {
    if(!(o = (db[i])))
      continue;
    if( ((o->flags & TYPE_PLAYER) || (o->flags & TYPE_THING)) && !check_combat(o))
      continue;
    else
      LOAD_COMBAT_OBJECT(o);
  }
}

 
/** END OF COMBAT.C **/

