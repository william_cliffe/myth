/****************************************************************************
 MYTH - List System
 Coded by Saruk (12/01/06)
 ---
 List Stuff
 ****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <netdb.h>

#include "db.h"
#include "net.h"
#include "externs.h"

void do_list(object *player, char *arg1, char *arg2)
{

  if(string_prefix(arg1, "creatures")) {
    CreatureList(player, arg2);
  } 
}

