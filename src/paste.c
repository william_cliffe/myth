/****************************************************************************
 MYTH - Paste/Edit utilities.
 Coded by Saruk (01/29/99)
 ---
 Crappy paste functions.
 ****************************************************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

void add_paste(d, buffer)
	DESCRIPTOR *d;
	char *buffer;
{
	TQUEUE *t, *tnew, *tprev = NULL;
		
	for(t=d->paste_queue; t; t=t->next)
		tprev = t;
	
	GENERATE(tnew, TQUEUE);
	tnew->buffer = NULL;
	SET(tnew->buffer, buffer);
	tnew->next = NULL;
	
	if(!tprev)
		d->paste_queue = tnew;
	else
		tprev->next = tnew;	
}

int HEADER_PASTE(d, tag)
	DESCRIPTOR *d;
	int tag;
{
	object *o;
	char player_name[MAX_BUFSIZE];
	
	
	strcpy(player_name, color_name(d->player, -1));
	if(!strcmpm(fparse(d->paste_cmd,'='), "channel"))
	  return error_report(d->player, ChannelPasteHeader(d->player, rparse(d->paste_cmd, '='), tag));
	else if (*d->paste_cmd == '+')
	  return error_report(d->player, ChannelPasteHeader(d->player, d->paste_cmd+1, tag));
	else if (!strcmpm(d->paste_cmd, "me")) {
		if(tag)
			notify(d->player, "--- Personal Paste Buffer ---");
		else
			notify(d->player, "--- End of Personal Paste Buffer ---");
		return TRUE;
	} else if (!strcmpm(d->paste_cmd, "here")) {
		if(tag)
			notify_in(d->player->location, NULL, pstr("--- Paste Buffer From %s in %s ---",
				player_name, color_name(d->player->location, -1)));
		else
			notify_in(d->player->location, NULL, pstr("--- End of Paste Buffer From %s in %s ---",
				player_name, color_name(d->player->location, -1)));
		return TRUE;
	} else if (!strcmpm(d->paste_cmd, "command")) {
		if(tag)
			notify(d->player, "--- Sending Commands From Buffer ---");
		else
			notify(d->player, "--- End of Commands From Buffer ---");
		return TRUE;
	} else {
		if(!(o = match_player(d->paste_cmd)))
			return FALSE;
		if(tag) 
			notify(o, pstr("--- Paste Buffer From %s ---",
				player_name));
		else {
			notify(o, pstr("--- End of Paste Buffer From %s ---",
				player_name));
			notify(d->player, pstr("[PASTE]: Paste sent to %s",
				color_name(o, -1)));
		}
		return TRUE;
	}
}

void paste_buffer(d)
	DESCRIPTOR *d;
{
	object *o;
	TQUEUE *t;
	
	if(!d->paste_queue) {
		dwrite(d, "[PASTE]: Empty paste buffer.");
		return;
	}
	
	if(!HEADER_PASTE(d, TRUE))
		return;
	
	for(t = d->paste_queue; t; t = t->next) {
	  if (!strcmpm(fparse(d->paste_cmd,'='), "channel"))
	    ChannelPaste(d->player, rparse(d->paste_cmd, '='), t->buffer);
	  else if (*d->paste_cmd == '+')
	    ChannelPaste(d->player, d->paste_cmd  + 1, t->buffer);
			else if (!strcmpm(d->paste_cmd, "me"))
				notify(d->player, t->buffer);
			else if (!strcmpm(d->paste_cmd, "here"))
				notify_in(d->player->location, NULL, t->buffer);
			else if (!strcmpm(d->paste_cmd, "command")) {
				/* Make sure it doesn't call any commands that may require added input. */
				if(strcmpm(front(t->buffer), "@paste") )
					filter_command(d->player, t->buffer, 1);
			} else {
				if(!(o = match_player(d->paste_cmd)))
					break;	
				notify(o, t->buffer);
			}
		}
		
	d->paste_queue = TQUEUE_FREE(d->paste_queue);
	HEADER_PASTE(d, FALSE);
	
}

void do_paste(d, buffer)
	DESCRIPTOR *d;
	char *buffer;
{
	if (!strcmpm(buffer, ".abort")) {
		d->paste_queue = TQUEUE_FREE(d->paste_queue);
		if(d->paste_cmd)
			block_free(d->paste_cmd);
		d->state = CONNECTED;
		process_output(d);
		dwrite(d, "[PASTE]: PASTE aborted.\n");
	} else if(!strcmp(buffer, ".")) {
		paste_buffer(d);
		if(d->paste_cmd)
			block_free(d->paste_cmd);
		d->state = CONNECTED;
		process_output(d);
	} else { 
		add_paste(d, buffer);
	}
}

char *paste_parse(arg1)
	char *arg1;
{
	object *o;
	// if no argument, assume personal.
	if(!*arg1)
		return "here";
	
	if(!strcmpm(arg1, "me") || !strcmpm(arg1, "here"))
		return arg1;
	else if (!strcmpm(fparse(arg1, '='), "channel"))
		return arg1;
	else if (!strcmpm(arg1, "command"))
		return arg1;
	else if (*arg1 == '+') {
	  if(FindChannel(arg1+1))
	    return arg1;
	  else
	    return "";
	} else {
		if(!(o = match_player(arg1)))
			return "";
		else
			return o->name;
	}
}

void start_paste(player, arg1)
	object *player;
	char *arg1;
{
	DESCRIPTOR *d;
	char *cmd;
	
	for(d=descriptor_list; d; d=d->next)
	  if(d->state == CONNECTED && d->player)
	    if(d->player == player)	
	      break;

	if(!d)
		return;
	
	cmd = paste_parse(arg1);	
	IS_ARGUMENT(player, (cmd));

	notify(player, "[PASTE]: Ready to paste... (. saves, .abort aborts)");			
	process_output(d);
	d->state = PASTE_MODE;
	d->paste_cmd = NULL;
	SET(d->paste_cmd, cmd);
}

