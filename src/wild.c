/****************************************************************************
 MYTH - Wildcard functions
 Coded by Jagged (01/14/01)
 ---
 Much of the wildcard matching routine is trial and error, but the basic gist
 is taken from several flavours of online M*s.
 ****************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <math.h>

#include "db.h"
#include "net.h"
#include "externs.h"

#include "trust.h"

#define LONG_LONG_OK
/* These are currently only used to store variables until they're stored in the queue's own
variables */
char *gvar[NUMARGS];
char *tvar[NUMARGS];

int arg = 0;

void (*math_twice)(void) = math_twice1;

 void
  set_args()
{
	int i; // index counter
	
	for(i = 0; i < NUMARGS; i++) {
	  gvar[i] = NULL;
	}
	
}


// Attempt to design a proper wildcard system while hacking global vars.
// this is a better wildmatch, but it doesn't conform to global variables.
/***
  We can wild card match properly with:
  * does * and *.
  Don Johnson does Nash Bridges and stuff.
  or
  Don Johnson does Nash Bridges and stuff all the time.
  (ALL CORRECT)
  
  But the problem /IS/ how to point multiple words into the global vars.
  

***/
int szWildMatch(astr, bstr)
	char *astr;
	char *bstr;
{
	char storage[MAX_BUFSIZE];
	char *s, *p, *wbuff=storage;
	int star = FALSE, ctr = -1;
	
	strcpy(storage, "");
	loopStart:
		for(s = bstr, p = astr; *s; ++s, ++p) {
			switch(*p) {
				case '?':
				  //SET(gvar[ctr++], pstr("%c", *s));
				  if(*s=='.') goto starCheck;
				break;
				case '|':
				  DEBUG(pstr("%s", s));
				  break;
				case '*':
				  // obviously, if we have a new *, we stuff
				  // previous wbuff and storage into gvar[?]...
				  //(if we already have wbuff)
				    *wbuff = '\0';
				    SET(gvar[ctr++], storage);
				    wbuff = storage;
				    strcpy(storage, "");
				  star = TRUE;
				  bstr = s, astr = p;
				  do {++astr;} while(*astr == '*');
				  if(!*astr) 
				  {
				    if(*bstr)
				    	SET(gvar[ctr++], bstr);
				    
				    return TRUE;
				  } 
				  goto loopStart;
				default:
				  if(toupper(*p) != toupper(*s)) 
				  	goto starCheck;
				  break;
			}
		}
		while(*p == '*') ++p;
		*wbuff = '\0';
		SET(gvar[ctr++], storage);
		return(!*p);
		
	starCheck:
	  if(!star)
	  	return FALSE;
	  *wbuff++ = *bstr;
	  bstr++;
	 // DEBUG(pstr("%s", storage));
	  goto loopStart;
}



int match_wild(astr, bstr)
	char *astr;
 	char *bstr;
{
	char storage[MIN_BUFSIZE];
	char *wbuff = storage;
	char onechar;
	
	strcpy(storage, "");
	
	while( ((*astr != '*') && *astr && *bstr) || *bstr == ' ') {
		if(*astr == '?') {
		  	onechar = *bstr;
		  	tvar[arg] = pstr("%c", onechar);
		  	arg++;
		  	bstr++;
		  	astr++;
		} else if (*astr == '\\') { // This would be the escape char. Going to save that for now.
			
		} else if (*astr == '|') {
		  DEBUG(pstr("%s vs %s", astr, bstr));
		}
		
		
		if( (*astr != *bstr) && (*astr != '*')) { // We don't have a proper match, exit.
			return 0;
		}
		astr++;
		bstr++;
	}

		
	// First check to make sure astr and bstr still exist, if they both have reached their NULL terminating point,
	// end in style.:)
	if(!*astr || !*bstr) {
		if(!*astr && !*bstr)
			return 1;
		else if( (*astr == '*') && !*bstr) {
			astr++;
			if(!*astr)
				return 1;
			else 
				return 0;
		} else
			return 0;
	}

	// save the bstr position
	*wbuff = *bstr;
	astr++;
	while(*astr == '*')
		astr++;
	//if(*astr == '*') // we don't want double '*'s, exit.
	//	return 0;
	
	
	while( (*astr != *bstr)) {
		if(!*bstr) {
			if(*bstr != *astr)
				return 0;
			else
				return 1;
		}	
		bstr++;
		wbuff++;
		*wbuff = *bstr;
	}
	*wbuff--='\0';

	tvar[arg] = storage;
	arg++;

	if(arg <= NUMARGS) 
		if(*bstr && *astr) 
			return(match_wild(astr, bstr));
	
	while(*astr && *bstr) {
		if(*astr != *bstr)
			return 0;
		astr++;
		bstr++;
	}
	if( (*bstr && !*astr) || (*astr && !*bstr) || (*bstr != *astr))
		return 0;
		
	return 1;	
	
}

void
  copy_args()
{
  int i;
  
  for(i=0;i<NUMARGS;i++) 
 	SET(gvar[i], tvar[i]);
}

int switch_match_wild(astr, bstr)
	char *astr, *bstr;
{
	switch(*astr) {
	  case '>':
	    astr++;
	    if(isdigit(astr[0]) || (*astr == '-'))
	      return(atoi(astr)<atoi(bstr));
	    else
	      return(strcmpm(astr,bstr)<0);
	    break;
	  case '<':
	    astr++;
	    if(isdigit(astr[0]) || (*astr == '-'))
	      return(atoi(astr)>atoi(bstr));
	    else
	      return(strcmpm(astr,bstr)<0);
	    break;
	  default:
	    return(szWildMatch(astr, bstr));
	    //return(match_wild(astr, bstr));
	}	

	return(FALSE);
}

int
  wild(astr, bstr, varlist)
  	char *astr;
  	char *bstr;
  	VARLIST **varlist;
{
	int truth = 0;
	int i;

	set_args();
	if(szWildMatch(astr, bstr))
		truth = 1;
	return(truth);
}

inline void cat_str(str, text)
	char *str;
	char *text;
{
	char *c;

	c = str;
	while(*str)
	  str++;
	  
	while(*text) {
	  *str++ = *text++;
	}

	*str++ = 0;
			
}

char *retBinary(uint64_t num)
{
	static char buf[2048];
	uint64_t i, b, c;

	if(!num)
	  return "0";

	*buf = '\0';
	for(i = num; i != 0; i >>= 1)
	  sprintf(buf + strlen(buf), "%ld", (i&01));
    
	strcpy(buf, flip(buf)); 
	return stralloc(buf);
}

uint64_t bitcount(uint64_t x)
{
  uint64_t b;
  
  for(b = 0; x <= 1; x >>= 1) {
    if(x & 01) {
      b++;
    }
  }
    
  return b;
}

typedef struct foo_t {
  uint64_t a:64;
  uint64_t b:32;
  uint64_t c:32;
  uint64_t d:32;
} TFOO;

// Wildcard matching.
void do_wildcard(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  if(match_wild(arg1, arg2)) {
    DEBUG("Matched!");
  } else {
    DEBUG("No match!");
  }
//  notify(player, "");
}

void math_twice1()
{
  DEBUG("24");
  
}
