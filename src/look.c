/****************************************************************************
 MYTH - Anything to do with looking at objects, database, etc.
 Coded by Saruk (01/25/99)
 ---
 Your basic 'eyes' part of MYTH.
 ****************************************************************************/
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>

#include "db.h" 
#include "net.h"
#include "externs.h"

extern void do_look_right_at P((object *, object *, object *));

// display TYPE_THING contents of victim to player
void SHOW_CONTENTS(player, victim)
	object *player;
	object *victim;
{
	char buf[MAX_BUFSIZE], buf2[MAX_BUFSIZE];
	CONTENTS *c;

	if(!victim->contents)
  	  return;
	
	strcpy(buf, "");
	strcpy(buf2, "");
	for(c=victim->contents;c;c=c->next) 
	  if( (c->content->flags & TYPE_THING) && (!(c->content->flags & INVISIBLE) || ((c->content->flags & INVISIBLE) && (player->flags & WIZARD))) ) {
	    strcat(buf, pstr("%s", (can_control(player, c->content)) ? unparse_object(c->content) : color_name(c->content,-1)));
	    if(*atr_get(c->content, A_CAPTION)) 
	      strcat(buf, pstr(" %s\n", atr_get(c->content, A_CAPTION)));
	    else
	      strcat(buf, "\n");
	  } else if((c->content->flags & TYPE_PLAYER) && (c->content != player) && (c->content->flags & CONNECT) 
	      && (can_control(player, c->content) || !(c->content->flags & INVISIBLE))) {
	    strcat(buf2, pstr("%s", (can_control(player, c->content)) ? unparse_object(c->content) : 
	                      color_name(c->content, -1)));
	    if(*atr_get(c->content, A_CAPTION))
	      strcat(buf2, pstr(" %s\n", atr_get(c->content, A_CAPTION)));
	    else
	      strcat(buf2, "\n");
	  }

	if(*buf || *buf2)
	  notify(player, "Contents:");
	if(*buf) {
	  buf[strlen(buf)-1] = '\0';
	  notify(player, buf);
	}
	if(*buf2) {
	  buf2[strlen(buf2)-1] = '\0';
	  notify(player, buf2);
	}
}

// display TYPE_EXIT contents of victim to player
void SHOW_EXITS(player, victim)
	object *player;
	object *victim;
{
	char buf[MAX_BUFSIZE];
	CONTENTS *c;
	attr *a;
	
	strcpy(buf, "");
	for(a = victim->alist; a; a=a->next)
	    if( (a->flags & AEXIT) && !(a->flags & ADARK) && *(atr_get(victim, a->atype))) {
	      strcat(buf, show_pseudo_exit(a->name));
	      strcat(buf, "  ");
	    }
	
	for(c=victim->contents;c;c=c->next)
	  if((c->content->flags & TYPE_EXIT) && !(c->content->flags & HIDDEN)) {
	    if(has_pow(player, victim, POW_SEEATR))
	      strcat(buf, unparse_object(c->content));
	    else
	      strcat(buf, color_name(c->content, -1));
	    strcat(buf, "  ");
	  }


	if(strlen(buf) > 0) {
	  buf[strlen(buf)-2] = '\0';
	  notify(player, "Exits:");
	  notify(player, buf);
	}
}

void look_exit(player, exit)
	object *player;
	object *exit;
{
	notify(player, pstr("Through the exit marked %s you see...", exit->name));
	do_look_right_at(player, exit->linkto, exit->linkto); 
}

void do_look_right_at(player, victim, thing)
	object *player, *victim, *thing;
{
	char buf[MAX_BUFSIZE];
	
	
	if( (thing->flags & TYPE_EXIT) && !(thing->flags & HIDDEN)) {
	  look_exit(player, thing);
	  return;
	}	  
	
	if( (thing->flags & TYPE_EXIT) && (thing->flags & HIDDEN) ) {
	  notify(player, "What are you looking at?");
	  return;
	}
	
	notify(player, pstr("%s", (can_control(player, victim)) ? unparse_object(thing) : thing->name));

	pronoun_substitute(buf, thing, thing->desc, thing);
	notify(player, buf + strlen(thing->name) + 1);

	//SHOW_PLAYERS(player, thing);
	SHOW_CONTENTS(player, thing);
	SHOW_EXITS(player, thing);
	
	if((player->flags & NOSPOOF) && (player->flags & TYPE_PLAYER))
		notify(player, pstr("(#%d)", thing->ref));
		
	if(*atr_get(thing, A_ADESC)) {
	  parse_queue(thing, atr_get(thing, A_ADESC), 0, NULL);
	}
}


// look at matched arg1
void do_look_at(player, arg1)
	object *player;
	char *arg1;
{
	char buf[MAX_BUFSIZE], *bf=buf;
	object *victim = NULL, *o;
	char *s, *p, *name = arg1;
	
	//o = db[1];
	if(!(victim = remote_match(player, arg1))) {
	  if(!(victim = match_pseudoexit(player, arg1))) {
	    victim = NULL;
	  }
	}
	
	/* Possessive look idea ripped from MUSE */
	if(!victim) {
	  if(*arg1) {
	    for(s = name; *s && *s != ' '; s++);
	    if(!*(s-1)) {
	      notify(player, "I can't find that.");
	      return;
	    }
	    p = name;
	    if( (*(s-1) == 's' && *(s-2) == '\'' && *(s-3) != 's') ||
	        (*(s-1) == '\'' && *(s-2) == 's') ) {
	      
	      if(*(s-1) == 's') 
	        *(s-2) = '\0';
	      else 
	        *(s-1) = '\0';
	      
	      name = s + 1;
	      if(!name) {
	        notify(player, "What are you trying to look at?");
	        return;
	      }
	      if(!(victim = match_partial(player->location, p))) {
	        if(!(victim=match_special(player, p))) {
	          if(!(victim = match_object(p))) {
	            notify(player, "What?");
	            return;
	          }
	        }
	      }
	      
	      if(!(o = match_partial(victim, name))) {
	        if(!(o = match_special(player, name))) {
	          if(!(o = match_object(name))) {
	            notify(player, "What are you looking at?");
	            return;
	          }
	        }
	      }
	      
	      if(o->location != victim) {
	        notify(player, "Where are you looking?");
	        return;
	      }
	      do_look_right_at(player, victim, o);
	      return;
	    }
	  } else {
	    notify(player, "Hmm. Try looking at something else!");
	    return;
	  }
	  notify(player, "Look at what!?");
	  return;
	} 
	
	do_look_right_at(player, victim, victim);
}

// look at player's location or arg1.
void do_look(player, arg1)
	object *player;
	char *arg1;
{	
	char buf[MAX_BUFSIZE];
	
	if(*arg1) {
		do_look_at(player, arg1);
		return;
	}
	
	
	notify(player, pstr("%s", (can_control(player, player->location)) ? unparse_object(player->location) : color_name(player->location, -1)));	
	
	pronoun_substitute(buf, player->location, player->location->desc, player->location);
	notify(player, buf + strlen(player->location->name) + 1);
	
	//SHOW_PLAYERS(player, player->location);	
	SHOW_CONTENTS(player, player->location);
	SHOW_EXITS(player, player->location);
	if((player->flags & NOSPOOF) && (player->flags & TYPE_PLAYER))
		notify(player, pstr("(#%d)", player->location->ref));
	
	if(*atr_get(player->location, A_ADESC)) 
	  filter_command(player->location, atr_get(player->location, A_ADESC), 0);
}

void do_laston(player, arg1)
	object *player;
	char *arg1;
{
	object *victim;
	
	IS_ARGUMENT(player, (arg1));
	
	if(!(victim=match_player(arg1))) {
		error_report(player, M_ENOPLAY);
		return;
	}
	
	if(!can_control(player, victim) && (victim->flags & INVISIBLE)) {
		error_report(player, M_ENOCNTRL);
		return;
	}

	if(!*(atr_get(victim, A_LASTON))) 
		notify(player, pstr("%s hasn't logged on since the introduction of this command.", color_name(victim, -1)));
	else 	
		notify(player, pstr("%s last logged on: %s", color_name(victim, -1), atr_get(victim, A_LASTON)));
	
	if(!*(atr_get(victim, A_LASTOFF)))
		notify(player, pstr("%s hasn't logged off since the introduction of this command.", color_name(victim, -1)));
	else
		notify(player, pstr("%s last logged off: %s", color_name(victim, -1), atr_get(victim, A_LASTOFF)));
	
}

