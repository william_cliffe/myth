/****************************************************************************
 MYTH - ANSI System
 Coded by Saruk (02/07/99)
 Rewrite by Saruk (05/03/04)
 ---
 The new ANSI System is my own creation.
 ****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "db.h"
#include "net.h"
#include "externs.h"

char *ansi_chartab[256] = {
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,           0,             ANSI_BBLUE,  ANSI_BCYAN,
    0,           0,             0,           ANSI_BGREEN,
    0,           0,             0,           0,
    0,           ANSI_BMAGENTA, 0,           0,
    0,           0,             ANSI_BRED,   0,
    0,           0,             0,           ANSI_BWHITE,
    ANSI_BBLACK, ANSI_BYELLOW,  0,           0,
    0,           0,             0,           0,
    0,           0,             ANSI_BLUE,   ANSI_CYAN,
    0,           0,             ANSI_BLINK,  ANSI_GREEN,
    ANSI_HILITE, ANSI_INVERSE,  0,           0,
    0,           ANSI_MAGENTA,  ANSI_NORMAL, 0,
    0,           0,             ANSI_RED,    0,
    0,           ANSI_UNDER,    0,           ANSI_WHITE,
    ANSI_BLACK,  ANSI_YELLOW,   0,           0,
    0,           0,             0,           0,                                                                                                                                                                                    	
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
};


// pad the end of a string with spaces.
char *spc(num)
	int num;
{
	static char buf[64];
	char *b = buf;
	int count = 0;
	
	for(b = buf;  count <num; count++)
		*b++ = ' ';
	
	*b = '\0';	
	return(buf);
}


/* Strips the color codes out (^c<etc>)
   To strip actual colour out we'll have to use the
   macro skip_escode();
*/
char *strip_color(text)
	char *text;
{
	char buf[10000];
	char *p = buf, *k, *bf;
	
	if(!text)
	  return("");
	
	if(!strchr(text, '^'))
	  return(stralloc(text));
	
	strcpy(buf, "");

	for(k = text; *k; k++) {
	  switch(*k) {
	    case '^':
	      k++;
	      switch(*k) {
	        case 'c':
	          k++;
	          if(!*k) {
	            *p++ = 0;
	            return(stralloc(text));
	          }
	          if(!(bf = ansi_chartab[*k])) {
	             *p++ = '^';
	             *p++ = 'c';
	             *p++ = *k;
	             continue;
	          }
	          break;
	        default:
	          *p = *--k;
	          *p++ = *k;
	          break;
	      }
	      break;
	    default:
	      *p++ = *k;
	      break;
	  }
	}
	
	*p++ = 0;
	return(stralloc(buf));  
}

/*
  First, we strip color.

    
*/
char *chop_cstr(text, len)
	char *text;
	int len;
{
	char buf[8192];
	char *p = buf, *k;
	int i;
	
	strcpy(buf, "");

	if(strlen(strip_color(text)) <= len)
	  return(stralloc(text));
		
	for(i = 0, k = text; *k || i < len; k++) {
	  switch(*k) {
	    case '^':
	      i++; 
	      *p++ = *k;
	      k++;
	      if(!*k) {
	        *p++ = '^'; *p++ = 'c'; *p++ = 'n'; *p++ = 0;;
	        return(stralloc(buf));
	      }
	      switch(*k) {
	        case 'c':
	          i--;
	          *p++ = *k;
	          k++;
	          if(!*k) {
	            *p++ = '^'; *p++ = 'c'; *p++ = 'n'; *p++ = 0;;
	            return(stralloc(buf));
	          }
	          *p++ = *k;
	          break;
	        default:
	          if(++i <= len)
	            *p++ = *k;
	          break;
	      }
	      break;
	    default:
	      if(++i <= len)
	        *p++ = *k;
	      break;
	  }  
	  if(i >= len)
	    break;
	}

	*p++ = '^'; *p++ = 'c'; *p++ = 'n'; *p++ = 0;
	return(stralloc(buf));
}

char *color_name(player, len)
	object *player;
	int len;
{
	char buf[1024], colour[1024];
	char *name = atr_get(player, A_RAINBOW);
	char *ncname = strip_color(name);
	
	if(len == -1) {
	  if(!strchr(name, '^'))
	    return name;
	  sprintf(buf, "%s^cn", name);
	  return(stralloc(buf));
	} else if (len > strlen(ncname)) {
	  sprintf(buf, "%s%s^cn", name, spc(len - strlen(ncname)));
	  return(stralloc(buf));
	} 
	
	//DEBUG(pstr("name: %s, cname: %s, len: %d", ncname, name, len));
	sprintf(buf, "%s^cn", chop_cstr(name, len));
        return(stralloc(buf));
}

char *format_color(char *string, int length)
{
	char buf[MAX_BUFSIZE];

	if(!*string) {
	  return(stralloc(spc(length)));
	}
	
	if(strlen(strip_color(string)) == length) {
	  sprintf(buf, "%s^cn", string);
	  return(stralloc(buf));
	}
	
	strcpy(buf, string);
	
	if (strlen(strip_color(string)) < length)
	  strcat(buf, spc(length - strlen(strip_color(buf))));
	else
	  strcpy(buf, chop_cstr(buf, length));
	
	sprintf(buf, "%s^cn", buf);
	return(stralloc(buf));
}

void do_ansi_toggle(player)
	object *player;
{
	if(player->flags & ANSI) {
		player->flags &=~ANSI;
		notify(player, "* ANSI flag turned OFF.");
	} else {
		player->flags |= ANSI;
		notify(player, "* ANSI flag turned ON.");
	}
}

