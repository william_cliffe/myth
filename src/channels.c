/****************************************************************************
 MYTH - Channel System
 Coded by Saruk (04/09/99)
 ---
 MYTH's channel system with as much functionality as we can muster. Almost
 ability to do IRC-ish things with the channel.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

CHANNEL *channel_list = NULL;

void __delete_channel(channel)
	CHANNEL *channel;
{
	CHANNEL *c, *cprev = NULL;
	
	for(c = channel_list; c; c = c->next) {
		if(c == channel)
			break;
		cprev = c;
	}
	
	if(!c)
		return;
	
	if(!cprev)
		channel_list = c->next;
	else
		cprev->next = c->next;
	
	if(c->name)
		block_free(c->name);
	if(c->desc)
		block_free(c->desc);
	if(c->title)
		block_free(c->title);
	if(c->locks)
		block_free(c->locks);
	if(c->alias)
		block_free(c->alias);
	if(c->colour)
		block_free(c->colour);

	block_free(c);		
}

void __remove_channel(player, channel)
	object *player;
	CHANNEL *channel;
{
	CHANNEL *c, *cprev = NULL;
	
	for(c = player->channels; c; c=c->next) {
		if(c == channel)
			break;
		cprev = c;
	}
	
	if(!c)
		return;
	
	if(!cprev)
		player->channels = c->next;
	else
		cprev->next = c->next;
	
	if(c->name)
		block_free(c->name);
	if(c->desc)
		block_free(c->desc);
	if(c->title)
		block_free(c->title);
	if(c->locks)
		block_free(c->locks);
	if(c->alias)
		block_free(c->alias);
	if(c->colour)
		block_free(c->colour);
	
	block_free(c);
}

/** Clear all channels from an object **/
void clear_channels(player)
	object *player;
{
	CHANNEL *c, *cnext;
	
	for(c = player->channels; c; c = cnext) {
		cnext = c->next;
		notify_channel(NULL, c->chobj, pstr("* %s has left this channel",
			color_name(player, -1)));
		__remove_channel(player, c);
	}
}
	
/** Return first block_free channel reference id **/
int channel_ref()
{
	CHANNEL *c;
	int ctr = 0;
	
	for(c = channel_list; c; c=c->next)
		ctr++;
	
	return ctr;
}

/** Create a whole new channel **/
CHANNEL *__create_channel(name, desc)
	char *name;
	char *desc;
{
	CHANNEL *newc;
	
	GENERATE(newc, CHANNEL);
	newc->ref = channel_ref();
	newc->oref = 0;
	newc->owner = NULL;
	newc->flags = 0;
	newc->powlvl = 0;
	newc->status = CHANNEL_CONSTRUCTION;
	newc->chdef = FALSE;
	
	newc->name = NULL;
	newc->desc = NULL;
	newc->title = NULL;
	newc->locks = NULL;
	newc->alias = NULL;
	newc->colour = NULL;
	SET(newc->name, name);
	SET(newc->desc, desc);
	SET(newc->colour, "X");
	
	newc->next = channel_list;
	channel_list = newc;
	newc->chobj = newc;
	newc->player = 0;
	
	return(newc);	
}

CHANNEL *__add_channel(player, channel)
	object *player;
	CHANNEL *channel;
{
	CHANNEL *newc;
	
	GENERATE(newc, CHANNEL);
	newc->ref = channel->ref;
	newc->name = NULL;
	newc->desc = NULL;
	newc->title = NULL;
	newc->locks = NULL;
	newc->alias = NULL;
	newc->colour = NULL;
	SET(newc->name, channel->name);
	SET(newc->alias, channel->alias);
	SET(newc->colour, channel->colour);
	
	newc->owner = channel->owner;
	newc->oref = channel->oref;
	newc->flags = channel->flags;
	newc->powlvl = channel->powlvl;
	newc->status = channel->status;
	newc->chdef = FALSE;
	newc->chobj = channel;
	newc->player = player;
	
	newc->next = player->channels;
	player->channels = newc;
	
	return(newc);
}

int can_setchannel(channel)
	CHANNEL *channel;
{
	if(!channel->name || !channel->desc || !channel->colour || !channel->alias ||
		!channel->title)
		return FALSE;
	
	return TRUE;
}



/** This will see if a channel actually exists on the channel list. **/
/* Returns (NULL) if not */
CHANNEL *__find_channel(matchname)
	char *matchname;
{
	CHANNEL *c;
	int ref;
	
	ref = match_num(matchname);
	
	for(c = channel_list; c; c=c->next)
		if(!strcmpm(c->name, matchname) || ref == c->ref)
			return(c);
	
	return(NULL);
}

/** Does a player have this channel? **/
CHANNEL *__has_channel(player, channel)
	OBJECT *player;
	CHANNEL *channel;
{
	CHANNEL *c;
	
	for(c = player->channels; c; c=c->next)
		if(c->chobj == channel)
			return(c);
	
	return(NULL);
}

/** Completely purge a channel's occupants **/
void __purge_channel(channel)
	CHANNEL *channel;
{
	OBJECT *o;
	CHANNEL *c;
	
	for(o = player_list; o; o=o->next)
		if((c = __has_channel(o, channel)))
			__remove_channel(o, c);
}

/* This will check for locks using pchannel... */
void notify_channel(pchannel, channel, message)
	CHANNEL *pchannel;
	CHANNEL *channel;
	char *message;
{
	CHANNEL *c;
	OBJECT *o;
	char buf[MAX_BUFSIZE];
	
	for(o = player_list; o; o = o->next)
		for(c = o->channels; c; c = c->next) 
			if(c->chobj == channel && c->status == CHANNEL_ON) {
				if( (pchannel && (!parse_lock(o, pchannel->locks) || !parse_lock(pchannel->player, c->locks))) )
					continue;
				sprintf(buf, "[%s] %s", channel->name, message);
				notify(o, buf);
				//__notify(o, buf, 1);
			}
}

/** Does player have control over the channel? **/
int channel_control(player, channel)
	object *player;
	CHANNEL *channel;
{
	if(player == channel->owner)
		return 1;
	
	if(can_control(player, channel->owner))
		return 1;
// Make room to allow for channel owners to extend priveleges to other users.	

	return 0;
}

int name_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	CHANNEL *check, *c;
	object *o;
	
	if(strchr(value, ' '))
		return M_EILLNAME;	
		
	if( (check = __find_channel(value)) && (check != channel) )
		return M_ECHSHDW;
	
	SET(channel->name, value);
	for(o = player_list; o; o=o->next)
		for(c = o->channels; c; c=c->next)
			if(c->chobj == channel)
				SET(c->name, channel->name);
	
	notify(player, pstr("The channel has been renamed to: %s",
	  channel->name));
	
	return TRUE;
}

int desc_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	SET(channel->desc, value);
	notify(player, pstr("[CHANNEL]: %s's description is now '%s'.",
	  channel->name, channel->desc));
		
	return TRUE;
}

int title_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	if(!strcmp(value, "NULL"))
		channel->title = NULL;
	else
		SET(channel->title, value);
	
	notify(player, pstr("* You have set the title on |%s|%s to: %s",
		channel->colour, channel->name, (channel->title) ? channel->title : "NONE"));
	
	return TRUE;
}

int lock_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	if(!strcmpm(value, "NULL"))
		channel->locks = NULL;
	else
		SET(channel->locks, value);
		
	notify(player, pstr("[CHANNEL]: |%s|%s's locks set to: %s",
		channel->colour, channel->name, (channel->locks) ? channel->locks : "NONE"));
	
	return TRUE;
}

int alias_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	CHANNEL *check;
	
	for(check = channel_list; check; check = check->next)
		if(!strcmpm(check->alias, value) && check != channel)
			return M_ECHALSHDW;
	
	SET(channel->alias, value);
	notify(player, pstr("[CHANNEL]: |%s|%s's alias set to: %s",
		channel->colour, channel->name, channel->alias));
	
	return TRUE;
}

int player_alias_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	CHANNEL *c;
	
	for(c = player->channels; c; c=c->next)
		if(!strcmpm(c->alias, value) && c != channel)
			return M_ECHALSHDW;
	
	SET(channel->alias, value);
	notify(player, pstr("[CHANNEL]: |%s|%s's alias set to: %s",
		channel->colour, channel->name, channel->alias));
	
	return TRUE;
}

int colour_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	if(strchr(value, '|')) {
		notify(player, "You do not need to uses the (|) pipes for selecting colours.");
		return FALSE;
	}
	
	SET(channel->colour, value);
	notify(player, pstr("[CHANNEL]: Channel is now coloured: |%s|%s",
		channel->colour, channel->name));
	
	return TRUE;
}

int flag_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	char buf[MAX_BUFSIZE];
	int ctr, tag = 0, check;
	
	if(*value == '!') {
		strcpy(buf, value + 1);	
		tag = 1;
	}
	
	if(strlen(buf) == 1)
		check = buf[0];
		
	for(ctr = 0; chflag_table[ctr].name; ctr++)
		if( (check == chflag_table[ctr].parsed) || string_prefix(buf, chflag_table[ctr].name) ) {
			if(tag)
				channel->flags &= ~chflag_table[ctr].flag;
			else
				channel->flags |= chflag_table[ctr].flag;
			notify(player, pstr("[CHANNEL]: |%s|%s's channel flag %s %s.",
				channel->colour, channel->name, chflag_table[ctr].name, (tag) ? "unset":"set"));
			return TRUE;
		}
	
	return M_ENOTFOUND;
}

int powlvl_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	int powerlevel;

	if (!isanumber(value)) 
		return M_ENOTANUM;
	
	powerlevel = match_num(value);
	
	if(powerlevel < MIN_POWER)
		return M_EINSUFF;
	else if (powerlevel > player->powlvl || powerlevel > MAX_POWER)
		return M_EEXCESS;

	channel->powlvl = powerlevel;		
	notify(player, pstr("[CHANNEL]: Powerlevel for |%s|%s  set to: %d",
		channel->colour, channel->name, channel->powlvl));
			
	return TRUE;		
}

int status_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	char buf[MAX_BUFSIZE];

	sprintf(buf, "[CHANNEL]: |%s|%s's status set to:",
		channel->colour, channel->name);
	
	if(string_prefix(value, "construction")) {
		channel->status = CHANNEL_CONSTRUCTION;
		sprintf(buf, "%s Under Construction", buf);
	} else if(string_prefix(value, "on")) {
		if(!can_setchannel(channel))
			return M_ECHNOTDONE;

		channel->status = CHANNEL_ON;
		sprintf(buf, "%s ON", buf);
	} else if(string_prefix(value, "off")) {
		if(!can_setchannel(channel))
			return M_ECHNOTDONE;
			
		channel->status = CHANNEL_OFF;
		sprintf(buf, "%s OFF", buf);
	} else {
		return M_ECHINVSTAT;
	}
	
	notify(player, buf);
	return TRUE;
}

int default_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	char buf[MAX_BUFSIZE];
	
	sprintf(buf, "[CHANNEL]: |%s|%s's default set to :",
		channel->colour, channel->name);

	if(string_prefix(value, "true")) {
		channel->chdef = TRUE;
		sprintf(buf, "%s TRUE", buf);
	} else if (string_prefix(value, "false")) {
		channel->chdef = FALSE;
		sprintf(buf, "%s FALSE", buf);
	} else {
		return M_ECHINVDEF;
	}

	notify(player, buf);
	return TRUE;		
}

int owner_channel(player, channel, value)
	object *player;
	CHANNEL *channel;
	char *value;
{
	object *newowner;
	
	if(!(newowner = match_player(value)))
		return M_ENOPLAY;
	
	if(!can_control(player, newowner))
		return M_ENOCNTRL;
	
	channel->owner = newowner;
	channel->oref = newowner->ref;
	notify(player, pstr("[CHANNEL]: %s is now the owner of |%s|%s",
		color_name(newowner, -1), channel->colour, channel->name));
		
	return TRUE;
}



/** Set an actual channel, not the player's mirror of said channel **/
int do_channel_set(player, matchname, checkfor)
	object *player;
	char *matchname;
	char *checkfor;
{
	CHANNEL *c;
	char argument[MAX_BUFSIZE];
	char *a = NULL, *b = NULL;

	strcpy(argument, checkfor);
	if(!(c = __find_channel(matchname))) 
		return M_ECHNOTFOUND;
	
	if(!*(fparse(argument,',')) || !*(rparse(argument,','))) 
		return M_EINVARG;
	
	a = fparse(argument, ',');
	b = rparse(argument, ',');
	
	if(!channel_control(player, c)) 
		return M_ENOCNTRL;

	if(!strcmpm(a, "name")) 
		return name_channel(player, c, b);
	else if (!strcmpm(a, "desc"))
		return desc_channel(player, c, b);
	else if (!strcmpm(a, "title"))
		return title_channel(player, c, b);
	else if (!strcmpm(a, "locks") || !strcmpm(a, "lock"))
		return lock_channel(player, c, b);
	else if (!strcmpm(a, "alias"))
		return alias_channel(player, c, b);
	else if (!strcmpm(a, "colour") || !strcmpm(a, "color"))
		return colour_channel(player, c, b);
	else if (!strcmpm(a, "flags"))
		return flag_channel(player, c, b);
	else if (!strcmpm(a, "powlvl") || !strcmpm(a, "powerlevel"))
		return powlvl_channel(player, c, b);
	else if (!strcmpm(a, "status"))
		return status_channel(player, c, b);
	else if (!strcmpm(a, "chdef") || !strcmpm(a, "default"))
		return default_channel(player, c, b);
	else if (!strcmpm(a, "owner")) 
		return owner_channel(player, c, b);
	
	return M_EUNKCMD;
}



int create_channel(player, argument)
	object *player;
	char *argument;
{
	CHANNEL *newc;
	
	if(!*fparse(argument, '=') || !*rparse(argument, '='))
		return M_EINVARG;
	
	if(atoi(atr_get(player, A_CHANNELQUOTA)) < 1)
		return M_ECHNOQUOTA;
	
	if( (newc = __find_channel(front(argument))) )
		return M_ECHSHDW;
		
	newc = __create_channel(fparse(argument, '='), rparse(argument, '='));
	newc->oref = player->ref;
	newc->owner = player;
	notify(player, pstr("[CHANNEL]: Channel %s created.",
		newc->name));

	return TRUE;	
}



int list_channels_on(player, argument)
	object *player;
	char *argument;
{
	CHANNEL *clist;
	object *who;
	
	if(!argument || !*argument)
		who = player;
	else
		if(!(who = match_player(argument)))
			return M_ENOPLAY;
	
	if(!can_control(player, who))
		return M_ENOCNTRL;
		
	if(!who->channels)
		return M_ECHEMPTY;
	
	notify(player, pstr("%s's Channel List", color_name(who, -1)));
	notify(player, pstr("/%s\\", make_ln("-", 76)));
	notify(player, pstr("| %-48.48s | %-10.10s | %-10.10s |", "CHANNEL NAME", "ALIAS", "STATUS"));
	notify(player, pstr("|%s|%s|%s|", make_ln("-", 50), make_ln("-", 12), make_ln("-", 12)));
	for(clist = who->channels; clist; clist = clist->next)
		notify(player, pstr("| |%s|%-48.48s | %-10.10s | %-10.10s |", clist->colour, clist->chobj->name, 
			clist->alias, (clist->status == CHANNEL_ON) ? "ON":"OFF"));
	notify(player, pstr("\\%s/", make_ln("-", 76)));	
	return TRUE;
}

int list_channels(player, argument)
	object *player;
	char *argument;
{
	CHANNEL *clist;
	
	if(*argument)
		return list_channels_on(player, argument);
		
	if(!channel_list)
		return M_ECHEMPTY;
	
	notify(player, pstr("/%s\\", make_ln("-", 76)));
	notify(player, pstr("| %-22.22s | %-23.23s | %-10.10s | %-10.10s |", "CHANNEL NAME", "OWNER", "ALIAS", "LOCKED"));
	notify(player, pstr("|%s|%s|%s|%s|", make_ln("-", 24), make_ln("-", 25), make_ln("-", 12), make_ln("-", 12)));
	for(clist = channel_list; clist; clist = clist->next)
		notify(player, pstr("| |%s|%-22.22s | %s | %-10.10s | %-10.10s |", clist->colour,  
			clist->name, color_name(clist->owner, 23), clist->alias,  (parse_lock(player, clist->locks)) ? "NO":"YES"));
	notify(player, pstr("\\%s/", make_ln("-", 76)));	
	
	return TRUE;
}

int leave_channel(player, argument)
	object *player;
	char *argument;
{
	CHANNEL *channel, *c;
	
	if(!(channel = __find_channel(argument)))
		return M_ENOTFOUND;
	
	if(!(c = __has_channel(player, channel)))
		return M_ECHNOTON;

	__remove_channel(player, c);
	notify(player, pstr("[CHANNEL]: Channel |%s|%s deleted from your list.",
		channel->colour, channel->name));
	notify_channel(NULL, channel, pstr("* %s has left this channel.",
		color_name(player, -1)));
			
	return TRUE;
}

int boot_channel(player, ch, value)
	object *player;
	char *ch;
	char *value;
{
	object *o;
	CHANNEL *channel, *c;

	if(!(channel = __find_channel(ch)))
		return M_ENOTFOUND;	
	
	if(!(o = match_player(value)))
		return M_ENOPLAY;

	if(!(c = __has_channel(o, channel)))
		return M_ECHNOTON;
	
	__remove_channel(o, c);
	notify(o, pstr("You have been booted from channel |%s|%s",
		channel->colour, channel->name));
	notify(player, pstr("%s booted from channel.", color_name(o, -1)));	
	notify_channel(NULL, channel, pstr("* %s has been booted from this channel.",
		color_name(o, -1)));
	return TRUE;
}

int join_channel(player, argument)
	object *player;
	char *argument;
{
	CHANNEL *check, *c;
	
	if(!(check = __find_channel(argument)))
		return M_ENOTFOUND;

	//if(!parse_lock(player, check->locks) || check->status != CHANNEL_ON)
	//	return M_ECHNOJOIN;

	if( (c = __has_channel(player, check)))
		return M_ECHALREADY;
	
		
	c = __add_channel(player, check);
	c->chdef = check->chdef;
		
	notify(player, pstr("|%s|%s added to your channel list with alias '%s'", 
		c->colour, check->name, check->alias));
	notify_channel(NULL, check, pstr("* %s has joined this channel.", color_name(player, -1)));
	return TRUE;
}

int view_channel_player(player, arg, channel)
	object *player;
	char *arg;
	CHANNEL *channel;
{
	CHANNEL *show;
	object *who;
	
	if(!(who = (*arg) ?  match_player(arg) : player))
		return M_ENOPLAY;
	
	if(!can_control(player, who))
		return M_ENOCNTRL;
			
	if(!(show = __has_channel(who, channel)))
		return M_ECHNOTFOUND;
	
	notify(player, pstr("Channel statistics for '%s'", color_name(who, -1)));
	notify(player, pstr("Channel Name.: |%s|%s [%s]", show->colour, show->chobj->name,
		show->alias));
	notify(player, pstr("Your Title...: %s", (show->title) ? show->title : "NONE"));
	notify(player, pstr("Your Locks...: %s", (show->locks) ? show->locks : "NONE"));
	notify(player, pstr("Colour Code..: |%s|%s", show->colour, show->colour));
	notify(player, pstr("%s", make_ln("-", 78)));
	notify(player, pstr("* Channel is a default channel: %s",
		(show->chdef == CHANNEL_ON) ? "TRUE" : "FALSE"));

	return TRUE;
}	

int view_channel(player, arg1)
	object *player;	
	char *arg1;
{
	CHANNEL *channel;
	
	if(!*arg1)
		return M_EINVARG;
		
	if(!(channel = __find_channel(arg1)))
		return M_ECHNOTFOUND;
		
	make_header(player, "--- Channel Information ---");
	notify(player, pstr("\tChannel Name.........: |%s|%s [%s]", 
		channel->colour, channel->name, channel->alias));
	notify(player, pstr("\tChannel Owner........: %s", color_name(channel->owner, -1)));
	notify(player, pstr("\tChannel Joinable.....: %s (%s)", (parse_lock(player, channel->locks)) ?
		"YES" : "NO", (channel->locks) ? channel->locks: "NONE"));
	notify(player, pstr("\tChannel Title........: %s", channel->title));
	notify(player, pstr("%s", make_ln("-", 78)));
	notify(player, pstr("\t%s", channel->desc));
	notify(player, pstr("%s", make_ln("=", 78)));
	return FALSE;
}

int channel_to(player, channel, pchannel, argument)
	object *player;
	CHANNEL *channel;
	CHANNEL *pchannel;
	char *argument;
{
	char buf[MAX_BUFSIZE];
	object *o;
	CHANNEL *c;
	
	if(!(o = match_player(front(argument)))) 
		o = NULL;

	if(!o) {
	  if(!(c = __has_channel(player, channel)))
	    return M_ENOPLAY;
	} else if(!(c = __has_channel(o, channel)))
	  return M_ENOPLAY;
	  
	// Locks are foobared.
	//if(!parse_lock(player, c->locks) || !parse_lock(o, pchannel->locks))
	//	return M_ECHLOCPLY;
	
	if(o) {	
	  sprintf(buf, "%s [to %s]: %s", color_name(player, -1), color_name(o, -1),
	    speech_parse(player, restof(argument)));
	} else {
	  sprintf(buf, "%s: '%s", color_name(player, -1), speech_parse(player, argument));
	}
	notify_channel(pchannel, channel, buf);
	return TRUE;
}

char *channeltitle(pchannel)
	CHANNEL *pchannel;
{
	char buf[MIN_BUFSIZE];
	
	if(pchannel->title)
		sprintf(buf, " (%s)", pchannel->title);
	else
		return "";
	
	return stralloc(buf);
}

int send_to_channel(player, channel, pchannel, argument)
	object *player;
	CHANNEL *channel;
	CHANNEL *pchannel;
	char *argument;
{
	char buf[MAX_BUFSIZE];
	
	if(*argument == POSE_TOKEN)
		sprintf(buf, "%s %s", color_name(player, -1), argument + 1);
	else if (*argument == POSS_TOKEN)
		sprintf(buf, "%s's %s", color_name(player, -1), argument + 1);
	else if (*argument == THINK_TOKEN)
		sprintf(buf, "%s . o O ( %s )", color_name(player, -1),
			argument + 1);
	else if (*argument == TO_TOKEN)
		return channel_to(player, channel, pchannel, argument + 1);
	else
		sprintf(buf, "%s%s: %s", color_name(player, -1), channeltitle(pchannel), argument);
	
	notify_channel(pchannel, channel, buf);
	return TRUE;
}

int turn_channel(player, channel, flag)
	object *player;
	CHANNEL *channel;
	int flag;
{
	CHANNEL *pchannel;
	
	if(!(pchannel = __has_channel(player, channel)))
		return M_ECHNOTFOUND;
	
	if(flag == CHANNEL_OFF) {
		if(pchannel->status == CHANNEL_OFF) {
			notify(player, "* Channel is already off.");
			return TRUE;
		} else {
			notify(player, pstr("[CHANNEL]: You turned |%s|%s OFF",
				pchannel->colour, channel->name));
			pchannel->status = CHANNEL_OFF;
			notify_channel(pchannel, channel, pstr("* %s has turned this channel off.",
				color_name(player, -1)));
			return TRUE;
		}
	} else if (flag == CHANNEL_ON) {
		if(pchannel->status == CHANNEL_ON) {
			notify(player, "* Channel is already on.");
			return TRUE;
		} else {
			notify(player, pstr("[CHANNEL]: You turned |%s|%s ON",
				pchannel->colour, channel->name));
			pchannel->status = CHANNEL_ON;
			notify_channel(pchannel, channel, pstr("* %s has turned this channel on.",
				color_name(player, -1)));
			return TRUE;
		}
	} 
	
	return M_EUNKCMD;
}

int player_default_channel(player, channel)
	object *player;
	CHANNEL *channel;
{
	CHANNEL *clist;
	
	for(clist = player->channels; clist; clist = clist->next)
		if(clist == channel)
			clist->chdef = TRUE;
		else
			clist->chdef = FALSE;
	
	notify(player, pstr("|%s|%s is now your default channel.", channel->colour, channel->name));
	return TRUE;	
}

int player_channel_set(player, channel, argument)
	object *player;
	CHANNEL *channel;
	char *argument;
{
	CHANNEL *pchannel;
	
	if(!(pchannel = __has_channel(player, channel)))
		return M_ECHNOTFOUND;
	
	if(!strcmpm(fparse(argument,','), "lock"))
		return lock_channel(player, pchannel, rparse(argument,','));
	else if (!strcmpm(fparse(argument,','), "alias"))
		return player_alias_channel(player, pchannel, rparse(argument,','));
	else if (!strcmpm(fparse(argument,','), "title"))
		return title_channel(player, pchannel, rparse(argument,','));
	else if (!strcmpm(fparse(argument,','), "colour") || !strcmpm(fparse(argument,','), "color"))
		return colour_channel(player, pchannel, rparse(argument,','));
	else if (!strcmpm(fparse(argument,','), "default") || !strcmpm(fparse(argument,','), "chdef"))
		return player_default_channel(player, pchannel);
	else {
		if(pchannel->status == CHANNEL_ON && channel->status == CHANNEL_ON)
			return send_to_channel(player, channel, pchannel, argument); 
		else
			return M_ECHNOTON;
	}
}

int who_channel(player, channel)
	object *player;
	CHANNEL *channel;
{
	CHANNEL *c, *cl;
	OBJECT *o;
	int ctr = 0;
	
	if(!(c = __has_channel(player, channel)))
		return M_ECHNOTON;

	if(c->status == CHANNEL_OFF)
		return M_ECHNOTON;
		
	notify(player, pstr("Channel %s who list:", c->name));
	notify(player, pstr("%s", make_ln("-",strlen(c->name) + 17)));
	for(o = player_list; o; o=o->next)
		if((o->flags & CONNECT) && ( !(o->flags & INVISIBLE) || ((o->flags & INVISIBLE) && (player->flags & WIZARD))))
			for(cl = o->channels; cl; cl=cl->next)
				if(cl->chobj == channel && cl->status == CHANNEL_ON) {
					notify(player, pstr("%s %s", color_name(o, -1), (o->flags & INVISIBLE) ?
						"(INVISIBLE)" : ""));
					ctr++;
				}
	notify(player, "-----");
	notify(player, pstr("Total Players: %d", ctr));
	return TRUE;	
}



int do_channel_event(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	CHANNEL *channel;
	char argument[MAX_BUFSIZE];
	
	strcpy(argument, arg2);
	if(!(channel = __find_channel(arg1)))
		return M_ECHNOTFOUND;

	if(!*argument)
		return view_channel_player(player, "", channel);
	
	if (!strcmpm(fparse(argument,','), "on") && !*rparse(argument,','))
		return turn_channel(player, channel, CHANNEL_ON);
	else if (!strcmpm(fparse(argument,','), "off") && !*rparse(argument,','))
		return turn_channel(player, channel, CHANNEL_OFF);
	else if (!strcmpm(fparse(argument,','), "who") && !*rparse(argument,','))
		return who_channel(player, channel);
	else if (!strcmpm(fparse(argument,','), "view")) 
		return view_channel_player(player, rparse(argument,','), channel);
	else
		return player_channel_set(player, channel, argument);
}

int delete_channel(player, chan)
	object *player;
	char *chan;
{
	CHANNEL *channel;
	
	if(!(channel = __find_channel(chan)))
		return M_ECHNOTFOUND;
	
	if(!channel_control(player, channel))
		return M_ENOCNTRL;
	
	SET_ATR(player, A_CHANNELQUOTA, pstr("%d", atoi(atr_get(player, A_CHANNELQUOTA)) + 1));
	__purge_channel(channel);
	__delete_channel(channel);
	notify(player, "* Channel deleted.");
	return TRUE;
		
}

void do_channel(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	if(!*arg1 && !*arg2) { // list the channels player is on.
		error_report(player, list_channels_on(player, NULL));
		return;
	}
	
	if (!strcmpm(arg1, "list")) {
		error_report(player, list_channels(player, arg2));
		return;
	} else if (!strcmpm(arg1, "boot")) {
		error_report(player, boot_channel(player, fparse(arg2,'='), rparse(arg2, '=')));
		return;
	} else if(!strcmpm(arg1, "create")) {
		error_report(player, create_channel(player, arg2));
		return;
	} else if (!strcmpm(arg1, "leave")) {
		error_report(player, leave_channel(player, arg2));
		return;
	} else if(!strcmpm(arg1, "join")) {
		error_report(player, join_channel(player, arg2));
		return;
	} else if (!strcmpm(arg1, "set")) {
		error_report(player, do_channel_set(player, fparse(arg2,'='), rparse(arg2,'=')));
		return;
	} else if (!strcmpm(arg1, "view")) {
		error_report(player, view_channel(player, front(arg2), restof(arg2)));
		return;
	} else if (!strcmpm(arg1, "delete")) {
		error_report(player, delete_channel(player, arg2));
		return;
	} else if (!*arg1) {
		error_report(player, do_channel_event(player, fparse(arg2,'='), rparse(arg2,'=')));
		return;
	} else {
		error_report(player, M_ENOSWITCH);
		return;
	}
}

int match_channels(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	CHANNEL *clist;
	
	for(clist = player->channels; clist; clist=clist->next)
		if(!strcmpm(clist->alias, arg1)) {
			error_report(player, do_channel_event(player, clist->name, arg2));
			return 1;
		}
	
	return 0;
}

void do_channel_default(player, sendstr)
	object *player;
	char *sendstr;
{
	CHANNEL *clist;
	
	for(clist = player->channels; clist; clist=clist->next)
		if(clist->chdef) {
			error_report(player, do_channel_event(player, clist->name, sendstr));
			return;
		}
	
	error_report(player, M_ECHNODFLT);
}

/** CHANNEL PASTE **/
int channel_paste_header(d, channame, tag)
	DESCRIPTOR *d;
	char *channame;
	int tag;
{
	CHANNEL *channel, *pchannel;
	
	if(!(channel = __find_channel(channame)))
		return M_ECHNOTFOUND;
	
	if(!(pchannel = __has_channel(d->player, channel)))
		return M_ECHNOTON;
	
	if(tag)
		notify_channel(pchannel, channel, pstr("--- Paste Buffer From %s ---",
			color_name(d->player, -1)));
	else
		notify_channel(pchannel, channel, pstr("--- End of Paste Buffer From %s ---",
			color_name(d->player, -1)));	
	return TRUE;
}

void paste_channel(player, channame, message)
	object *player;
	char *channame;
	char *message;
{
	CHANNEL *channel, *pchannel;
	
	if(!(channel = __find_channel(channame)))
		return;
	
	if(!(pchannel = __has_channel(player, channel)))
		return;
	
	notify_channel(pchannel, channel, message);
}

/** CHANNEL LOGS **/
void log_channel(channame, message)
	char *channame;
	char *message;
{
	char buf[MAX_BUFSIZE];
	CHANNEL *channel;
	FILE *fp;
	
	if(!(channel = __find_channel(channame)))
		return;
	
	sprintf(buf, "logs/%s", channame);
	
	if(!(fp = fopen(buf, "a"))) {
		notify_channel(NULL, channel, "* Could not open log!");
		return;
	}
	
	notify_channel(NULL, channel, message);
	
	fprintf(fp, "(%s)[%s] %s\n", get_date(time(0)), channel->name, strip_color(message));
	fclose(fp);
}

