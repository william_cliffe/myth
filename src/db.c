/****************************************************************************
 MYTH - Database structures/routines
 Coded by Saruk (01/21/99)
 ---
 When I get it right....
 ****************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <bson.h>
//#include <mongoc.h>

#include "db.h"
#include "net.h"
#include "externs.h"


// Global variable
char *gvar[10];

// OBJECTs
OBJECT *cmd_player = NULL;

// Clear all the lists.
OBJECT *exit_list = 0;
OBJECT *thing_list = 0;
OBJECT *player_list = 0;
OBJECT *room_list = 0;

// Clear all the list tails.
OBJECT *exitlist_tail = 0;
OBJECT *thinglist_tail = 0;
OBJECT *playerlist_tail = 0;
OBJECT *roomlist_tail = 0;

OBJECT *rootobj = 0;
OBJECT **db = NULL;

FLIST *flist = NULL;
FLIST *ftail = NULL;

ATTR *alist = NULL;
ENV *envglobal = NULL;

// Mongo
///mongoc_collection_t *collection = NULL;

int dbtop = 0;
int dbsize = 0;
int dbinit = 0;
int db_version = 0;
int dbloading = 0;

/*******************************************
 Append database object.
 *******************************************/
void append_object(object **list, object **listtail, object *o)
{
	if(!*list) {
	  o->next = NULL;
	  o->prev = NULL;
	  *listtail = o;
	  *list = o;	  
	} else {
	  o->next = NULL;
	  o->prev = *listtail;
	  (*listtail)->next = o;
	  *listtail = o;
	}
}


/*******************************************
 Initial database routines.
 *******************************************/
// find out where something is.
object *loc(player)
	object *player;
{
	object *o;
	
	for(o=room_list; o; o=o->next)
		if(o->ref == player->loc)
			return o;
	for(o=player_list; o; o=o->next)
		if(o->ref == player->loc)
			return o;
	for(o=thing_list; o; o=o->next)
		if(o->ref == player->loc)
			return o;
	
	return (NULL);
}

// reference the link.
object *linkit(player)
	object *player;
{
	object *o;
	
	for(o=room_list; o; o=o->next)
		if(o->ref == player->link)
			return o;
	for(o=player_list; o; o=o->next)
		if(o->ref == player->link)
			return o;
	for(o=thing_list; o; o=o->next)
		if(o->ref == player->link)
			return o;
	
	return (NULL);
}

// reference the owner.
object *owner(player)
	object *player;
{
	object *o;
	
	for(o=room_list; o; o=o->next)
		if(o->ref == player->owner)
			return o;
	for(o=player_list; o; o=o->next)
		if(o->ref == player->owner)
			return o;
	for(o=exit_list; o; o=o->next)
		if(o->ref == player->owner)
			return o;
	for(o=thing_list; o; o=o->next)
		if(o->ref == player->owner)
			return o;
	
	return (NULL);
}

// calculates the number of commands on an alias set.
int calculate_cmds(o)
	OBJECT *o;
{
	int ctr = 0;
	CLIST *c;
	
	for(c=o->cmdlist;c;c=c->next)
		ctr++;
		
	return ctr;
}
		
// calculate the size of an objects attribute list (in number of attributes)
void calculate_attrs(fp, o)
	FILE *fp;
	OBJECT *o;
{
	int ctr = 0;
	attr *a;
	
	for(a = o->alist; a; a=a->next)
	  ctr++;

	fprintf(fp, "%d\n", ctr);
	
	for(a = o->alist; a; a=a->next)
	  fprintf(fp, "%d:%d:%s:%s\n", a->powlvl, a->flags, a->name, (a->value) ? a->value : "");
}

// this is a temporary procedure. logs the database - this is defunct, killing (01/15/03)
void log_database(void)
{
	OBJECT *o;
	int i;
	
	printf("rewrite this if you want it!\n");	
	for(i = 0; i < dbtop; i++) {
	  if(!db[i])
	    continue;
	  
	  o = db[i];
	  //printf("%s has %x\n", o->name, o->parent);
	}
}

// this is where we calculate the size of the database.
int calculate_dbsize(void)
{
	int counter = 0;
	OBJECT *o;
	
	for(o=player_list;o;o=o->next)
		counter++;
	for(o=room_list;o;o=o->next)
		counter++;
	for(o=exit_list;o;o=o->next)
		counter++;
	for(o=thing_list;o;o=o->next)
		counter++;
		
	return counter;
}


int get_topdb(void)
{
	int top = -1;
	object *o;
	
	for(o = player_list; o; o=o->next)
		if(o->ref > top)
			top = o->ref;
	for(o = thing_list; o; o=o->next)
		if(o->ref > top)
			top = o->ref;
	for(o = room_list; o; o=o->next)
		if(o->ref > top)
			top = o->ref;
	for(o = exit_list; o; o=o->next)
		if(o->ref > top)
			top = o->ref;
	
	return(top+1);		
}

/* GET NEXT FREE DB # */


// get a free database number.

int db_num(void)
{
	int *array;
	OBJECT *o;
	int db_top = 0, last_num = -1;
	
	db_top = get_topdb();
	array = calloc(db_top, sizeof(int));
	for(o = player_list; o; o=o->next)
		array[o->ref] = 1;
	for(o = thing_list; o; o=o->next)
		array[o->ref] = 1;
	for(o = room_list; o; o=o->next)
		array[o->ref] = 1;
	for(o = exit_list; o; o=o->next)
		array[o->ref] = 1;
	for(last_num = 0; last_num < db_top ;++last_num)
		if(!array[last_num])
			break;
	free(array);
	return(last_num);
}

/**void init_mongo()
{
    
    mongoc_client_t *client;
    mongoc_init();
    
    client = mongoc_client_new("mongodb://localhost:27017");
    collection = mongoc_client_get_collection(client, "test", "MYTH");
}**/

// We want to set up a db[1..??]
void init_db()
{
	OBJECT *o;
	int ctr = 0;
	
	db = (OBJECT **)stack_em((sizeof(OBJECT *) * (get_topdb() + 1)), SOLID);
	
	dbtop = get_topdb();
	for(ctr = 0; ctr < dbtop + 1; ctr++)
		db[ctr] = 0;
		
	for(o=player_list; o; o=o->next)
		db[o->ref] = o;
	for(o=room_list; o; o = o->next)
		db[o->ref] = o;
	for(o=thing_list; o; o=o->next)
		db[o->ref] =o;
	for(o=exit_list; o; o=o->next)
		db[o->ref] =o;
	
	dbtop = get_topdb();
}

void do_dbcheck()
{
	int i;
	
	for(i = 0; i < dbtop; i++)
		if(db[i])
			DEBUG(pstr("%s", (db[i])->name));
}

/************************************************
 Remove an object (o) from its list (olist).
 
 Eventually collect removed o's and put them in 
 a garbage container...
 ************************************************/
object *delete_object(olist, otail, o)
	object **olist;
	object **otail;
	object *o;
{
	if(o->prev)
	  o->prev->next = o->next;
	
	if(o->next)
	  o->next->prev = o->prev;
	
	if( (*olist) == o )
	  (*olist) = o->next;
	
	if( (*otail) == o)
	  (*otail) = o->prev;
	
	//clear_channels(o);
	clear_children(o);
	clear_parent(o);
	free_mail(o);
	KILL_ATTRS(o);
	RemoveAllItems(o);
	RemoveFromChannels(o);
	CheckChannels(o);
	
	if(o->name)
		block_free(o->name);
	if(o->desc)
		block_free(o->desc);
	if(o->pass)
		block_free(o->pass);
	if(o->pows)
		block_free(o->pows);
		
	// remember to clear the db list
	db[o->ref] = 0;

	block_free(o);	
	o = NULL;
	
	return *olist;
}


/** ZeroObject(): In order to reduce dependency on two large object 
                  creation functions, just put all eggs in one basket.
                  All this does is set all appropriate object properties
                  to NULL.
**/
static void ZeroObject(object *o)
{
  ENV *space;
  unsigned int i;
  
  o->name = o->desc = o->pass = NULL;
  o->pows = NULL;
  o->location = o->linkto = o->ownit = o->zone = NULL;
  o->pows = NULL;
  o->cmdlist = NULL;
  o->mail = NULL;
  o->channels = NULL;
  o->parent = o->children = NULL;
  o->alist = o->atail = NULL;
  o->combat = NULL;
  o->items = o->itemstail = o->equipped = o->equippedtail = NULL;
  /*o->creature = NULL;*/
  o->qlev = o->clev = 0;
  
  GENERATE(space, ENV);
  space->max_size = 4096;
  space->cur_size = 0;
  for(i = 0; i < MAX_REGISTERS; i++)
    space->registers[i] = NULL;
    
  space->variables = space->vtail = NULL;
  o->environment = space;
}


// create a database object and link it to the appropriate list.
object *create_object(owner, type, name)
	dbref owner;
	int type;
	char *name;
{
	object *o;
	int ref = 0; 
	
	ref = db_num();
	if(ref >= dbtop) {
	  dbtop++;
	  db = (OBJECT **)stack_realloc(db, sizeof(struct object_t *) * (ref+1), SOLID);
	}
	
	GENERATE(o, OBJECT);
	o->ref = db_num();
	o->owner = owner;
	o->flags = type;
	ZeroObject(o);

	SET(o->name, strip_color(name));
	SET(o->pass, "*");
	SET(o->desc, return_config("default_description"));
        
	// this will set the object up with it's initial attributes.
	SET_ATR(o, A_RAINBOW, name);
	SET_ATR(o, A_CREATED, pstr("%ld", time(0)));

	if(type == TYPE_PLAYER) {
		SET(o->pass, mcrypt(return_config("default_password")));
		
		o->owner = o->ref;
		o->loc = 0;
		o->link = o->loc;
		o->powlvl = PLAYER_POWINIT;
		append_object(&player_list, &playerlist_tail, o);
		//o->next = player_list;
		//player_list = o;
	} else if (type == TYPE_ROOM) {
		SET_ATR(o, A_ZONE, atr_get(db[0], A_ZONE));
		o->zone = match_object(atr_get(db[0], A_ZONE));
		o->link = o->ref;
		o->loc = o->ref;
		o->powlvl = ROOM_POWINIT;
		append_object(&room_list, &roomlist_tail, o);
		//o->next = room_list;
		//room_list = o;
	} else if (type == TYPE_EXIT) {
		o->loc = -1;	// this is meant to be defined in exit_create.
		o->link = o->loc;
		o->powlvl = EXIT_POWINIT;
		append_object(&exit_list, &exitlist_tail, o);
		//o->next = exit_list;
		//exit_list = o;
	} else if (type == TYPE_THING) {
		o->loc = o->owner;
		o->link = o->loc;
		o->powlvl = THING_POWINIT;
		append_object(&thing_list, &thinglist_tail, o);
		//o->next = thing_list;
		//thing_list = o;
	} else {
		printf("[ERROR]: Invalid object type!\n");
		return (NULL);
	}

	db[o->ref] = o;

	return o;	
}

/*******************************************
 Database IN/OUT routines...
 *******************************************/
// Object IN.
int IN_OBJ(fp)
	FILE *fp;
{
	OBJECT *o;
	int cmdnum, index, attrnum;
	int ref;
	
	
	ref = getint(fp);
		
	GENERATE(o, OBJECT);
	o->ref = ref;
	ZeroObject(o);
	
	SET(o->name, getstring(fp));
	SET(o->desc, getstring(fp));
	SET(o->pass, getstring(fp));
	o->flags = getlong(fp);
	o->powlvl = getint(fp);
	o->owner = getint(fp);
	o->loc = getint(fp);
	o->link = getint(fp);
	
	if(o->flags & TYPE_PLAYER)
	  get_powers(o, getstring(fp));
	
	// I bet this is where all the time is spent loading crap up.
	attrnum = getint(fp);
	for(index = 0; index < attrnum; index++)
		mkattr(o, getstring(fp));
	
	cmdnum = getint(fp);
	for(index = 0; index < cmdnum; index++) 
		o->cmdlist = mkcmd(o->cmdlist, getstring(fp));

	if(*(getstring(fp)) != '\\')
		return 0;

	if(o->flags & TYPE_PLAYER) {
		append_object(&player_list, &playerlist_tail, o);
		//o->next = player_list;
		//player_list = o;	
	} else if (o->flags & TYPE_ROOM) {
		append_object(&room_list, &roomlist_tail, o);
		//o->next = room_list;
		//room_list = o;
	} else if (o->flags & TYPE_EXIT) {
		append_object(&exit_list, &exitlist_tail, o);
		//o->next = exit_list;
		//exit_list = o; //o->next;
	} else if (o->flags & TYPE_THING) {
		append_object(&thing_list, &thinglist_tail, o);
		//o->next = thing_list;
		//thing_list = o;
	} else {
		printf("ERROR: Invalid object type, skipping object.\n");
	}
	
	return 1;
}

/**
int IN_BBLIST(fp)
	FILE *fp;
{
	BBOARD *b;
	object *o;
	char title[1024], message[2048];
	int num_read, c_ref;
	long created, expiration;
	
	strcpy(title, getstring(fp));
	c_ref = getint(fp);
	strcpy(message, getstring(fp));
	created = getlong(fp);
	expiration = getlong(fp);
	num_read = getint(fp);

	if(*(getstring(fp)) != '\\')
		return 0;
		
	if(!(o = match_player(pstr("#%d", c_ref)))) 
		return 0;
		
	b = add_to_board(o, title, message);
	b->created = created;
	b->expiration = expiration;
	b->num_read = num_read;
	
	printf("We added a +board to the board system... %s %s", b->topic, b->message);
	return 1;
}
**/

// For database loading... set locations
void object_reference(void)
{
	OBJECT *o;
	
	for(o=room_list; o; o=o->next) {
		o->location = db[o->loc];
		o->linkto = db[o->link];
		o->ownit = db[o->owner];
	}
	
	for(o=player_list; o; o=o->next) {
		o->location = db[o->loc];
		o->linkto = db[o->link];
		o->ownit = db[o->owner];
		
	}
	
	for(o=exit_list; o; o=o->next) {
		o->location = db[o->loc];
		o->linkto = db[o->link];
		o->ownit = db[o->owner];
	}
	
	for(o=thing_list; o; o=o->next) {
		o->location = db[o->loc];
		o->linkto = db[o->link];
		o->ownit = db[o->owner];
	}
}

// We only need to set the zones on ROOMs.
void set_zones(void)
{
	object *o;
	
	for(o = room_list; o; o=o->next)
	  if(!*(atr_get(o, A_ZONE))) {
	    SET_ATR(o, A_ZONE, atr_get(db[0], A_ZONE));
	    o->zone = match_object(atr_get(db[0], A_ZONE));
	  } else 
	    o->zone = match_object(atr_get(o, A_ZONE));
	
	for(o = thing_list; o; o=o->next)
	  if(o->flags & GUEST) { // old zone flag.
	    o->flags &= ~GUEST;
	    o->flags |= ZONE;
	  }
}

// Store the contents of a 'thing'.
static void store_contents(thing)
	object *thing;
{
	object *o;
	
	if(thing->location == thing)
		add_content(thing, thing);
	
	for(o=player_list; o; o=o->next)
		if(o->location == thing)
			add_content(thing, o);
				
	for(o=thing_list;o;o=o->next)
		if(o->location == thing)
			add_content(thing, o);
	
	for(o=exit_list;o;o=o->next)
		if(o->location == thing)
			add_content(thing, o);
	
}

// Reference object contents. EXITS are not allowed to have contents. 
void reference_contents()
{
	object *o;
	
	for(o=player_list; o; o=o->next)
		store_contents(o);
	for(o=room_list; o; o=o->next)
		store_contents(o);
	for(o=thing_list; o; o=o->next)
		store_contents(o);
}

// this will set the root object - on error, the entire MYTH will shut down.
void set_rootobj()
{
	rootobj = db[ROOTOBJ];
	
	if(!rootobj) {
		printf("** PANIC ** Error finding root object!\n");
		exit(1);
	}
}

void desc_write(string)
	char *string;
{
	DESCRIPTOR *d;
    int result;
	
	for(d = descriptor_list; d; d=d->next)
	  if(reboot_style < 2) {
	    result = write(d->descriptor, string, strlen(string));
	    result = write(d->descriptor, "\n", 1);
	  }
}

void set_inherency()
{
	char buf[MAX_BUFSIZE];
	char *p = buf, *k;
	object *o;
	int i;
	
	strcpy(buf, "");
	for(i = 0; i < dbtop; i++) {
		if(!db[i])
			continue;
			
		o = db[i];
		if(*(atr_get(o, A_PARENT))) {
			strcpy(buf, atr_get(o, A_PARENT));
			p = buf;
			while( (k = parse_up(&p, ' '))) {
				add_parent(o, k, 1);
			}
		}
		if(*(atr_get(o, A_CHILDREN))) {
			strcpy(buf, atr_get(o, A_CHILDREN));
			p = buf;
			while( (k = parse_up(&p, ' '))) {
				add_parent(o, k, 2);
			}
		}
	}
}

void initializeGlobalEnvironment()
{
  ENV *space;
  int i;
  
  GENERATE(space, ENV);
  space->max_size = MAX_ENVGLOBAL_SIZE;
  space->cur_size = 0;
  for(i = 0; i < MAX_REGISTERS; i++)
    space->registers[i] = NULL;

  space->variables = space->vtail = NULL;
  envglobal = space;
}

// Initial load procedure.
void load_database(void)
{
	char buf[1024];
	DESCRIPTOR *d;
	FILE *fp;
	int counter;
	int index;
    int result;
	
	initializeGlobalEnvironment();
	
	if ( (fp = fopen(return_config("db_name"),"r")) == NULL) {
		printf("ERROR: Cannot load database!\n");
		// no sense in booting up, eh?
		exit(1);
	}
	
	dbloading = 1;
	db_version = getint(fp);
	counter = getint(fp);
	
	if(reboot_style < 2)
	{
	  for(d = descriptor_list; d; d=d->next) {
	    sprintf(buf, "* Loading %d database objects... (%s) \n", counter, get_date(time(0)));
	    result = write(d->descriptor, buf, strlen(buf));
	  }
	}

	for(index = 0; index < counter; index++) {
	  if(reboot_style < 2)
	    if((index % 2000 == 0)) {
              for(d = descriptor_list; d; d=d->next) {
	        sprintf(buf, "* Loading object #%d of %d... (%s)\n", index, counter, get_date(time(0)));
		result = write(d->descriptor, buf, strlen(buf));
	      }
	    }
	  if(!IN_OBJ(fp)) {
	    printf("** PANIC ** Corrupted database, exiting!\n");
	    shutdown_flag = 1;
	    return;
	  }
	}
	
	if(reboot_style < 2)
  	  for(d = descriptor_list; d; d=d->next) {
	    sprintf(buf, "* Finished loading database objects... (%s) \n", get_date(time(0)));
	    result = write(d->descriptor, buf, strlen(buf));
	  }
	
	// initialise the db list
	init_db();

    // Initialize MongoDB
    //init_mongo();
	
	if(reboot_style < 2)
	  for(d = descriptor_list; d; d=d->next) {
	    sprintf(buf, "* Finished initializing db... (%s) \n", get_date(time(0)));
	    result = write(d->descriptor, buf, strlen(buf));
	  }

	fclose(fp);
	
	// REFERENCE the database, nutcase.
	object_reference();
	reference_contents();
	set_rootobj();
	set_inherency();
	set_zones();
	do_combat_load();

	if(reboot_style < 2) {
	  for(d = descriptor_list; d; d=d->next) {
	    sprintf(buf, "* Referencing complete... (%s)\n", get_date(time(0)));
	    result = write(d->descriptor, buf, strlen(buf));
	  }
	}

		
	dbloading = 0;
	// now we're going to log it.
	//log_database();
	// Now create the db array. 
}

// Object output.
void OUT_OBJ(fp, o)
	FILE *fp;
	OBJECT *o;
{
	CLIST *c;
    //bson_error_t error;
    //bson_oid_t oid;
    //bson_t *doc;

    // Set up the new document
    //doc = bson_new();
    //bson_oid_init(&oid, NULL);
    //BSON_APPEND_OID(doc, "_oid", &oid);

	// What object are you?
	fprintf(fp, "%d\n", o->ref);
    //BSON_APPEND_INT32(doc, "dbref", o->ref);

	// We'll save our strings here.
	fprintf(fp, "%s\n", o->name);
//    BSON_APPEND_UTF8(doc, "name", o->name);
	fprintf(fp, "%s\n", o->desc);
//    BSON_APPEND_UTF8(doc, "desc", o->desc);
	fprintf(fp, "%s\n", o->pass);
//    BSON_APPEND_UTF8(doc, "password", o->pass);
	
	// We'll save our integers/longs/etc. here.
	fprintf(fp, "%llu\n", o->flags);
    //BSON_APPEND_INT64(doc, "flags", o->flags);
	fprintf(fp, "%d\n", o->powlvl);
//    BSON_APPEND_INT32(doc, "powlvl", o->powlvl);
	fprintf(fp, "%d\n", o->owner);
//    BSON_APPEND_INT32(doc, "owner", o->owner);
	fprintf(fp, "%d\n", o->loc);
//    BSON_APPEND_INT32(doc, "loc", o->loc);
	fprintf(fp, "%d\n", o->link);
//    BSON_APPEND_INT32(doc, "link", o->link);
	
	// Calculate any attributes that may have been newly defined in the default section.
	//check_attrs(o);
	
	if(o->flags & TYPE_PLAYER)
		put_powers(fp, o);
	
	// We'll save our attribute system here.
	calculate_attrs(fp, o);
				
	
	// We'll save our list of aliased commands here.
	fprintf(fp, "%d\n", calculate_cmds(o));
	for(c = o->cmdlist; c; c=c->next)
		fprintf(fp, "%s %s\n", c->cmd, c->send);
	
	fprintf(fp, "\\\n");

/*    if(!mongoc_collection_insert(collection, MONGOC_INSERT_NONE, doc, NULL, &error)) {
        printf("%s\n", error.message);
    }
    bson_destroy(doc);*/
}	

// Initial save procedure. 
void save_database(filename)
	char *filename;
{
	OBJECT *o;
	FILE *fp;
	int i = 0;
	
	if ((fp = fopen(filename,"w")) == NULL) {
		printf("ERROR: Could not create database... EXITING!\n");
		exit(0);
	}

	// set all players to !CONNECTED
	// 
	fprintf(fp, "%d\n", DB_VERSION);
	fprintf(fp, "%d\n", calculate_dbsize());
	for(i = 0; i < dbtop; i++) {
	  if(!(db[i]))
	    continue;
	  SAVE_COMBAT_OBJECT((db[i]));
	  if( ((db[i])->flags & TYPE_PLAYER) )
	    (db[i])->flags &= ~CONNECT;
	  OUT_OBJ(fp, (db[i]));
        }
	
	fclose(fp);
}

// Make a fresh new database.
void new_database(void)
{
	OBJECT *o;
	
	create_object(1, TYPE_ROOM, "Zero Room");
	o = create_object(1, TYPE_PLAYER, "Controller");
	o->powlvl = 100;
	o->flags |= WIZARD;
	SET_ATR(o, A_QUOTA, "1000");
	SET_ATR(o, A_CHANNELQUOTA, "1000");
	
	save_database(return_config("db_name"));
}

// fork a child process and dump databases.
void fork_and_dump()
{
	char buf[MAX_BUFSIZE], mail[MAX_BUFSIZE], com[MAX_BUFSIZE];
	char item[1024], itemt[1024];
	int child;
	
	log_channel("database", pstr("* (%s) Database dumped.",
		get_date(time(0))));

	// Where the database should go.
	sprintf(buf, "saved/%s.%d", return_config("db_name"), database_saves);
	sprintf(mail, "saved/%s.%d", return_config("maildb"), database_saves);
	sprintf(com, "saved/%s.%d", return_config("comdb"), database_saves);
	sprintf(item, "saved/%s.%d", return_config("item_db"), database_saves);
	sprintf(itemt, "saved/%s.%d", return_config("item_template"), database_saves);
	
	// If not enough memory, use a virtual fork.
	if ( (child = fork()) == -1)
		child = vfork();
	
	if (child == 0) {
		save_database(buf);
		save_database(return_config("db_name"));
		dump_mail(mail);
		dump_mail(return_config("maildb"));
		ComDBSave(return_config("comdb"));
		ComDBSave(com);
		SaveUserItems(item);
		SaveUserItems(return_config("item_db"));
		SaveTemplates(itemt);
		SaveTemplates(return_config("item_template"));
		_exit(0);
	} else if (child<0) {
		printf("[ERROR!] Database save failed.\n");
	}
}

