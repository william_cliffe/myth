/****************************************************************************
 MYTH - Examine Code
 Coded by Saruk (03/20/99)
 ---
 Your basic examine code with possible tweaks.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int g_has_attributes(victim)
	object *victim;
{
	attr *a;

	for(a = victim->alist; a; a=a->next)
	  return TRUE;

	return(0);	
}

void do_examine_attr(player, victim, attribute)
	object *player;
	object *victim;
	char *attribute;
{
	/* First check for builtins */
	if(string_prefix(attribute, "powlvl") || string_prefix(attribute, "powerlevel")) {
	  notify(player, pstr("Powerlevel: %d", victim->powlvl));
	  return;
	} else if (string_prefix(attribute, "description")) {
	  notify(player, pstr("Description: %s", victim->desc));
	  return;
	} else if (string_prefix(attribute, "location")) {
	  notify(player, pstr("Location: %s", color_name(victim->location, -1)));
	  return;
	} else if (string_prefix(attribute, "home") || string_prefix(attribute, "link")) {
	  notify(player, pstr("Home: %s", color_name(victim->linkto, -1)));
	  return;
	} else if (string_prefix(attribute, "name")) {
	  notify(player, pstr("Name: %s", color_name(victim, -1)));
	  return;
	} else if(string_prefix(attribute, "password") || string_prefix(attribute, "exitkeys")) {
	  if(player == rootobj || (victim->flags & TYPE_EXIT)) {
	    notify(player, pstr("ExitKeys/Password: %s", victim->pass));
	  } else {
	    notify(player, "* You can't see that...");
	  }
	  return;
	} else if(string_prefix(attribute, "owner")) {
	  notify(player, pstr("Owner: %s", color_name(victim->ownit, -1)));
	  return;
	} else if(string_prefix(attribute, "flags")) {
	  notify(player, pstr("Flags: %s", flaglist(victim)));
	  return;
	} else { // search the attribute list
	  atr_examine(player, victim, attribute);		
	  return;
	}

	error_report(player, M_ENOTFOUND);
}

void do_examine(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *victim, *o;
	CONTENTS *con;
	ATTR *a;
	
	if(!*arg1)
		arg1 = player->name;
	
	if(strchr(arg1, '/')) // player is looking for an attribute.
	{
		arg2 = rparse(arg1, '/');
		arg1 = fparse(arg1, '/');
	}
	
	if(!(victim = match_special(player, arg1))) 	
		if(!(victim=remote_match(player, arg1))) {  //match_object(arg1))) {
			notify(player, pstr("I don't see %s here", arg1));
			return;
		}
	
	if(!has_pow(player, victim, POW_SEEATR) && !can_control(player, victim)) {
		error_report(player, M_ENOCNTRL);
		return;
	}

	if(*arg2) {
		do_examine_attr(player, victim, arg2);
		return;
	}
		
	notify(player, pstr("%s (#%d%s)",color_name(victim, -1), victim->ref,
		flaglist(victim)));
	notify(player, victim->desc);
	notify(player, pstr("PowerLevel: %d", victim->powlvl));
	notify(player, pstr("Owner: %s (%d%s)\tCreated: %s", color_name(victim->ownit, -1),
		victim->ownit->ref, flaglist(victim->ownit), get_date(atol(atr_get(victim, A_CREATED)))));
	notify(player, pstr("Location: %s (%d%s)", color_name(victim->location, -1),
		victim->location->ref, flaglist(victim->location)));
	if((victim->flags & TYPE_EXIT)) {
		if(!victim->linkto) 
		  notify(player, "Link: Unlinked!");
		else
		  notify(player, pstr("Link: %s (%d%s)", color_name(victim->linkto, -1),
			victim->linkto->ref, flaglist(victim->linkto)));
		notify(player, pstr("ExitKeys: %s", victim->pass));
	} else {
		notify(player, pstr("Home: %s (%d%s)", color_name(victim->linkto, -1),
			victim->linkto->ref, flaglist(victim->linkto)));
	}
	
	/* List all attributes that are defined */
	/* Changing this over to global attribute struct as of 1/8/03 */
	if(g_has_attributes(victim))
		notify(player, "^ccAttributes^ch^cw:");

	/** Here's where we'll put the new inherency checks for attributes **/	
	for(a = victim->alist; a; a=a->next) {
	  if( (player->flags & WIZARD) || ((!(player->flags & WIZARD) && !(a->flags & ADARK)) && (a->value)) )  
	    ncnotify(player, pstr("%s(%s): %s",
	      a->name, aflaglist(a->flags), (a->flags & AEXIT) ? (o = match_object(a->value)) ? unparse_object(o) : a->value : a->value));
	}
	
	/* List all contents */
	if(victim->contents)
		notify(player, "Contents:");
	for(con = victim->contents; con; con=con->next)
		if( !(con->content->flags & TYPE_ROOM) && (con->content != victim) )
			notify(player, pstr("%s (#%d%s)", color_name(con->content, -1), con->content->ref,
			  flaglist(con->content)));
}

