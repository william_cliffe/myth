/****************************************************************************
 MYTH - Report System
 Coded by Saruk (02/12/99)
 ---
 This is a report system for displaying data on all different sections of the
 MYTH server.
 ****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <netdb.h>

#include "db.h"
#include "net.h"
#include "externs.h"

static char *WhatState(int state, int paste)
{
  if(state == 0)
    return("WAITCONNECT");
  else if (state == 1)
    return("WAITPASS");
  else if (state == 2)
    return("CONNECTED");
  else if (state == 3)
    return("RELOADCONNECT");
  else if (state == 4)
    return("SETNAME");
  else if (state == 5)
    return("SETPASS");
  else if (state == 6)
    return("SETEMAIL");
  else if (state == 7)
    return("BOOTITOFF");
  else if (state == 8)
    return("PASTE MODE");
  else
    return("UNKNOWN!!");
}

/* List the connections */
void do_connections(player)
	object *player;
{
	DESCRIPTOR *d;
	char *topic_list[]= {"DESC", "STATE", "NAME", "ADDR"};
	
 	notify(player, pstr("%-5.5s %-12.12s %-24.24s %-24.24s", topic_list[0], topic_list[1], topic_list[2], topic_list[3]));
	
	for(d = descriptor_list; d; d=d->next) {
		if(d->player)
			if((d->player->flags & INVISIBLE) && !can_control(player, d->player))
				continue;			
		notify(player, pstr("%-5d %-12.12s %s %-24.24s", d->descriptor, WhatState(d->state, d->paste_flag), ((d->player)) ? color_name(d->player, 24) : format_color("NOT CONNECTED", 24), d->addr));
	}
	
	notify(player, pstr("Max Descriptor is set to %d", maxd));	
}

void do_report(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	make_header(player, "--- Report System ---");
	notify(player, pstr("\tProcess ID.............: %d", getpid()));
	notify(player, pstr("\tRoot object............: %s\n", color_name(rootobj, -1)));

	clear_stack();
	notify(player, pstr("\tTotal Volatile Calls.........: %s", comma((long long)total_stack_calls)));
	notify(player, pstr("\tTotal Volatile Frees.........: %s", comma((long long)total_stack_frees)));
	notify(player, pstr("%s", make_ln("=", 78)));
}

void do_dbinfo(player)
	object *player;
{
	object *o;
	attr *a;
	int num_rooms, num_players, num_guests, num_things, num_exits, total_num, player_owned, pseudo_exits;
	int num_wizards, i = 0;
	
	pseudo_exits = num_rooms = num_players = num_guests = num_things = total_num = num_exits = 0;
	player_owned = num_wizards = 0;
	while(i < dbtop) {
	  if(db[i]) {
	    for(a = (db[i])->alist; a; a=a->next) 
	      if((a->flags & AEXIT))
	        pseudo_exits++;
	  }
	  i++;
	}
	
	for(o = room_list; o; o=o->next)
	{
		if(o->ownit == player) player_owned++;
		num_rooms++;
	}	
	
	for(o = player_list; o; o=o->next) {
		if(o->ownit == player) player_owned++;
		if(o->flags & GUEST)
			num_guests++;
		else
			num_players++;
		if(o->flags & WIZARD)
			num_wizards++;
	}
	for(o = exit_list; o; o=o->next) {
		if(o->ownit == player) player_owned++;
		num_exits++;
	}
	for(o = thing_list; o; o=o->next) {
		if(o->ownit == player) player_owned++;		
		num_things++;
	}
		
	total_num = num_players + num_rooms + num_guests + num_things + num_exits;
	
	make_header(player, "--- Database Information ---");
	notify(player, pstr("\tObjects owned by you...: %d", player_owned));
	notify(player, pstr("\tNumber of objects......: %d", total_num));
	notify(player, pstr("\tNumber of Wizards......: %d\n", num_wizards));
	notify(player, pstr("\tNumber of rooms........: %d", num_rooms));
	notify(player, pstr("\tNumber of exits........: %d", num_exits));
	notify(player, pstr("\tNumber of things.......: %d", num_things));
	notify(player, pstr("\tNumber of guests.......: %d", num_guests));
	notify(player, pstr("\tNumber of players......: %d", num_players));
	notify(player, pstr("\tNumber of pseudo exits.: %d", pseudo_exits));
	notify(player, pstr("%s", make_ln("=", 78)));
}


void do_info(player, arg1)
	object *player;
	char *arg1;
{

	if(!strcmpm(arg1, "mem")) {
	  do_report(player, NULL, NULL);
	  return;
	} else if (!strcmpm(arg1, "db")) {
	  do_dbinfo(player);
	  return;
	} else if (!strcmpm(arg1, "config")) {
	  list_config(player);
	  return;
	}
	
	notify(player, "Syntax: @info mem|db|config");
}


int check_flags(what, flaglist)
	object *what;
	char *flaglist;
{
	char *p;
	int i, flags = 0;
	
	for(p = flaglist; *p; p++) {
	  for(i = 0; flag_table[i].name; i++) {
	    if(*p == flag_table[i].parsed) {
	      if((what->flags & flag_table[i].flag))
	        flags = TRUE;
	      else
	        flags = FALSE;
	    }
	  }
	  if(!flags)
	    return(FALSE);
	}

	return(flags);
}


void do_search(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *what, *o;
	int class = 0, type, i, found;
	
	if(!*arg1) {
	  what = player;
	  class = 0;
	} else if (!strcmpm(arg1, "TYPE")) {
	  class = 1;
	  if(!*arg2) {
	    notify(player, "What are you looking for?");
	    return;
	  }
	  if(string_prefix(arg2, "ALL"))
	    type = 0;
	  else if(string_prefix(arg2, "ROOM"))
	    type = TYPE_ROOM;
	  else if(string_prefix(arg2, "THING"))
	    type = TYPE_THING;
	  else if(string_prefix(arg2, "EXITS"))
	    type = TYPE_EXIT;
	  else if(string_prefix(arg2, "PLAYERS"))
	    type = TYPE_PLAYER;
	  else {
	    notify(player, "I don't know that type.");
	    return;
	  }
	} else if (!strcmpm(arg1, "FLAGS")) {
	  class = 2;
	  if(!*arg2) {
	    notify(player, "What flag?");
	    return;
	  }
	} else if (!strcmpm(arg1, "ALL")) {
	  class = 1;
	  type = 0;
	} else {
	  if(!(what = remote_match(player, arg1))) {
	    notify(player, "Bad @search argument.");
	    return;
	  }
	  if(!can_control(player, what) && !has_pow(player, what, POW_REMOTE)) {
	    notify(player, "You cannot @search that.");
	    return;
	  }
	  class = 3;
	}
	
	if(!class) {
	  for(i = 0; i < dbtop; i++) {
	    if(!db[i])
	      continue;
	    
	    o = db[i];
	    if(o->ownit == what) 
	      notify(player, pstr("%s", unparse_object(o)));
	  }
	} else if (class == 1) {
	  for(i = 0; i < dbtop; i++) {
	    if(!db[i])
	      continue;
	    
	    o = db[i];
	    if(!type) {
	      if(can_control(player, o) || has_pow(player, o, POW_REMOTE))
	        notify(player, pstr("%s", unparse_object(o)));
	    } else {
	      if((o->flags & type) && (can_control(player, o) || has_pow(player, o, POW_REMOTE)))
	        notify(player, pstr("%s", unparse_object(o)));
	    }
	  }
	} else if (class == 2) {
	  found = 0;
	  for(i = 0; i < dbtop; i++) {
	    if(!db[i])
	      continue;
	      
	    o = db[i];
	    if(check_flags(o, arg2)) {
	      notify(player, pstr("%s", unparse_object(o)));
	      found = TRUE;
	    }
	  }
	  if(!found)
	    notify(player, "Nothing found.");
	} else if (class == 3) {
	  for(i = 0; i < dbtop; i++) {
	    if(!db[i])
	      continue;
	    
	    o = db[i];
	    if(o == what || o->ownit == what)
	      notify(player, pstr("%s", unparse_object(o)));
	  }
	}
}

/* Eventually we'll make this @find via attributes, etc. */
void do_find(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	int i;
	
	notify(player, "Find...");
	for(i=0;i<dbtop;i++) {
	  if(db[i]) {
	    if(string_prefix(arg1, (db[i])->name))
	      notify(player, pstr("%s", (can_control(player, db[i])) ? unparse_object(db[i]) : color_name(db[i], -1)));
	  }
	}
}

