/****************************************************************************
 MYTH - Lock Parser
 Coded by Saruk (03/30/99)
 ---
 Parse the lock system on MYTH.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "db.h"
#include "net.h"
#include "externs.h"

char *get_chunk(char *instr, int start, int end)
{
	static char buf[MAX_BUFSIZE];
	char *p = buf, *q;
	
	for(p = instr, q = p; *p; p++)
		if(*p == start)
			q = p;
		else if (*p == end)
			break;

	*p = '\0';
	q++;
	
	return (q);
}

int lock_power(player, value)
	object *player;
	char *value;
{
	char buf[512];
	char *p;
	int tag = 0, ref;
	
	strcpy(buf, get_chunk(value, '[', ']'));
	p = buf;
		
	if(*p == '<')
		p++, tag = 1;
	else if (*p == '>')
		p++, tag = 2;
	
	if(!isanumber(p))
		return 0;
	
	ref = match_num(p);
	
	if(!tag) {
		if(player->powlvl == ref)
			return 1;
		else
			return 0;
	} else if (tag == 1) {
		if(player->powlvl <= ref)
			return 1;
		else 
			return 0;
	} else if (tag == 2) {
		if(player->powlvl >= ref)
			return 1;
		else
			return 0;
	}
	
	return 0;
}

int lock_flag(player, flagstr)
	object *player;
	char *flagstr;
{
	char buf[MAX_BUFSIZE];
	int ctr;
	
	strcpy(buf, flagstr);
	strcpy(buf, get_chunk(buf, '[', ']'));
		
	for(ctr = 0; flag_table[ctr].name; ctr++)
		if(buf[0] == flag_table[ctr].parsed)
			if(player->flags & flag_table[ctr].flag)
				return 1;
	return 0;
}

// For use wwith '!' (NOT), returns 1 if NOT, returns 0 if IS
int match_false(player, check)
	object *player;
	object *check;
{
	if(player == check)
		return 0;
	else
		return 1;
}

// If player is check, return 1 - else return 0...
int match_truth(player, check)
	object *player;
	object *check;
{
	if(player == check)
		return 1;
	else
		return 0;
}

int lock_function(player, str)
	object *player;
	char *str;
{
	LOCKS *l;
	char buf[MAX_BUFSIZE];
	char *p = buf;
	int ctr;
	
	while(*str && *str != '[') {
		*p = *str;	
		p++, str++;
	}
	*p++='\0';
	
	for(ctr = 0; lock_table[ctr].name; ctr++) {
		l = &lock_table[ctr];
		if(!strcmpm(l->name, buf)) {
			strcpy(buf, str);
			return l->func(player, buf);
		}
	}

	return 0;	
}



char *lock_parse(player, str)
	object *player;
	char *str;
{
	char buf[MAX_BUFSIZE];
	char *p = buf, *c;
	
	while(*str && *str != ']')
		*p++ = *str++;
	
	return(p);
}

/**
 This is where we check if we need to evaluate functions, if we do we'll see a [, 
 which we can then take until we see a new ].
 
 @LOCK_SET #266=#6&#2&#3&#4
 Example: Lock is: [get(#266,LOCK_SET)]
  When evaluated, it will return:
  	#6&#2&#3&#4 
**/
int check_truefalse(player, str)
	object *player;
	char *str;
{
	object *check;
	char *s;
	
	// We've pronouned and figured things out to this point.
	// Hmm.
	s = str;
	while(isdigit(*s)) s++;
	if(!s || !*s)
	  return atoi(str);
	
	if(*str == '!') {
		if( (check = match_object(str+1)) ) {
			return match_false(player, check);
		} else	
			return 1;
	} else {
		if( (check = match_object(str)) )
			return match_truth(player, check);
		else
			return 0;
	}
	
	return 0;
}

char *strcheck(player, str)
	object *player;
	char *str;
{
	if( (!strcmp(str, "+")) || (!strcmp(str, "-")))
		return str;
	
	// let's check if we're trying to check for !+ or !- (return opposite)
	if( *str == '!') {
		if(!strcmpm(str + 1, "+"))
			return "-";
		else if (!strcmpm(str + 1, "-"))
			return "+";
	}
	
	if(!check_truefalse(player, str))
		return "-";
	else
		return "+";
}

char *parse_them(player, str)
	object *player;
	char *str;
{
	if(strchr(str,'&')) {
		if(!strcmp(strcheck(player, fparse(str, '&')), "+") && !strcmp(strcheck(player, rparse(str, '&')), "+"))
			return "+";
		else
			return "-";
	} else if(strchr(str, '|')) {
		if(!strcmp(strcheck(player, fparse(str, '|')), "+") || !strcmp(strcheck(player, rparse(str, '|')), "+"))
			return "+";
		else
			return "-";
	} else {
		return strcheck(player, str);
	}
}

char *evaluate_inside(player, str)
	object *player;
	char *str;
{
	static char retstr[MAX_BUFSIZE];
	char buf[MAX_BUFSIZE], buf2[MAX_BUFSIZE];
	char buf3[MAX_BUFSIZE], buf4[MAX_BUFSIZE];
	char buf5[MAX_BUFSIZE];
	char *p, *q, *x;
	
	if(!strchr(str, '&') && !strchr(str, '|'))
		return strcheck(player, str);
	
	if(strchr(str, '&')) {
		strcpy(buf, fparse(str, '&'));
		strcpy(buf2, rparse(str, '&'));
		
		strcpy(buf3, buf);
		for(p = buf3, q = p; *p; p++)
			if(*p == '|')
				q = p;

		strcpy(buf3, buf3 + (strlen(buf) - strlen(q)));
		buf[strlen(buf) - strlen(buf3)] = '\0';
		
		
		strcat(buf3, "&");
		strcpy(buf4, buf2);
		p = strlen(buf3) + buf3;
		x = buf4;
		while(*x && *x != '|' && *x != '&') {
			*p = *x;
			x++, p++;
		}
		strcpy(buf4, buf4 + (strlen(buf4) - strlen(x)));
		strcpy(buf3, parse_them(player, buf3));
		
		sprintf(buf5, "%s%s%s", buf, buf3, buf4);
	} else if (strchr(str, '|')) {
		strcpy(buf, fparse(str, '|'));
		strcpy(buf2, rparse(str, '|'));
		strcat(buf, "|");
		strcpy(buf3, buf2);
		p = strlen(buf) + buf;
		x = buf3;
		while(*x && *x != '|' && *x != '&') {
			*p = *x;
			x++, p++;
		}
		strcpy(buf2, buf2 + (strlen(buf2) - strlen(x)));
		sprintf(buf5, "%s%s", parse_them(player, buf), buf2);
	}

	if(strchr(buf5, '&') || strchr(buf5, '|')) 
		strcpy(retstr, evaluate_inside(player, buf5));
	else
		strcpy(retstr, buf5);
	
	return retstr;
}


char *innermost_paren(str)
	char *str;
{
	char *p, *s;
	
	for(p = str, s = p; *p; p++)
		if(*p == '(')
			s = p;
		else if (*p == ')')
			break;
	
	return (s);		
}

char *evaluate_lock(player, str)
	object *player;
	char *str;
{
	static char buf[MAX_BUFSIZE];
	char savestr[MAX_BUFSIZE], buf2[MAX_BUFSIZE];
	char buf3[MAX_BUFSIZE];
	char *p, *x = buf2;
	
	strcpy(savestr, str);
	if(!strchr(savestr, '('))
		return evaluate_inside(player, savestr);
		
	p = strstr(str, innermost_paren(str));
	savestr[strlen(savestr) - strlen(p)] = '\0';
	p++;
	for (;*p; p++, x++)
		if(*p == ')')
			break;
		else
			*x = *p;
	*x++ = '\0';
	p++;
	
	sprintf(buf3, "%s%s%s", savestr, evaluate_inside(player, buf2),p);

	if(strchr(buf3, '('))
		strcpy(buf3, evaluate_lock(player, buf3));
	if (strchr(buf3, '&')||strchr(buf3, '|')||strchr(buf3, '!'))
		strcpy(buf3, evaluate_lock(player, buf3));
	
	strcpy(buf, buf3);
	return buf;
}

int parse_lock(player, str)
	object *player;
	char *str;
{
	char savestr[MAX_BUFSIZE], buf[MAX_BUFSIZE];
	char *bf = buf;
	char *p;
	
	if(!str || !*str)
		return(TRUE);
	
	//DEBUG(pstr("%s", str));
	
	for(p = str; *p; p++)
		if(*p == '[')
		{
			pronoun_substitute(buf, player, str, player);
			bf = buf + strlen(player->name) + 1;
			str = bf;
		}	

	
	if(!str || !*str)
		return 1;
	
	//DEBUG(pstr("%s", str));
	
	if(!strchr(str, '(') && !strchr(str, '&') && !strchr(str, '|'))
		return check_truefalse(player, str);

	strcpy(savestr, str);
	strcpy(savestr, evaluate_lock(player, savestr));
	//DEBUG(pstr("savestr: %s"));
	if(!strcmp(savestr, "+"))
		return 1;
	else
		return 0;
	return 0;	
}
