/***********************************
 MYTH - Configuration Tools
 Coded by Saruk (01/20/99)
 ---
 This will allow MYTH users to setup
 their MYTH, as well as creating a 
 new database. The new database 
 command is separate just to be safe.
 ***********************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "db.h"
#include "net.h"
#include "externs.h"
#include "tools.h"

void add_config(FILE *f, int index)
{
	char instr[MAX_BUFSIZE];
	
	printf("%s [%s]: ",config_table[index].cfg_prompt, config_table[index].cfg_default);
	if(fgets(instr, sizeof(instr), stdin)==NULL)
		return;
	if(instr[strlen(instr)-1]=='\n')
		instr[strlen(instr)-1] = '\0';
	if(instr[0])
		fprintf(f,"%s %s\n",config_table[index].cfg_designation,instr);
	else
		fprintf(f,"%s %s\n",config_table[index].cfg_designation, 
			config_table[index].cfg_default);
	
}

void setup_myth(void)
{
	FILE *f;
	int index; 
	
	if ( (f = fopen("myth.cnf","w")) == NULL) {
		printf("Error creating myth config... exiting.\n");
		return;
	}
	
	// call the table to define config stuff.
	for(index = 0; 	config_table[index].cfg_prompt;index++)
		add_config(f, index);
	
	fclose(f);
	

}

char *return_config(char *designation)
{
	int index;
	
	for(index = 0; config_table[index].cfg_prompt;index++)
		if(!strcmp(config_table[index].cfg_designation, designation)) 
			return config_table[index].cfg_default;
		
	return NULL;
}

void list_config(player)
	object *player;
{
	int index;
	
	for(index = 0; config_table[index].cfg_prompt; index++)
	  if(strcmpm(config_table[index].cfg_designation, "default_password") || (player->flags & WIZARD))
	  notify(player, pstr("%-24.24s %-32.32s", config_table[index].cfg_designation, return_config(config_table[index].cfg_designation)));
}

int store_config(char *designation, char *putstr)
{
	int index;
	int true = 0;
	
	for(index = 0; config_table[index].cfg_prompt; index++)
		if(!strcmp(config_table[index].cfg_designation, designation)) {
			strcpy(config_table[index].cfg_default = (char *)malloc(strlen(putstr)+1),putstr);
			true = 1;
		}
	
	
	if (!true) {
		printf("ERROR: Configuration file error! Please rerun the config parameter!\n");
		return 0;
	}
	
	return 1;
}

// move this to a string utility file later.
char *front(char *instr)
{
	static char buf[MAX_BUFSIZE];
	char *p = buf;
	
	strcpy(buf, instr);
	
	for(p = buf; *p; p++)
		if(*p == ' ')
			break;
	
	buf[strlen(buf) - strlen(p)] = '\0';
	return buf; //stralloc(buf);
}

char *restof(char *instr)
{
	static char buf[MAX_BUFSIZE];
	char *p = buf;
	
	strcpy(buf, instr);
	if(buf[strlen(buf) - 1] == '\n')
		buf[strlen(buf) - 1] = '\0';
		
	for(p = buf; *p; p++)
		if(*p == ' ') {
			p++;
			break;
		}
	
	if(*p)
		return p; //stralloc(p);
	else
		return "";
}

int load_config(void)
{
	FILE *f;
	char buf[MAX_BUFSIZE];
	
	if ( (f=fopen("myth.cnf", "r")) == NULL) {
		printf("ERROR: Cannot find myth.cnf\n");
		return 0;
	}	

	while(fgets(buf, sizeof(buf), f))
		store_config(front(buf),restof(buf));

	fclose(f);	
	return 1;
}
