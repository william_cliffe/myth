/*** comsys.c $ MYTH ***/
/**
  The channel system should be saved using the following format:
  
  ^CHANNEL_DB_VERSION
  $CHANNEL_NAME
  CHANNEL_FLAGS
  #CHANNEL_OWNER_DBREF
  CHANNEL_TIME
  -#1 #2 #4 #5
  +#7 #9 #3 #6 #8 #10 #123
  \
  ^END
  
  If a channel is created and there is NO ban list and NO contents:
  $CHANNEL_NAME
  CHANNEL_FLAGS
  -
  +
  \
 
  !! NOTE: Please note that IsOnChannel and HasChannelOn are not in reference to the same thing.
    IsOnChannel refers to whether the player has that channel in their list.
    HasChannelOn refers to whether the player has that channel turned ON.
**/
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/** Variables **/
COMOBJECT *comsys = NULL;
COMOBJECT *comsystail = NULL;
float com_db_version = 0.0;

/*** CHANNEL FLAGS ***/
struct com_flags_t comflags_table[] = {
 {"Admin", COM_ADMIN},
 {"Log", COM_LOG},
 {"Server", COM_SERVER},
 {"Notalk", COM_NOTALK},
 {"Recall", COM_RECALL},
 {"Dump", COM_DUMP},
 {NULL}};

void AddRecall P((COMOBJECT *, char *, int));

/*** CHANNEL TRUE/FALSE FUNCTIONS ***/

/** IsIgnored():

    ?? We'll use parse_lock on an attribute to figure out if we're ignoring a player.
    ?? For now we'll assume no ignores.
**/
int IsIgnored(object *victim, object *player)
{
  return FALSE;
}

/** IsComBanned():
**/
int IsComBanned(COMOBJECT *channel, object *player)
{
  COMBAN *ban;
  
  if(!channel->banlist)
    return FALSE;
  
  for(ban = channel->banlist; ban; ban=ban->next)
    if(ban->player == player)
      return TRUE;
  
  return FALSE;
}

/** IsComAdmin():
**/
int IsComAdmin(COMOBJECT *channel)
{
  if(channel->flags & COM_ADMIN)
    return TRUE;

  return(FALSE);
}

int IsOnChannel(COMOBJECT *channel, object *player)
{
  COMWHO *w;
  for(w = channel->wholist; w; w=w->next)
    if(w->player == player)
      return TRUE;
  
  return FALSE;
}

int HasChannelOn(COMOBJECT *channel, object *player)
{
  COMWHO *w;
  
  for(w = channel->wholist; w; w=w->next)
    if(w->player == player && w->state == CHAN_STATE_ON)
      return TRUE;
  
  return FALSE;
}

COMOBJECT *FindChannel(char *channel)
{
  COMOBJECT *list;
  
  for(list = comsys; list; list=list->next) {
    if(!strcmp(strip_color(list->name), channel)) {
      return(list);
    }
  }
      
  return(NULL);
}

/*** SOME BASIC CHANNEL FUNCTIONS ***/
/** ComNotify():
**/
void ComNotify(COMOBJECT *channel, char *text)
{
  COMWHO *w;
  char buf[8192];
  
  if( (channel->flags & COM_RECALL) )
    AddRecall(channel, text, 1);
  
  sprintf(buf, "^ch^cw[\033[0m%s^ch^cw]\033[0m %s", channel->name, text);
  
  channel->total_lines += 1;
  
  for(w = channel->wholist; w; w=w->next) {
    if( (w->state == CHAN_STATE_ON) && (w->player->flags & CONNECT) && !(w->player->flags & QUIET)) 
      notify(w->player, buf);
  }
  
}


/*** CHANNEL CREATE FUNCTIONS ***/

/** ServerComCreate(): Create's a channel with no permission checking.
**/
COMOBJECT *ServerComCreate(char *channel_name, object *creator)
{
  COMOBJECT *c;
  
  if(!channel_name || !*channel_name)
    return(NULL);
  
  GENERATE(c, COMOBJECT);
  c->name = NULL;
  SET(c->name, channel_name);
  c->banlist = c->bantail = NULL;
  c->recall = c->recalltail = NULL;
  c->created = 0;
  c->creator = creator->ref;
  c->wholist = c->whotail = NULL;
  c->recall_lines = 0;
  c->total_lines = 0;
  c->flags = 0x0;
  c->flags |= COM_RECALL;	// All channels start off COM_RECALL.
  
  if(!comsys) {
    c->next = c->prev = NULL;
    comsys = comsystail = c;
  } else {
    comsystail->next = c;
    c->prev = comsystail;
    c->next = NULL;
    comsystail = c;
  }
  
  return(c);
}

/** ComCreate(): Permission-checking channel creator.
**/
COMOBJECT *ComCreate(player, channel_name)
  object *player;
  char *channel_name;
{
  COMOBJECT *c;
  int quota;
  
  if((c = FindChannel(channel_name)))
    return(NULL);
  
  if(!(quota = atoi(atr_get(player, A_CHANNELQUOTA))))
    return(NULL);
  
  if(!(c = ServerComCreate(channel_name, player)))
    return(NULL);
  
  c->owner = player;
  SET_ATR(player, A_CHANNELQUOTA, pstr("%d", (quota-1)));
  
  c->created = time(0);

  notify(player, pstr("You have created the channel: %s", c->name));
  return(c);
}


/*** CHANNEL BAN FUNCTIONS ***/

/** ComAddBan(): Add a single dbref to a channel's ban list
**/
int ComAddBan(COMOBJECT *channel, char *dbref)
{
  object *who;
  COMBAN *nban;
  
  if(!(who = match_object(dbref)))
    return FALSE;
  
  GENERATE(nban, COMBAN);
  nban->player = who;
  
  if(!channel->bantail) {
    nban->next = nban->prev = NULL;
    channel->banlist = channel->bantail = nban;
  } else {
    channel->bantail->next = nban;
    nban->prev = channel->bantail;
    nban->next = NULL;
    channel->bantail = nban;
  }
  
  return TRUE;
}

/** ComAddBanList(): Parse and add a list of banned dbrefs.
**/
int ComAddBanList(channel, list)
  COMOBJECT *channel;
  char *list;
{
  char *k;
  
  if(!list || !*list)
    return(TRUE);
  
  while( (k = parse_up(&list, ' '))) {
    if(!ComAddBan(channel, k))
      printf("* Error banning %s from channel %s.\n", k, channel->name);
  }
  
  return TRUE;
}

/** ComRemoveBan():
**/
void ComBanRemove(COMOBJECT *c, COMBAN *ban)
{
  if(ban->next)
    ban->next->prev = ban->prev;
    
  if(ban->prev)
    ban->prev->next = ban->next;
  
  if(c->banlist == ban)
    c->banlist = ban->next;
  
  if(c->bantail == ban)
    c->bantail = ban->prev;
    
  block_free(ban);
}

/*** ADD TO WHO LIST ***/

/** ComAddWho(): Add the who list to the channel on a db load.
**/
COMWHO *ComAddWho(COMOBJECT *channel, char *dbref)
{
  COMWHO *w;
  object *player;
  
  if(!(player = match_object(dbref)))
    return(NULL);
  
  GENERATE(w, COMWHO);
  w->player = player;
  w->state = CHAN_STATE_ON;
  
  if(!channel->wholist) {
    w->next = w->prev = NULL;
    channel->wholist = channel->whotail = w;
  } else {
    channel->whotail->next = w;
    w->prev = channel->whotail;
    w->next = NULL;
    channel->whotail = w;
  }
  
  return(w);
}

/** ComAddWhoList(): Parse and add a list of who's on channel.
**/
int ComAddWhoList(channel, list)
  COMOBJECT *channel;
  char *list;
{
  COMWHO *w = NULL;
  char *k;
  
  if(!list || !*list)
    return(TRUE);
    
  while( (k = parse_up(&list, ' '))) {
    if(*k == '.') {
      if(!(w = ComAddWho(channel, k+1))) {
        printf("** Error adding player to channel who list!\n");
        return FALSE;
      }
      w->state = CHAN_STATE_OFF;
    } else {
      if(!(w = ComAddWho(channel, k))) {
        printf("** Error adding player to channel who list!\n");
        return FALSE;
      }
    }
  }
  
  return(TRUE);
}

void SetDefaultChannel(player, channel)
  object *player;
  COMOBJECT *channel;
{
  SET_ATR(player, A_DEFCHANNEL, strip_color(channel->name));
  notify(player, "Channel default set.");
}

/*** CHANNEL RECALL FUNCTIONS ***/

/** RemoveRecallLine():
**/
void RemoveRecallLine(COMOBJECT *channel)
{
  COMRECALL *cr; // channel recall
  
  if(!(cr = channel->recall))
    return;

  if(cr->next)
    cr->next->prev = NULL;

  channel->recall = cr->next;

  if(cr->buffer)
    block_free(cr->buffer);

  channel->recall_lines--;
  
  block_free(cr);  
}

/** AddRecall():
**/
void AddRecall(COMOBJECT *channel, char *buffer, int status)
{
  COMRECALL *cbuf;
  char buf[MAX_BUFSIZE];
  
  if(!buffer || !*buffer)
    return;
  
  if(status)
    sprintf(buf, "(%s) %s", get_timestamp(time(0)), buffer);
  else
    strcpy(buf, buffer);
    
  if(channel->recall_lines >= MAX_COM_RECALL) 
    RemoveRecallLine(channel); // Remove the first recall line.

  GENERATE(cbuf, COMRECALL);
  cbuf->buffer = NULL;
  //SET(cbuf->buffer, strip_color(buf));
  SET(cbuf->buffer, buf);
  cbuf->len = strlen(cbuf->buffer);
  
  channel->recall_lines++;
  
  if(!channel->recall) {
    cbuf->next = cbuf->prev = NULL;
    channel->recall = channel->recalltail = cbuf;
  } else {
    channel->recalltail->next = cbuf;
    cbuf->prev = channel->recalltail;
    cbuf->next = NULL;
    channel->recalltail = cbuf;
  }
}

/** RemoveComRecalls():
**/
void RemoveComRecalls(COMOBJECT *c)
{
  COMRECALL *r, *rnext;
  
  for(r = c->recall; r; r=rnext)
  {
    rnext = r->next;
    RemoveRecallLine(c);
  }
}

/** ChannelFlushRecall():
**/
void ChannelFlushRecall(object *player, COMOBJECT *channel)
{
  if(!can_control(player, channel->owner)) {
    notify(player, "You cannot flush that channel!");
    return;
  }
  
  RemoveComRecalls(channel);
  notify(player, pstr("Channel [%s] recall buffer has been flushed!", channel->name));
}

/** RecallChannel(): Recall the lines
**/
void RecallChannel(object *player, COMOBJECT *channel, char *arg2)
{
  COMRECALL *lines;
  int nl = atoi(arg2), start, idx;
  
  if(!channel->recall) {
    notify(player, "There are no lines to recall.");
    return;
  }

  if(!strcmpm(arg2, "flush"))
  {
    ChannelFlushRecall(player, channel);
    return;
  }

  if(channel->recall_lines <= nl)
    start = 0;
  else
    start = channel->recall_lines - nl;
  
  if(nl == 0)
    start = 0;
  
  notify(player, pstr("[%s\033[0m] ^ch^cw--- ^ccChannel Recall Start ^cw---", channel->name));
  for(idx = 0, lines = channel->recall; lines; lines=lines->next, idx++) {
    if(start <= idx)
      notify(player, lines->buffer);
  }
  notify(player, pstr("[%s\033[0m] ^ch^cw--- ^ccChannel Recall End ^cw---", channel->name)); 
}


/*** CHANNEL LIST FUNCTIONS ***/

/** WhoChannel():
**/
void WhoChannel(channel, player)
  COMOBJECT *channel;
  object *player;
{
  COMWHO *c;
  
  notify(player, pstr("Players on ^ch^cy%s^cw:", channel->name));
  for(c = channel->wholist; c; c=c->next) {
    if(can_control(player, c->player) && (c->player->flags & CONNECT) && HasChannelOn(channel, c->player))
      notify(player, pstr("%s \033[0mis on channel ^ch^cy%s\033[0m.", color_name(c->player, -1), channel->name));
  }
  notify(player, "^ch^cw---");
}

/** ListComBanned():
**/
char *ListComBanned(COMBAN *banlist)
{
  char buf[8192];
  COMBAN *b;
  
  buf[0] = '\0';
  
  for(b = banlist; b; b=b->next)
    strcat(buf, pstr("#%d ", b->player->ref));
  
  return(stralloc(buf));
}

/** ListComWho():

    Eventually we'll save the data like so: #%d.%d (player->ref.state)
**/
char *ListComWho(COMWHO *wholist)
{
  char buf[8192];
  COMWHO *w;
  
  buf[0] = '\0';
  
  for(w = wholist; w; w=w->next) {
    if(w->state == CHAN_STATE_OFF)
      sprintf(buf + strlen(buf), ".#%d ", w->player->ref);
    else
      sprintf(buf + strlen(buf), "#%d ", w->player->ref);
  }
  
  return(stralloc(buf));
}

/** ListChannels(): List all the channels 
**/
void ListChannels(player)
  object *player;
{
  COMOBJECT *list;
  
  for(list = comsys; list; list=list->next)
    notify(player, pstr("%s\033[0m", list->name));
}

/** ListPlayerChannels():
**/
void ListPlayerChannels(player)
  object *player;
{
  COMOBJECT *list;
  COMWHO *w;
  char buf[1024];
  
  strcpy(buf, "");
  notify(player, "Your Channel List:");
  for(list = comsys; list; list=list->next)
    for(w = list->wholist; w; w=w->next) {
      if(w->player == player) {
        if(*buf) 
          strcat(buf, "\n");
        sprintf(buf + strlen(buf), "%s\033[0m", list->name);
      }
    }
  
  if(!*buf) {
    notify(player, "You have no channels");
    return;
  }
  
  notify(player, buf);
}

/** ViewChannel():
**/
void ViewChannel(COMOBJECT *channel, object *player)
{
  int see_all;
  
  if(!can_control(player, channel->owner)) 
    see_all = FALSE;
  else
    see_all = TRUE;
  
  notify(player, "--- Channel View ---");
  notify(player, pstr("Channel: %s \t Owner: %s", channel->name, (see_all) ? unparse_object(channel->owner) : color_name(channel->owner, -1)));
  notify(player, pstr("Total # of Lines: %ld", channel->total_lines));
}

/*** CHANNEL JOIN/LEAVE FUNCTIONS ***/

/** JoinChannel():
**/
void JoinChannel(channel, player)
  COMOBJECT *channel;
  object *player;
{
  COMWHO *w;
  
  if(IsComBanned(channel, player)) {
    notify(player, "You're banned from that channel.");
    return;
  }
  
  if(IsComAdmin(channel) && !(player->flags & WIZARD)) {
    notify(player, "This channel is reserved for admin.");
    return;
  }
  
  if(IsOnChannel(channel, player)) {
    notify(player, "You're already on that channel!");
    return;
  }
  
  // Notify the channel before he's on so that we're not notifying the joiner.
  ComNotify(channel, pstr("* %s just joined the channel.", color_name(player, -1)));  
  
  GENERATE(w, COMWHO);
  w->player = player;
  w->state = CHAN_STATE_ON;
  
  if(!channel->wholist) {
    w->next = w->prev = NULL;
    channel->wholist = channel->whotail = w;
  } else {
    channel->whotail->next = w;
    w->prev = channel->whotail;
    w->next = NULL;
    channel->whotail = w;
  }

  notify(player, pstr("You just joined channel %s.", channel->name));
  
}

/** RemoveFromChannel():
**/
void RemoveFromChannel(COMOBJECT *channel, COMWHO *w)
{
  if(w->prev)
    w->prev->next = w->next;

  if(w->next)
    w->next->prev = w->prev;
    
  if(channel->wholist == w)
    channel->wholist = w->next;
  
  if(channel->whotail == w)
    channel->whotail = w->prev;
  
  block_free(w);
}

/** LeaveChannel():
**/
void LeaveChannel(channel, player)
  COMOBJECT *channel;
  object *player;
{
  COMWHO *w = NULL;
  
  if(!IsOnChannel(channel, player)) {
    notify(player, "You are not on that channel!");
    return;
  }

  for(w = channel->wholist; w; w=w->next)
    if(w->player == player)
      break;

  if(!w)
    return;
  
  notify(player, pstr("You are no longer on channel %s", channel->name));
  w->state = CHAN_STATE_OFF;
  ComNotify(channel, pstr("* %s has left the channel.", color_name(player, -1)));
  RemoveFromChannel(channel, w);  
}

/** RemoveFromChannels():
**/
void RemoveFromChannels(object *player)
{
  COMOBJECT *list;
  COMWHO *w, *wnext = NULL;
  
  for(list = comsys; list; list=list->next) {
    wnext = NULL;
    for(w = list->wholist; w; w=wnext) {
      wnext = w->next;
      if(w->player == player)
        RemoveFromChannel(list, w);
    }  
  }
}


/*** CHANNEL DESTROY FUNCTIONS  ***/
/** Including removal of all Recall, Ban, and WHO lists. **/


void RemoveComBans(COMOBJECT *c)
{
  COMBAN *b, *bnext;
  
  for(b = c->banlist; b; b=bnext)
  {
    bnext = b->next;
    ComBanRemove(c, b);
  } 
}

void RemoveComWhos(COMOBJECT *c)
{
  COMWHO *w, *wnext;

  for(w = c->wholist; w; w=wnext)
  {
    wnext = w->next;
    RemoveFromChannel(c, w);
  }  
}

/** ComDestroy(): 
**/
void ComDestroy(COMOBJECT *c)
{
  if(c->next)
    c->next->prev = c->prev;

  if(c->prev)
    c->prev->next = c->next;

  if(comsys == c)
    comsys = c->next;

  if(comsystail == c)
    comsystail = c->prev;

  if(c->recall)
    RemoveComRecalls(c);

  if(c->banlist)
    RemoveComBans(c);
    
  if(c->wholist)
    RemoveComWhos(c);
  
  if(c->name)
    block_free(c->name);
    
  block_free(c);
}


/** DestroyChannel():
**/
void DestroyChannel(COMOBJECT *channel, object *player)
{
  if(!can_control(player, channel->owner)) {
    notify(player, "You cannot destroy that channel.");
    return;
  }
  
  ComDestroy(channel);
  notify(player, "You have destroyed that channel.");
}

/** CheckChannels():
**/
void CheckChannels(object *player)
{
  COMOBJECT *c, *cnext;
  
  for(c = comsys; c; c=cnext)
  {
    cnext = c->next;
    if(c->owner == player) {
      c->creator = rootobj->ref;
      c->owner = rootobj;
    }
  }
}

/*** CHANNEL SPEECH FUNCTIONS ***/

/** ComToPlayer(): 
**/
char *ComToPlayer(COMOBJECT *channel, object *player, char *chat)
{
  static char retbuf[2048];
  object *who;

  if(!*chat) 
    return pstr("%s", color_name(player, -1));
  

  retbuf[0] = '\0';
  
  /** !! CHANGE THIS TO ChannelMatch() SOON!!! **/
  if(!(who = match_object(front(chat)))) {
    sprintf(retbuf, "%s says, \"'%s\".",  color_name(player, -1), chat);
    return(retbuf);
  }
  
  if(!HasChannelOn(channel, who))
    notify(player, "That player is not on the channel.");
  
  switch(*restof(chat)) {
    case ':':
      sprintf(retbuf, "[to %s] %s %s",  color_name(who, -1), color_name(player, -1), restof(chat)+1);
      break;
    case ';':
      sprintf(retbuf, "[to %s] %s's %s",  color_name(who, -1), color_name(player, -1), restof(chat)+1);
      break;
    case '.':
      sprintf(retbuf, "[to %s] %s . o O ( %s )",  color_name(who, -1), color_name(player, -1), restof(chat) + 1);
      break;
    default:
      sprintf(retbuf, "[to %s] %s says, \"%s\033[0m\".", color_name(who, -1), color_name(player, -1), restof(chat));
      break;
  }
  
  return(retbuf);  
}

/** TalkChannel():

    decide whether to talk on the channel of choice.
**/
int TalkChannel(COMOBJECT *channel, object *player, char *chat)
{
  COMWHO *c;
  char buf[2048], *txt, ref[512];
  
  if((channel->flags & COM_NOTALK)) {
    notify(player, "That channel has been set NOTALK.\nTry again later.");
    return TRUE;
  }
  
  if(IsComBanned(channel, player)) {
    notify(player, "You are banned from that channel.");
    return FALSE;
  }

  if(player->flags & QUIET) {
    notify(player, "You've set yourself QUIET!");
    return TRUE;
  }

  buf[0] = '\0';  
  
  if(*(txt = atr_get(player, A_PCOMTITLE))) {
    DEBUG(pstr("%s [%zd]", txt, strlen(txt)));
    snprintf(ref, 512, "%s^cn %s", txt, color_name(player, -1));
  } else
    snprintf(ref, 512, "%s", color_name(player, -1));
    
  switch(*chat) {
    case ':': /** Pose **/
      sprintf(buf, "%s %s", ref, chat + 1);
      break;
    case ';': /** Possessive **/
      sprintf(buf, "%s's %s", ref, chat + 1);
      break;
    case '.': /** Think **/
      sprintf(buf, "%s . o O ( %s )", ref, chat + 1);
      break;
    case '\'': /** To Player **/
      sprintf(buf, "%s", ComToPlayer(channel, player, chat+1));
      break;
    default: /** Say On Channel **/
      sprintf(buf, "%s says, \"%s\033[0m\".", ref, chat);
      break;
  }

  // Modify this to work with Ignore in the future.
  ComNotify(channel, buf);
  return TRUE;
}


/** ON/OFF the channel **/

/** OnChannel(): 
**/
void OnChannel(COMOBJECT *channel, object *player)
{
  COMWHO *w;
  
  for(w = channel->wholist; w; w=w->next)
    if(w->player == player) {
      if(w->state == CHAN_STATE_ON) {
        notify(player, "You already have that channel turned on!");
      } else {
        notify(player, "We're turning that channel on now!");
        w->state = CHAN_STATE_ON;
        ComNotify(channel, pstr("* %s has resumed listening to this channel.", color_name(player, -1)));
      }
      return;
    }
  
  notify(player, "Talk to an administrator, something has gone wrong with the com system.");
}

/** OffChannel():
**/
void OffChannel(COMOBJECT *channel, object *player)
{
  COMWHO *w;
  
  for(w = channel->wholist; w; w=w->next)
    if(w->player == player) {
      if(w->state == CHAN_STATE_OFF) {
        notify(player, "You already have that channel turned off!");
      } else {
        notify(player, "We're turning that channel off now!");
        w->state = CHAN_STATE_OFF;
        ComNotify(channel, pstr("* %s is no longer listening to this channel.", color_name(player, -1)));
      }
      return;
    }
    
  notify(player, "Talk to an administrator, something has gone wrong with the com system.");
}


/** SetChannel():
  +channel/set <channel>=<flags>
  
**/
void SetChannel(COMOBJECT *channel, object *player, char *flag)
{
  struct com_flags_t *f = NULL;
  short not = FALSE;
  int i = 0;
  
  if(!flag || !*flag) {
    notify(player, "What flag?");
    return;
  }
  
  if(!can_control(player, channel->owner)) {
    notify(player, "You cannot modify the flags on this channel.");
    return;
  }   
  
  if(*flag == '!') {
    not = TRUE;
    flag++;
  }
  
  for(i = 0; comflags_table[i].name; i++) {
    if(string_prefix(comflags_table[i].name, flag)) {
      f = &comflags_table[i];
      break;
    }
  }  
  
  if(!f) {
    notify(player, "What flag?");
    return;
  }
  
  // We have the flag, we know whether it's a NOT flag...
  // Let's modify the flag now, let them know, and exit out.
  if(not) {
    channel->flags &= ~f->flag;
  } else {
    channel->flags |= f->flag;
  }
  notify(player, "Flags on channel reset.");
}

/** RenameChannel():
**/
void RenameChannel(COMOBJECT *channel, char *cname)
{
  block_free(channel->name);
  SET(channel->name, cname);
}

/** NameChannel():
**/
void NameChannel(COMOBJECT *channel, object *player, char *cname)
{
  COMOBJECT *c = NULL;
  
  if(!can_control(player, channel->owner)) {
    notify(player, "You cannot rename this channel!");
    return;
  }

  if(!(c = FindChannel(strip_color(cname)))) {
    RenameChannel(channel, cname);
    notify(player, "The channel has been renamed.");
    return;
  }
  
  if(c != channel) {
    notify(player, "That name shadows another channel!");
    return;
  }  
  
  RenameChannel(channel, cname);
  notify(player, "The channel has been renamed.");
}

/** ParseChannel():

    ** Channel switches: create, destroy, list, set, join, default
    ? create - Creates a channel.
    ? destroy - Destroys a channel.
    ? list - List all channels you can join.
    ? set - Set a flag, or some field, on the channel.
    ? join - Join the channel.
    ? default - Sets the default channel token (usually '='.)
**/
void ParseChannel(player, cmdswitch, arg1, arg2)
  object *player;
  char *cmdswitch, *arg1, *arg2;
{
  COMOBJECT *channel;

  // Before we do anything else, let's see if we're listing channels or creating a channel.
  if(!strcmpm(cmdswitch, "list")) {
    ListChannels(player);
    return;
  } else if(!strcmpm(cmdswitch, "create")) {
    if(!*arg1) {
      notify(player, "I'm not creating thin air.");
      return;
    }
    ComCreate(player, arg1);
    return;
  } else if(!*cmdswitch && !*arg1) {
    ListPlayerChannels(player);
    return;
  }
  
  if(!(channel = FindChannel(arg1))) {
    notify(player, "I cannot find that channel.");
    return;
  }
  
  if(IsComAdmin(channel) && !(player->flags & WIZARD)) {
    notify(player, "That is an admin channel.");
    return;
  }
  
  if(!channel) {
    notify(player, "What channel?");
    return;
  }
    
  if((channel->flags & COM_SERVER)) {
    notify(player, "That channel is a server-only channel.");
    return;
  }
  
  if(!strcmpm(cmdswitch, "default")) {
    SetDefaultChannel(player, channel);
    return;
  } else if (!strcmpm(cmdswitch, "join")) {
    JoinChannel(channel, player);
    return;
  } else if (!strcmpm(cmdswitch, "set")) {
    SetChannel(channel, player, arg2);
    return;
  } else if (!strcmpm(cmdswitch, "name")) {
    NameChannel(channel, player, arg2);
    return;
  } else if (!strcmpm(cmdswitch, "destroy")) {
    DestroyChannel(channel, player);
    return;
  } else if (!strcmpm(cmdswitch, "leave")) {
    LeaveChannel(channel, player);
    return;
  } else if (!strcmpm(cmdswitch, "recall")) {
    RecallChannel(player, channel, arg2);
    return;
  } else if (!strcmpm(cmdswitch, "view")) {
    ViewChannel(channel, player);
    return;
  }

  if(!IsOnChannel(channel, player)) {
    notify(player, "You are not on that channel.");
    return;
  }

  if(!strcmpm(arg2, "who")) {
    WhoChannel(channel, player);
    return;
  } else if (!strcmpm(arg2, "off")) {
    OffChannel(channel, player);
    return;
  } else if(!strcmpm(arg2, "on")) {
    OnChannel(channel, player);
    return;
  }

  // If there's a command switch, then it's invalid -- let them know.
  if(*cmdswitch) {
    notify(player, "Invalid command switch.");
    return;
  }

  // If we're just doing: +ch <channel>=<text> we'll continue.  
  if(!HasChannelOn(channel, player)) {
    notify(player, "You don't have that channel on!");
    return;
  }
  
  if(!arg2 || !*arg2) {
    notify(player, "Did you wish to say something?");
    return;
  }
  
  TalkChannel(channel, player, arg2);
}

/** ParseChannelShortcut():
**/
int ParseChannelShortcut(object *player, char *text)
{
  COMOBJECT *c = NULL;
  COMWHO *w;
  char *chan;
  
  if(!text || !*text)
    return FALSE;
  
  for(c = comsys; c; c=c->next)
    if(string_prefix(fparse(text, ' '), strip_color(c->name)))
      break;
  
  if(!c)
    return FALSE;
    
  ParseChannel(player, "", strip_color(c->name), rparse(text, ' '));
  return(TRUE);  
}

/** DefaultChannel(): Preform default channel functions
**/
void DefaultChannel(player, mesg)
  object *player;
  char *mesg;
{
  COMOBJECT *channel;
  char *chan = atr_get(player, A_DEFCHANNEL);
  
  if(!*chan)
    return;
  
  ParseChannel(player, "", chan, mesg);
}


/*** CHANNEL LOG FUNCTIONS ***/

/**LogChannel():
**/
void LogChannel(char *chan, char *string)
{
  COMOBJECT *channel;
  char buf[2048];
  
  if(!(channel = FindChannel(chan))) {
    printf("** error ** no channel %s found!\n", chan);
    return;
  }
  
  sprintf(buf, "%s", string);
  ComNotify(channel, buf);
}

/*** CHANNEL PASTE FUNCTIONS ***/

/** ChannelPasteHeader():
**/
int ChannelPasteHeader(object *player, char *chan, int tag)
{
  COMOBJECT *channel;
  
  if(!(channel = FindChannel(chan))) 
    return M_ECHNOTFOUND;
    
  if(!IsOnChannel(channel, player))
    return M_ECHNOTON;
    
  if(tag) {
    ComNotify(channel, pstr("==> Channel paste from %s <==", color_name(player, -1)));
  } else {
    ComNotify(channel, "==> End Channel Paste <==");
  }
  
  return TRUE;
     
}

/** ChannelPaste():
**/
int ChannelPaste(object *player, char *chan, char *text)
{
  COMOBJECT *channel;
  
  if(!(channel = FindChannel(chan))) 
    return M_ECHNOTFOUND;
    
  if(!IsOnChannel(channel, player))
    return M_ECHNOTON;

  ComNotify(channel, pstr("%s", text));
  
  return TRUE;
}

/*** CHANNEL FILE FUNCTIONS ***/

/** RestoreRecall():
**/
void RestoreRecall(COMOBJECT *channel)
{
  FILE *fp;
  char rfile[1024];
  
  sprintf(rfile, "%s.recall", strip_color(channel->name));
  if(!(fp = fopen(rfile, "r"))) // obviously, no recall buffer was dumped.
    return;
    
  while(!feof(fp)) {
    AddRecall(channel, getstring(fp), 0);
  }
  
  fclose(fp);
}

/** DumpRecall():
**/
void DumpRecall(COMOBJECT *channel)
{
  COMRECALL *lines;
  FILE *fp;
  char rfile[1024];
  
  if(!(channel->flags & COM_RECALL))
    return;
  
  sprintf(rfile, "%s.recall", strip_color(channel->name));
  
  fp = fopen(rfile, "w");
  
  for(lines = channel->recall; lines; lines=lines->next)
    fprintf(fp, "%s\n", lines->buffer);

  fclose(fp);  
}

/** ComSave():
**/
void ComSave(FILE *fp, COMOBJECT *channel)
{
  COMWHO *c;
  
  fprintf(fp, "$%s\n", channel->name);
  fprintf(fp, "%ld\n", channel->flags);
  if(COM_DB_VERSION >= 1.01)
    fprintf(fp, "%ld\n", channel->total_lines);
  fprintf(fp, "#%d\n", channel->creator);
  fprintf(fp, "%ld\n", channel->created);
  fprintf(fp, "-%s\n", ListComBanned(channel->banlist));
  fprintf(fp, "+%s\n", ListComWho(channel->wholist));
  fprintf(fp, "\\\n");
  
  if((channel->flags & COM_DUMP))
    DumpRecall(channel);
}

/** ComDBSave():
**/
void ComDBSave(char *filename)
{
  FILE *fp;
  COMOBJECT *c;
  int cnt;
  
  if(!(fp = fopen(filename, "w"))) {
    printf("* Error saving com database!\n");
    return;
  }
  
  fprintf(fp, "^%.2f\n", COM_DB_VERSION);
  for(cnt = 0, c = comsys; c; c=c->next,cnt++);
  
  fprintf(fp, "%d\n", cnt);

  for(c = comsys; c; c=c->next)
    ComSave(fp, c);

  fprintf(fp, "^END\n");
  
  fclose(fp);
}

/** LoadChannel():
**/
int LoadChannel(FILE *fp)
{
  COMOBJECT *channel;
  object *creator;
  char ChanName[64], BanList[1024], WhoList[1024], ChanCreator[12];
  unsigned long flags, created, total_lines = 0;
  
  strcpy(ChanName, getstring(fp));
  flags = (unsigned long)getlong(fp);

  if(com_db_version >= (float)1.01)
    total_lines = (unsigned long)getlong(fp);

  strcpy(ChanCreator, getstring(fp));
  created = getlong(fp);
  strcpy(BanList, getstring(fp));
  strcpy(WhoList, getstring(fp));
  if(*getstring(fp) != '\\')
    return FALSE; 

  if(*ChanName != '$' || *BanList != '-' || *WhoList != '+')
    return FALSE;

  if(!(creator = match_object(ChanCreator)))
    return FALSE;

  if(!(channel = ServerComCreate(ChanName + 1, creator)))
    return FALSE;

  channel->flags = flags;
  channel->created = created;
  channel->owner = creator;
  channel->total_lines = total_lines;
  if(!ComAddBanList(channel, BanList+1))
    return FALSE;
  if(!ComAddWhoList(channel, WhoList+1))
    return FALSE;

  // Check for DUMP and restore the recall lines, if applicable.
  if( (channel->flags & COM_DUMP) ) {
    RestoreRecall(channel);
  }

  return TRUE;
}

void ComDBLoad(char *filename)
{
  FILE *fp;
  char *firstline;
  int a, cnt;
  
  if(!(fp = fopen(filename, "r"))) {
    printf("error loading com db!\n");
    return;
  }
  
  firstline = getstring(fp);
  
  if(*firstline != '^') {
    printf("error in comdb format!\n");
    fclose(fp);
    return;
  }

  com_db_version = atof(firstline + 1);
  
  if(!(cnt = getlong(fp))) {
    printf("no channels to load!\n");
    fclose(fp);
    return;
  }
  
  for(a = 0; a < cnt; a++)  
    if(!(LoadChannel(fp))) {
      printf("error loading channel!\n");
      fclose(fp);
      return;
    }
  
  fclose(fp);
}



/** END OF COMSYS.C **/
