/****************************************************************************
 MYTH - Zones
 Coded by sarukie (12/19/03)
 ---
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

void do_zlink(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *what, *zone;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	if(!(what = remote_match(player, arg1))) {
	  notify(player, "What are you trying to link?");
	  return;
	}
	
	if(!(what->flags & TYPE_ROOM)) {
	  notify(player, "Zones only work on rooms.");
	  return;
	}
	
	if(!(zone = remote_match(player, arg2))) {
	  notify(player, "What zone?");
	  return;
	}
	
	if(!(zone->flags & ZONE)) {
	  notify(player, "That is not a zone.");
	  return;
	}
	
	if(!can_control(player, what) && !has_pow(player, what, POW_MODIFY)) {
	  notify(player, "You cannot modify that object.");
	  return;
	}
	
	SET_ATR(what, A_ZONE, pstr("#%d", zone->ref));
	what->zone = zone;
	
	notify(player, pstr("%s zone set to %s", color_name(what, -1), color_name(zone, -1)));
}

void do_unzlink(player, arg1)
	object *player;
	char *arg1;
{
	object *what;
	
	IS_ARGUMENT(player, (arg1));
	
	if(!(what = remote_match(player, arg1))) {
	  notify(player, "What?");
	  return;
	}
	
	if(!can_control(player, what) && !has_pow(player, what, POW_MODIFY)) {
	  notify(player, "You cannot modify that.");
	  return;
	}
	
	if(!(what->flags & TYPE_ROOM)) {
	  notify(player, "Only ROOMs have zones.");
	  return;
	}
	
	
	if(what->ref == 0) {
	  notify(player, "If you want to change the default zone, @zlink a new one.");
	  return;
	}
	
	if(!(what->zone = match_object(atr_get(db[0], A_ZONE)))) {
	  log_channel("log_imp", "PANIC!: Something is very bad.");
	  return;
	}
	
	SET_ATR(what, A_ZONE, atr_get(db[0], A_ZONE));
	notify(player, "Zone unset.");
}

int DOZONE(enactor, what, str)
        object *enactor;
	object *what;
	char *str;
{
	PARENT *p;
	ATTR *a;
	int triggered = FALSE;
	
	for(p = what->parent; p; p=p->next) {
	  if(p) {
	    if(DOZONE(enactor, p->what, str))
	      return(TRUE);
	  }
	}
	
	for(a = what->alist; a; a=a->next) {
	  if( (a->flags & APROGRAM)) {
	    if(!a->value)
	      continue;
	    if((*a->value == '$')) { 
	      if(listen_match(what, a->value + 1, str, 0))
	        triggered = TRUE;
	    }
	  }
	}
	
	return(triggered);
}

