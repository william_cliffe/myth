/****************************************************************************
 MYTH - Queue
 Coded by Jagged (01/14/01)
 ---
 Queue
 ****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

QUEUE *queue = 0;
QUEUE *qnow = 0;
QUEUE *queuetail = 0;
QUEUE *qnowtail = 0;

QUEUE *delete_from_qnow(q)
	QUEUE *q;
{
	int i;
	
	if(q->prev)
	  q->prev->next = q->next;
	
	if(q->next)
	  q->next->prev = q->prev;
	
	if(qnow == q)
	  qnow = q->next;
	
	if(qnowtail == q)
	  qnowtail = q->prev;

	for(i = 0; i < NUMARGS; i++)
          block_free(q->var[i]);
               
        if(*q->action)
          block_free(q->action);

        q->enactor->clev--;
        block_free(q);
                                          
	  
	return(NULL);	
}

QUEUE 
  *delete_from_queue(q)
	QUEUE *q;
{
	int i;
	
	if(q->prev)
	  q->prev->next = q->next;
	
	if(q->next)
	  q->next->prev = q->prev;
	
	if(queue == q)
	  queue = q->next;
	
	if(queuetail == q)
	  queuetail = q->prev;

	for(i = 0; i < NUMARGS; i++)
          block_free(q->var[i]);
               
        if(*q->action)
          block_free(q->action);

        q->enactor-clev--;
        
        block_free(q);
                                          
	  
	return(NULL);	

}

QUEUE *add_to_qnow(char *action)
{
	int i;
	QUEUE *qnew;
	
	if(!action || !*action)
	  return(NULL);
	
	GENERATE(qnew, QUEUE);
        qnew->action = NULL;
        SET(qnew->action, action);
        qnew->enactor = NULL;
        qnew->seconds = 0;
        qnew->timestamp = time(0);
        
        for(i = 0; i < NUMARGS; i++) {
          SET(qnew->var[i], gvar[i]);
        }
        
	if(!qnow) {
	  qnew->next = qnew->prev = NULL;
	  qnow = qnowtail = qnew;
	} else {
	  qnew->next = NULL;
	  qnew->prev = qnowtail;
	  qnowtail->next = qnew;
	  qnowtail = qnew;
	}        

	return(qnew);        
}

QUEUE *add_to_queue(action)
	char *action;
{
	int i;
	QUEUE *qnew;
	
	if(!*action)
	  return(NULL);
	
	GENERATE(qnew, QUEUE);
	qnew->action = NULL;
	qnew->action = NULL;
        SET(qnew->action, action);
        qnew->enactor = NULL;
        qnew->seconds = 0;
        qnew->timestamp = time(0);
        
        for(i = 0; i < NUMARGS; i++)
          SET(qnew->var[i], gvar[i]);

	return(qnew);    
}

/*
*/
void
  set_inqueue(player, action, sec, var_list)
  	object *player;
  	char *action;
  	int sec;
  	char *var_list;
{
	QUEUE *q, *qs;

	/** If the player's queue level is above the maximum queue level, stop **/
	if(player->qlev > MAX_QLEV)
		return;
	
	q = add_to_queue(action);
	player->qlev++;
	q->enactor = player;
	q->activator = cmd_player;
	q->seconds = time(0) + sec;
	
	// Now, we search the QUEUE and put the new action in
	// just before the action with the greater time left.
	// But first we see if there IS a queue, if not, we 
	// just add the new process in.
	if(!queue) {
	  q->next = NULL;
	  q->prev = NULL;
	  queue = queuetail = q;
	} else { // Okay, so there is a queue -- search and insert.
	  for(qs = queue; qs; qs=qs->next) {
	    if(qs->seconds > q->seconds)
	      break;
	  }
	  
	  if(qs) {  
	    if(qs->prev)
	      qs->prev->next = q;

	    q->prev = qs->prev;
	    q->next = qs;
	    qs->prev = q;
	    if(queue == qs)
	      queue = q;
	  } else {
	    q->next = NULL;
	    q->prev = queuetail;
	    queuetail->next = q;
	    queuetail = q;
	  }
	}
}


void set_innow(player, action)
	object *player;
	char *action;
{
	QUEUE *q;
	
	q = add_to_qnow(action);
	q->enactor = player;
}


void parse_queue(player, action, sec, var_list)
	object *player;
	char *action;
	int sec;
	char *var_list;
{
	char buf[MAX_BUFSIZE], result[MAX_BUFSIZE];
	char *r, *s=buf, *p;
	
	strcpy(buf, action);
	while((r=(char *)parse_up(&s, ';'))) { // okay, if it's sec == 0 then just filter..
	  if(sec > 0) {
	    set_inqueue(player, r, sec, var_list);
	  } else if (sec == 0) {
	    pronoun_substitute(result, player, r, player);
	    p = result + strlen(player->name) + 1;
	    filter_command(player, p, 0);
	  } else {
	    set_innow(player, r);
	  }
	}
	player->clev--;
}

void DO_QNOW()
{
	QUEUE *q, *qnext = NULL;
	char dstr[8000];
	char result[MAX_BUFSIZE], *k, *p, *d = dstr;
	int i;
	
	for(q = qnow; q; q=qnext) {
	  qnext = q->next;
	  for(i = 0; i < NUMARGS; i++)
	    SET(gvar[i], q->var[i]);
	  strcpy(dstr, q->action);
	  while( (k = parse_up(&d, ';')) ) {
	    pronoun_substitute(result, q->enactor, k, q->enactor);
	    p = result + strlen(q->enactor->name) + 1;
	    filter_command(q->enactor, p, 0);
	    if( (q->enactor->clev--) < 0)
	      q->enactor->clev = 0;
          }
	  delete_from_qnow(q);
	}
	
	while(qnow)
	  DO_QNOW();
	
}


void remove_queue(object *player)
{
        QUEUE *q, *qnext = NULL;
        
        for(q = queue; q; q=qnext) {
          qnext = q->next;
          if(q->enactor == player)
            delete_from_queue(q);
        }
 
       for(q = qnow; q; q=qnext) {
          qnext = q->next;
          if(q->enactor == player)
            delete_from_qnow(q);
        }

}

