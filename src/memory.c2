/****************************************************************************
 MYTH - Memory Allocation
 Coded by Saruk (04/10/99)
 ---
 An attempt at MYTH's very own VM.
 ****************************************************************************/
  
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

MSTACK *MYTH_stack = NULL;
MSTACK *MYTH_nonvolatile = NULL;
MSTACK *MYTH_free = NULL;
int total_stack_calls = 0;
int total_stack_frees = 0;
int free_frames = 0;

void clear_frame P((MSTACK *));
void add_to_free P((MSTACK *));

void *new_block_alloc(size)
	size_t size;
{
	MSTACK *snew;
	
	snew = (MSTACK *)malloc(sizeof(MSTACK));
	snew->ptr = malloc(size);
	snew->size = size;
	
	if(snew->ptr)
		memset(snew->ptr, 0, size);
		
	snew->next = MYTH_nonvolatile;
	MYTH_nonvolatile = snew;		
	
	return(snew->ptr);
}

void *block_alloc(size)
	size_t size;
{
	MSTACK *s = NULL, *fptr;
	void *p;

	if(!MYTH_free)
	  return(new_block_alloc(size));

	for(s = MYTH_free; s; s=s->next) {
	  if(s->size >= size)
	    break;
	}

	if(!s)
	  return(new_block_alloc(size));

	MYTH_free = s->next;
	free_frames--;
	
	if(s->size > (size+3)) {
	  fptr = (MSTACK *)malloc(sizeof(MSTACK));
	  fptr->ptr = s->ptr+size;
	  fptr->size = s->size - size;
	  add_to_free(fptr);
	  s->size = (s->size - fptr->size);
	}
	
	memset(s->ptr, 0, size);
	s->next = MYTH_nonvolatile;
	MYTH_nonvolatile = s;	
	
	return(s->ptr);
}

void *stack_em(size, tag)
	size_t size;
	int tag;
{
	MSTACK *addstack;
	
	if(tag)
		return(block_alloc(size));
		
	addstack = (MSTACK *)malloc(sizeof(MSTACK));
	addstack->ptr = malloc(size);
	addstack->size = size;
	addstack->instance = 0;

	if(addstack->ptr)
		memset(addstack->ptr, 0, size);
	
	addstack->next = MYTH_stack;
	MYTH_stack = addstack;
	
	total_stack_calls++;	
	
	return(addstack->ptr);
}



void __free_stack(stack)
	MSTACK *stack;
{
	MSTACK *s, *sprev = NULL;
	
	for(s = MYTH_stack; s; s=s->next) {
		if(s == stack)
			break;
		sprev = s;
	}
	
	if(!s) 
		return;
	
	if(!sprev)
		MYTH_stack = s->next;
	else
		sprev->next = s->next;
	
	if(s->ptr)
		free(s->ptr);
	total_stack_frees++;
	
	free(s);
}


void add_to_free(stack)
	MSTACK *stack;
{
	MSTACK *s;

	printf("Adding %x to free list.\n", (MSTACK *)stack);
	
	for(s = MYTH_free; s; s=s->next) {
	  if(s->ptr + s->size == stack->ptr) {
	    s->size = s->size + stack->size;
	    free(stack);
	    return;
	  } else if (stack->ptr + stack->size == s->ptr) {
	    s->size = s->size + stack->size;
	    s->ptr = stack->ptr;
	    free(stack);
	    return;
	  }
	}

	stack->next = MYTH_free;
	free_frames++;		
	MYTH_free = stack;		
}

void stack_clear_frame(stack)
	MSTACK *stack;
{
	MSTACK *s, *sprev = NULL;

	for(s = MYTH_nonvolatile; s; s=s->next) {
		if (s == stack)
			break;
		sprev = s;
	}
	
	if(!s)	
		return;
	
	if(!sprev)
		MYTH_nonvolatile = s->next;
	else
		sprev->next = s->next;

	add_to_free(s);
}

void block_free(ptr)
	void *ptr;
{
	MSTACK *s=NULL, *snext=NULL;

	for(s = MYTH_nonvolatile; s; s=snext) {
		if(!s)
			printf("ERROR!!!\n");
		snext = s->next;
		if(s->ptr == ptr)
			stack_clear_frame(s);		
	}

}

void clear_stack()
{
	MSTACK *s, *snext;
	
	for(s = MYTH_stack; s; s=snext) {
		snext = s->next;
		__free_stack(s);
	}
	
}

void generate_stack()
{
	FILE *fp;
	MSTACK *s;
	
	fp = fopen("MYTHSTACK", "w");
	for(s = MYTH_stack; s; s=s->next) {
		fprintf(fp, "%p\n", s->ptr);
		fprintf(fp, "%d\n", s->size);
	}
	fclose(fp);
}

void report_stack()
{
	MSTACK *s;
	
	for(s=MYTH_nonvolatile;s;s=s->next) 
		if(!s->ptr)
			log_channel("log_debug", "s->ptr does not exist!");
}		

void clear_frame(stack)
	MSTACK *stack;
{
	MSTACK *s, *sprev = NULL;
	
	for(s = MYTH_nonvolatile; s; s=s->next) {
		if(s == stack)
			break;
		sprev = s;
	}
	
	if(!s)
		return;
	
	if(!sprev)
		MYTH_nonvolatile = s->next;
	else
		sprev->next = s->next;
		
	if(s->ptr)
		free(s->ptr);
	
	free(s);
}

void *stack_realloc(void *ptr, int size, int flag)
{
	MSTACK *s;
	void *tempptr;
	
	for(s = MYTH_nonvolatile; s; s=s->next)
		if(s->ptr == ptr)
			break;
	
	if(!s)
		return(stack_em(size, flag));
	
	//else, we've got to hold the current data and realloc it.
	// the size of the structure that holds the ptr will not change, just the ptr itself.
	tempptr = realloc(s->ptr, size);

	s->ptr = tempptr;
	
	return (s->ptr);	
}

