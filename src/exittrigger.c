/****************************************************************************
 MYTH - Trigger System
 Coded by Saruk (03/27/99)
 ---
 This is the MYTH trigger system, thinks get triggered on occasion.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/* Locking exits */
int exit_lock(player, exit)
	object *player;
	object *exit;
{
	if(parse_lock(player, atr_get(exit, A_THROUGH))) 
		return 1;
	else
		return 0;
}

void exit_trigger(player, exit)
	object *player;
	object *exit;
{
	object *oldloc;
	CONTENTS *exception = NULL;
	char buf[MAX_BUFSIZE];

	if(!exit->linkto)
	  return;

	if(!exit_lock(player, exit)) {
		notify(player, pstr("* %s", (*(atr_get(exit, A_FTHROUGH))) ? atr_get(exit, A_FTHROUGH):
			"You can't go that way."));
		return;
	} else {
		if(*(atr_get(exit, A_STHROUGH)))
			notify(player, pstr("* %s", atr_get(exit, A_STHROUGH)));
			
	}
	
	add_exception(&exception, player);
		
	if(*(atr_get(exit, A_LEAVE))) {
		sprintf(buf, "%s %s", color_name(player, -1), atr_get(exit, A_LEAVE));
		notify_in(player->location, exception, buf);
	}

	notify_in(player->location, exception, pstr("%s leaves through the exit marked: %s",
	  color_name(player, -1), color_name(exit, -1)));
	oldloc = player->location;
	
	move_to(player, exit->linkto);
	
	notify_in(player->location, exception, pstr("%s arrives from %s",
	  color_name(player, -1), color_name(oldloc, -1)));
	
	
	if(*(atr_get(exit, A_ARRIVE))) {
		sprintf(buf, "%s %s", color_name(player, -1), atr_get(exit, A_ARRIVE));
		notify_in(player->location, exception, buf);
	}
	clear_exception(exception);
}

void take_trigger(player, thing)
	object *player;
	object *thing;
{
	
	if(!parse_lock(player, atr_get(thing, A_TAKE))) {
		notify(player, pstr("* %s", (*(atr_get(thing, A_FTAKE))) ? atr_get(thing, A_FTAKE) : "You can't take that."));
		return;
	}

	move_to(thing, player);
	notify(player, pstr("* %s", (*(atr_get(thing, A_STAKE))) ? atr_get(thing, A_STAKE) :
		"You pick it up."));
}


int pseudo_move(player, whereto, exitname)
	object *player;
	char *whereto;
	char *exitname;
{
	CONTENTS *exception = NULL;
	object *location;
	char oldloc[MAX_BUFSIZE];
	
	if(!*whereto)
		return -1;
	
	if(!(location = match_object(whereto)))
		return -1;
	
	if(!can_control(player->location->ownit, location) && !(location->flags & LINK_OK))
		return -1;
	
	strcpy(oldloc, color_name(player->location, -1));
	add_exception(&exception, player);
	if(!(player->flags & INVISIBLE))
		notify_in(player->location, exception, pstr("* %s goes through the exit marked %s",
			color_name(player, -1), exitname));
	move_to(player, location);
	if(!(player->flags & INVISIBLE))
		notify_in(player->location, exception, pstr("* %s arrives from %s",
			color_name(player, -1), oldloc));
	clear_exception(exception);	
	return 1;	
}

int is_a_direction(direction)
	char *direction;
{
	if(!strcmp(direction, "south") || !strcmpm(direction, "s") || !strcmpm(direction, "north") ||
		!strcmpm(direction, "n") || !strcmpm(direction, "east") || !strcmpm(direction, "e") ||
		!strcmpm(direction, "west") || !strcmpm(direction, "w") || !strcmpm(direction, "southwest") ||
		!strcmpm(direction, "sw") || !strcmpm(direction, "southeast") || !strcmpm(direction, "se") ||
		!strcmpm(direction, "northwest") || !strcmpm(direction, "nw") || !strcmpm(direction, "northeast") ||
		!strcmpm(direction, "ne") || !strcmpm(direction, "up") || !strcmpm(direction, "u") || 
		!strcmpm(direction, "down") || !strcmpm(direction, "d") || !strcmpm(direction, "out") ||
		!strcmpm(direction, "o"))
			return -1;

	return 0;
}

int match_pseudo_exit(player, matchit)
	object *player;
	char *matchit;
{
	attr *a, *b;
	
	
	for(a = player->location->alist; a; a=a->next)
  	  if( (a->flags & AEXIT) ) {
	    if(!strcmpm(matchit, "south") || !strcmpm(matchit, "s"))
	      return pseudo_move(player, atr_get(player->location, A_SOUTH), "South");
	    else if (!strcmpm(matchit, "north") || !strcmpm(matchit, "n"))
	      return pseudo_move(player, atr_get(player->location, A_NORTH), "North");
	    else if (!strcmpm(matchit, "east") || !strcmpm(matchit, "e"))
	      return pseudo_move(player, atr_get(player->location, A_EAST), "East");
	    else if (!strcmpm(matchit, "west") || !strcmpm(matchit, "w"))
	      return pseudo_move(player, atr_get(player->location, A_WEST), "West");
            else if (!strcmpm(matchit, "southwest") || !strcmpm(matchit, "sw"))
	      return pseudo_move(player, atr_get(player->location, A_SOUTHWEST), "SouthWest");
	    else if (!strcmpm(matchit, "southeast") || !strcmpm(matchit, "se"))
	      return pseudo_move(player, atr_get(player->location, A_SOUTHEAST), "SouthEast");
	    else if (!strcmpm(matchit, "northwest") || !strcmpm(matchit, "nw"))
	      return pseudo_move(player, atr_get(player->location, A_NORTHWEST), "NorthWest");
	    else if (!strcmpm(matchit, "northeast") || !strcmpm(matchit, "ne"))
	      return pseudo_move(player, atr_get(player->location, A_NORTHEAST), "NorthEast");
	    else if (!strcmpm(matchit, "up") || !strcmpm(matchit, "u"))
	      return pseudo_move(player, atr_get(player->location, A_UP), "Up");
	    else if (!strcmpm(matchit, "down") || !strcmpm(matchit, "d"))
	      return pseudo_move(player, atr_get(player->location, A_DOWN), "Down");
	    else if (!strcmpm(matchit, "out") || !strcmpm(matchit, "o"))
	      return pseudo_move(player, atr_get(player->location, A_OUT), "Out");
	    else
	      return 0;
	  }

	if (is_a_direction(matchit))
		return -1;
		
	return 0;
}

char *show_pseudo_exit(exitname)
	char *exitname;
{
	if(!strcmpm(exitname, "south"))
		return("South <S>");
	else if (!strcmpm(exitname, "north"))
		return("North <N>");
	else if (!strcmpm(exitname, "east"))
		return("East <E>");
	else if (!strcmpm(exitname, "west"))
		return("West <W>");
	else if (!strcmpm(exitname, "southwest"))
		return("SouthWest <SW>");
	else if (!strcmpm(exitname, "southeast")) 
                return("SouthEast <SE>");
	else if (!strcmpm(exitname, "northwest")) 
                return("NorthWest <NW>");
        else if (!strcmpm(exitname, "northeast"))
        	return("NorthEast <NE>");
        else if (!strcmpm(exitname, "up"))
        	return("Up <U>");
        else if (!strcmpm(exitname, "down"))
        	return("Down <D>");
        else if (!strcmpm(exitname, "out"))
        	return("Out <O>");
	else
		return "";
}

int match_real_exit(player, exitkey)
	object *player;
	char *exitkey;
{
	CONTENTS *c;
	
	for(c = player->location->contents; c; c=c->next)
	  if( (c->content->flags & TYPE_EXIT) ) {
	    if(match_key(c->content->pass, exitkey)) {
	      exit_trigger(player, c->content);
	      return TRUE;
	    }
	  }
	    
	return FALSE;
}

