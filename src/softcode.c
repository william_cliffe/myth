/****************************************************************************
 MYTH - Softcode
 softcode.c
 ---
 - Covers environments, parsing, and other misc. softcode tasks.
 ****************************************************************************/
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>

#include "db.h"
#include "net.h"
#include "externs.h"

static unsigned int _level = 0;

void displayEnvironmentStats(player)
  object *player;
{
  DATA *v;
  unsigned int idx, envs = 0;

  for(idx = 0; idx < dbtop; idx++)
    if((db[idx])) {
      envs += (db[idx])->environment->cur_size;
    }
      
  DEBUG(pstr("There are %d environments, totalling %d used bytes.", dbtop, envs));

  for(v = player->environment->variables; v; v=v->next)
    notify(player, pstr("You have %s -> %s", v->label, v->data));
    
   
   
}

// We need to be able to remove the variable if the clobbering data is NULL
int clobberVar(ENV *environment, char *label, char *assigned, int varlen, int asslen)
{
  DATA *var;

  for(var = environment->variables; var; var=var->next)
  {
    if(!strcmp(var->label, label)) {
      if(*var->data)
        block_free(var->data);
      SET(var->data, assigned);
      environment->cur_size -= var->len;
      environment->cur_size += asslen;
      var->len = asslen;
      return TRUE;
    }
  }
  
  return FALSE;
}

// This will assign a variable to the player's environment
// space. 
char *assignVariable(player, privs, label, assigned, varlen, asslen)
  object *player, *privs;
  char *label, *assigned;
  unsigned int varlen, asslen; // haha
{
  DATA *var;
  char buff[MAX_BUFSIZE];
  char *bf;
  
  // We will need to throw an error eventually. For now, just back out in fear.
  if(player->environment->cur_size + varlen + asslen > player->environment->max_size)
  {
    DEBUG("Maximum amount of enivornment space exceeded!");
    return FALSE;
  }

  *buff = '\0';
  pronoun_substitute(buff, player, assigned, privs);
  bf = buff + strlen(player->name) + 1;
  
  if(!bf || !*bf) return FALSE; //bf = "(null)";

  // Perform a check here so we can determine variable clobbering.
  if(clobberVar(player->environment, label, bf, varlen, asslen))
    return bf;

  GENERATE(var, DATA);

  var->scope = SCOPE_LOCAL;
  var->len = asslen;  
  SET(var->label, label);		
  SET(var->data, bf);      
  
  player->environment->cur_size += varlen + asslen;

  if(!player->environment->variables) {
    var->next = var->prev = NULL;
    player->environment->variables = player->environment->vtail = var;
  } else {
    var->next = NULL;
    var->prev = player->environment->vtail;
    player->environment->vtail->next = var;
    player->environment->vtail = var;
  }           

  return bf;                                                                           
}

void getVariable(player, str, label)
  object *player;
  char **str, *label;
{
  DATA *var;
  char *c, *aNull = "(null)";
  unsigned int found = FALSE;
  
  for(var = player->environment->variables; var; var=var->next)
  {
    if(!strcmp(var->label, label)) {
      for(c = var->data; *c; c++) *(*str)++ = *c;
      found = TRUE;
    }
  }

  //if(!found) { for(c = aNull; *c; c++) *(*str)++ = *c; }
}


void pronoun_substitute(result, player, str, privs) /**, eFlags) **/
  char *result;
  object *player;
  char *str;
  object *privs;
  /*unsigned long eFlags;*/
{
  char temp[1024];
  char *p = result, *c;

  DEBUG(pstr("Parsing: %s", str));  
  _level += 2;
  if(_level > MAX_RECURSION) {
    DEBUG("Recursion detected.");
    return;
  }
  for(c = player->name; *c; c++) *p++= *c;
  *p++ = ' ';

  while(*str)
  {
    *temp = '\0';
    switch(*str) {
      case '\\':// The escape character should _ALWAYS_ advance.
	str++;
	if(*str) *p++ = *str;
	break;
      case '(': // We want to be able to do the following:
                // ('$foo=Hi there you silly monkey!')
                // However, we _also_ want to be able to do 
                // ('$foo=[get(me,va)]')
                // The key chars here are (, $, =, and )
                // If a comma is detected, fall back out.
	{
	  unsigned int eq = FALSE, endparen = FALSE, fail = FALSE, testnom = TRUE, comma = FALSE;
	  unsigned int idx = 0, deep = 0, varlen = 0;
	  char *x, var[MAX_VARNOM_LEN];		// We'll store our variable assignment here.
	  
	  *var = '\0';
	  *str++;
	  while(*str && isspace(*str)) str++;
	  if(*str != '$') { *p++ = '('; *str--; break; } // Advance and break back out.
	  str++;
	  c = temp;
	  while(*str) {
	    switch(*str) {
	      case '(':
	        deep++;
	        *c++ = *str;
	        break;
	      case ')':
	        if(deep) {
	          deep--;
	          *c++ = *str;
	          break;
	        }
	        endparen = TRUE;
	        break;
	      case '=':  // We have to be careful here and accrue these appropriately
	        if(eq) {
	          *c++ = '=';
	          break;
	        }
	        *c++ = '\0';
	        if((idx - 1) > MAX_VARNOM_LEN) {
	          eq = FALSE;
	          break;
	        }
	        strcpy(var, temp);
	        varlen = idx;
                *temp = '\0'; c = temp; eq = TRUE; idx = 0;
                testnom = FALSE;
	        break;
              case ',':
                if(!deep)
                  comma = TRUE;
                else
                  *c++ = *str;
                break;
	      default:
	        if(testnom) {
	          if(testnom && ((*str > 64 && *str < 91) || (*str > 47 && *str < 58) || (*str > 95 && *str < 123) || *str == '_')) {
  	            *c++ = *str;
  	          } else {
  	            *p++ = '$';
                    fail = TRUE;  
                  }
	        } else {
	          *c++ = *str;
	        }
	        break;
	    }
	    if(comma || fail || endparen) break;
	    *str++, idx++;
	  }
	  *c++ = '\0';
	  if(!endparen || !eq) {
	    *p++ = '(';
	    str -= idx+1;
	    if(comma) str--;
	    break;
	  } else {
	    if((x = assignVariable(player, privs, var, temp, varlen, idx))) {
	      if(_level > 2) { while(*x) { *p++ = *x++; } }
            }
	  }
	}
	break;
      case '%':	// Unlike $, % only allows a 2-char specification.. AND IT MUST BE 1 or 2.
                // Ie: %00 .. %99 (%0A .. %0Z, %AA - %ZZ are current for future spec)
        str++;
        DEBUG("We're going to check this player's environment space...");
        {
          switch(*str) {
            case '#':
              sprintf(temp, "#%d", genactor->ref);
              c = temp;
              while(*c) {
                *p++ = *c++;
              }
              
              break;
            case 'r':
            case 'R':
              *p++ = '\r'; *p++ = '\n';
              break;
            case '0':
              break;
            default:
              *p++ = '%';
              str--;
              break;
          }
        }
        break;
      case '$': // Must contain letters and/or numbers AND the _ character. And only MAX_VARNOM_LEN characters long.
                // You can clobber the hell out of your own variables, too, which is cool.
                // In order to make things efficient, we'll slam the variable back at you if you exceed
                // the variable character length.
        {
	  int idx = 0;
          *str++;
          if(*str == '$') { *p++ = *str; break; }
          c = temp;
          while(*str && ((*str > 64 && *str < 91) || (*str > 47 && *str < 58) || (*str > 95 && *str < 123) || *str == '_'))
            *c++ = *str++, idx++;
          *c++ = '\0';
          if(idx > MAX_VARNOM_LEN) { *p++ = '$'; str -= idx; }
          else { getVariable(player, &p, temp); }
          *str--;
        }
        break;
      case '[':
      /***
        {
          int deep = 0, endbracket = FALSE;
          c = temp;
          while(*str) {
            switch(*str) {
              case '[':
                *c++ = *str;
                deep++;
                break;
              case ']':
                if(deep) {
                  deep--;
                  *c++ = *str;
                  break;
                }
                endbracket = TRUE;
                break;
              default:
                *c++ = *str;
                break;
            }
            str++;
          }
          *c++ = '\0';
          if(endbracket)
            DEBUG("We can throw a function here.");
        }
        ***/
        {
          char buff[MAX_BUFSIZE], *r = buff;
          str++;
          exec(&str, buff, privs, player, 0);
          while(*r) *p++ = *r++;
        }                                                                                                                                                                                                                 
        break;
      default:
        *p++ = *str;
        break;
    }
    if(*str) *str++;
  }
  
  *p++ = '\0';
  _level -= 2;
}


/** END OF ENVIRONMENT **/
