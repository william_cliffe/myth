#include <stdio.h>
#include <arpa/telnet.h>

static char echo_on_str[] = {IAC, WONT, TELOPT_ECHO, '\0'};
static char echo_off_str[] = {IAC, WILL, TELOPT_ECHO, '\0'};
static char eor_str[] = { '\377', '\357', '\0'};

char *t_echo_on(void)
{
  return(echo_on_str);
}

char *t_echo_off(void)
{
  return(echo_off_str);
}

char *t_eor(void)
{
  return(eor_str);
}
