/****************************************************************************
 MYTH - Setting attributes, flags, etc.
 Coded by Saruk (02/12/99)
 ---
 All the set routines are active here.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// So that we can use crypt();
#include <unistd.h>

#include "db.h"
#include "net.h"
#include "externs.h"

// Un-set an attribute flags.
int aflag_unset(setter, player, setflag)
	OBJECT *setter;
	OBJECT *player;
	char *setflag;
{
	char *flagname, *atrname;
	int index;
	flags *f;
	attr *a;

	atrname = fparse(setflag, ':');
	flagname = rparse(setflag, ':');
	
	
	for(a = player->alist; a; a=a->next)
	  if(!strcmpm(a->name, atrname) )
	    for(index = 0; aflag_table[index].name; index++) {
	      f = &aflag_table[index];
	      if(string_prefix(flagname + 1, f->name)) {
	        a->flags &=~f->flag;
	        return TRUE;
	      }
	    }

	return M_EBADAFLG;
}

// Set an attribute flag.
int aflag_set(setter, player, setflag)
	OBJECT *setter;
	OBJECT *player;
	char *setflag;
{
	char *flagname, *atrname;
	int index;
	flags *f;
	attr *a;

	atrname = fparse(setflag, ':');
	flagname = rparse(setflag, ':');
	
	
	for(a = player->alist; a; a=a->next)
	  if(!strcmpm(a->name, atrname) )
	    for(index = 0; aflag_table[index].name; index++) {
	      f = &aflag_table[index];
	      if(string_prefix(flagname, f->name)) {
	        a->flags |=f->flag;
	        return TRUE;
	      }
	    }

	return M_EBADAFLG;
}

int HASWIZ(setter, player, flag)
	object *setter;
	object *player;
	int flag;
{
	if ( (setter == player) && (setter->flags & WIZARD) && (flag & WIZARD) )
		return 1;
	
	return 0;
}

// Un-setting flags.
int flag_unset(setter, player, setflag)
	OBJECT *setter;
	OBJECT *player;
	char *setflag;
{
	// we put flags in a table so they're easier to parse and unparse.
	int index;
	flags *f;
	
	for(index = 0; flag_table[index].name; index++) {
		f = &flag_table[index];
		if(string_prefix(setflag, f->name) && (setter->flags & f->oflags) 
			&& (player->flags & f->type) && (f->oflags != -1) ) {
#ifdef ROOTONLY_SETWIZ
			if( (f->flag & WIZARD) && setter != rootobj)
				return M_ENOCNTRL;
#endif
			if(HASWIZ(setter,player, f->flag))
				return M_EUNSET;
			player->flags &= ~f->flag;
			if(f->oflags & WIZARD)
				log_channel("log_imp", pstr("* (%s) %s unset %s's flag: %s",
					get_date(time(0)), color_name(setter, -1), color_name(player, -1), f->name));


			notify(setter, pstr("[FLAG]: Flag %s unset.",
				f->name));
			return 1;
		}
	}
	
	return M_EBADFLG;	
}

// setting a flag.
int flag_set(setter, player, setflag)
	OBJECT *setter;
	OBJECT *player;
	char *setflag;
{
	int index;
	flags *f;
	
	for(index = 0; flag_table[index].name; index++) {
		f=&flag_table[index];
		if(string_prefix(setflag, f->name) && (setter->flags & f->oflags) && 
			(player->flags & f->type) && (f->oflags != -1) ) {
#ifdef ROOTONLY_SETWIZ
			if( (f->flag & WIZARD) && (setter != rootobj))
				return M_ENOCNTRL;
#endif
			player->flags |= f->flag;
			if(f->oflags & WIZARD)
				log_channel("log_imp", pstr("* (%s) %s set %s's flag: %s",
					get_date(time(0)), color_name(setter, -1), color_name(player, -1), f->name));

			notify(setter, pstr("[FLAG]: Flag %s set.",
				f->name));
			return 1;		
		}
	}
	
	return M_EBADFLG;
}


int is_grouped(player, what)
	object *player;
	object *what;
{
	char *buf;
	
	if(!*atr_get(what, A_GROUP))
	  return(FALSE);
	
	buf = pstr("#%d", player->ref);
	
	if(!strstr(atr_get(what, A_GROUP),buf))
	  return(FALSE);
	
	return TRUE;
}

int __find_player(name)
	char *name;
{
	object *check;
	
	if( (check = match_player(strip_color(name))) )
		return 1;

	return 0;	
}

int __allowed_name(name)
	char *name;
{
	char buf[MAX_BUFSIZE];
	char *c=buf;
	
	strcpy(buf, name);

	if(!*name)
	  return FALSE;

	 
	if(strchr(name, '%') || strchr(name, '@') || strchr(name, '^') || strchr(name, '+') ||
	   strchr(name, '!') || (*name == '#'))
          return FALSE;
	else if (ISNUM(match_num(name)))
	  return FALSE;
	
	if(!strcmpm(name, "me"))
	  return FALSE;
	
	if(!strcmpm(name, "self"))
	  return FALSE;
	  
	if(!strcmpm(name, "here"))
	  return FALSE;
	  
	for(c = buf; *c; c++)
		if(*c!=' ')
			return 1;
					
	return 0;
}

int __name_player(player, victim, name)
	object *player;
	object *victim;
	char *name;
{
	char stripped[MAX_BUFSIZE];
	
	strcpy(stripped, strip_color(name));
	if( (__find_player(stripped) && (strcmpm(victim->name, stripped))) || !__allowed_name(stripped) ) 
	  return M_EILLNAME;
        
	notify(player, pstr("[BUILTIN]: Changing %s to %s",
		color_name(victim, -1), name));
	if(victim->flags & TYPE_PLAYER)
		log_channel("log_sens", pstr("* %s changes %s's name to: %s",
			color_name(player, -1), color_name(victim, -1), name));
	SET(victim->name, stripped);
	SET_ATR(victim, A_RAINBOW, name);
	notify(victim, "[BUILTIN]: Your name has been changed!");
	
	return 1;
}

// Someone should redo this. Currently it edits (allowed to be edited) builtins.
int set_builtin(player, victim, type, newvalue)
	object *player;
	object *victim;
	char *type;
	char *newvalue;
{
	int result; 
	
	if(victim->flags & LOCKED)
		return M_EISLOCKED;

	// we're going to redo this here.
	//if(!*newvalue)
	//	newvalue = " ";
	
	if(string_prefix(type, "description")) {
		if(!*newvalue) newvalue = " ";
		SET(victim->desc, newvalue);
		ncnotify(player, pstr("[BUILTIN]: DESCRIPTION for %s set to '%s'.",
			color_name(victim, -1), newvalue));
		ncnotify(victim, pstr("[BUILTIN]: %s set your DESCRIPTION to '%s'.",
			color_name(player, -1), newvalue));
		return 1;
	} else if (string_prefix(type, "name")) {
		if(!*newvalue) newvalue = " ";
		return __name_player(player, victim, newvalue);
	} else if (string_prefix(type, "powlvl")) {
		if(!*newvalue) newvalue = " ";
		if((player->flags & WIZARD) && can_control(player, victim)) {
			if( (atoi(newvalue) > MAX_POWER) || (atoi(newvalue) > player->powlvl)) 
				return M_EEXCESS;
			else if ( atoi(newvalue) < MIN_POWER)
				return M_EINSUFF;
			else if ( player == victim ) 
				return M_ENOCANDO;
			victim->powlvl = atoi(newvalue);
			log_channel("log_imp", pstr("* %s set %s's power level to: %d",
				color_name(player, -1), color_name(victim, -1), victim->powlvl));
			notify(player, pstr("[BUILTIN]: POWER LEVEL for %s set to '%d'.",
				color_name(victim, -1), atoi(newvalue)));
			notify(victim, pstr("[BUILTIN]: %s set your POWER LEVEL to '%d'.",
				color_name(player, -1), atoi(newvalue)));
			return 1;
		} else {
			return M_ENOTWIZ;
		}
	} else if (!strcmp(type, "password")) {
		if(!*newvalue) newvalue=" ";
		if(!(victim->flags & TYPE_PLAYER))
			return M_ENOPLAY;
		SET(victim->pass, mcrypt(newvalue));
		notify(player, pstr("[BUILTIN]: %s PASSWORD changed!", 
			color_name(victim, -1)));
		notify(victim, "[BUILTIN]: Your PASSWORD is changed!");
		return 1;
	} else if(string_prefix(type, "exitkeys")) {
		if(!*newvalue) newvalue=" ";
		if(!(victim->flags & TYPE_EXIT))
			return M_ENOTEXIT;
		SET(victim->pass, newvalue);
		notify(player, pstr("[BUILTIN]: %s EXIT KEY changed!",
			color_name(victim, -1)));
		return 1;
	} else if (string_prefix(type, "owner")) {
		if(!*newvalue) newvalue = " ";
		return change_ownership(player, victim, newvalue);
	} else if (string_prefix(type, "link") || string_prefix(type, "home")) {
		if(!*newvalue) newvalue = " ";
		return change_home(player, victim, newvalue);
	} else {
		if( (result = edit_atr_set(player, victim, type, newvalue)) > 0) { 
			if(*newvalue)
				ncnotify(player, pstr("* Attribute %s set to: '%s'.",
				type, newvalue));
			else
				notify(player, pstr("Attribute %s reset.", type));
		}
		return result;
	}
	
	return M_ENOBLTIN;
}

// set an object of arguments.
/* In order to change @set to allow for /switching, we will have to change it so that arg1 is passed as the
switch command, whereas arg2 is the inherit data... so:

	@set/name me=<new name>
	In order to stay in touch with MUSE, we will default a NULL switch to 'flag'.
*/
void do_set(player, cmdswitch, arg1, arg2)
	object *player;
	char *cmdswitch;
	char *arg1;
	char *arg2;
{
	OBJECT *what;
	char buffer[MAX_BUFSIZE];
	
	if(!*cmdswitch)
	  cmdswitch = "flag";

	if(string_prefix(cmdswitch, "item")) {
	  SetItem(player, arg1, arg2);
	  return;
	} else if (string_prefix(cmdswitch, "combat")) {
	  SetCombat(player, arg1, arg2);
	  return;
	} else if (string_prefix(cmdswitch, "creature")) {
	  CreatureSetTemplate(player, arg1, arg2);
	  return;
	}

	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));

	if(!(what = remote_match(player, arg1))) {
	  notify(player, "Who?");
	  return;
	}
	
	if(!can_control(player, what) && !has_pow(player, what, POW_MODIFY)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	if(string_prefix(cmdswitch, "attribute")) {
		error_report(player, set_builtin(player, what, fparse(arg2, ':'), rparse(arg2, ':')));
		return;
	} else if(string_prefix(cmdswitch, "flag")) {
		if(*arg2 == '!') {
			error_report(player, flag_unset(player, what, arg2 + 1));
		} else {
			error_report(player, flag_set(player, what, arg2));
		}
		return;
	} else if (string_prefix(cmdswitch, "aflag")) {
		strcpy(buffer, arg2);
		if(*(rparse(buffer, ':')) == '!') {
		  if(!error_report(player, aflag_unset(player, what, buffer))) 
		    return;
		  notify(player, pstr("Attribute %s's %s flag unset.",
		    fparse(buffer, ':'), rparse(buffer, ':')));
		} else {
		  DEBUG(pstr("%s | %s | %s", player->name, what->name, buffer));
		  if(!error_report(player, aflag_set(player, what, buffer)))
		    return;
		  notify(player, pstr("Attribute %s's %s flag set.",
		    fparse(buffer, ':'), rparse(buffer, ':')));
		}
	}
}


attr *__local_attribute_search(victim, atrname)
	object *victim;
	char *atrname;
{
	ATTR *a;
	
	for(a = victim->alist; a;a=a->next)
	  if(!strcmpm(a->name, atrname) )
	    return(a);
	
	return(NULL);
}  

/* Since builtin has been changed to include attribute, this currently changes description or
   attributes */
int edit_builtin(player, victim, attrname, editstr)
	object *player;
	object *victim;
	char *attrname;
	char *editstr;
{
	ATTR *a = NULL;
	char findstr[MAX_BUFSIZE], replacestr[MAX_BUFSIZE], savestr[MAX_BUFSIZE];
	char buf[MAX_BUFSIZE], buf2[MAX_BUFSIZE];
	char *p = buf;
	
	if(string_prefix(attrname, "description"))
		strcpy(savestr, victim->desc);
	else if((a = __local_attribute_search(victim, attrname)))
		strcpy(savestr, atr_get(victim, a->atype));
	else
		return M_ENOCANDO;
	
	
	if(!strchr(editstr, '{') || !strchr(editstr, '}'))
		return M_EILLSTRING;
			
	strcpy(buf2, editstr);
	strcpy(findstr, get_inside(buf2, '{', '}', ','));
	strcpy(buf2, buf2 + strlen(findstr) + 2);
	
	if(!strchr(buf2, '{') || !strchr(buf2, '}'))
		return M_EILLSTRING;
		
	strcpy(replacestr, get_inside(buf2, '{', '}', ','));

	strcpy(buf2, "");
	if(!(p = strstr(savestr, findstr)) || !*findstr)
		return M_ENOTFOUND;
	
	strcpy(buf, p);
	savestr[strlen(savestr) - strlen(p)] = '\0';	
	strcpy(buf2, p + strlen(findstr));
	sprintf(buf, "%s%s%s", savestr, replacestr, buf2);

	if(string_prefix(attrname, "description")) {
		SET(victim->desc, (*buf) ? buf : "");
		notify(player, pstr("[EDIT]: Description changed to '%s'.",
			buf));
		return 1;
	} else {
	       DEBUG(pstr("Changing attribute: %s (%d)", a->name, a->atype));
		if(error_report(player, atr_set(player, victim, a->atype, buf)))
			notify(player, pstr("[EDIT]: Attribute %s changed to '%s'.",
				a->name, buf));
		return 1;
	}
	
	return 0;
}

void do_edit(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *what;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (fparse(arg2,':')));
	IS_ARGUMENT(player, (rparse(arg2,':')));

	if(!(what = match_partial(player, arg1))) {
		if(!(what = match_partial(player->location, arg1))) {
			if(!(what = match_object(arg1))) {
				if(!(what = match_special(player, arg1))) {
					error_report(player, M_EILLNAME);
					return;
				}
			}
		}
	}	

	if(what->flags & LOCKED) {
		error_report(player, M_EISLOCKED);
		return;
	}

	if(!can_control(player, what)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	error_report(player, edit_builtin(player, what, fparse(arg2, ':'), rparse(arg2,':')));
}
