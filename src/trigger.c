/****************************************************************************
 MYTH - Triggers
 Coded by Jagged (01/14/01)
 ---
 Welcome to MYTH's trigger parsing section.
 ****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

/* INHERENCY TRIGGERING...

	1.) Go through content list
	2.) On each content, go through parents
	3.) On each parent go through attribute list
	4.) on each attribute, check dbref to match parent->what dbref.
	5.) If match, do a trigger.

*/


int done = FALSE; // we need to make sure we're not going to activate several differing events.
/*
*/
int
  listen_match(player, tomatch, str, sec)
  	object *player;
  	char *tomatch;
  	char *str;
  	int sec;
{
//	char *junk[10]; // why ten junk vars? we'll see later.
//	char buf[MAX_BUFSIZE];
	char *k, *p;
	int i;
	
	if(!strchr(tomatch, ':')) // there is no event seperator, exit.
		return FALSE;	
	
	p = rparse(tomatch, ':');
	if(wild(fparse(tomatch, ':'), str, NULL)) {
	  while( (k = parse_up(&p, ';')) )
	    parse_queue(player, k, sec, NULL);
	  
	  return TRUE;
	}
	
	return FALSE;
}


int did_listen(enactor, player, str)
  object *enactor;
  object *player;
  char *str;
{
	PARENT *p;
  	ATTR *a;
  	
  	for(p = player->parent; p; p=p->next) {
  	  if(p->what) {
  	    if(did_listen(enactor, p->what, str))
  	      return(TRUE);
  	  }
  	}
  	
  	  for(a = player->alist; a; a=a->next) {
  	    if( (a->flags & APROGRAM) ) {
  	      if(!a->value)
  	        continue;
  	      if((*a->value == '!') && (enactor != genactor)) {
  	        if(listen_match(player, a->value + 1, str, -1))
  	          return TRUE;
  	      }
  	    }
  	  } 
  	//}
  	
	return FALSE;  	  
}

/*
*/
void 
  do_listen(player, str)
  	object *player;
  	char *str;
{
	ATTR *a;
	char buf[MAX_BUFSIZE];
	
	strcpy(buf, strip_color(str));
	buf[strlen(buf)]= '\0';
/** PARENTING LISTEN...
  for(p = player->parent; p; p=p->next)
    if(p->what)
      if(parent_check(p->what, buf))
        return;
    for(a = alist; a; a=a->next)
**/
	if(did_listen(player, player, buf))
	  return;


	for(a = player->alist; a; a=a->next) {
	  if( (a->flags & APROGRAM) ) {
	    if(!a->value)
	      continue;
	    if((*a->value == '!') && (player != genactor))
	      listen_match(player, a->value + 1, buf, -1);
	  }
	}
	
	
}

/*
*/
int
  match_trigger(player, str)
    	object *player;
  	char *str;
{
	object *defaultzone=match_thing(atr_get(db[0], A_ZONE));
	CONTENTS *c;
	ATTR *a;
	int triggered = FALSE;
	
	/* We want to search through the attributes for LISTEN (!) or ACTION ($) code... 
	   First, we want to search through attributes ON the player, or inherent of the player.
	   Second, we want to search through attributes IN the room, in this order:
	   	Room
	   	Contents (with exception of enactor)
	   	Exits
	*/
	
	for(a = player->location->alist; a; a=a->next) {
	    if(!a->value)
	      continue;
	    if(*a->value == '$') {
	      if(listen_match(player->location, a->value + 1, str, 0))
	        triggered = TRUE;
	    }
	}
	
	if(triggered)
	  return(triggered);
	
	for(c = player->location->contents; c; c=c->next)
	{
	  for(a = c->content->alist; a; a=a->next) {
	    if( (a->flags & APROGRAM) ) {
	      if(!a->value)
	        continue;
	      if(*a->value == '$') {
	        if(listen_match(c->content, a->value + 1, str, 0))
	          triggered = TRUE;
	      }
	    }
	  }
	  if(!triggered) {
	    if(DOPARENT(c->content, c->content, str, FALSE))
	      triggered = TRUE;
	  }
	}

	if(!triggered) {
	  if(player->location->zone) { // must be in a ROOM
	    if(DOZONE(player, player->location->zone, str))
	      triggered = TRUE;
	  } else {
	    triggered = FALSE;
	  }
	}
	
	if(!triggered) {
	  if(player->location->zone != defaultzone) {
	    if(DOZONE(player, defaultzone, str))
	      triggered = TRUE;
	  } else {
	    triggered = FALSE;
	  }
	}
	

	//do_timer();
	return(triggered);

}
