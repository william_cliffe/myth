/****************************************************************************
 MYTH - Player communications handling.
 Coded by Saruk (01/23/99)
 ---
 Takes care of player creation and other player-specific functions.
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "db.h"
#include "net.h"
#include "externs.h"


char *color_eval(char *str)
{
	char result[10000];
	char *p, *e, *c;
	char *bf;
	
	strcpy(result, "");
	e = result;
	for(p = str; *p; p++) {
	  if(*p == '^') {
	    p++;
	    if(*p == '^') {
	      *e++ = *p;
	      continue;
	    } else if(*p == 'c') {
	      p++;
	      if(*p == ' ') 
	        continue;
	      if(!(bf = ansi_chartab[(unsigned char)*p])) {
	        *e++ = '^'; *e++ = 'c';
	        *e++ = *p;
	        continue;
	      }
	      for(c = bf; *c; c++)
	        *e++ = *c;
	    } else {
	      p--;
	      *e++ = *p;
	      //continue;
	    }
	  } else {
	    *e++ = *p;
	  }
	}

	bf = ansi_chartab['n'];
	for(c = bf; *c; c++)
	  *e++ = *c;
	*e = '\0';
	return(stralloc(result));
}

void sstrcat(old, string, app)
	char *old;
	char *string;
	char *app;
{
	char *s;
	int hasnonalpha = 0;
	
	if((strlen(app)+(s=(strlen(string)+string))-old)>950)
		return;
	strcpy(s, app);
	for(;*s;s++)
		if( (*s== '(') && !hasnonalpha)
			*s = '<';
		else {
			if( (*s==',') || (*s == ';') || (*s == '['))
				*s = ' ';
			if(!isalpha(*s) && *s != '#' && *s != '.')
				hasnonalpha = 1;
		}
}


// player refers to the activating player
// privs refers to the object with priveleges.
void o_pronoun_substitute(result, player, str, privs)
	char *result;
	object *player;
	char *str;
	object *privs;
{
	char c;
	static char buf[MAX_BUFSIZE];
	char *ores, *sex;
	char *t = buf;
	int coloured = 0;
	
	sex = atr_get(player, A_SEX);
	if(!*sex || !sex)
		sex = "eir";
	else if(string_prefix(sex, "female"))
		sex = "her";
	else if(string_prefix(sex, "male"))
		sex = "his";
	else
	        sex = "eir";
	        
	strcpy(ores = result, player->name);
	result += strlen(result);
	*result++ = ' ';
	while(*str && ((result-ores)<1000)) {
		if(*str == '[') {
			char buff[MAX_BUFSIZE];
			str++;
			exec(&str, buff, privs, player, 0);
			if((strlen(buff)+(result-ores))>950)
				continue;
			strcpy(result, buff);
			result+=strlen(result);
			if(*str==']')
				str++;
		} else if (*str == '%') { // percent 
			*result = '\0';
			c = *(++str);
			
			switch(c) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					if(!gvar[c-'0'])
						break;
					sstrcat(ores, result, gvar[c-'0']);
					break;
				case 'r':
				case 'R':
					sstrcat(ores, result, "\n");
					//strcat(result+strlen(result), "\n");
					break;
				case 'b':
				case 'B':
					sstrcat(ores, result, " ");
					break;
				case 't':
				case 'T':
					sstrcat(ores, result, "\t");
					break;
				case 'n':
				case 'N':
				        //sstrcat(ores, result, pstr("%s", color_name(genactor, -1)));
				        cat_str(result, color_name(genactor, -1));
					break;
				case 'p':
				case 'P':
				        sprintf(result+strlen(result), "%s", sex);
				        break;
				case 'x':
				case 'X':
					str++;
					if(*str == ' ')
					  break;

				        cat_str(result, (ansi_chartab[(unsigned char)*str]) ? ansi_chartab[(unsigned char)*str] : "");
				        coloured = 1;
					break;		
				case '#':
					if(!genactor)
					  genactor = player;
				        sprintf(result+strlen(result),"#%d",genactor->ref);
				        break;
				default: 
					if((result-ores)>990)
						break;
					*result = *str;
					result[1] = '\0';
					break;
			}
			if(isupper(c) && c != 'N')
				*result = to_upper(*result);
			
			result += strlen(result);
			if(*str)
				str++;
		} else {
			if((result-ores)>990) break;
			if((*str=='\\') && (str[1]))
				str++;
			*result++ = *str++;
		}
	}

        if(coloured) {
          *result = '\0';
	  sprintf(result + strlen(result), "%s", ansi_chartab['n']);
	  result += strlen(result);
	  *result = '\0';
	} else
	  *result = '\0';	
}

// substitute percent r (%r)
char *substitute(char *in)
{
	static char buf[HUGE_BUFSIZE];
	char *p=buf;
	char *s, *c;
	int i;
	
	if(!*in)
		return "";
	
	strcpy(buf, "");
	for(s = in; *s; s++, p++)
		if(*s == '%') {
			s++;
			switch(*s) {
			  case 'r':
			  	*p = '\n';
			  	break;
			  case 't':
			  	*p = '\t';
			  	break;
			  case 'b':
			  	*p = ' ';
			  	break;
			  case 'a':
			  	*p = '\a';
			  	break;
			  case 'N':
			  case 'n':
			  	for(c= genactor->name; *c; c++,p++)
			  		*p = *c;
			  	p--;
			  	break;
			  case '#':
			  	for(c= pstr("#%d", genactor->ref); *c; c++,p++)
			  		*p = *c;
			  	p--;
			  	break;
			  case '0':
			  case '1':
			  case '2':
			  case '3':
			  case '4':
			  case '5':
			  case '6': 
			  case '7':
			  case '8':
			  case '9':
			  	i = match_num(pstr("%c", *s));
			  	if(gvar[i] != NULL) {
			  		for(c = gvar[i]; *c; c++, p++) {
			  			*p = *c;
			  		}
			  	}
			  	
			  	p--;
			  	break;
			 
			  default:
			  	*p = *s;
			}
		} else {
			*p = *s;
		}
	*p++ = '\0';
	return buf;
}

// lowest level notify player procedure.
void __notify(player, sendstr, tag)
	OBJECT *player;
	char *sendstr;
	int tag;
{	
	DESCRIPTOR *d;
	
	for(d = descriptor_list; d; d=d->next) {
	  if(!d)
	    continue;
	  if(!d->player)
	    continue;
	  if( ((d->state == CONNECTED) || (d->state == PASTE_MODE)) && (d->player == player)) {
	    dwrite(d, sendstr);
	    if(tag)
	      dwrite(d, "\r\n");
	  }
	}
}

/** notify():

    notify also strips out the ANSI code if player doesn't
    have the ANSI flag on.
**/
void notify(player, sendstr)
	OBJECT *player;
	char *sendstr;
{
	if(!sendstr)
	  sendstr = "";

	do_listen(player, strip_color(sendstr));
	
	if(!(player->flags & ANSI))
	  __notify(player, strip_color(sendstr), 1);
	else
	  __notify(player, color_eval(sendstr), 1);
}

/** cnotify(): Colour Notify
**/
void cnotify(player, sendstr)
	object *player;
	char *sendstr;
{
	notify(player, sendstr); //color_eval(sendstr));
}

/** ncnofity(): No Colour/No Parse Notify
**/
void ncnotify(player, sendstr)
	object *player;
	char *sendstr;
{
	__notify(player, sendstr, 1);
}

void clear_exception(exception)
	CONTENTS *exception;
{
	CONTENTS *c, *cnext = NULL;
	
	if(!exception) {
		exception = NULL;
		return;
	}
		
	for(c = exception; c; c=cnext) {
		cnext = c->next;
		block_free(c);
	}

	if(exception)
		exception = NULL;
}

void add_exception(exception, player)
	CONTENTS **exception;
	OBJECT *player;
{
	CONTENTS *newc;
	
	if(!*exception)
		*exception = NULL;
		
	GENERATE(newc, CONTENTS);
	newc->content = player;
	newc->next = *exception;
	(*exception) = newc;
}

// notify in (aside from exception) object.
void notify_in(location, exception, sendstr)
	object *location;
	CONTENTS *exception;
	char *sendstr;
{
	CONTENTS *c, *e = NULL;
	int flagged = 0;
	
	for(c = location->contents; c; c=c->next) {
	  for(e = exception; e; e=e->next)
	    if(e->content == c->content)
	      break;
	  
	  if(e)
	    continue;
	  
	  notify(c->content, sendstr);
	}
}

// MOVE THIS TO: misc/error.c
void perm_denied(player, str)
	OBJECT *player;
	char *str;
{
	notify(player, pstr("[ERROR]: %s", str));
}

// Parse speech
char *speech_parse(player, saystr)
	object *player;
	char *saystr;
{
	static char buf[MAX_BUFSIZE];
	
	if(*saystr == ':')
		strcpy(buf, pstr("%s %s", color_name(player, -1), saystr + 1)); //substitute(saystr+1)));
	else if (*saystr == ';')
		strcpy(buf, pstr("%s's %s", color_name(player, -1), saystr+ 1)); // substitute(saystr+1)));
	else if (*saystr == '.') {
		strcpy(buf, pstr("%s . o O ( %s )",
			color_name(player, -1), saystr + 1)); // substitute(saystr+1)));
	} else 
		strcpy(buf, saystr); //substitute(saystr));
	
	return buf;
}

char *spoof(player, o)
	object *player;
	object *o;
{
	static char buf[MAX_BUFSIZE];
	
	if((o->flags & NOSPOOF) && (o->flags & TYPE_PLAYER))
		sprintf(buf, " (#%d)", player->ref);
	else
		strcpy(buf, "");
	
	return buf;
}

// this is a speech process.
void do_speak(player, saystr, type)
	object *player;
	char *saystr;
	int type;
{
	CONTENTS *c;
	OBJECT *o=NULL, *what=NULL;
	char buf[MAX_BUFSIZE], buff[MAX_BUFSIZE], *bf = buff;
	
	if(player->location->flags & QUIET) {
		error_report(player, M_ENOTALK);
		return;
	}

	pronoun_substitute(buff, player, saystr, player);
	bf = buff + strlen(player->name) + 1;
	strcpy(buf, bf);

	for(c = player->location->contents; c; c=c->next) {
			o = c->content;
			if(type == SPEAK_SAY && o->ref != player->ref) {
				notify(o, pstr("%s says, \"%s\033[0m\"%s", color_name(player, -1), buf, spoof(player, o)));
			} else if(type==SPEAK_SAY && o->ref == player->ref)
				notify(o, pstr("You say, \"%s\033[0m\"", buf));
			else if(type==SPEAK_POSE)
				notify(o, pstr("%s %s%s", color_name(player, -1), buf, spoof(player, o)));
			else if(type==SPEAK_POSS)
				notify(o, pstr("%s's %s%s", color_name(player, -1), buf, spoof(player, o)));	
			else if(type==SPEAK_THINK)
				notify(o, pstr("%s . o O ( %s )%s",
					color_name(player, -1), buf, spoof(player, o)));
			else if(type == SPEAK_TO) { // this gets more complicated.
				if (!(what = match_player(front(saystr)))) {
					error_report(player, M_ENOPLAY);
					return;
				}
				if(player->loc != what->loc) {
					error_report(player, M_ENOTHERE);
					return;
				}
				notify(o, pstr("%s [to %s]: %s%s",
					color_name(player, -1), color_name(what, -1), speech_parse(player, restof(saystr)),
						spoof(player, o)));
			}
	}
}

// remotely talk to a single player.
void do_page(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	OBJECT *who;	// who we're paging.
	char page_buffer[2][MAX_BUFSIZE];
	char topage[MAX_BUFSIZE], alias[MAX_BUFSIZE];
	char buff[MAX_BUFSIZE];
	char pref[8192], suff[8192], buf[8192];
	char *p = NULL;
	
	/* if it's just argument 1 and NOT argument 2, then it's obviously a 
	    match for LastPageTo */
	if(*arg1 && !*arg2) {
		if(!(who = match_player(atr_get(player, A_LASTPAGETO)))) {
		  error_report(player, M_ENOPLAY);
		  return;
		}
		for(p = arg1; *p == ' '; p++);
		strcpy(topage, p);
	} else {
		if(!(who=match_player(arg1))) {
			error_report(player, M_ENOPLAY);
			return;
		}
		for(p = arg2; *p == ' '; p++);
		strcpy(topage, p);
	}

	if(!parse_lock(player, atr_get(who, A_PLAYERLOCK)) || !parse_lock(who, atr_get(player, A_PLAYERLOCK))) {
		error_report(player, M_EPAGELOCK);
		return;
	}
	
	
	if(!(who->flags & CONNECT)) {
		perm_denied(player, "That player is not connected!");
		return;
	}
	
	if((who->flags & INVISIBLE) && !can_control(player, who)) {
		perm_denied(player, "That player is not connected!");
		return;
	}
	
	
	strcpy(pref, "^ch^cw(^cy");
	strcpy(suff, "^ch^cw)\033[0m");
	sprintf(buf, "%s", (*atr_get(player, A_ALIAS)) ? format_color(atr_get(player, A_ALIAS), -1) : pstr("%s%s#%d", ANSI_HILITE, ANSI_YELLOW, player->ref));
	sprintf(alias, "%s%s%s", format_color(pref, -1), buf, format_color(suff, -1));

	if(topage[0] == ':') {
		sprintf(page_buffer[0], "%s %s pages: %s %s", color_name(player, -1), alias,
			color_name(player, -1), topage + 1);
		sprintf(page_buffer[1], "You paged %s with: %s %s", color_name(who, -1),
			color_name(player, -1), topage + 1);
	} else if (topage[0] == ';') {
		sprintf(page_buffer[0], "%s %s pages: %s's %s", color_name(player, -1),
			alias, color_name(player, -1), topage + 1);
		sprintf(page_buffer[1], "You paged %s with: %s's %s",
			color_name(who, -1), color_name(player, -1), topage + 1);
	} else if (topage[0] == '.') {
		sprintf(page_buffer[0], "%s %s pages: %s . o O ( %s )",
			color_name(player, -1), alias, color_name(player, -1), topage + 1);
		sprintf(page_buffer[1], "You paged %s with: %s . o O ( %s )", 
			color_name(who, -1), color_name(player, -1), topage + 1);
	} else {
		sprintf(page_buffer[0], "%s %s pages: %s", color_name(player, -1), alias,
			topage);
		sprintf(page_buffer[1], "You paged %s with: %s", color_name(who, -1),
			topage);
	}

	sprintf(page_buffer[0], "%s%s", page_buffer[0], spoof(player, who));
	notify(who, page_buffer[0]);
	notify(player, page_buffer[1]);	
	
	SET_ATR(player, A_LASTPAGETO, who->name);
}


// empty arg2 can default to "%s whispers sweet nothing to you..."
void do_whisper(player, arg1, arg2)
	object *player;
	char *arg1, *arg2;
{
	object *who;
	
	if(!*arg1) {
	  notify(player, "Exactly who are you trying to whisper to?");
	  return;
	}
	
	if(!(who = remote_match(player, arg1))) {
	  notify(player, "Who?");
	  return;
	}
	
	if(!*arg2) {
	  notify(who, pstr("%s whispers sweet nothings in your ear.", color_name(player, -1)));
	  notify(player, pstr("You whisper sweet nothings to %s.", color_name(who, -1)));
	  return;
	}
	
	notify(who, pstr("%s whispers, \"%s\033[0m\".", color_name(player, -1), arg2));
	notify(player, pstr("You whisper, \"%s\033[0m\" to %s.", arg2, color_name(who, -1)));
}


void do_broadcast(player, arg1)
	object *player;
	char *arg1;
{
	DESCRIPTOR *d;
	char cname[MAX_BUFSIZE], buf[MAX_BUFSIZE];

	strcpy(cname, color_name(player, -1));
	if(*arg1 == ':')
			sprintf(buf, "Official broadcast by %s: %s %s",
				cname, cname, arg1+1);
		else if (*arg1 ==';')
			sprintf(buf, "Official broadcast by %s: %s's %s",
				cname, cname, arg1+1);
		else if(*arg1 == '.')
			sprintf(buf, "Official broadcast by %s: %s . o O ( %s )",
				cname, cname, arg1+1);
		else
			sprintf(buf, "Official broadcast by %s: %s",
				cname, arg1);
	
	for(d = descriptor_list; d; d=d->next)
	  if((d->state == CONNECTED||d->state==PASTE_MODE) && !d->internal)
	    notify(d->player, buf);
}

void do_announce(player, arg1)
	object *player;
	char *arg1;
{
	DESCRIPTOR *d;
	char cname[MAX_BUFSIZE], buf[MAX_BUFSIZE];
	
	strcpy(cname, color_name(player, -1));
	if(player->flags & NO_WALLS) {
		error_report(player, M_EFLGSET);
		return;
	}
	
	if(*arg1 == ':')
			sprintf(buf, "%s announces: %s %s", cname, cname, arg1+1);
		else if(*arg1 == ';')
			sprintf(buf, "%s announces: %s's %s", cname, cname, arg1+1);
		else if (*arg1 == '.') 
			sprintf(buf, "%s announces: %s . o O ( %s\033[0m )",
				cname, cname, arg1+1 );
		else
			sprintf(buf, "%s announces: %s", cname, arg1);
	
	for(d=descriptor_list; d; d=d->next)
		if((d->state == CONNECTED||d->state==PASTE_MODE) && !d->internal)
			if (!(d->player->flags & NO_WALLS) )
				notify(d->player, buf);
}

void do_emit(player, arg1)
	object *player;
	char *arg1;
{
	CONTENTS *c;
	char buf[MAX_BUFSIZE], *bf=buf;

	pronoun_substitute(buf, player, arg1, player);
	bf = buf + strlen(player->name) + 1;
		
	for(c = player->location->contents; c;c=c->next)
		notify(c->content, pstr("%s%s", bf, (((c->content->flags & NOSPOOF) &(c->content->flags & TYPE_PLAYER))) ? pstr("\n(Object: %d)", player->ref):""));
}


void do_pemit(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *who;
	char WHO[MAX_BUFSIZE], *wh = WHO;
	char buf[MAX_BUFSIZE], *bf = buf;

	pronoun_substitute(WHO, player, arg1, player);
	wh = WHO + strlen(player->name) + 1;

	if(!(who = remote_match(player, wh))) {
		notify(player, "Who are you talking about?");
		return;
	}
	
	if(!has_pow(player->ownit, who, POW_REMOTE) && player->loc != who->loc) {
		notify(player, "You need POW_REMOTE for that.");
		return;
	}
	
	if(!arg2 || !*arg2) {
		strcpy(buf, "");
		bf = buf;
	} else {
		pronoun_substitute(buf, player, arg2, player);
		bf = buf + strlen(player->name) + 1;
	}
		
	notify(who, pstr("%s%s", bf, ((who->flags & NOSPOOF) && (who->flags & TYPE_PLAYER)) ? pstr("(#%d)", player->ref):""));
}

void do_zemit(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	object *zone, *o;
	CONTENTS *c;
	
	IS_ARGUMENT(player, (arg1));
	IS_ARGUMENT(player, (arg2));
	
	if(!(zone = remote_match(player, arg1))) {
	  notify(player, "What?");
	  return;
	}
	
	if(!(zone->flags & ZONE) || !(zone->flags & TYPE_THING)) {
	  notify(player, "That is not a zone.");
	  return;
	}
	
	for(o = room_list; o; o=o->next)
	  if(o->zone == zone) {
	    for(c = o->contents; c; c=c->next)
	      if(c->content != o)
	        notify(c->content, pstr("%s", arg2));
	  }
}
