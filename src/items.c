/** items.c $ havencrag (11/17/2004)
  !! As of this moment, proficiency with a weapon will only increase while the weapon is
  maintained in the equipment list. Once unequipped the bonus proficiency is lost.
  
  - It may be fair that when removing a template item and all its corresponding items
    to log it and note it for future player reimbursement. 
  - Selling items and buying items will eventually cost on a sliding scale associated
    with amount onhand and base cost.  
  - Remember, we _MUST_ change the code to allow for selling items to anyone.
  - ** NOTE ** Remember to code in the carry & weight properties and make checks to see if
    purchased items can be carried!
  - Items that are flagged as NOCLONE must be moved each time, no matter who they're sold to. 
    And they always sell for the templated value.
  - One should be able to set flags like: @set/item Sword=flags:WEAPON|BREAKABLE instead of
    having to specify flags on a separate lines.
  - Make it so you can give items.
  - Make the 'use' command.
  - !!! PRETTY UP THE ITEM LISTS, SHOP MENUS, ETC. !!!
  - Remember to add in the formula for HEALTH INCREASE/DECREASE, etc.
**/

#include <stdlib.h> 
#include <string.h> 
#include <stdio.h> 
#include <time.h>
#include <ctype.h>

#include "db.h"
#include "net.h"
#include "externs.h"

int itemtop = 0;
ITEMLIST **itemdb = NULL;

/*** ITEM RETURN FUNCTIONS ***/
char *ShowMaterials(unsigned long long flags)
{
  char buf[4096];
  int idx;
  
  *buf = '\0';
  
  for(idx = 0; materials_list[idx].name; idx++)
    if( (flags & materials_list[idx].material))
      sprintf(buf + strlen(buf), "%s ", materials_list[idx].name);
  
  buf[strlen(buf)-1] = '\0';
  return(stralloc(buf));
}

char *ShowItemFlags(unsigned long long flags, int type)
{
  char buf[4096];
  int idx;
  
  *buf = 0;

  if(!type) {
    for(idx = 0; combatflag_list[idx].name; idx++)
      if( (flags & combatflag_list[idx].flag) )
        sprintf(buf + strlen(buf), "%s ", combatflag_list[idx].name);
  } else if (type == 1) {
    for(idx = 0; affect_list[idx].name; idx++)
      if( (flags & affect_list[idx].flag) )
        sprintf(buf + strlen(buf), "%s ", affect_list[idx].name);
  } else if (type == 2) {
    for(idx = 0; effect_list[idx].name; idx++)
      if( (flags & effect_list[idx].flag) )
        sprintf(buf + strlen(buf), "%s ", effect_list[idx].name);
  } else if (type == 3) {
    for(idx = 0; wear_list[idx].name; idx++)
      if( (flags & wear_list[idx].flag) )
        sprintf(buf + strlen(buf), "%s ", wear_list[idx].name);
  }
  
  buf[strlen(buf)-1] = '\0';
  return(stralloc(buf));
}

/** ShowItemAttrValue():
**/
static char *ShowItemAttrValue(ITEMATTR *iattr)
{
  static char buf[4096];

  *buf = '\0';
  switch(iattr->type) {
    case MOD_STRENGTH:		/** ALL INTEGER VALUES SWITCH HERE **/
    case MOD_INTELLIGENCE:
    case MOD_DEXTERITY:
    case MOD_STAMINA:
    case MOD_CONSTITUTION:
    case MOD_WISDOM:
    case MOD_LUCK:
    case MOD_CHARISMA:
      sprintf(buf, "%d", iattr->value.i);
      break;
    case INITIATE_SOFTCODE:	/** ALL STRING VALUES SWITCH HERE **/
      sprintf(buf, "%s", iattr->value.ch);
      break;
    default: 			/** STUFF THAT DOESN'T HAVE A SWITCH RETURNS NULL **/
      strcpy(buf, "");
      break;
  }
  
  return(buf);
}

/** ShowItemAttr():
**/
static char *ShowItemAttr(ITEMATTR *iattr)
{
  static char buf[4096];
  
  *buf = '\0';
  switch(iattr->type) {
    case MOD_STRENGTH:
      sprintf(buf, "\033[1m^cc+\033[0m^ccStrength\033[1m^cw: ^cy%d", iattr->value.i);
      break;
    case MOD_INTELLIGENCE:
      sprintf(buf, "\033[1m^cc+\033[0m^ccIntelligence\033[1m^cw: ^cy%d", iattr->value.i);
      break;
    case MOD_DEXTERITY:
      sprintf(buf, "\033[1m^cc+\033[0m^ccDexterity\033[1m^cw: ^cy %d", iattr->value.i);
      break;
    case MOD_STAMINA:
      sprintf(buf, "\033[1m^cc+\033[0m^ccStamina\033[1m^cw: ^cy %d", iattr->value.i);
      break;
    case MOD_CONSTITUTION:
      sprintf(buf, "\033[1m^cc+\033[0m^ccConstitution\033[1m^cw: ^cy %d", iattr->value.i);
      break;
    case MOD_WISDOM:
      sprintf(buf, "\033[1m^cc+\033[0m^ccWisdom\033[1m^cw: ^cy %d", iattr->value.i);
      break;
    case MOD_LUCK:
      sprintf(buf, "\033[1m^cc+\033[0m^ccLuck\033[1m^cw: ^cy %d", iattr->value.i);
      break;
    case MOD_CHARISMA:
      sprintf(buf, "\033[1m^cc+\033[0m^ccCharisma\033[1m^cw: ^cy%d", iattr->value.i);
      break;
    case INITIATE_SOFTCODE:
      sprintf(buf, "\033[1m^cc+\033[0m^ccSoftcode\033[1m^cw: ^cy%s", iattr->value.ch);
      break;
    default:
      strcpy(buf, "\033[1m^crUnknown modifier!");
      break;
  }
  
  return(buf);
}



/*** ITEM ATTRIBUTE CREATION ***/

/** SetItemAttrType(): 
**/
void SetItemAttrType(ITEMATTR *iattr, char *value)
{
  switch(iattr->type) {
    case MOD_STRENGTH:
    case MOD_WISDOM:
    case MOD_INTELLIGENCE:
    case MOD_DEXTERITY:
    case MOD_CONSTITUTION:
    case MOD_STAMINA:
    case MOD_LUCK:
    case MOD_CHARISMA:
      iattr->value.i = atoi(value);
      break;
    case INITIATE_SOFTCODE:
      if(iattr->value.ch)
        block_free(iattr->value.ch);
      iattr->value.ch = NULL;
      SET(iattr->value.ch, value);
      break;
    default:
      break;
  }
}

/** ItemAttributeCreate():
**/
ITEMATTR *ItemAttributeCreate(ITEMLIST *item, int type, char *value)
{
  ITEMATTR *i = NULL;
  
  for(i = item->list; i; i=i->next)
    if(i->type == type)
      break;
  
  if(i)
    return(i);
  
  GENERATE(i, ITEMATTR);
  i->type = type;
  SetItemAttrType(i, value);
  
  if(!item->list) {
    i->next = i->prev = NULL;
    item->list = item->listtail = i;
  } else {
    i->next = NULL;
    i->prev = item->listtail;
    item->listtail->next = i;
    item->listtail = i;
  }
  
  return(i);
}


/** RemoveItemAttr():
**/
void RemoveItemAttr(head, tail, iattr)
  ITEMATTR **head, **tail, *iattr;
{
  if(iattr->next) 
    iattr->next->prev = iattr->prev;
  
  if(iattr->prev) 
    iattr->prev->next = iattr->next;
  
  if((*head) == iattr)
    (*head) = iattr->next;
  
  if((*tail) == iattr)
    (*tail) = iattr->prev;
  
  // We'll have to do this on a per-type basis:
  switch(iattr->type) {
    case INITIATE_SOFTCODE:
      if(iattr->value.ch)
        block_free(iattr->value.ch);
      break;
    default:
      break;
  }
  
  block_free(iattr);
}

/** ItemAttributeModify():
**/
ITEMATTR *ItemAttributeModify(ITEMLIST *item, int type, char *value)
{
  ITEMATTR *iattr = NULL;
  
  for(iattr = item->list; iattr; iattr=iattr->next)
    if(iattr->type == type) 
      break;
  
  if(!iattr)
    return(ItemAttributeCreate(item, type, value));
  
  if(isanumber(value)) {
    if(atoi(value) == 0) {
      RemoveItemAttr(&item->list, &item->listtail, iattr);
      return(NULL);
    }
  }
  
  SetItemAttrType(iattr, value);
  return(iattr);
}


/** CountItemAttrs():
**/
int CountItemAttrs(ITEMLIST *i)
{
  ITEMATTR *ia = NULL;
  int cnt = 0;
  
  if(!i->list)
    return 0;
  
  for(cnt = 0, ia = i->list; ia; ia=ia->next, cnt++);
  
  return(cnt);
}


/** ITEM CREATION **/
ITEMLIST *GetItemTemplate(char *item)
{
  ITEMLIST *i=NULL;
  
  if(!item || !*item)
    return(NULL);  

  for(i = itemlist; i; i=i->next)
    if(!strcmpm(i->name, item))
      break;
  
  if(!i)
    return(NULL);

  return(i);
}

int IsTemplateDone(ITEMLIST *item)
{
  unsigned long long tempflag;
  
  tempflag = item->flags;
  tempflag &= ~UNFINISHED;

  
  if(!item->desc || !*item->desc) 
    return 0;
  
  if(!item->affect || !item->effect || !tempflag || !item->weight)
    return 0;

  return 1;
}


unsigned int TopItem()
{
  ITEMLIST *i;
  unsigned int top = 0;
  
  if(!itemlist)
    return 0;
    
  for(i = itemlist; i; i=i->next)
    if(i->id > top)
      top = i->id;
  
  return(top+1);    
}



unsigned int UniqueId()
{
  ITEMLIST *i;
  unsigned int ret = 0, idx;
  int *array;

  if(!itemlist)
    return 0;

  ret = TopItem();

  array = calloc(ret, sizeof(int));
  
  for(i = itemlist; i; i=i->next)
    array[i->id] = 1;  

  for(idx = 0; idx < ret; ++idx)
    if(!array[idx])
      break;
  
  free(array);
  return(idx);
}


void InitItems()
{
  ITEMLIST *i;
  int cnt;
  
  itemtop = TopItem();
  itemdb = (ITEMLIST **)stack_em((sizeof(ITEMLIST *) * (TopItem()+1)), SOLID);
  
  for(cnt = 0; cnt < itemtop; cnt++)
    itemdb[cnt] = 0;
  
  for(i = itemlist; i; i=i->next)
    itemdb[i->id] = i;
}

int CanNameTemplate(char *str)
{
  char *p;
  
  for(p = str; *p; p++)
    if(isupper(*p) || islower(*p) || isspace(*p) || *p == '\'')
      continue;
    else
      return FALSE;
  
  return TRUE;
}

void ItemCreate(player, arg1)
  object *player;
  char *arg1;
{
  ITEMLIST *i;
  int idx, ref;
  
  
  printf("%s\n", arg1);
  ref = UniqueId();
  
  if(ref > itemtop) {
    itemtop++;
    itemdb = (ITEMLIST **)stack_realloc(itemdb, sizeof(struct combat_itemtable_t *) * (ref + 1), SOLID);
  }
  
  if(!has_pow(player, player, POW_COMBAT)) {
    notify(player, "You cannot manipulate combat.");
    return;
  }
  
  for(i = itemlist; i; i=i->next)
    if(!strcmpm(i->name, arg1)) {
      notify(player, "Sorry, that template already exists.");
      return;
    }
  
  
  if(!CanNameTemplate(arg1)) {
    notify(player, "Sorry, you can't name it that.");
    return;
  }
  
  GENERATE(i, ITEMLIST);
  i->id = ref;
  i->name = NULL;
  SET(i->name, arg1);
  i->desc = NULL;
  i->wear = 0;
  i->flags = 0;
  i->flags |= UNFINISHED;
  i->affect = i->effect = i->material = i->worn = 0;
  i->value = i->weight = i->equipped = i->kills = i->uses = 0;
  i->cost = i->proficiency = i->blocks = i->onhand = 0;
  i->alignment = 0;
  i->stresspoint = 1;
  i->list = NULL;
  
  notify(player, pstr("Created %s with unique id %d.", i->name, i->id));
  
  if(!itemlist) {
    i->next = i->prev = NULL;
    itemlist = itemlisttail = i;
  } else {
    i->next = NULL;
    i->prev = itemlisttail;
    itemlisttail->next = i;
    itemlisttail = i;
  }  
  
  itemdb[i->id] = i;
}

/** RemoveItem():
**/
void RemoveItem(ITEMLIST **head, ITEMLIST **tail, ITEMLIST *item)
{
  ITEMATTR *ia, *ianext = NULL;
  
  if(item->next)
    item->next->prev = item->prev;
  
  if(item->prev) 
    item->prev->next = item->next;
  
  if((*head) == item) 
    (*head) = item->next;
  
  if((*tail) == item) 
    (*tail) = item->prev;
  
  if(item->name)
    block_free(item->name);
  if(item->desc)
    block_free(item->desc);

  for(ia = item->list; ia; ia=ianext) {
    ianext = ia->next;
    RemoveItemAttr(&item->list, &item->listtail, ia);
  }
  
  itemdb[item->id] = 0;
  
  block_free(item);  
}

/** RemoveAllItems():
**/
void RemoveAllItems(thing)
  object *thing;
{
  ITEMLIST *i, *inext = NULL;
  
  for(i = thing->items; i; i=inext) {
    inext = i->next;
    RemoveItem(&thing->items, &thing->itemstail, i);
  }
  
  for(i = thing->equipped; i; i=inext) {
    inext = i->next;
    RemoveItem(&thing->equipped, &thing->equippedtail, i);
  }
  
  thing->items = thing->equipped = thing->itemstail = thing->equippedtail = NULL;
}

/** CloneItem(): Only clones the DEFAULT item properties, nothing else.
**/
ITEMLIST *CloneItem(ITEMLIST *item)
{
  ITEMATTR *ia, *ianext = NULL;
  ITEMLIST *i;
  int mods;
  
  if(!item)		// Why this would happen, I dunno. But just in case.
    return(NULL);
  
  if(!IsTemplateDone(item))	// All templates should be finished before anything happens to them.
    return(NULL);
  
  GENERATE(i, ITEMLIST);
  i->name = i->desc = NULL;
  SET(i->name, item->name);
  SET(i->desc, item->desc);
  i->id = item->id;
  i->cost = item->cost;
  i->flags = item->flags;
  i->affect = item->affect;
  i->effect = item->effect;
  i->wear = item->wear;
  i->material = item->material;
  i->value = item->value;
  i->alignment = item->alignment;
  i->weight = item->weight;
  i->stresspoint = item->stresspoint;

  i->equipped = 0;
  i->kills = i->uses = i->proficiency = i->blocks = i->onhand = 0;
  i->worn = 0;
  i->list = NULL;
  i->next = i->prev = NULL;
  
  for(ia = item->list; ia; ia=ianext) {
    ianext = ia->next;
    ItemAttributeModify(i, ia->type, ShowItemAttrValue(ia));
  }
   
  return(i);
}


/** RemoveTemplate():
**/
void RemoveTemplate(player, arg1)
  object *player;
  char *arg1;
{
  ITEMLIST *i = NULL, *ilist, *inext = NULL;
  int idx;
  
  if(!itemlist) {
    notify(player, "There are no templates to remove!");
    return;
  }
  
  for(i = itemlist; i; i=i->next)
    if(!strcmpm(i->name, arg1))
      break;
  
  if(!i) {
    notify(player, "What template are you trying to remove?");
    return;
  }    
  
  // Okay, now we have to go through the entire DB and remove any items
  // associated with this template.
  for(idx = 0; idx < dbtop; idx++) {
    if(!(db[idx]))
      continue;
    
    for(inext = NULL, ilist = (db[idx])->items; ilist; ilist = inext) {
      inext = ilist->next;
      if(ilist->id == i->id)
        RemoveItem(&(db[idx])->items, &(db[idx])->itemstail, ilist);
    }
    
    for(inext = NULL, ilist = (db[idx])->equipped; ilist; ilist = inext) {
      inext = ilist->next;
      if(ilist->id == i->id)
        RemoveItem(&(db[idx])->items, &(db[idx])->itemstail, ilist);
    }
  }
  
  RemoveItem(&itemlist, &itemlisttail, i);
  notify(player, "Template removed.");
}

// Set combatflaglist flags...
int SetCombatFlag(ITEMLIST *item, char *value, int type)
{
  int idx, truth;

  if(!(truth = (*value == '!') ? 0 : 1))
    value++;

  switch(type) {
    case 0: // Combat Flags;
      for(idx = 0; combatflag_list[idx].name; idx++)
        if(string_prefix(value, combatflag_list[idx].name)) {
          if(truth)
            item->flags |= combatflag_list[idx].flag;
          else {
            if(combatflag_list[idx].flag == UNFINISHED) 
              if(!IsTemplateDone(item))
                return -1;
            item->flags &= ~combatflag_list[idx].flag;
          }
          return 1;
        }
      break;
    case 1: // Affect Flags;
      for(idx = 0; affect_list[idx].name; idx++)
        if(string_prefix(value, affect_list[idx].name)) {
          if(truth)
            item->affect |= affect_list[idx].flag;
          else
            item->affect &= ~affect_list[idx].flag;
          return 1;
        }
      break;
    case 2: // Effect Flags;
      for(idx = 0; effect_list[idx].name; idx++)
        if(string_prefix(value, effect_list[idx].name)) {
          if(truth)
            item->effect |= effect_list[idx].flag;
          else
            item->effect &= ~effect_list[idx].flag;
          return 1;
        }
      break;
    case 3: // Wear Flags;
      for(idx = 0; wear_list[idx].name; idx++)
        if(string_prefix(value, wear_list[idx].name)) {
          if(truth)
            item->wear |= wear_list[idx].flag;
          else
            item->wear &= ~wear_list[idx].flag;
          return 1;
        }
      break;
    default:
      return 0;
      break;
  }

  return 0;
}


int SetMaterialFlag(ITEMLIST *item, char *flag)
{
  char *check;
  int idx;
  
  check = (*flag == '!') ? flag+1 : flag;
  
  for(idx = 0; materials_list[idx].name; idx++)
    if(!strcmpm(materials_list[idx].name, check)) {
      if(*flag == '!') {
        item->material &= ~materials_list[idx].material;
      } else {
        if((item->flags & materials_list[idx].flag)) {
          item->material |= materials_list[idx].material;
        }
        else
          return 0;
      }
      return 1;
    }
  
  return 0;  
}

/** UpdateItemProperties():
**/
void UpdateItemProperties(ITEMLIST *i, ITEMLIST *item, int type)
{
  ITEMATTR *ia, *ianext;
  
  if(strcmpm(i->name, item->name)) {
    SET(i->name, item->name);
  }
  
  if(strcmpm(i->desc, item->desc)) {
    SET(i->desc, item->desc);
  }  

  i->cost = item->cost;
  i->flags = item->flags;
  i->affect = item->affect;
  i->effect = item->effect;
  i->wear = item->wear;
  i->material = item->material;
  i->value = item->value;
  i->alignment = item->alignment;
  i->weight = item->weight;
  i->stresspoint = item->stresspoint;
  
  if(type == 2) {
    for(ia = i->list; ia; ia=ianext) {
      ianext = ia->next;
      RemoveItemAttr(&i->list, &i->listtail, ia);
    }
  }
  
  for(ia = item->list; ia; ia=ianext) {
    ianext = ia->next;
    ItemAttributeModify(i, ia->type, ShowItemAttrValue(ia));
  }    
}



/** UpdateItems(): Updates ALL items in the db from the newly modified template
**/
void UpdateItems(ITEMLIST *item, int type)
{
  ITEMLIST *i = NULL;
  object *o;
  int idx = 0;
  
  for(idx = 0; idx < dbtop; idx++) {
    if(!(o = (db[idx])))
      continue;
    for(i = o->items; i; i=i->next)
      if(i->id == item->id)
        break;
    if(i) 
      UpdateItemProperties(i, item, type);
    
    if(!(o->equipped))
      continue;
    for(i = o->equipped; i; i=i->next)
      if(i->id == item->id)
        break;
    if(!i)
      continue;
    
    UpdateItemProperties(i, item, type);
  }
}

/** SetItemModifier():
**/
int SetItemModifier(object *player, ITEMLIST *item, char *argument, char *value)
{
  ITEMATTR *iattr;
  int idx;
  
  for(idx = 0; iattr_list[idx].name; idx++) {
    if(string_prefix(argument, iattr_list[idx].name)) {
      if(!(iattr = ItemAttributeModify(item, iattr_list[idx].type, value)))
        return FALSE;
      return TRUE;
    }
  }
  
  return FALSE;
}

// The ideal syntax is: @set/item Sword=flags:WEAPON
// .................or: @set/item Sword=desc:A non descript sword.
void SetItem(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  ITEMLIST *i = NULL;
  char argument[1024], value[1024];
  int ret; // used for unique returns from flag lists.
  
  if(!has_pow(player, player, POW_COMBAT)) {
    notify(player, "You need the ability to modify combat properties.");
    return;
  }  
  
  for(i = itemlist; i; i=i->next)
    if(!strcmpm(i->name, arg1))
      break;
  
  if(!i) {
    notify(player, "That item does not appear in the template database.");
    return;
  }
  
  strcpy(argument, fparse(arg2, ':'));
  strcpy(value, rparse(arg2, ':'));
  
  if(string_prefix(argument, "update")) {
    UpdateItems(i, 1);
    notify(player, "Updated all items corresponding to the template.");
    return;
  } else if (string_prefix(argument, "reset")) {
    UpdateItems(i, 2);
    notify(player, "Reset all items corresponding to the template.");
    return;
  }
  
  if(!*argument || !*value) {
    notify(player, "Not enough information to proceed.");
    return;
  }
  
  if(string_prefix(argument, "flags")) {
    if(!(ret = SetCombatFlag(i, value, 0))) {
      notify(player, "What combat flag did you mean?");
      return;
    }
    if(ret == -1) {
      notify(player, "You cannot unset UNFINISHED until the template has been completed.");
      return;
    }
    notify(player, "Combat flag set.");
  } else if (string_prefix(argument, "affect")) {
    if(!(ret = SetCombatFlag(i, value, 1))) {
      notify(player, "What affect flag did you mean?");
      return;
    } 
    
    notify(player, "Affect set.");
  } else if (string_prefix(argument, "effect")) {
    if(!SetCombatFlag(i, value, 2)) {
      notify(player, "What effect flag did you mean?");
      return;
    }
    notify(player, "Effect set.");
  } else if (string_prefix(argument, "wear")) {
    if(!SetCombatFlag(i, value, 3)) {
      notify(player, "What location flag did you mean?");
      return;
     }
    notify(player, "Location set.");
  } else if (string_prefix(argument, "desc")) {
    SET(i->desc, value);
    notify(player, pstr("%s's description now reads: %s", i->name, i->desc));
  } else if (string_prefix(argument, "material")) {
    if(!SetMaterialFlag(i, value)) {
      notify(player, "Invalid material.");
      return;
    }
    notify(player, "Material set.");
  } else if (string_prefix(argument, "value")) {
    i->value = atoi(value);
    notify(player, pstr("%s's value set to %d.", i->name, i->value));
  } else if (string_prefix(argument, "weight")) { 
    if( (i->weight = atof(value)) <= 0) {
      notify(player, "The item cannot be weightless.");
      return;
    }
    notify(player, pstr("%s weighs %f lbs.", i->name, i->weight));
  } else if (string_prefix(argument, "stresspoint")) {
    if( (i->stresspoint = atoi(value)) <= 0) {
      notify(player, "That's below the minimum stress point.");
      i->stresspoint = 1;
      return;
    }
    notify(player, pstr("%s's stress point is set to %d.", i->name, i->stresspoint));
  } else if (string_prefix(argument, "cost")) {
    i->cost = atol(value);
    // Different currencies will be introduced later.
    notify(player, pstr("The item now costs %ld gold", i->cost));
  } else if (string_prefix(argument, "alignment")) {
    i->alignment = atoi(value);
    if(i->alignment < -1 || i->alignment > 1) {
      notify(player, "Invalid alignment assignment.");
      return;
    }
    notify(player, pstr("Alignment on %s set to %s.", i->name, (i->alignment == -1) ? "evil" : i->alignment ? "good" : "neutral"));
  } else {
    if(!SetItemModifier(player, i, argument, value))
      notify(player, "I'm not sure what you want to do.");
    return;
  }
}

void ListItem P((object *, char *));

void ShowItems(player, arg1)
  object *player;
  char *arg1;
{
  ITEMLIST *i;
  int cnt;
  
  if(!itemlist) {
    notify(player, "There are no items.");
    return;
  }
  
  if(!has_pow(player, player, POW_COMBAT)) {
    notify(player, "Access to the item template list is restricted.\nYou can view items in a shop.");
    return;
  }
  
  if(*arg1) {
    ListItem(player, arg1);
    return;
  }
  
  for(cnt = 0, i = itemlist; i; i=i->next, cnt++) {
    if( (i->flags & UNFINISHED) ) 
      notify(player, pstr("%s is unfinished, please complete.", i->name));
    else
      notify(player, pstr("\033[1m^cy%d^cw.) ^cr%s ^cw[^cmID^cw:^cg%d^cw] ^cw(%s^cy%d^cw)", cnt, i->name, i->id, ((i->value) < 1) ? "" : "+", i->value));
  }
}

void ShowItemTo(object *player, ITEMLIST *i)
{
  ITEMATTR *iattr;
  notify(player, pstr("^ccName\033[1m^cw: ^cr%s ^cw(^cgID^cw: ^cy%d^cw)\n\033[0m^ccDesc\033[1m^cw: ^cc%s", i->name, i->id, i->desc));
  notify(player, pstr("^ccFlags\033[1m^cw: ^cy%s\n\033[0m^ccAffects^cw\033[1m: ^cy%s\n\033[0m^ccEffects\033[1m^cw: ^cy%s\n\033[0m^cc%s\033[1m^cw: ^cy%s",
    ShowItemFlags(i->flags, 0), ShowItemFlags(i->affect, 1), ShowItemFlags(i->effect, 2), 
    ((i->flags & WEAPON)) ? "Wield" : "Wear", ShowItemFlags(i->wear, 3)));
  notify(player, pstr("^ccMaterials\033[1m^cw: ^cy%s", ShowMaterials(i->material)));
  notify(player, pstr("^ccBase affecting value\033[1m^cw: ^cy%d\n\033[0m^ccCost\033[1m^cw: ^cy%ld", i->value, i->cost));
  notify(player, pstr("^ccStress point\033[1m^cw: ^cy%d\n\033[0m^ccThis weapon is preferred for a%s ^ccalignment.",
    i->stresspoint, (i->alignment == -1) ? "n \033[1m^cyevil\033[0m" : (i->alignment) ? " \033[1m^cygood\033[0m" : " \033[1m^cyneutral\033[0m"));
  notify(player, pstr("^ccThis object weighs \033[1m^cy%-3.2f \033[0m^cclbs.", i->weight));
  for(iattr = i->list; iattr; iattr=iattr->next)
    notify(player, ShowItemAttr(iattr));
}

void ListItem(object *player, char *showitem)
{
  ITEMLIST *i = NULL;
  
  if(!itemlist) {
    notify(player, "No items in template database.");
    return;
  }
  
  for(i = itemlist; i; i=i->next)
    if(!strcmpm(i->name, showitem))
      break;
  
  if(!i) {
    notify(player, "No such item in template database.");
    return;
  }
  
  ShowItemTo(player, i);
}

void ShowItem(player, location, showitem)
  object *player, *location;
  char *showitem;
{
  ITEMLIST *i = NULL;
  ITEMATTR *iattr;
  
  for(i = location->items; i; i=i->next)
    if(!strcmpm(i->name, showitem))
      break;
  
  if(!i) {
    notify(player, "We don't have that in stock.");
    return;
  }
  
  ShowItemTo(player, i);  
}

void ShowStoreMenu(player, arg1)
  object *player;
  char *arg1;
{
  ITEMLIST *i;
  
  if(!(player->location->flags & SHOP)) {
    notify(player, "This is not a shop!");
    return;
  }
  
  if(!(i = player->location->items)) {
    notify(player, "This shop has no items.");
    return;
  }

  if(*arg1) {
    ShowItem(player, player->location, arg1);
    return;
  }

  notify(player, pstr("\033[1m^cw.%s.", make_ln("-", 58)));
  notify(player, pstr("\033[1m^cw| ^cb%-14.14s ^cw| ^cb%-24.24s ^cw| ^cb%-4.4s ^cw| ^cb%-5.5s ^cw|",
    "NAME", "DESCRIPTION", "COST", "STOCK"));
  notify(player, pstr("\033[1m^cw|%s|%s|%s|%s|", make_ln("-", 16), make_ln("-", 26), make_ln("-", 6), make_ln("-", 7)));
  for(;i;i=i->next) {
    notify(player, pstr("\033[1m^cw| ^cr%-14.14s ^cw| \033[0m^cc%-24.24s \033[1m^cw| ^cy%-4ld ^cw| ^cg%-5d ^cw|", i->name, i->desc, i->cost, i->onhand));
  }
  notify(player, pstr("\033[1m^cw'%s'", make_ln("-", 58)));
  notify(player, "\n^ccTo purchase an item, type\033[1m^cw: ^cybuy item \033[0m^ccor \033[1m^cybuy item^cw=^cc#\033[0m\n^ccTo view an item, type\033[1m^cw: ^cymenu item");
}

ITEMLIST *HasItem(object *what, ITEMLIST *item)
{
  ITEMLIST *i = NULL;
  
  for(i = what->items; i; i=i->next)
    if(i->id == item->id)
      break;
    
  if(i)
    return(i);
    
  return(NULL);
}

ITEMLIST *SellItemTo(object *player, ITEMLIST *item, int num)
{
  ITEMLIST *i = NULL;
  int idx;
  
  // If the player has the item, but it is equipped, don't worry about it.
  // There are some minor adjustments needed to get things to work smoothly
  // with the coming proficiency formulas.
  for(i = player->items; i; i=i->next)
    if(i->id == item->id)
      break;
  
  // We may have to make a few adjustments for things such as
  // Weapons and Armour. 
  if(i) {
    i->onhand += num;
    return(i);
  }
  
  i = CloneItem(item);
  i->onhand += num;
  
  if(!player->items) {
    i->next = i->prev = NULL;
    player->items = player->itemstail = i;
  } else {
    i->next = NULL;
    i->prev = player->itemstail;
    player->itemstail->next = i;
    player->itemstail = i;
  }
 
  return(i); 
  
}

void BuyItem(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  ITEMLIST *i = NULL, *item;
  long long onhand;
  int tobuy;
  
  if(!*arg1) {
    notify(player, "Buy what?");
    return;
  }
  
  if(!(player->location->flags & SHOP) || !(player->location->flags & TYPE_ROOM)) {
    notify(player, "This is not a shop!");
    return;
  }
  
  if(!player->location->items) {
    notify(player, "There are no items for sale at this shop.");
    return;
  }
  
  onhand = atoll(atr_get(player, A_GOLD));

  for(i = player->location->items; i; i=i->next)
    if(!strcmpm(i->name, arg1))
      break;

  if(!i) {
    notify(player, "That item is not in stock.");
    return;
  }
  
  if(*arg2) {
    if((tobuy = atoi(arg2)) <= 0) {
      notify(player, "Very funny.");
      return;
    }
  } else {
    tobuy = 1;
  }
  
  if(tobuy > i->onhand) {
    notify(player, "There are not that many items in stock.");
    return;
  }
  
  if( (i->cost * tobuy) > onhand) {
    notify(player, "You don't have enough gold for that.");
    return;
  }
  
  // Let's think about assigning trouble tickets for these kind of errors.
  if(!(item = SellItemTo(player, i, tobuy))) {
    notify(player, "\033[1m^cr**\033[0m There was an error giving you the item. Please tell an administrator.");
    return;
  }
  
  onhand -= (i->cost * tobuy);
  notify(player, pstr("You paid %ld gold for %d %s.", (i->cost*tobuy), tobuy, i->name));
  SET_ATR(player, A_GOLD, pstr("%lld", onhand));
  
  if( (i->onhand -= tobuy) == 0) {
    RemoveItem(&player->location->items, &player->location->itemstail, i);  
  }
}

void SellItem(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  ITEMLIST *i = NULL, *instock = NULL, *item;
  int num;
  long long onhand;

  if(!(player->location->flags & SHOP) || !(player->location->flags & TYPE_ROOM)) {
    notify(player, "You can only sell items in shops, currently.");
    return;
  }
  
  if(!*arg1) {
    notify(player, "We're not sure what you want to sell.");
    return;
  }
  
  if(*arg2) {
    if( (num = atoi(arg2)) <= 0) {
      notify(player, "That's a bit silly.");
      return;
    }
  } else {
    num = 1;
  }
  
  if(num > MAX_INSHOP_ITEM) {
    notify(player, pstr("Sorry, but this shop cannot exceed %d of any specific item.", MAX_INSHOP_ITEM));
    return;
  }
  
  for(i = player->items; i; i=i->next)
    if(!strcmpm(i->name, arg1))
      break;
  
  if(!i) {
    notify(player, "You don't have that item.");
    return;
  }
  
  if(i->onhand < num) {
    notify(player, "You don't have that many items to sell.");
    return;
  }
  
  onhand = atoll(atr_get(player, A_GOLD));
  
  if( (instock = HasItem(player->location, i)) ) {
    if(instock->onhand + num > MAX_INSHOP_ITEM) {
      notify(player, pstr("This shop already has %d items in stock. In cannot exceed %d.", instock->onhand, MAX_INSHOP_ITEM));
      return;
    }
  } else {
    if(num > MAX_INSHOP_ITEM) {
      notify(player, pstr("This shop cannot exceed %u items in stock.", MAX_INSHOP_ITEM));
      return;
    }
  }

  instock = SellItemTo(player->location, i, num);
  
  SET_ATR(player, A_GOLD, pstr("%lld", onhand + (num * instock->cost)));
  notify(player, pstr("You made %ld gold from selling your %s.", (num * instock->cost), instock->name));
  
  if( (i->onhand -= num) == 0) 
    RemoveItem(&player->items, &player->itemstail, i);
  
}

// The proper format for AddToShop is:
// @add/shop #100=11,Sword
// or @add/shop #100=Sword
void AddToShop(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  ITEMLIST *item, *i = NULL;
  object *what;
  char itemadd[1024];
  int num = 0, idx;

  if(!*arg1 || !*arg2) {
    notify(player, "Sorry, not enough arguments.");
    return;
  }
  
  if(!(what = remote_match(player, arg1))) {
    notify(player, "We're not sure what you're referring to.");
    return;
  }
  
  if(!(what->flags & SHOP) && !(what->flags & TYPE_ROOM)) {
    notify(player, "That's not a shop!");
    return;
  }
  
  if(strchr(arg2, ',')) {
    num = atoi(fparse(arg2, ','));
    strcpy(itemadd, rparse(arg2, ','));
  } else {
    num = 1;
    strcpy(itemadd, arg2);
  }
  
  if(num < 1) {
    notify(player, "Please pick a valid number.");
    return;
  } else if (num > MAX_INSHOP_ITEM) {
    notify(player, pstr("Sorry, the maximum number of any specific item in a shop is %s.\nResetting to %s items.",
      comma(MAX_INSHOP_ITEM), comma(MAX_INSHOP_ITEM)));
    num = MAX_INSHOP_ITEM;
  }
  
  for(item = what->items; item; item=item->next)
    if(!strcmpm(item->name, itemadd))
      break;
      
  if(item) {
    notify(player, pstr("Added %d %ss.", num, item->name));
    item->onhand += num;
    return;
  }

  if(!(i = GetItemTemplate(itemadd))) {
    notify(player, "What template?");
    return;
  }
  
  if((i->flags & UNFINISHED)) {
    notify(player, "You cannot add unfinished items to a store.\nPlease fix this template.");
    return;
  }

  if(i->cost == 0)
    notify(player, pstr("** Warning ** This item has no cost.\nWe will add it anyway, but you can delete it using: @del/shop #%d=ALL,%s", what->ref, i->name));
  else if (i->cost < 0) 
    notify(player, "** Warning ** Adding this item will give the player the associated negative cost.\nContinuing...");

  item = CloneItem(i);
  item->onhand += num;
  if(!(what->items)) {
    item->next = item->prev = NULL;
    what->items = what->itemstail = item;
  } else {
    what->itemstail->next = item;
    item->next = NULL;
    item->prev = what->itemstail;
    what->itemstail = item;
  }
  
  notify(player, pstr("%d %s now in stock at %s", num, item->name, color_name(what, -1)));
}



void DelFromShop(player, arg1, arg2)
  object *player;
  char *arg1;
  char *arg2;
{
  ITEMLIST *i = NULL;
  object *what;
  char itemdel[1024];
  int num = 0;
  
  if(!*arg1 || !*arg2) {
    notify(player, "Invalid number of arguments.");
    return;
  }
  
  if(!(what = remote_match(player, arg1))) {
    notify(player, "What shop?");
    return;
  }
  
  if(!(what->flags & SHOP) && !(what->flags & TYPE_ROOM)) {
    notify(player, "That is not a shop!");
    return;
  }

  if(!what->items) {
    notify(player, "This shop is empty.");
    return;
  }
  
  if(strchr(arg2, ',')) {
    if(strcmpm(fparse(arg2, ','), "ALL"))
      num = atoi(fparse(arg2, ','));
    else
      num = MAX_INSHOP_ITEM+1;
    strcpy(itemdel, rparse(arg2, ','));
  } else {
    num = 1;
    strcpy(itemdel, arg2);
  }
  
  if(num <= 0) {
    notify(player, "Invalid number of items.");
    return;
  }
  
  for(i = what->items; i; i=i->next)
    if(!strcmpm(i->name, itemdel))
      break;
  
  if(!i) {
    notify(player, "This store does not have that.");
    return;
  }
  
  if(num > MAX_INSHOP_ITEM || num >= i->onhand) {
    RemoveItem(&what->items, &what->itemstail, i);
    notify(player, "Item removed from shop.");
  } else {
    i->onhand -= num;
    notify(player, pstr("Removed %d %s%s from shop.\n%d remain.", num, i->name, (num) ? "s" : "", i->onhand));
  }
}

/*** USE SECTION ***/

/** UseItemPotion():
**/
void UseItemPotion(object *player, ITEMLIST *item, char *arg1)
{
  object *who;
  CSTATS *c;
  
  if(!*arg1)
    who = player;
  else {
    if(!(who = remote_match(player, arg1))) {
      notify(player, "Who are you talking about?");
      return;
    }
  }
  
  if(!(c = who->combat)) {
    notify(player, "They're not set up for combat.");
    return;
  }
  
  if((c->cflags & DEAD)) {
    notify(player, "Items cannot be used on the dead.");
    return;
  }
  
  if((item->affect & AFFECT_HEALTH)) {
    if((item->effect & EFFECT_INCREASE)) { // Eventually add formula in.
      if(c->hp >= c->maxhp) {
        notify(player, pstr("%s%s hitpoints are already at maximum!",
          (who==player) ? "Your" : color_name(who, -1), (who == player) ? "" : "'s"));
        return;
      }
      if( (c->hp += item->value) > c->maxhp)
        c->hp = c->maxhp;
      if(player == who) {
        notify(player, pstr("Your HP increased by %d points!", item->value));
      } else {
        notify(who, pstr("%s just used %s on you, and increased your HP by %d!",
          color_name(player, -1), item->name, item->value));
        notify(player, pstr("You just increased %s's hitpoints by %d!", color_name(who, -1), item->value));
      }
    } else if ( (item->effect & EFFECT_DECREASE) ) {
      c->hp -= item->value;
    }
  }
  
  // Once used, destroy it or reduce it.
  if( (item->onhand--) <= 0)
    RemoveItem(&player->items, &player->itemstail, item);
}

/** UseItem():
**/
void UseItem(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  ITEMLIST *item;
  CSTATS *c;
  
  if(!*arg1) {
    notify(player, "Use what?");
    return;
  }
  
  if(!(c = player->combat)) {
    notify(player, "You cannot use items unless you're set up for combat.");
    return;
  }
  
  if((c->cflags & DEAD)) {
    notify(player, "You cannot use items while dead!");
    return;
  }
  
  for(item = player->items; item; item=item->next)
    if(!strcmpm(item->name, arg1))
      break;
  
  if(!item) {
    notify(player, "You don't have that.");
    return;
  }
  
  if( (item->flags & WEAPON) || (item->flags & SHIELD) || (item->flags & ARMOUR) ) {
    notify(player, "Try equipping that so you can use it in a fight.");
    return;
  }
  
  if((item->flags & POTION)) {
    UseItemPotion(player, item, arg2);
  }
}


/** EQUIPMENT SECTION **/

/** IsEquipped():
**/
char *IsEquipped(object *player, unsigned long long flag)
{
  ITEMLIST *i;
  
  for(i = player->equipped; i; i=i->next) {
    if( (i->worn & flag) && i->equipped) 
      return(i->name);
  }
  
  return("");
}

/** EqName():
**/
static char *EqName(ITEMLIST *item, unsigned long long loc)
{
  ITEMLIST *i;
  static char buf[1024];
  
  *buf = '\0';
  for(i = item; i; i=i->next) {
    if( (i->worn & loc) && i->equipped) {
      sprintf(buf, "%s (+%d)", i->name, i->value);
      return(buf);
    }
  }
      
  return("Nothing");
}

/** ShowEquipment():
**/
void ShowEquipment(player, arg1)
  object *player;
  char *arg1;
{
  ITEMLIST *item;
  object *who;
  int attack = 0, defend = 0;

  if(!arg1 || !*arg1)
    who = player;
  else if(!(who = remote_match(player, arg1))) {
    notify(player, "Which player?");
    return;
  }
  
  if(!(can_control(player, who))) {
    notify(player, "You cannot do that.");
    return;
  }
  
  if(!who->equipped) {
    notify(player, pstr("%s are naked!", (who == player) ? "You" : "They"));
    return;
  }
      
  notify(player, pstr("---<%s's Equipment>%s", color_name(who, -1), make_ln("-",53 - strlen(who->name))));
  item = who->equipped;
  notify(player, pstr("    ^ccHead\033[1m^cw:\033[0m %s", EqName(item, LOC_HEAD)));
  notify(player, pstr("    ^ccNeck\033[1m^cw:\033[0m %s", EqName(item, LOC_NECK)));
  notify(player, pstr("   ^ccTorso\033[1m^cw:\033[0m %s", EqName(item, LOC_TORSO)));
  notify(player, pstr("   ^ccHands\033[1m^cw:\033[0m %s", EqName(item, LOC_HANDS)));
  notify(player, pstr("    ^ccLegs\033[1m^cw:\033[0m %s", EqName(item, LOC_LEGS)));
  notify(player, pstr("    ^ccFeet\033[1m^cw:\033[0m %s", EqName(item, LOC_FEET)));
  notify(player, pstr("   ^ccCloak\033[1m^cw:\033[0m %s", EqName(item, LOC_CLOAK)));
  notify(player, pstr("\n^ccWielding\033[1m^cw:\033[0m %s", EqName(item, LOC_DUALWIELD)));
  notify(player, pstr("  ^ccShield\033[1m^cw:\033[0m %s", EqName(item, LOC_SHIELD)));
  notify(player, make_ln("-", 70));
  for(item = who->equipped; item; item=item->next)
    if( ((item->flags & SHIELD)||(item->flags & ARMOUR)) && (item->equipped))
      defend+=item->value;
    else if( (item->flags & WEAPON) && (item->equipped))
      attack+=item->value;
  
  notify(player, pstr("%s's ^ccattack advantage is \033[1m^cy%d\033[0m^cc, defense advantage is \033[1m^cy%d\033[0m^cc.", color_name(who, -1), attack, defend));
}

/** MoveEquipItem():
**/
void MoveEquipItem(ITEMLIST **head, ITEMLIST **tail, ITEMLIST **tohead, ITEMLIST **totail, ITEMLIST *item, unsigned long long flag, int equipping)
{
  if(item->next)
    item->next->prev = item->prev;
  
  if(item->prev)
    item->prev->next = item->next;
  
  if((*head) == item)
    (*head) = item->next;
  
  if((*tail) == item)
    (*tail) = item->prev;
  
  if(equipping) {
    item->equipped = TRUE;
    item->worn = flag;
  } else
    item->equipped = FALSE;
  
  if(!(*tohead)) {
    item->prev = item->next = NULL;
    (*tohead) = (*totail) = item;
  } else {
    item->prev = (*totail);
    item->next = NULL;
    (*totail)->next = item;
    (*totail) = item;
  }
}

/** CopyToEquip():
**/
void CopyToEquip(object *player, ITEMLIST *item, unsigned long long flag)
{
  ITEMLIST *inew;
  int idx;
  
  item->onhand -= 1;
  inew = CloneItem(item);  // onhand doesn't matter to equipped, since only one should ever make it there.
  inew->equipped = TRUE;
  inew->worn = flag;
  
  if(!player->equipped) {
    inew->next = inew->prev = NULL;
    player->equipped = player->equippedtail = inew;
  } else {
    inew->next = NULL;
    inew->prev = player->equippedtail;
    player->equippedtail->next = inew;
    player->equippedtail = inew;
  }
    
}

/** WearItem(): 
**/
void WearItem(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  ITEMLIST *i = NULL;
  CSTATS *c;
  char *equipped;
  int idx;
  
  if(!*arg1) {
    notify(player, "What do you expect to wear?");
    return;
  }
  
  if(!(c = player->combat)) {
    notify(player, "You can't wear armour with no combat stats.");
    return;
  }
  
  if( (c->cflags & FIGHT) ) {
    notify(player, "Try changing your armour when you're done fighting!");
    return;
  }
  
  
  for(i = player->items; i; i=i->next)
    if(!strcmpm(i->name, arg1))
      break;
  
  if(!i) {
    notify(player, "You don't have that item.");
    return;
  }
  
  if(*arg2) {
    for(idx = 0; wear_list[idx].name; idx++) {
      if(string_prefix(arg2, wear_list[idx].name) && (wear_list[idx].check & ARMOUR) 
         && (i->wear & wear_list[idx].flag)) {
        if( *(equipped = IsEquipped(player, wear_list[idx].flag))) {
          notify(player, pstr("Please unequip your %s first.", equipped));
          return;
        }
        // We've specified where we want it to go, that place is clear, and that's the right equipment.
        notify(player, pstr("You have equipped your %s.", i->name));
          
        if(i->onhand > 1 && !(i->flags & NOCLONE))
          CopyToEquip(player, i, wear_list[idx].flag);
        else 
          MoveEquipItem(&player->items, &player->itemstail, &player->equipped, &player->equippedtail, i, wear_list[idx].flag, TRUE);
        return;
      }
    }
  }
  
  for(idx = 0; wear_list[idx].name; idx++) {
    if((i->wear & wear_list[idx].flag) && (wear_list[idx].check & ARMOUR)) {
      if( *(equipped = IsEquipped(player, wear_list[idx].flag))) {
        notify(player, pstr("Please unequip your %s first.", equipped));
        return;
      }
      notify(player, pstr("You have equipped your %s.", i->name));
      if(i->onhand > 1 && !(i->flags & NOCLONE))
        CopyToEquip(player, i, wear_list[idx].flag);
      else
        MoveEquipItem(&player->items, &player->itemstail, &player->equipped, &player->equippedtail, i, wear_list[idx].flag, TRUE);
      return;
    }
  }
  
  notify(player, "Incorrect usage for wear.");
}

/** WieldItem():
**/
void WieldItem(player, arg1, arg2)
  object *player;
  char *arg1, *arg2;
{
  CONTENTS *exception = NULL;
  ITEMLIST *i = NULL;
  CSTATS *c;
  char *equipped, *gender;
  int idx;
  
  if(!*arg1) {
    notify(player, "What do you expect to wield?");
    return;
  }
  
  if(!(c = player->combat)) {
    notify(player, "You can't wield a weapon with no combat stats.");
    return;
  }
  
  if( (c->cflags & FIGHT) ) {
    notify(player, "Try changing your wielded weapon when you're done fighting!");
    return;
  }
  
  
  for(i = player->items; i; i=i->next)
    if(!strcmpm(i->name, arg1))
      break;
  
  if(!i) {
    notify(player, "You don't have that item.");
    return;
  }
  
  gender = atr_get(player, A_SEX);
  gender = (*gender == 'f') ? "her" : (*gender == 'm') ? "his" : "its";
  
  if(*arg2) {
    for(idx = 0; wear_list[idx].name; idx++) {
      if(string_prefix(arg2, wear_list[idx].name) && ((wear_list[idx].check & WEAPON) || (wear_list[idx].check & SHIELD))
         && (i->wear & wear_list[idx].flag)) {
        if( *(equipped = IsEquipped(player, wear_list[idx].flag))) {
          notify(player, pstr("Please unequip your %s first.", equipped));
          return;
        }
        // We've specified where we want it to go, that place is clear, and that's the right equipment.
        add_exception(&exception, player);
        notify(player, pstr("You are now wielding your %s.", i->name));
        notify_in(player->location, exception, pstr("%s begins wielding %s %s!", 
          color_name(player, -1), gender, i->name));
        clear_exception(exception);
        if(i->onhand > 1 && !(i->flags & NOCLONE))
          CopyToEquip(player, i, wear_list[idx].flag);
        else 
          MoveEquipItem(&player->items, &player->itemstail, &player->equipped, &player->equippedtail, i, wear_list[idx].flag, TRUE);
        return;
      }
    }
  }
  
  for(idx = 0; wear_list[idx].name; idx++) {
    if((i->wear & wear_list[idx].flag) && ((wear_list[idx].check & WEAPON)||(wear_list[idx].check & SHIELD))) {
      if( *(equipped = IsEquipped(player, wear_list[idx].flag))) {
        notify(player, pstr("Please unequip your %s first.", equipped));
        return;
      }
      if(!(i->flags & SHIELD) && !(i->flags & WEAPON)) {
        notify(player, "You cannot equip that there!");
        return;
      }
      add_exception(&exception, player);
      notify(player, pstr("You are now wielding your %s.", i->name));
      notify_in(player->location, exception, pstr("%s begins wielding %s %s!", 
        color_name(player, -1), gender, i->name));
      clear_exception(exception);
      if(i->onhand > 1 && !(i->flags & NOCLONE))
        CopyToEquip(player, i, wear_list[idx].flag);
      else
        MoveEquipItem(&player->items, &player->itemstail, &player->equipped, &player->equippedtail, i, wear_list[idx].flag, TRUE);
      return;
    }
  }
  
  notify(player, "Incorrect usage for wield.");
}

/** UnequipItem():
  When you unequip an item, you unequip it from all locations.
  People are going to complain, but oh well.
**/
void UnequipItem(player, arg1)
  object *player;
  char *arg1;
{
  ITEMLIST *i = NULL, *inv;
  CSTATS *c;
  
  if(!(c = player->combat)) {
    notify(player, "You cannot unequip items on a non-combat object.");
    return;
  }
  
  if((c->cflags & FIGHT)) {
    notify(player, "You cannot unequip while fighting!");
    return;
  }
  
  if(!player->equipped) {
          notify(player, "You don't have that equipped.");
    return;
  }
  
  if(!*arg1) {
    notify(player, "Unequip what?");
    return;
  }
  
  for(i = player->equipped; i; i=i->next)
    if(!strcmpm(i->name, arg1) && i->equipped)
      break;
  
  if(!i) {
    notify(player, "You don't have that equipped.");
    return;
  }

  if(!(i->flags & NOCLONE)) {
    for(inv = player->items; inv; inv=inv->next)
      if(inv->id == i->id) {
        inv->onhand += 1;
        notify(player, pstr("%s unequipped.", inv->name));
        RemoveItem(&player->equipped, &player->equippedtail, i);
        return;
    }
  }

  notify(player, pstr("Unequipping %s.", i->name));

  MoveEquipItem(&player->equipped, &player->equippedtail, &player->items, &player->itemstail, i, i->wear, FALSE);  
}

/** ParseItemModifiers():
**/
void ParseItemModifiers(FIGHTQUEUE *fight, object *target, ITEMATTR *list)
{
  ITEMATTR *i;
  CSTATS *c = target->combat;
  
  for(i = list; i; i=i->next) {
    switch(i->type) {
      case MOD_STRENGTH:
        c->strmod += i->value.i;
        break;
      case MOD_WISDOM:
        c->wismod += i->value.i;
        break;
      case MOD_DEXTERITY:
        c->dexmod += i->value.i;
        break;
      case MOD_STAMINA:
        c->stamod += i->value.i;
        break;
      case MOD_LUCK:
        c->lckmod += i->value.i;
        break;
      case MOD_CONSTITUTION:
        c->conmod += i->value.i;
        break;
      case MOD_CHARISMA:
        c->chamod += i->value.i;
        break;
      case MOD_INTELLIGENCE:
        c->intmod += i->value.i;
        break;
      default:
        break;
    }  
  }
}


/** ITEM SAVING **/

/** DumpTemplateItems():
**/
int DumpTemplateItem(FILE *fp, ITEMLIST *i)
{
  ITEMATTR *iattr;
  char buf[2048];
  int cnt;
  
  *buf = 0;
  fprintf(fp, "^%d\n", i->id);
  fprintf(fp, "%s\n", i->name);
  fprintf(fp, "%s\n", i->desc);
  fprintf(fp, "%ld\n", i->cost);
  fprintf(fp, "%lld\n", i->wear);
  fprintf(fp, "%lld\n", i->flags);
  fprintf(fp, "%lld\n", i->affect);
  fprintf(fp, "%lld\n", i->effect);
  fprintf(fp, "%lld\n", i->material);
  fprintf(fp, "%d\n", i->value);
  fprintf(fp, "%d\n", i->alignment);
  fprintf(fp, "%f\n", i->weight);
  fprintf(fp, "%d\n", i->stresspoint);
  fprintf(fp, "+%d\n", CountItemAttrs(i));
  for(iattr = i->list; iattr; iattr=iattr->next)
    fprintf(fp, "%d:%s\n", iattr->type, ShowItemAttrValue(iattr));
  fputs("\\$\n", fp);				// End is \$
  
  return TRUE;
}

/** NumTemplates():
**/
int NumTemplates()
{
  ITEMLIST *i;
  int cnt;

  for(cnt = 0, i = itemlist; i; i=i->next, cnt++);
    
  return cnt;
}

/** SaveTemplates():
**/
void SaveTemplates(char *filename)
{
  FILE *fp;
  ITEMLIST *i;
  
  if(!(fp = fopen(filename, "w"))) {
    log_channel("log_err", "* Error opening template db for writing.");
    return;
  }

  fprintf(fp, "%d\n", NumTemplates());
  
  for(i = itemlist; i; i=i->next)
    DumpTemplateItem(fp, i);
 
  fclose(fp); 
}

/** DumpItem():
**/
int DumpItem(FILE *fp, ITEMLIST *i)
{
  ITEMATTR *iattr;
  char buf[2048];
  int cnt;
  
  *buf = 0;
  fprintf(fp, "^%d\n", i->id);
  fprintf(fp, "%s\n", i->name);
  fprintf(fp, "%s\n", i->desc);
  fprintf(fp, "%ld\n", i->cost);
  fprintf(fp, "%lld\n", i->wear);
  fprintf(fp, "%lld\n", i->flags);
  fprintf(fp, "%lld\n", i->affect);
  fprintf(fp, "%lld\n", i->effect);
  fprintf(fp, "%lld\n", i->material);
  fprintf(fp, "%d\n", i->value);
  fprintf(fp, "%d\n", i->alignment);
  fprintf(fp, "%f\n", i->weight);
  fprintf(fp, "%d\n", i->stresspoint);
  fprintf(fp, "%d\n", i->equipped);
  fprintf(fp, "%d\n", i->kills);
  fprintf(fp, "%d\n", i->uses);
  fprintf(fp, "%d\n", i->proficiency);
  fprintf(fp, "%d\n", i->blocks);
  fprintf(fp, "%d\n", i->onhand);
  fprintf(fp, "%lld\n", i->worn);
  fprintf(fp, "+%d\n", CountItemAttrs(i));
  for(iattr = i->list; iattr; iattr=iattr->next)
    fprintf(fp, "%d:%s\n", iattr->type, ShowItemAttrValue(iattr));
  fprintf(fp, "\\$\n"); 
  return TRUE;
}

/** DumpUserItems():
**/
int DumpUserItems(FILE *fp, object *o)
{
  ITEMLIST *i;
  int numitems, numequipped;
  
  for(numitems = 0, i = o->items; i; i=i->next, numitems++);
  for(numequipped = 0, i=o->equipped; i; i=i->next, numequipped++);
  
  fprintf(fp, "#%d\n", o->ref);
  fprintf(fp, "%d\n", numequipped);
  for(i = o->equipped; i; i=i->next)
    DumpItem(fp, i);
  fprintf(fp, "%d\n", numitems);
  for(i = o->items; i; i=i->next)
    DumpItem(fp, i);    
  fputs("\\#\n", fp);
  
  return TRUE;
}

/** SaveUserItems():
**/
void SaveUserItems(char *filename)
{
  FILE *fp;
  int idx;

  if(!(fp = fopen(filename, "w"))) {
    log_channel("log_err", "* Error writing to user item database.");
    return;
  }

  fprintf(fp, "!%d\n", calculate_dbsize());
  for(idx = 0; idx < dbtop; idx++) {
    if(!(db[idx]))
      continue;
    DumpUserItems(fp, (db[idx]));
  }
  fprintf(fp, "!END\n");
  fclose(fp);  
}

/** LoadItemAttrs():
**/
void LoadItemAttrs(FILE *fp, ITEMLIST *item, int cnt)
{
  char buf[4096];
  int idx;
  
  *buf = 0;
  for(idx = 0; idx < cnt; idx++) {
    strcpy(buf, getstring(fp));
    ItemAttributeCreate(item, atoi(fparse(buf, ':')), rparse(buf, ':'));
  }
}

/** LoadUserItem():
**/
ITEMLIST *LoadUserItem(ITEMLIST **head, ITEMLIST **tail, FILE *fp)
{
  ITEMLIST *item;
  char buf[2048], *instr, *p = buf;
  int id, cnt;
  
  instr = getstring(fp);
  if(*instr != '^')
    return(NULL);
  else 
    id = atoi(instr + 1);
    
  GENERATE(item, ITEMLIST);
  item->id = id;
  item->desc = item->name = NULL;
  SET(item->name, getstring(fp));
  SET(item->desc, getstring(fp));
  item->cost = getlong(fp);
  item->wear = getllong(fp);
  item->flags = getllong(fp);
  item->affect = getllong(fp);
  item->effect = getllong(fp);
  item->material = getllong(fp);
  item->value = getint(fp);
  item->alignment = getint(fp);
  item->weight = getfloat(fp);
  item->stresspoint = getint(fp);
  item->equipped = getint(fp);
  item->kills = getint(fp);
  item->uses = getint(fp);
  item->proficiency = getint(fp);
  item->blocks = getint(fp);
  item->onhand = getint(fp);
  item->worn = getllong(fp);
  if( (cnt = atoi(getstring(fp))) > 0)
    LoadItemAttrs(fp, item, cnt);
    
  if(*(getstring(fp)) != '\\')
    return(NULL);
    
  if(!(*head)) {
    item->next = item->prev = NULL;
    (*head) = (*tail) = item;
  } else {
    item->prev = (*tail);
    item->next = NULL;
    (*tail)->next = item;
    (*tail) = item;
  }
      
  return(item);
}

/** LoadUserItems():
**/
int LoadUserItems(FILE *fp)
{
  object *o;
  char *p;
  int cnt, idx;
  char buf[2048];
  
  p = getstring(fp);
  if(!(o = match_object(p)))
    return FALSE;

  cnt = getint(fp);
  for(idx = 0; idx < cnt; idx++)
    LoadUserItem(&o->equipped, &o->equippedtail, fp);
  cnt = getint(fp);
  for(idx = 0; idx < cnt; idx++)
    LoadUserItem(&o->items, &o->itemstail, fp);
  if( *(getstring(fp)) != '\\')
    return FALSE;
}

/** LoadItems():
**/
void LoadItems(char *filename)
{
  FILE *fp;
  char *p;
  int cnt, i;
  
  if(!(fp=fopen(filename, "r"))) {
    log_channel("log_err", "* Cannot load item database.");
    return;
  }
  
  p = getstring(fp);
  if(*p != '!') {
    log_channel("log_err", "* Error in item database format!");
    return;
  }
  cnt = atoi(p+1);
  for(i = 0; i < cnt; i++)
    if(!LoadUserItems(fp)) {
      log_channel("log_err", "* Error loading item from database.");
      return;
    }
  
  fclose(fp);
}

/** LoadTemplateItem():
**/
int LoadTemplateItem(FILE *fp)
{
  ITEMLIST *item;
  char buf[2048], *instr, *p = buf;
  int id, cnt;
  
  instr = getstring(fp);
  if(*instr != '^')
    return FALSE;
  else
    id = atoi(instr+1);
  
  GENERATE(item, ITEMLIST);
  item->id = id;
  item->desc = item->name = NULL;
  SET(item->name, getstring(fp));
  SET(item->desc, getstring(fp));
  item->cost = getlong(fp);
  item->wear = getllong(fp);
  item->flags = getllong(fp);
  item->affect = getllong(fp);
  item->effect = getllong(fp);
  item->material = getllong(fp);
  item->value = getint(fp);
  item->alignment = getint(fp);
  item->weight = getfloat(fp);
  item->stresspoint = getint(fp);
  if( (cnt = atoi(getstring(fp))) > 0)
    LoadItemAttrs(fp, item, cnt);
    
  if(*(getstring(fp)) != '\\')
    return FALSE;

  if(!itemlist) {
    item->next = item->prev = NULL;
    itemlist = itemlisttail = item;
  } else {
    item->next = NULL;
    item->prev = itemlisttail;
    itemlisttail->next = item;
    itemlisttail = item;
  }
    
  return TRUE;
  
}

/** LoadTemplates():
**/
void LoadTemplates(char *filename)
{
  FILE *fp;
  int amt, i;
  
  if(!(fp = fopen(filename, "r"))) {
    log_channel("log_err", "* Error opening template db for loading.");
    return;
  }
  
  amt = getint(fp);
  for(i = 0; i < amt; i++) {
    if(!LoadTemplateItem(fp)) {
      DEBUG("error!");
      log_channel("log_err", "* Error loading template item!");
      return;
    }
  }
  
  fclose(fp);
  
  // Initialize items into the itemdb array.
  InitItems();
}
    
/** END OF ITEMS.C **/

