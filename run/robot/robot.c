/****************************************************************
 * robot: Automaton for TinyMUD
 ****************************************************************/

# include <stdio.h>
# include <ctype.h>
# include <sys/types.h>
# include <sys/ioctl.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <netdb.h>
# include <ctype.h>
# include <varargs.h>

# define MUDHOST		"daisy.learning.cs.cmu.edu"
# define MUDPORT		4201

# define MYNAME			"guest"
# define MYPASS			"guest"

# define PREFIX			"BEGINROBOT"
# define SUFFIX			"ENDROBOT"

# define ctrl(C) ((C) & 037)
# define streq(A,B) (!strcmp ((A), (B)))
# define READ		0
# define WRITE		1

int   fmud, tmud;
int   debug = 0;
int   testing = 0;
int   dead = 0;
int   hanging = 0;
char *getmud ();

/****************************************************************
 * Main routine
 ****************************************************************/

main (argc, argv)
int   argc;
char *argv[];

{ int   ptc[2], ctp[2];
  char *mfile=NULL;

  /* Get the options from the command line */
  while (--argc > 0 && (*++argv)[0] == '-')
  { while (*++(*argv))
    { switch (**argv)
      { case 'd': debug++; break;
	case 't': testing++; break;
        default:  printf ("Usage: ga [-options]\n");
		  exit (1);
      }
    }
  }

  if (testing)
  { tmud = 1; fmud = 0; }
  else
  { tmud = fmud = connectmud (); }

  if (tmud < 0)
  { fprintf (stderr,"Connect failed\n");
    exit (-1);
  }

  robot ();
  
  close (tmud);
  exit (0);
}

/****************************************************************
 * robot: TinyMUD automaton.  reads/writes to tmud/fmud
 ****************************************************************/

robot ()
{ 
  waitfor ("Use the WHO command to find out who is currently active.");
 
  sendmud ("connect %s %s", MYNAME, MYPASS);
  sendmud ("OUTPUTPREFIX %s", PREFIX);
  sendmud ("OUTPUTSUFFIX %s", SUFFIX);
  
  sendmud ("@link me = 0");	waitfor (SUFFIX);
  sendmud ("home");		waitfor (SUFFIX);
  sendmud ("north");		waitfor (SUFFIX);
  hangaround (60);
  sendmud ("home");		waitfor (SUFFIX);
}

/****************************************************************
 * waitfor:
 ****************************************************************/

waitfor (pat)
char *pat;
{ char *msg;

  while (1)
  { if (msg = getmud ())
    { readmsg (msg);
      if (streq (msg, pat)) return (1);
    }
    else
    { sleep (1); }
  }
}

/****************************************************************
 * hangaround: Wait here a specified number of seconds, but
 * keep processing incoming messages;
 ****************************************************************/

hangaround (sec)
int sec;
{ char *msg;
  int alarm;

  alarm = time (0) + sec;
  hanging = 1;

  while (time (0) < alarm && !dead && hanging)
  { if (msg = getmud ())
    { readmsg (msg); }
    else
    { sleep (1); }
  }
  
  return (1);
}

/****************************************************************
 * readmsg: Handle messages
 ****************************************************************/

readmsg (msg)
char *msg;
{ 
  fprintf (stderr, "Msg: %s\n", msg);
}

/****************************************************************
 * quit_robot: We are exiting.  Write out any long term memory first.
 ****************************************************************/

quit_robot ()
{
  close (tmud);
  close (fmud);

  /* Write out any memory files needed */

  exit (0);
}

/****************************************************************
 * connectmud: Open the MUD socket
 ****************************************************************/

connectmud()
{
  struct sockaddr_in sin;
  struct hostent *hp;
  int     fd;

  if (debug) fprintf (stderr, "Connecting...\n");

  bzero((char *) &sin, sizeof(sin));

  sin.sin_port = htons(MUDPORT);

  if ((hp = gethostbyname(MUDHOST)) == 0) return (-1);

  bcopy(hp->h_addr, (char *) &sin.sin_addr, hp->h_length);
  sin.sin_family = hp->h_addrtype;

  fd = socket(AF_INET, SOCK_STREAM, 0);
  if (fd < 0) return -1;

  if (connect(fd,(struct sockaddr *) &sin, sizeof(sin)) < 0) return -1;

  return fd;
}

/****************************************************************
 * sendmud: Send a command to the TinyMUD process
 ****************************************************************/

sendmud (fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10)
char *fmt;
int a1, a2, a3, a4, a5, a6, a7, a8, a9, a10;
{ int len;
  char buf[10240];
  
  sprintf (buf, fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);
  strcat (buf, "\n");
  len = strlen (buf);

  if (debug) fprintf (stderr, "\nSend: %s\n", buf);

  if (write (tmud, buf, len) != len)
  { fprintf (stderr, "Write failed: %s", buf);
    quit_robot ();
  }
}

/****************************************************************
 * getmud: Read one line from TinyMUD
 ****************************************************************/

char *getmud ()
{ int len, result = 0;
  static char buf[BUFSIZ], rbuf[4];
  register char *s=buf;

  /* No input waiting */
  if (!charsavail (fmud)) return (NULL);

  /* Read one line, save printing chars only */
  while ((len = read (fmud, rbuf, 1)) > 0)
  { if (*rbuf == '\n')		break;
    if (isprint (*rbuf))	*s++ = *rbuf;
  }
  *s = '\0';

  /* Check for error */  
  if (len < 0)
  { fprintf (stderr, "Error %d reading from mud\n", len);
    quit_robot ();
  }

  return (s = buf);
}

/*****************************************************************
 * charsavail: check for input available from 'fd'
 *****************************************************************/

charsavail (fd)
int fd;
{ long n;
  int retc;
  
  if (retc = ioctl (fd, FIONREAD, &n))
  { fprintf (stderr, "Ioctl returns %d, n=%ld.\n", retc, n);
    quit_robot ();
  }

  return ((int) n);
}

/****************************************************************
 * swap_files: Do a pointer swap with a temp file and a real file
 ****************************************************************/

swap_files (tmp, real)
char *tmp, *real;
{ char bakfile[256];

  sprintf (bakfile, "%s.BAK", real);

  unlink (bakfile);

  if (link (real, bakfile) < 0)
  { fprintf (stderr, "Cannot link old %s to %s in swap_files\n",
	     real, bakfile);
    fprintf (stderr, "New file left in %s\n", tmp);
    return (0);
  }
  
  if (unlink (real) < 0)
  { fprintf (stderr, "Cannot unlink old %s in swap_files\n", real);
    fprintf (stderr, "New file left in %s\n", tmp);
    return (0);
  }
  
  if (link (tmp, real) < 0)
  { fprintf (stderr, "Cannot link new %s to %s in swap_files\n",
	     tmp, real);
    fprintf (stderr, "New file left in %s\n", tmp);
    return (0);
  }
  
  unlink (tmp);
  
  return (1);
}

/****************************************************************
 * makestring:
 ****************************************************************/

char *makestring (str)
char *str;
{ register char *s;

  if (s = (char *) malloc (strlen (str) + 1))
  { return ((char *) strcpy (s, str)); }
  
  else
  { crash_robot ("malloc returns NULL in makestring"); }
}

/****************************************************************
 * crash_robot:
 ****************************************************************/

crash_robot (fmt, a1, a2, a3, a4)
char *fmt;
{ long now;
  char msg[1024];

  sprintf (msg, fmt, a1, a2, a3, a4);

  sendmud ("@desc me = %s has crashed.", MYNAME);
  sendmud ("QUIT");

  /* Checkpoint your files */

  now = time (0);
  fprintf (stderr, "\n\nCrash at %15.15s: %s\n", ctime (&now)+4, msg);
  abort ();
}
