/*** mysql.c $ (c) 2005 WILLCLIFFE.COM ***/

// Plugin for TinyMYTH 
 
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <mysql/mysql.h>

#define def_host_name "localhost"       /* host to connect to (default = localhost) */
#define def_user_name "########"            /* user name (default = your login name) */
#define def_password  "########"      /* password (default = none) */
#define def_db_name   "########"         /* database to use (default = none) */

MYSQL *mysql_conn = 0;

/** We build a MySQL set for each object as they use the mysql system.
    Only one result is allowed per object at the present time.
    Further expansion may allow the ability to ID a result to query multiple
    queries, but the advantages to that may not outweigh the drawbacks.
 **/
typedef struct _mysql_sqldata {
  char *id;
  char *data;
} SQLDATA;

typedef struct _mysql_fields {
  SQLDATA *fields;
} SQLFIELD;

typedef struct _mysql_object_storage {
  MYSQL_RES *result;
  SQLFIELD *fieldlist;
  unsigned int rows, fields;
} MYSQLOBJ;

MYSQLOBJ **sqlobjs = NULL;
unsigned int mysqlobjs = 0;

/* API Stuff */
#include "db.h"
#include "net.h"
#include "externs.h"

void do_mysql P((object *, char *, char *));
int do_mysql_clear P((object *));
int do_mysql_set P((object *));
int do_mysql_query P((object *, char *));

CL simple_cmd_add[]={
  {"+mysql", do_mysql, 0, 0},
  { NULL }
};

CL *additional_command()
{
  return simple_cmd_add;
}

static void fun_mysqlquery(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  if(!do_mysql_query(doer, args[0]))
    strcpy(buf, "0");
  else
    strcpy(buf, "1");
}

static void fun_mysqlset(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  if(!do_mysql_set(doer))
    strcpy(buf, "0");
  else
    strcpy(buf, "1");
}      

static void fun_mysqlclear(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  if(!do_mysql_clear(doer))
    strcpy(buf, "0");
  else
    strcpy(buf, "1");
}
        
static void fun_mysqlgetrows(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  object *what;
  
  if(!(what = remote_match(doer, args[0]))) {
    strcpy(buf, "0");
    return;
  }
  
  if(!(sqlobjs[what->ref])) { strcpy(buf, "0"); return; }
  
  *buf = '\0';
  sprintf(buf, "%d", (sqlobjs[what->ref])->rows);
}      

static void fun_mysqlgetrow(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  object *what;
  unsigned int row=atoi(args[1]), i, check_field = FALSE;
  
  if(!(what = remote_match(doer, args[0])))
    return;
  
  if(*args[2]) check_field = TRUE;
  
  if(!(sqlobjs[what->ref]) || !(sqlobjs[what->ref])->fieldlist) return;
  
  if(row >= (sqlobjs[what->ref])->rows) return;
  
  *buf = '\0';
  
  for(i = 0; i < (sqlobjs[what->ref])->fields; i++)
  {
    if(!check_field)
      sprintf(buf + strlen(buf), "%s: %s ", (sqlobjs[what->ref])->fieldlist[row].fields[i].id, (sqlobjs[what->ref])->fieldlist[row].fields[i].data);
    if(check_field && !strcmp((sqlobjs[what->ref])->fieldlist[row].fields[i].id, args[2]))
      sprintf(buf, "%s ", (sqlobjs[what->ref])->fieldlist[row].fields[i].data);
  }

  if(*buf) buf[strlen(buf)-1] = '\0';
}                       

static void fun_mysqlescape(buf, args, player, doer, nargs)
  char *buf, *args[NUM_FUN_ARGS];
  object *player, *doer;
  int nargs;
{
  char buff[MAX_BUFSIZE];
  char *c, *p = buf;
  
  *buff = '\0';
  *buf = '\0';
  
  mysql_real_escape_string(mysql_conn, buff, args[0], strlen(args[0]));
  
  for(c = buff; *c; c++)
  {
    if(*c == '\\')
      *p++ = '\\';
    *p++ = *c;
  }
  *p++ = '\0';
  
  //if(*buf) buf[strlen(buf)-1] = '\0';
}      

FUN mysqllist[]={
                {"mysql_escape_string", fun_mysqlescape, 1},
                {"mysql_query", fun_mysqlquery, 1},
                {"mysql_clear", fun_mysqlclear, 0},
                {"mysql_set", fun_mysqlset, 0},
                {"mysql_get_rows", fun_mysqlgetrows, 1},
                {"mysql_get_row", fun_mysqlgetrow, 3},
                {NULL},
                };

void __attribute__((constructor)) simple_init(void)
{
  
  int i;
  DEBUG("I'm trying to init my scrotum");
  mysql_conn = mysql_init (NULL);
  mysql_real_connect (mysql_conn, def_host_name, def_user_name, def_password, def_db_name, 0, NULL, 0);
  mysqlobjs = dbtop;
  if(!(sqlobjs = (MYSQLOBJ **)stack_em((sizeof(MYSQLOBJ *) * (mysqlobjs + 1)), SOLID)))
    DEBUG("Error initializing the sqlobjs.");

  for(i = 0; i < mysqlobjs; i++)
    sqlobjs[i] = NULL;
    
  for(i = 0; mysqllist[i].name; i++)
    addFunc(&mysqllist[i]);
}
                
void __attribute__((destructor)) simple_fini(void)
{
  int i;
  
  mysql_close(mysql_conn);
  
  for(i = 0; mysqllist[i].name; i++)
    delFunc(mysqllist[i].name);
}

void do_mysql_fetch(player, arg2)
  object *player;
  char *arg2;
{
  unsigned int row=atoi(arg2), i;
  
  if(!(sqlobjs[player->ref]) || !(sqlobjs[player->ref])->fieldlist) return;
  
  if(row >= (sqlobjs[player->ref])->rows) return;
  
  for(i = 0; i < (sqlobjs[player->ref])->fields; i++)
    notify(player, tprintf("%s: %s", (sqlobjs[player->ref])->fieldlist[row].fields[i].id, (sqlobjs[player->ref])->fieldlist[row].fields[i].data));
}

int do_mysql_clear(player)
  object *player;
{
  int ridx;
  
  if(!(sqlobjs[player->ref])) return FALSE;
  
  /** URGENT: We need to clear any previous sqlfields **/
  if((sqlobjs[player->ref])->fieldlist) {
    for(ridx = 0; ridx < (sqlobjs[player->ref])->rows; ridx++)
    {
      if((sqlobjs[player->ref])->fieldlist[ridx].fields)
        block_free((sqlobjs[player->ref])->fieldlist[ridx].fields);
    }
    block_free((sqlobjs[player->ref])->fieldlist);
  }
  (sqlobjs[player->ref])->fieldlist = NULL;
  (sqlobjs[player->ref])->rows = 0;
  (sqlobjs[player->ref])->fields = 0;
 
  return TRUE;
}

int do_mysql_set(player)
  object *player;
{
  SQLFIELD *sqlfields;
  MYSQL_RES *result;
  MYSQL_ROW row;
  unsigned int num_fields, num_rows;
  unsigned int i, ridx;

  
  if(player->ref >= mysqlobjs) return FALSE;

  if(!(result = (sqlobjs[player->ref])->result)) { return FALSE; }
  
  num_rows = mysql_num_rows(result);
  num_fields = mysql_num_fields(result);
  
  /** URGENT: We need to clear any previous sqlfields **/
  do_mysql_clear(player);
  
  (sqlobjs[player->ref])->rows = num_rows;
  (sqlobjs[player->ref])->fields = num_fields;
  
  
  
  GENERATE(sqlfields, SQLFIELD);
  sqlfields = (SQLFIELD *)stack_em(sizeof(SQLFIELD) * num_rows + 1, SOLID);
  ridx = 0;
  
  while ((row = mysql_fetch_row(result)))
  {
    SQLDATA *dnew = NULL;
    MYSQL_FIELD *fields;
    unsigned long *lengths;
    fields = mysql_fetch_fields(result);
    
    GENERATE(dnew, SQLDATA);
    dnew = (SQLDATA *)stack_em(sizeof(SQLDATA) * num_fields + 1, SOLID);
    
    lengths = mysql_fetch_lengths(result);
    for(i = 0; i < num_fields; i++)
    {
      
      SET(dnew[i].id, fields[i].name);
      if(lengths[i] > 2048) { DEBUG("Maximum length reached, truncating."); }
      SET(dnew[i].data, tprintf("%.*s", 2048, row[i]));
    }
    
    sqlfields[ridx].fields = dnew;
    ridx++;
  }
 
  (sqlobjs[player->ref])->fieldlist = sqlfields;

  if((sqlobjs[player->ref])->fieldlist) return TRUE;

  return FALSE;
}

void sqlAttach(player, result)
  object *player;
  MYSQL_RES *result;
{
  MYSQLOBJ *sqlobj;
  
  GENERATE(sqlobj, MYSQLOBJ);
  sqlobj->result = result;
  sqlobj->fieldlist = 0;
  sqlobj->fields = 0;
  sqlobj->rows = 0;
  
  sqlobjs[player->ref] = sqlobj;
}

int do_mysql_query(player, query)
  object *player;
  char *query;
{
  if(!query) return FALSE;
  
  if(mysql_real_query(mysql_conn, query, strlen(query)))
    return FALSE;
  
  /* In order to properly assign the result, we'll assign a space in an array identified
     by the player's ID */
  sqlAttach(player, mysql_store_result(mysql_conn));
  
  return TRUE;
}

void do_mysql(player, arg1, arg2)
	object *player;
	char *arg1, *arg2;
{
  if(!*arg1) {
    if(mysql_conn) { notify(player, "We are connected to the database."); }
  } else if(string_prefix(arg1, "query")) {
    do_mysql_query(player, arg2);
  } else if(string_prefix(arg1, "set")) {
    do_mysql_set(player);
  } else if(string_prefix(arg1, "fetch")) {
    do_mysql_fetch(player, arg2);
  } else if(string_prefix(arg1, "clear")) {
    do_mysql_clear(player);
  }
}


/*** end of mysql.c ***/
