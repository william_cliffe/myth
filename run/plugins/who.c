/*** who.c $ (c) 2005 WILLCLIFFE.COM ***/

// Plugin for TinyMYTH 
 
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "db.h"
#include "net.h"
#include "externs.h"

void do_who P((object *, char *));

CL simple_cmd_add[]={
  {"special_who", do_who, 0, 0},
  { NULL }
};

CL *additional_command()
{
	return simple_cmd_add;
}


void __attribute__((constructor)) simple_init(void)
{
}

void __attribute__((destructor)) simple_fini(void)
{
}

char *who_flags(player)
	object *player;
{
	static char flags[5];
	int i;

	for(i=0;i<5;i++)
		flags[i] = ' ';
	
	if(player->flags & WIZARD)
		flags[0]='W';
	if(player->flags & NO_WALLS)
		flags[1]='n';
	if(player->flags & QUIET)
		flags[2]='q';
	if(player->flags & GUEST)
		flags[0]='G'; // there should be no way a wizard is a guest.
	if(player->flags & INVISIBLE)
		flags[3]='i';
	
	return (flags);
}

int who_header(player, whostr)
	object *player;
	char *whostr;
{
	char buf[MAX_BUFSIZE];
	char *p;
	
	strcpy(buf, "");
	for(p = whostr; *p; p++)
	{
	  switch(*p) {
		case 'n': // 12
		  sprintf(buf + strlen(buf), "%-12.12s ", "NAME");
		  break;
		case 'N': // 24
		  sprintf(buf + strlen(buf), "%-24.24s ", "NAME");
		  break;
		case 'a': // 8
		case 'A': 
		  sprintf(buf + strlen(buf), "%-8.8s ", "ALIAS");
		  break;
		case 'f': // 8
		case 'F':
		  sprintf(buf + strlen(buf), "%-8.8s ", "FLAGS");
		  break;
		case 'o': // 10
		case 'O':
		  sprintf(buf + strlen(buf), "%10.10s ", "ONFOR");
		  break;
		case 'i': // 5
		case 'I':
		  sprintf(buf + strlen(buf), "%5.5s ", "IDLE");
		  break;
		case 'd': // 18
		  sprintf(buf + strlen(buf), "%-18.18s ", "DOING");
		  break;
		case 'D': // 32
		  sprintf(buf + strlen(buf), "%-32.32s ", "DOING");
		  break;
		case 'c':
		case 'C': // 12
		  sprintf(buf + strlen(buf), "%-12.12s ", "CLASS");
		  break;
		case 'h':
		  sprintf(buf + strlen(buf), "%-12.12s ", "HOST");
		  break;
		case 'H':
		  sprintf(buf + strlen(buf), "%-32.32s ", "HOST");
		  break;
		default:
		  notify(player, pstr("Bad who_flag: %c", *p));
		  return(0); 
		break;
	  }
	}
	
	notify(player, pstr("%s%s---<%sWho's Online%s>%s%s", 
	  ANSI_HILITE, ANSI_WHITE, ANSI_YELLOW, ANSI_WHITE, make_ln("-", 61), ANSI_NORMAL));
	notify(player, pstr("%s%s%s%s", ANSI_HILITE, ANSI_WHITE, buf, ANSI_NORMAL));
	return(1);
}

char *find_who_duplicates(char *instr)
{
  static char buf[4096];
  char *p, *r = buf, *t;
  int caught = 0;
  
  if(!instr || !*instr)
    return("nacoiD");

  memset(buf, 0, sizeof(buf));
  
  for(p = instr; *p; p++) {
    for(t = buf; *t!=0; t++) {
      if(tolower(*t) == tolower(*p))
        caught = 1;
    }
    if(!caught) {
      *r++ = *p;
    }
    caught = 0;
  }
  
  *r++ = 0;
  return(buf);
}

void dump_user(d, player, whostr)
	DESCRIPTOR *d;
	object *player;
	char *whostr;
{
	char buf[MAX_BUFSIZE];
	char *p;

	*buf = '\0';
	
	if(d->state == PASTE_MODE)
	  strcpy(buf, "*");

	for(p = whostr; *p; p++) {
		switch(*p) {
			case 'n':  // small name, maybe 12 chars.
			  sprintf(buf + strlen(buf), "%s^cn ", color_name(d->player, 12));
			  break;
			case 'N':  // max name, maybe 24 chars.
			  sprintf(buf + strlen(buf), "%s^cn ", color_name(d->player, 24));
			  break;
			case 'a':  // same for both, 8 chars.
			case 'A':
			  sprintf(buf + strlen(buf), "%s^cn ", format_color( (*(atr_get(d->player, A_ALIAS))) ? atr_get(d->player, A_ALIAS) : " ", 8));
  			  break;	
			case 'f': 
			case 'F':  // either one, 8 chars for flags.
			  sprintf(buf + strlen(buf), "%-8.8s ", who_flags(d->player));
			  break;
			case 'i':  // either one, 5 chars for idle
			case 'I':
			  sprintf(buf + strlen(buf), "%5.5s ", count_idle(time(0) - d->last_time));
			  break;
			case 'o':  // either one, 10 chars for onfor
			case 'O':
			  sprintf(buf + strlen(buf), "%10.10s ", TimeFormat(time(0) - d->connected_at)); 
  			  break;
			case 'd':  // small doing, maybe 18 chars.
			  sprintf(buf + strlen(buf), "%s^cn ", format_color((*(atr_get(d->player, A_DOING))) ? atr_get(d->player, A_DOING) : " ", 18));
			  break;
			case 'D':  // max doing, maybe 32 chars.
			  sprintf(buf + strlen(buf), "%s^cn ", format_color((*(atr_get(d->player, A_DOING))) ? atr_get(d->player, A_DOING) : " ", 32));
			  break;
			case 'c': // either one, 12 chars for class
			case 'C': 
			  sprintf(buf + strlen(buf), "%-12.12s ", class_to_name(*d->player->pows));
			  break;
			case 'h':
			  if(player->flags & WIZARD)
			    sprintf(buf + strlen(buf), "%-12.12s ", d->addr);
			  else
			    sprintf(buf + strlen(buf), "%-12.12s", "?");
			  break;
			case 'H':
			  if(player->flags & WIZARD)
			    sprintf(buf + strlen(buf), "%-32.32s ", d->addr);
			  else
			    sprintf(buf + strlen(buf), "%-32.32s", "?");
			  break;
			default:
			  break;
		}
	      }

	notify(player, buf);
}


void do_who(player, arg1)
	object *player;
	char *arg1;
{
	DESCRIPTOR *d;
	char idle[MAX_BUFSIZE];
	char *str = find_who_duplicates(atr_get(player, A_WHOFLAGS));
	char *whostr;
	int darkstaff, total_players, total_staff; 
	long connected_for, idle_for;
	
	darkstaff = total_players = total_staff = 0;
	connected_for = 0, idle_for = 0, total_staff = 0;
	

	if(*arg1) {
	  whostr = find_who_duplicates(arg1);
	} else if (*str) {
	  whostr = str;
	}
	
	if(!who_header(player, whostr))
		return;
	
	for(d = descriptor_list; d; d=d->next) {
	  if(!d) {
	    continue;
      }
	  if( (d->state == CONNECTED || d->state==PASTE_MODE) && d->player) {
	    if( (!(d->player->flags & WIZARD)) && (!(d->player->flags & INVISIBLE) || can_control(player, d->player))) {
	    	dump_user(d, player, whostr);
	    	// Do any adding for player stats here.
	    connected_for += time(0) - d->connected_at;
	    idle_for += time(0) - d->last_time;
	    total_players++;
	    }
	  }
	}
	
	
	// Yes, if there's no staff on, it'll still put this line in. But that's fine.
	notify(player, pstr("%s%s---<%sStaff%s>%s%s", ANSI_HILITE, ANSI_WHITE, ANSI_RED, ANSI_WHITE, make_ln("-", 68), ANSI_NORMAL));	

    
	for(d = descriptor_list; d; d=d->next) {
      if(!d)
        continue;
	  if( (d->state == CONNECTED || d->state == PASTE_MODE) && d->player) {
	    if( (d->player->flags & WIZARD) && (!(d->player->flags & INVISIBLE) || can_control(player, d->player)) ) {
	    	dump_user(d, player, whostr);
	    	
	    	// Add to staff stats.
	    	connected_for += time(0) - d->connected_at;
	    	idle_for += time(0) - d->last_time;
	    	total_staff++;
	    }
	    //if ((d->player->flags & INVISIBLE) && (d->player->flags & WIZARD)) {
	    // 	darkstaff++;
	    //}
	  }
	}
	
	notify(player, pstr("^ch%s", make_ln("-", 78)));
	notify(player, pstr("There %s ^ch^cy%d ^cnplayer%s and ^ch^cg%d ^cnstaff online. ^ch^cc%d ^cnrecord.%s",
		(total_players == 1) ? "is" : "are", total_players, 
		  (total_players == 1) ? "" : "s", total_staff, ConnectRecord,
		    (darkstaff) ? pstr(" ^ch^cx%d ^cndarkstaff.", darkstaff) : ""));
	connected_for = connected_for / (total_players + total_staff);
	idle_for = idle_for / (total_players + total_staff);
	strcpy(idle, time_format(idle_for));
	notify(player, pstr("Average Onfor: %s     Average Idle: %s",
		time_format(connected_for), idle));
	if(*(atr_get(db[0], A_COMMENT))) {
	  notify(player, pstr("^ch^cw---<^cbComment ^cy(^ccTry +board^cy)^cw>%s", make_ln("-", 53)));
	  notify(player, pstr("%s", atr_get(db[0], A_COMMENT)));
	  notify(player, pstr("^ch%s^cn", make_ln("-",78)));
	}
}


/*** end of who.c ***/
