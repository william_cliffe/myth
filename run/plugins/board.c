/****************************************************************************
 MYTH - (Bulletin Board System)
 Coded by Saruk (01/14/00)
 ---
 All the nifty BB functions.
 ****************************************************************************/
 
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#include "db.h"
#include "net.h"
#include "externs.h"

#include "board.h"

int LoadBoard P((char *));
int SaveBoard P((char *));
void do_board P((object *, char *, char *));

BBOARD *bblist = NULL;

CL simple_cmd_add[]={
  {"+board", do_board, 0, 0},
  { NULL }
};

CL *additional_command()
{
	return simple_cmd_add;
}


void __attribute__((constructor)) simple_init(void)
{
  if(!LoadBoard("board.txt"))
    log_channel("log_err", "Could not load +board database.");
}

void __attribute__((destructor)) simple_fini(void)
{
  if(!SaveBoard("board.txt"))
    log_channel("log_err", "Could not save +board database.");
}


BBOARD *add_to_board(player, title, comment)
	object *player;
	char *title;
	char *comment;
{
	BBOARD *b, *bnew, *bprev = NULL;
	
	for(b = bblist; b; b=b->next)
		bprev = b;
	
	GENERATE(bnew, BBOARD);
	bnew->topic = NULL;
	if(!*title)
		SET(bnew->topic, "Untitled");
	else
		SET(bnew->topic, title);
	bnew->message = NULL;
	SET(bnew->message, comment);
	bnew->c_ref = player->ref;
	bnew->creator = player;
	bnew->created = time(0);
	bnew->expiration = time(0) + (DEF_BBEXP * 86400);
	bnew->num_read = 0;
	bnew->next = NULL;
	
	if(!bprev) 
		bblist = bnew;
	else
		bprev->next = bnew;
		
	return bnew;
}

void do_add_msg(player, msg)
	object *player;
	char *msg;
{
	int amt=0;
	BBOARD *b;
	char *title;
	char *message;
	
	for(b = bblist; b; b=b->next)
	  if(b->creator == player)
	    amt++;
	
	if(amt >= BOARD_MAX && !(player->flags & WIZARD)) {
		error_report(player, M_ETOOBOARD);
		return;
	}
			
	title = fparse(msg, ':');
	message = rparse(msg, ':');

	if(!*message)
		add_to_board(player, "", title);
	else 
		add_to_board(player, title, message);
	
	notify(player, "* Your message has been added.");
	world_dwrite("* A message has been added to the bulletin board!\n");
}

void delete_bboard_msg(bdel)
	BBOARD *bdel;
{
	BBOARD *b, *bprev = NULL;
	
	for(b = bblist; b; b=b->next) {
	  if(b == bdel)
	    break;
	  bprev = b;
	}
	
	if(!b)
	  return;
	
	if(!bprev)
	  bblist->next = b->next;
	else
	  bprev->next = b->next;
	
	if(b->topic)
	  block_free(b->topic);
	if(b->message)
	  block_free(b->message);
	
	block_free(b);
}

void do_del_msg(player, arg1)
	object *player;
	char *arg1;
{
	BBOARD *b, *bprev = NULL;
	int cnt, del;
	
	if( (del = match_num(arg1)) < 1) {
		error_report(player, M_ENOTFOUND);
		return;
	}
	
	for(b = bblist, cnt = 1; b; b=b->next, cnt++) {
		if(cnt == del)
			break;
		bprev = b;
	}
	
	if(!b) {
		error_report(player, M_ENOTFOUND);
		return;
	}
	
	if(!can_control(player, b->creator)) {
		error_report(player, M_ENOCNTRL);
		return;
	}
	
	if(!bprev)
		bblist = b->next;
	else
		bprev->next = b->next;
	
	block_free(b->topic);
	block_free(b->message);
	block_free(b);
	
	notify(player, pstr("* %d has been deleted.", del));
}


void do_board_view(player)
	object *player;
{
	BBOARD *b;
	int cnt = 1;
	
	notify(player, pstr("/%s\\",make_ln("=",76)));
	notify(player, pstr("| %-3.3s | %-4.4s | %-4.4s | %-23.23s | %-28.28s |", "NUM", 
		"READ", "AGE", "AUTHOR", "TITLE"));
	notify(player, pstr("|%s|%s|%s|%s|%s|", make_ln("-",5), make_ln("-", 6), 
		make_ln("-", 6), make_ln("-",  25), make_ln("-", 30)));
	if(!bblist) 
		notify(player, pstr("|%s|", cjust("Nothing to view!",76)));
	else {
		for(b = bblist; b; b=b->next) {
			notify(player, pstr("| %3d | %4d | %-4.4s | %s | %s |", cnt, 
				b->num_read, count_idle(time(0)-b->created), color_name(b->creator, 23), format_color(b->topic, 28)));
			cnt++;
		}
			
	}
	notify(player, pstr("\\%s/",make_ln("=",76)));
}

void do_read_board(player, arg1)
	object *player;
	char *arg1;
{
	int num, cnt;
	BBOARD *b;
	
	num = match_num(arg1);
	if(num < 1) {
		error_report(player, M_ENOTFOUND);
		return;
	}
	
	for(b=bblist,cnt=1;b;b=b->next,cnt++) 
		if(cnt == num) {
			notify(player, pstr("Title: %s", b->topic));
			notify(player, pstr("Creator: %s", color_name(b->creator, -1)));
			notify(player, pstr("Age: %s Will Expire: %s", time_format2(time(0) - b->created),
				get_date(b->expiration)));
			notify(player, pstr("Number Read: %d", b->num_read));
			notify(player, pstr("Message: %s", substitute(b->message)));
			notify(player, "---");
			b->num_read++;
			return;
		}
	
	error_report(player, M_ENOTFOUND);
		
}


void do_extend_time(player, arg1)
	object *player;
	char *arg1;
{
	BBOARD *b;
	int msg, cnt;
	
	if(!bblist) {
	  notify(player, "Sorry, the +board is empty.");
	  return;
	}
	
	if(!(msg = atoi(arg1))) {
	  notify(player, "What message were you looking to extend?");
	  return;
	}
	
	for(cnt = 1, b = bblist; b; b=b->next, cnt++)
	  if(cnt == msg) {
	    if(can_control(player, b->creator)) {
	      b->expiration += (DEF_BBEXP * 86400);
	      notify(player, pstr("Board message %d successfully extended...", msg));
	      return;
	    }
	  }
	  
	notify(player, "You cannot do that!");
}

void do_board(player, arg1, arg2)
	object *player;
	char *arg1;
	char *arg2;
{
	if(!*arg1) {
		do_board_view(player);
		return;
	}

	if(!strcmpm(arg1, "add") && (!(player->flags & GUEST))) {
		if(!*arg2) {
			error_report(player, M_EINVARG);
			return;
		}
		do_add_msg(player, arg2);	
	} else if (!strcmpm(arg1, "del") && (!(player->flags & GUEST))) {
		if(!*arg2) {
			error_report(player, M_EINVARG);
			return;
		}
		do_del_msg(player, arg2);
	} else if (!strcmpm(arg1, "extend") && (!(player->flags & GUEST))) {
	  if(!*arg2) {
	    error_report(player, M_EINVARG);
	    return;
	  }
	  do_extend_time(player, arg2);
	} else {
		do_read_board(player, arg1);
	}
}

void sort_bboard()
{
	BBOARD *b, *bnext=NULL;
	int cnt;
	
	//return;
	for(b = bblist, cnt = 1; b; b=bnext, cnt++) {
		bnext = b->next;
		if(b->expiration <= time(0))
		  do_del_msg(rootobj, pstr("%d", cnt));
		  //delete_bboard_msg(b);
	}
}


/** FILE SAVING **/

void OUT_BBLIST(fp, b)
        FILE *fp;
        BBOARD *b;
{
        fprintf(fp, "%s\n", b->topic);
        fprintf(fp, "%d\n", b->c_ref);
        fprintf(fp, "%s\n", b->message);
        fprintf(fp, "%ld\n", b->created);
        fprintf(fp, "%ld\n", b->expiration);
        fprintf(fp, "%d\n", b->num_read);
        fprintf(fp, "\\\n");
}


int num_bmsgs()
{
        BBOARD *b;
        int num = 0;

        for(b=bblist;b;b=b->next) num++;

        return num;
}
 
int SaveBoard(char *filename)
{
  FILE *fp;
  BBOARD *b;

  if(!(fp = fopen(filename, "w"))) 
    return FALSE;

  fprintf(fp, "%d\n", num_bmsgs());
  for(b = bblist; b; b=b->next)
    OUT_BBLIST(fp, b);
  
  fclose(fp);
  
  return TRUE;
}


int IN_BBLIST(fp)
        FILE *fp;
{
        BBOARD *b;
        object *o;
        char title[1024], message[2048];
        int num_read, c_ref;
        long created, expiration;

        strcpy(title, getstring(fp));
        c_ref = getint(fp);
        strcpy(message, getstring(fp));
        created = getlong(fp);
        expiration = getlong(fp);
        num_read = getint(fp);

        if(*(getstring(fp)) != '\\')
                return 0;

        if(!(o = match_player(pstr("#%d", c_ref))))
                return 0;

        b = add_to_board(o, title, message);
        b->created = created;
        b->expiration = expiration;
        b->num_read = num_read;

        //printf("We added a +board to the board system... %s %s", b->topic, b->message);
        return 1;
}

int LoadBoard(char *filename)
{
  FILE *fp;
  int index, counter;

  if(!(fp = fopen(filename, "r")))
    return FALSE;

  counter = getint(fp);

  for(index = 0; index < counter; index++)
    if(!IN_BBLIST(fp))
      printf("** ERROR ** Cannot load bulletin board.\n");

  fclose(fp);                        
  
  return TRUE;
}
                                                                                                                                                                                     

/*** END BOARD.C ***/
