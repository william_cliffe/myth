#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifdef MYTHPLUGIN
  #include "db.h"
  #include "net.h"
#endif

#include "externs.h"

#ifndef MYTHPLUGIN
  #include "plugins.h"

  #if (PLUGIN_SUPPORT_VERSION < 2)
    #error "This server doesn't have support for this plugin"
  #endif
#endif 

// Special Conditions
#ifdef MYTHPLUGIN
  #define log_error(x) log_channel("log_error", x);
  #define now time(0)
#endif

#define HTTP_HEADER "HTTP/1.1 200 OK\r\nServer: WebWho\r\nContent-type: text/html\r\nAccept: text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c\r\nRequest-Line: POST\r\n\r\n"
#define WEBWHO_PORT 4209
#ifdef PARSEPOST
  #define HTML_PLAYER 7		// On MYTH, this is test
#endif

#ifdef MYTHPLUGIN
  static int color = -1, bright = 0;
#else
  extern unsigned char *swlm_msg;
  extern int swlm_author;
  extern time_t swlm_time;
#endif

typedef struct client_struct
{
  int des;
  struct client_struct *prev;
  struct client_struct *next;
} CLIENT;

CLIENT *client_list = NULL;
int listen_sock = -1;

typedef struct
{
  char c;
  int codes[2];
} COLOR;


// ->> CHANGE THIS BACK <<-
COLOR color_list[] =
{
#ifdef MYTHPLUGIN
  { 'n', { 0x999999, 0xFFFFFF } },
  { 'b', { 0x0000FF, 0x0066FF } },
  { 'c', { 0x009999, 0x00FFFF } },
  { 'g', { 0x009900, 0x00FF00 } },
  { 'r', { 0x990000, 0xFF0000 } },
  { 'm', { 0x9900CC, 0xCC00FF } },
  { 'x', { 0x000000, 0x666666 } },
  { 'w', { 0x999999, 0xFFFFFF } },
  { 'y', { 0x996600, 0xFFFF00 } },
#else
  { 'n', { 0x999999, 0xFFFFFF } },
  { 'B', { 0x0000FF, 0x0066FF } },
  { 'C', { 0x009999, 0x00FFFF } },
  { 'G', { 0x009900, 0x00FF00 } },
  { 'R', { 0x990000, 0xFF0000 } },
  { 'M', { 0x9900CC, 0xCC00FF } },
  { 'N', { 0x000000, 0x666666 } },
  { 'W', { 0x999999, 0xFFFFFF } },
  { 'Y', { 0x996600, 0xFFFF00 } },
#endif   
  { 0 }
};

char *get_plugin_version(void)
{
  return("v3.0");
}

#ifdef MYTHPLUGIN
CL *additional_command()
{
  return(NULL);
}
#endif

#ifdef MYTHPLUGIN
  void __attribute__((constructor)) simple_init(void)
#else
  void do_plugin_load(void)
#endif
{
  struct sockaddr_in servaddr;
  int opt = 1;

  if((listen_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    log_error(tprintf("webwho:socket() - %s", strerror(errno)));
    return;
  }

  if(setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
    sizeof(opt)) < 0)
  {
    log_error(tprintf("webwho:setsockopt() - %s", strerror(errno)));
    close(listen_sock);
    return;
  }

  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port = htons(WEBWHO_PORT);

  if(bind(listen_sock, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
  {
    log_error(tprintf("webwho:bind() - %s", strerror(errno)));
    close(listen_sock);
    return;
  }

  if(listen(listen_sock, 5))
  {
    log_error(tprintf("webwho:listen() - %s", strerror(errno)));
    close(listen_sock);
    return;
  }
}

#ifdef MYTHPLUGIN
  void __attribute__((destructor)) simple_fini(void)
#else
  void do_plugin_unload(void)
#endif
{
  close(listen_sock);
}

int *do_plugin_readset(void)
{
  CLIENT *c;
  int i = 0;
#ifdef MYTHPLUGIN
  int *set = stack_em(sizeof(int)*2, VOLATILE);
#else  
  int *set = mvm_alloc_tmp(sizeof(int) * 2);
#endif

  LIST_CYCLE(client_list, c)
  {
#ifdef MYTHPLUGIN  
    set = stack_realloc(set, sizeof(int) * (i + 3), VOLATILE);
#else
    set = mvm_realloc_tmp(set, sizeof(int) * (i + 3));
#endif    
    set[i++] = c->des;
  }
  set[i++] = listen_sock;
  set[i] = -1;

  return set;
}

static void new_connection(void)
{
  struct sockaddr_in clntaddr;
  int size = sizeof(struct sockaddr_in);
  CLIENT *newclient;
  int sock;

  if((sock = accept(listen_sock, (struct sockaddr *)&clntaddr, &size)) < 0)
  {
    log_error(tprintf("webwho:accept() - %s", strerror(errno)));
    return;
  }

  fcntl(sock, F_SETFL, O_NONBLOCK);

/*  log_io(tprintf("New webwho connection from %s",
    inet_ntoa(clntaddr.sin_addr)));
*/
#ifdef MYTHPLUGIN
  GENERATE(newclient, CLIENT);
#else
  newclient = mvm_alloc(sizeof(CLIENT));
#endif
  newclient->des = sock;
  LIST_ADD_NODE_TAIL(client_list, newclient);
}

static void shutdown_client(CLIENT *c)
{
  close(c->des);

  LIST_REM_NODE(client_list, c);
#ifdef MYTHPLUGIN
  block_free(c);
#else
  mvm_free(c);
#endif

}

static int color_code(char c)
{
  int i;

  for(i = 0;color_list[i].c;++i)
    if(color_list[i].c == c)
      break;

  return color_list[i].c ? i : -1;
}

static char *html_color_tag(char **str)
{
  char buf[4096], *s;
#ifndef MYTHPLUGIN
  int color = -1, bright = 0;
#endif  
  int code, bad = 0;

#ifdef MYTHPLUGIN
  s = (*str)+1;
  switch(*s)
  {
    case 'c':
      s++;
      if(!*s)
        break;
  
      if((code = color_code(*s)) == -1 && *s != 'h')
      {
        bad = 1;
        break;
      }

      if(code == -1)
        bright = 1;
      else
        color = code;
        
      if(*s == 'n')
        bright = 0;
        
      break;
  }
#else  
  for(s = (*str) + 1;*s;s++)
  {
    if(*s == '|')
      break;

    if((code = color_code(*s)) == -1 && *s != '+')
    {
      bad = 1;
      break;
    }

    if(code == -1)
      bright = 1;
    else
      color = code;
  }
#endif

  if(!*s || bad)
  {
    (*str)++;
    return NULL;
  }

  if(color == -1)
    color = 0;

  sprintf(buf, "<font color=\"#%06X\">", color_list[color].codes[bright]);
  
  *str = s + 1;
  
#ifdef MYTHPLUGIN
  return stralloc(buf);
#else
  return mvm_str_alloc_tmp(buf);
#endif
}

static void write_str(int fd, char *str)
{
  char buf[4096], *b, *c;
  int in_color = 0, ctr = 0;

  for(b = buf;*str && b - buf < sizeof(buf);)
  {
#ifdef MYTHPLUGIN
    if(*str != '^')
#else  
    if(*str != '|')
#endif    
    {
      *b++ = *str++;
      continue;
    }

    *b = '\0';
    write(fd, buf, b - buf);

    if((c = html_color_tag(&str)))
    {
      if(in_color)
        write(fd, "</font>", strlen("</font>"));

      write(fd, c, strlen(c));
      in_color = 1;
      b = buf;
      ctr++;
    }
    else
      *b++ = *str++;
  }

  if(b - buf)
    write(fd, buf, b - buf);

  if(ctr)
    write(fd, "</font>", strlen("</font>"));
}

static char *esc_html(char *str)
{
  char buf[4096], *b;
  struct { char c; char alt; char *rep; }
    list[] = { { '<', '(', "&lt;" }, { '>', ')', "&gt;" }, { 0 } };
  int i;

  for(b = buf;*str && b - buf < sizeof(buf);str++)
  {
    for(i = 0;list[i].c;++i)
      if(list[i].c == *str)
        break;
    if(list[i].c)
    {
      if(b - buf < sizeof(buf) - strlen(list[i].rep))
      {
        strcpy(b, list[i].rep);
        b += strlen(list[i].rep);
      }
      else
        *b++ = list[i].alt;
    }
    else
      *b++ = *str;
  }

  *b = '\0';
  return 
#ifdef MYTHPLUGIN
  stralloc(buf);
#else
  mvm_str_alloc_tmp(buf);
#endif  
}

static void write_who(int fd)
{
  DDATA *d;
  FILE *fp;
  char buf[4096];

  write_str(fd, HTTP_HEADER);

  write_str(fd, "<html>\r\n<head>\r\n");
  if(!(fp = fopen("style.css", "r")))
    write_str(fd, "<style></style>");
  else
  {
    write_str(fd, "<style>\r\n");
    while(fgets(buf, sizeof(buf), fp))
      write_str(fd, buf);
    write_str(fd, "</style>\r\n");
  }

  write_str(fd, tprintf("<title>%s Who List</title>\r\n</head>\r\n", 
#ifdef MYTHPLUGIN  
  return_config("myth_name")
#else
  config.maze_name
#endif  
  ));
  write_str(fd, "<body>\r\n");

  write_str(fd, "<form method=\"post\" action=\"postit.web\"><input id=\"TEXT\" name=\"TEXT\" type=\"text\"><input type=\"submit\"></form>");
  write_str(fd, "<div class=\"whomain\"><table>\r\n");
#ifndef MYTHPLUGIN  
  write_str(fd,
    tprintf("<tr><td class=\"whoswlm\">|+B|SWLM |+W|(%s old): %s</td></tr>\r\n",
    trim(time_format(now - swlm_time, 1)), esc_html(swlm_msg)));
  write_str(fd,
    "<tr class=\"whoheaders\"><td>|+B|NAME</td><td>|+B|ALIAS</td><td>|+B|LEVEL</td><td align=\"right\">|+B|ONFOR</td><td align=\"right\">|+B|IDLE</td><td>|+B|DOING</td></tr>\r\n");
#else
  write_str(fd,"<tr class=\"whoheaders\"><td>^ch^cbNAME</td><td>^ch^cbALIAS</td><td>^ch^cbLEVEL</td><td align=\"right\">^ch^cbONFOR</td><td align=\"right\">^ch^cbIDLE</td><td>^ch^cbDOING</TD></TR>\r\n");
#endif


  LIST_CYCLE(descriptor_list, d)
  {
#ifdef MYTHPLUGIN
    if(d->state != CONNECTED || d->player->flags & INVISIBLE)
#else  
    if(!check_state(d, STATE_CONNECTED) || *atr_get(d->player, A_LHIDE))
#endif
      continue;

    write_str(fd, "<tr>\r\n");
    write_str(fd, tprintf("<form method=\"post\"><td class=\"whoname\"><input name=\"whois\" type=\"hidden\" value=\"%s\"><input type=\"submit\" value=\"?\">%s</td></form>\r\n", d->player->name, 
#ifdef MYTHPLUGIN
             esc_html(atr_get(d->player, A_RAINBOW))
#else    
             esc_html(name(d->player))
#endif            
             ));
    write_str(fd,
      tprintf("<td class=\"whoalias\">%s</td>\r\n", esc_html(atr_get(d->player, A_ALIAS))));
    write_str(fd, "<td class=\"wholevel\">");
#ifdef MYTHPLUGIN
    if((d->player->flags & TYPE_PLAYER) && (Level(d->player) == CLASS_CITIZEN) )
#else    
    if(Typeof(d->player) == TYPE_PLAYER &&
      get_class(d->player) == CLASS_CITIZEN)
#endif    
    {
      write_str(fd, esc_html(atr_get(d->player, A_LEVEL)));
    }
    else
      write_str(fd, "N/A");
    write_str(fd, "</td>\r\n");
    write_str(fd, tprintf("<td class=\"whoonfor\">%s</td>\r\n",
#ifdef MYTHPLUGIN
      TimeFormat(now - d->connected_at)
#else    
      time_format(now - d->connected_at, 2)
#endif       
      ));      
    write_str(fd, tprintf("<td class=\"whoidle\">%s</td>\r\n",
#ifdef MYTHPLUGIN
      TimeFormat(now - d->last_time)
#else    
      time_format(now - d->last_time, 1)
#endif      
      ));
    write_str(fd,
      tprintf("<td class=\"whodoing\">%s</td>\r\n", esc_html(atr_get(d->player, A_DOING))));

    write_str(fd, "</tr>\r\n");
  }


  write_str(fd, "</table></div>\r\n");
  write_str(fd, "<form method=\"post\" action=\"...\"><input name=\"id\" type=\"text\"><br/><input name=\"id2\" type=\"text\"><input type=\"submit\"></form>");
#ifdef MYTHPLUGIN
  write_str(fd, tprintf("<TABLE class=\"whocomment\"><TR><TD>^ch^cwCOMMENT:^cn%s^cn\r\n</TD></TR></TABLE>", esc_html(atr_get(db[0], A_COMMENT))));
#endif 
  
  write_str(fd, "</body>\r\n");
  write_str(fd, "</html>\r\n");
}

#ifdef PARSEPOST
void parse_label(int fd, char *label, char *value)
{
  object *o;
  
  if(!value || !*value)
  {
    DEBUG(pstr("Label: %s Value: None", label));
    return;
  }
  
  DEBUG(pstr("Label: %s Value: %s", label, value));
  if(!strcmpm(label, "id"))
    do_page(db[HTML_PLAYER], value, tprintf("[ON BEHALF OF A WEB USER]: %s", value));
  else if (!strcmpm(label, "id2"))
    ParseChannel(db[HTML_PLAYER], "", "public", value);
  else if (!strcmpm(label, "whois"))
    write_str(fd, "<HTML>Rar.</HTML>\r\n");
}

void parse_post(int fd, char *str)
{
  char *k;
  
  for(k = str; *k; k++)
    if(*k == '+')
      *k = ' ';
  
  while( (k = (char *)parse_up(&str, '&')))
    parse_label(fd, fparse(k, '='), rparse(k, '='));
}
#endif

static void do_readwrite(CLIENT *c)
{
  char buf[4096];
  int got;
#ifdef PARSEPOST  
  char *f;
#endif
  

  *buf = '\0';
  for(;;)
  {
    while((got = read(c->des, buf, sizeof(buf) - 1)) > 0)
    {
      buf[got] = '\0';
#ifdef PARSEPOST
      if(strstr(buf, "POST")) {
        DEBUG(pstr("Post: %s", buf));
        DEBUG(pstr("Post has length of: %d", strlen(buf)));
        for(f = buf + strlen(buf)-1; *f; f--)
        {
          if(*f == '\n')
            break;
        }
        f++;
        // Okay, parse out the POST.
        parse_post(c->des, f);
      }
#endif      
      if(strstr(buf, "\r\n\r\n"))
      {
        write_who(c->des);
        shutdown_client(c);
        return;
      }
    }

    if(!got)
    {
//      log_io("Disconnect by client");
      shutdown_client(c);
      return;
    }

    if(got < 0)
    {
      if(errno != EAGAIN)
      {
        log_error(tprintf("webwho:read() - %s", strerror(errno)));
        shutdown_client(c);
        return;
      }
    }
  }
}

void do_plugin_selectchk(fd_set rset, fd_set wset)
{
  CLIENT *c, *cnext;

  if(FD_ISSET(listen_sock, &rset))
    new_connection();

  LIST_CYCLE_SAFE(client_list, c, cnext)
  {
    if(FD_ISSET(c->des, &rset))
      do_readwrite(c);
  }
}

/** webwho.c plugin (c) 2005 TinyMAZE **/
